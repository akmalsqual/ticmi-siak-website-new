<?=
  view($header);
?>
{!! $config->header !!}
<?php
    $searchArray = array("v_batch", "v_program","v_nama_peserta","v_email_peserta","v_kp","v_tgl_daftar","v_no_invoice","v_voucher", "v_nilai_voucher", "v_va","v_status","v_harga", "v_pembayaran");
    $replaceArray = array($batch, $program, $nama_peserta , $email_peserta, $kp, $tgl_daftar, $no_invoice, $voucher, $nilai_voucher, $va, $status, $harga, $pembayaran);

    echo str_replace($searchArray, $replaceArray, $konfirmasi->content);
?>

{!! $config->footer !!}
<?=
  view($footer);
?>