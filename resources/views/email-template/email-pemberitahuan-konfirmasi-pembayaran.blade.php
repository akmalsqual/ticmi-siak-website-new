<?=
  view($header);
?>
{!! $config->header !!}
<?php
    $searchArray = array("v_nama_peserta", "v_invoice_no", "v_nama_batch", "v_tgl_transfer", "v_jam_transfer", "v_nominal_transfer", "v_nama_rekening", "v_no_rekening");
    $replaceArray = array($nama_peserta, $invoice_no, $nama_batch, $tgl_transfer, $jam_transfer, $nominal_transfer, $nama_rekening, $no_rekening);

    echo str_replace($searchArray, $replaceArray, $konfirmasi->content);
?>

{!! $config->footer !!}
<?=
  view($footer);
?>