<?=
  view($header);
?>
{!! $config->header !!}
<?php
    $searchArray = array("v_nama_peserta", "v_token", "v_link");
    $replaceArray = array($peserta->nama, $token, $link);

    echo str_replace($searchArray, $replaceArray, $konfirmasi->content);
?>

{!! $config->footer !!}
<?=
  view($footer);
?>