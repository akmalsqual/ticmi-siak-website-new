            @if($programs && $programs->count() > 0)
                @foreach($programs as $key => $program)
                    <div class="row">
                        <div class="col-sm-12" style="box-sizing: border-box; padding: 0px 45px <?= ($programs && $programs->count() > 1) ? 5: 40; ?>px 45px; margin-bottom: 50px;">
                            <h4>{{ $program->nama }} : </h4>

                            @if($program->batch_publish->count() == 1)
                                @foreach($program->batch_publish as $key => $value)
                                    <!-- Event Column -->
                                    <div class="col-sm-3">
                                        <div class="item">
                                            <!-- Related Wrapper -->
                                            <div>
                                                <!-- Related Image Wrapper -->
                                                <div class="img-wrap">
                                                    <img alt="Event" class="img-responsive" src="{{ $batch_path.$value->logo }}" width="600" height="220">
                                                </div>
                                                <!-- Related Content Wrapper -->
                                                <div class="related-content2">
                                                    <h5 class="title">{{ $value->nama }}</h5>

                                                    <ul class="events-meta">
                                                        <li><i style="font-size: 0.85em;" class="fa fa-calendar-o"></i> {{ $value->tgl_mulai->formatLocalized('%A, %#d %B %Y') }}</li>
                                                        <li><i style="font-size: 0.85em;" class="fa fa-map-marker"></i> &nbsp;{{ !empty($value->lembg_pdkn_id) ? $value->lembaga->keterangan :"Gedung Bursa Efek Indonesia" }}</li>
                                                        <li><i style="font-size: 0.85em;" class="fa fa-users"></i> Minimal Peserta {{ !empty($value->min_peserta) ? $value->min_peserta: ' - ' }} Orang</li>
                                                    </ul>

                                                    <a href="{{ url('daftar-batch/'.$value->batch_id.'/'.$value->nama) }}" class="btn btn-lg btn-block" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $value->nama }}', '{{ url()->current() }}' )"><i class="fa fa-angle-double-right" style="font-size: 1.2em;"></i> Saya ingin daftar</a>
                                                </div><!-- Related Content Wrapper -->
                                            </div><!-- Related Wrapper -->
                                        </div><!-- Item -->
                                    </div><!-- Column -->
                                @endforeach
                            @else
                                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-tablet="2" data-desktopsmall="2"  data-mobile="1" data-autoplay="true" data-delay="3000" data-navigation="true" data-desktop="4">

                                @foreach($program->batch_publish as $key => $value)
                                    <div class="item">
                                        <!-- Related Wrapper -->
                                        <div class="related-wrap">
                                            <!-- Related Image Wrapper -->
                                            <div class="img-wrap">
                                                <img alt="Event" class="img-responsive" src="{{ $batch_path.$value->logo }}" width="600" height="220">
                                            </div>
                                            <!-- Related Content Wrapper -->
                                            <div class="related-content2">
                                                <h5 class="title">{{ $value->nama }}</h5>

                                                <ul class="events-meta">
                                                    <li><i style="font-size: 0.85em;" class="fa fa-calendar-o"></i> {{ $value->tgl_mulai->formatLocalized('%A, %#d %B %Y') }}</li>
                                                    <li><i style="font-size: 0.85em;" class="fa fa-map-marker"></i> &nbsp;{{ !empty($value->lokasi) ? $value->lokasi:"Gedung Bursa Efek Indonesia" }}</li>
                                                    <li><i style="font-size: 0.85em;" class="fa fa-users"></i> Minimal Peserta {{ !empty($value->minimal) ? $value->minimal:10 }} Orang</li>
                                                </ul>

                                                <a href="{{ url('daftar-batch/'.$value->batch_id.'/'.$value->nama) }}" class="btn btn-lg btn-block" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $value->nama }}', '{{ url()->current() }}' )"><i class="fa fa-angle-double-right" style="font-size: 1.2em;"></i> Saya ingin daftar</a>
                                            </div><!-- Related Content Wrapper -->
                                        </div><!-- Related Wrapper -->
                                    </div><!-- Item -->
                                @endforeach

                                </div><!-- Related Post -->
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
            @endif