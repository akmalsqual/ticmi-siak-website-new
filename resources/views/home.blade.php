@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
        <!-- Home Slider -->
        <div class="rs-container light rev_slider_wrapper">
            <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 220}'>
                <ul>
                    <li data-transition="fade" class="typo-dark heavy">
                        <a href="{{ url('kelas-online/wppeonline01') }}">
                            <img src="{{ url('assets/ticmi/images/ticmi/ppl-online-ticmi.jpeg') }}"
                                 alt=""
                                 data-bgposition="top center"
                                 data-bgfit="cover"
                                 data-bgrepeat="no-repeat"
                                 class="rev-slidebg">
                        </a>

                        {{--<div class="tp-caption big-text"--}}
                             {{--style="background: rgba(0,0,0,.5);width: 600px;height: 100px;"--}}
                             {{--data-x="left" data-hoffset="67"--}}
                             {{--data-y="top" data-voffset="30"--}}
                             {{--data-start="590"--}}
                             {{--data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>--}}

                        {{--<div class="tp-caption sm-text"--}}
                             {{--data-x="left" data-hoffset="70"--}}
                             {{--data-y="95" data-voffset="-70"--}}
                             {{--data-start="600"--}}
                             {{--data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Program Pendidikan Lanjutan - eClass</span></div>--}}

                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="67"
                             data-y="160"
                             data-start="2000"
                             data-transform_in="y:[100%];opacity:0;s:500;"><a href="{{ url('ppl') }}" class="btn"><span class="color-white">Selengkapnya disini</span></a></div>
                    </li>
                    <li data-transition="fade" class="typo-dark heavy">
                        <img src="{{ url('assets/ticmi/images/banner/banner_2.jpg') }}"
                             alt=""
                             data-bgposition="top center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 600px;height: 100px;"
                             data-x="left" data-hoffset="67"
                             data-y="top" data-voffset="30"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="70"
                             data-y="95" data-voffset="-70"
                             data-start="600"
                             data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Pusat Edukasi Pasar Modal Indonesia</span></div>

                        <div class="tp-caption big-text"
                             data-x="left" data-hoffset="70"
                             data-y="center" data-voffset="-50"
                             data-start="1500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Pelatihan & Sertifikasi</span></div>

                        <!--                    <div class="tp-caption sm-text"-->
                        <!--                         data-x="left" data-hoffset="67"-->
                        <!--                         data-y="135"-->
                        <!--                         data-start="2000"-->
                        <!--                         data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn">Lihat Semua Program Kami</a></div>-->
                    </li>

                    <li data-transition="fade" class="typo-dark heavy">

                        <img src="{{ url('assets/ticmi/images/banner/banner_3.jpg') }}"
                             alt=""
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 925px;height: 130px;"
                             data-x="right" data-hoffset="67"
                             data-y="top" data-voffset="15"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption md-text"
                             data-x="right" data-hoffset="70"
                             data-y="top" data-voffset="10"
                             data-start="500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">{{-- Promo Diskon 20% Program Reguler WPPE dan WMI --}}</span>
                        </div>

                        <div class="tp-caption sm-text"
                             data-x="right" data-hoffset="70"
                             data-y="top" data-voffset="70"
                             data-start="1000"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">{{-- Segera daftarkan diri Anda untuk mendapatkan Promo Diskon 20% untuk <br/> Program Reguler WPPE dan WMI untuk 10 pendaftar pertama. --}}</span>
                        </div>

                        <!--                    <div class="tp-caption big-text"-->
                        <!--                         data-x="right" data-hoffset="67"-->
                        <!--                         data-y="top" data-voffset="130"-->
                        <!--                         data-start="2000"-->
                        <!--                         data-whitespace="nowrap"-->
                        <!--                         data-transform_in="y:[100%];s:500;"-->
                        <!--                         data-transform_out="opacity:0;s:500;"-->
                        <!--                         data-mask_in="x:0px;y:0px;"><a href="#" class="btn">View All Courses</a>-->
                        <!--                    </div>-->

                    </li>

                    <li data-transition="fade" class="typo-dark heavy">
                        <img src="{{ url('assets/ticmi/images/banner/banner_1.jpg') }}"
                             alt=""
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 900px;height: 120px;"
                             data-x="left" data-hoffset="67"
                             data-y="top" data-voffset="10"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption md-text"
                             data-x="left" data-hoffset="70"
                             data-y="0" data-voffset="30"
                             data-start="1500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[-300%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Unduh Data Pasar Modal </span></div>

                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="70"
                             data-y="60"
                             data-start="500"
                             data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Anda dapat mengunduh data pasar modal seperti LapKeu dan Prospectus <br/> dengan gratis jika Anda memiliki SID.</span></div>


                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="67"
                             data-y="160"
                             data-start="2000"
                             data-transform_in="y:[100%];opacity:0;s:500;"><a href="{{ url('register') }}" class="btn"><span class="color-white">Silahkan registrasi disini.</span></a></div>
                    </li>
                </ul>
            </div>
        </div><!-- Home Slider -->



        <!-- Section -->
        <section class="slider-below-section" style="">
            <div class="container">
                <div class="slider-below-wrap bg-color typo-light">
                    <div class="row">

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="http://library.ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'perpustakaan', '{{ url()->current() }}' );" target="_blank"><i class="uni-address-book"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="http://library.ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'perpustakaan', '{{ url()->current() }}' );" target="_blank"><h5 class="heading">Perpustakaan</h5></a>
                                    <p>Perpustakaan Pasar Modal yang berisi buku-buku untuk pengetahuan dan belajar Pasar Modal.</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="{{ URL::to('/') }}" onclick="ga('send', 'event', 'Banner Menu', 'sertifikasi', '{{ url()->current() }}' );"><i class="uni-medal-3"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="{{ URL::to('/') }}" onclick="ga('send', 'event', 'Banner Menu', 'sertifikasi', '{{ url()->current() }}' );"><h5 class="heading">Sertifikasi Profesi</h5></a>
                                    <p>Sertifikasi Profesi WPPE, WMI dan WPEE untuk pelaku pasar modal</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="{{ url('simulasi-saham') }} {{-- {{ route('member.simulasisaham') }} --}}" onclick="ga('send', 'event', 'Banner Menu', 'simulasi-saham', '{{ url()->current() }}' );"><i class="uni-line-chart4"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="{{ url('simulasi-saham') }} {{-- {{ route('member.simulasisaham') }} --}}" onclick="ga('send', 'event', 'Banner Menu', 'simulasi-saham', '{{ url()->current() }}' );"><h5 class="heading">Online Trading Simulation</h5></a>
                                    <p>Simulasi Perdagangan Saham untuk investor pemula dan calon investor</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="{{ route('member.suratriset') }}" onclick="ga('send', 'event', 'Banner Menu', 'surat-riset', '{{ url()->current() }}' );"><i class="uni-letter-open"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="{{ url('member.suratriset') }}" onclick="ga('send', 'event', 'Banner Menu', 'surat-riset', '{{ url()->current() }}' );"><h5 class="heading">Permohonan Surat Riset</h5></a>
                                    <p>Untuk mahasiswa yang ingin skripsi dengan data yang ada di TICMI.</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                    </div><!-- Slider Below Wrapper -->
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->

        <!-- Section -->
        <section class="bg-lgrey typo-dark" style="padding: 20px 0;">
            <div class="container" style="padding: 0 15px ; margin: 0;max-width: 1200px;">
                <div class="shadow active bg-white typo-dark" style="margin-top: -5px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-12">
                                <div class="title-container sm">
                                    <div class="title-wrap">
                                        <h3 class="title" style="padding-top: 30px;">Program Unggulan Kami</h3>
                                        <span class="separator line-separator  pad-tb-none"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- Search -->
                            <div class="search margin-top-30">
                                <form id="formSearch" class="light form-horizontal" action="{{-- {{ route('search') }} --}}" method="post">
                                    <div class="form-group">
                                        <div class=" col-sm-2 col-sm-offset-2">
                                            <select name="cabang" class="form-control">
                                                <option value="">Pilih Kota</option>
                                                @foreach($cabang as $item)
                                                    <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class=" col-sm-2">
                                            <select name="bulan" class="form-control">
                                                <option value="">Pilih Bulan</option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="kd_program" class="form-control">
                                                <option value="">Pilih Program</option>
                                                @foreach($select_program as $value)
                                                    <option value="{{ $value->program_id }}">{{ $value->kode }} - {{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="form-control btn search searchProgram" type="button" autocomplete="off" data-loading-text="<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i> Loading..."><i class="fa fa-search"></i> CARI</button>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- Search -->
                        </div><!-- Column -->
                    </div><!-- Slider Below Wrapper -->
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center searchResult">
                                <img src="{{ url('assets/images/loading2.gif') }}" style="display: none;" class="loadingSearch" alt="TICMI">
                            </div>
                            <!-- Program Seminar / Workshop -->
                            <div class="defaultBatch">
                                @if($workshops && $workshops->count() > 0)
                                    @foreach($workshops as $workshop)
                                        <div class="col-sm-3 item-batch">
                                            <div class="event-wrap">
                                                <div class="event-img-wrap course-banner-wrap color-white text-center" style="">
                                                    @if($workshop->program->nama == "Forum")
                                                        <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/forum-course-ticmi.jpg') }}" width="600" height="220">
                                                    @elseif($workshop->program->nama == "Seminar")
                                                        <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/SEMINAR.png') }}" width="600" height="220">
                                                    @elseif($workshop->program->nama == "Workshop")
                                                        <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/WORKSHOP.png') }}" width="600" height="220">
                                                    @elseif($workshop->program->nama == "PPL")
                                                        @if($workshop->is_online == 1)
                                                            <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/ONLINE-CLASS.png') }}" width="600" height="220">
                                                        @else
                                                            <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/PPL.png') }}" width="600" height="220">
                                                        @endif
                                                    @else
                                                        @if($workshop->is_online == 1)
                                                            <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/ONLINE-CLASS.png') }}" width="600" height="220">
                                                        @else
                                                            <img alt="Event" class="img-responsive" src="{{ asset('assets/images/course/new/SEMINAR.png') }}" width="600" height="220">
                                                        @endif
                                                    @endif

                                                    <span class="cat bg-yellow">{{ $workshop->program->nama }} {{ (($workshop->is_online == 1) ? 'ONLINE':'') }}</span>
                                                </div>
                                                <!-- Event Detail Wrapper -->
                                                <div class="event-details" style="height: 450px;">
                                                    <h4 class="batch-title" style="font-size: 16px;">
                                                        @if($workshop->is_online == 1)
                                                            <a href="{{ route('kelasonline',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Online Class', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a>
                                                        @else
                                                            <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a>
                                                        @endif
                                                    </h4>
                                                    @if($workshop->is_online == 0)
                                                        <ul class="events-meta">
                                                            <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($workshop->tgl_mulai)) }}</li>
                                                            <li><i class="fa fa-map-marker"></i>{{ $workshop->lokasi }}</li>
                                                            <li><i class="fa fa-users"></i> {{ $workshop->waktu }}</li>
                                                        </ul>
                                                    @else
                                                        <ul class="events-meta">
                                                            <li><i class="fa fa-map-marker"></i>http://elearning.ticmi.co.id</li>
                                                            <li><i class="fa fa-clock-o"></i>Periode Akses : {{ $workshop->lama_belajar }} Hari</li>
                                                        </ul>
                                                    @endif
                                                    <p>{!! substr(strip_tags($workshop->deskripsi),0,200) !!}</p>
                                                    @if($workshop->is_online == 1)
                                                        <a href="{{ route('kelasonline',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Online Class', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                                                    @else
                                                        <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>

                                                    @endif
                                                </div><!-- Event Meta -->
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                                <div class="clearfix"></div><br>
                                    @if($programs && $programs->count() > 0)
                                       
                                        @foreach($programs as $key => $program)
                                            <!-- Event Column -->
                                                <!-- Event Wrapper -->
                                                @if($program->batch_publish->count() > 0)
                                                    <div id="{{$program->kode}}" >
                                                        @foreach($program->batch_publish as $key => $batch)
                                                            <div class="item">
                                                                <div class="event-wrap">
                                                                    <div class="event-img-wrap course-banner-wrap color-white text-center" style="">
                                                                        <img alt="Event" class="img-responsive" alt="Event" class="img-responsive" src="{{ $batch_path.$batch->logo }}" width="600" height="220">
                                                                        <span class="cat bg-yellow">{{ $batch->program->kode }}</span>
                                                                    </div>
                                                                    <!-- Event Image Wrapper -->
                                                                    <!-- Event Detail Wrapper -->
                                                                    <div class="event-details" style="height: 450px;">
                                                                        <h4 class="batch-title" style="font-size: 16px;"><a href="{{ url('daftar-batch/'.$batch->batch_id.'/'.$batch->nama) }}" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $batch->nama }}', '{{ url()->current() }}' )">{{ $batch->nama }}</a></h4>
                                                                        <ul class="events-meta">
                                                                            <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($batch->tgl_mulai)) }}</li>
                                                                            {{--<li><i class="fa fa-calendar-o"></i> {{ date('l, d M Y',strtotime($batch->tanggal)) }}</li>--}}
                                                                            <li><i class="fa fa-map-marker"></i> {{ !empty($batch->lokasi->nama) ? $batch->lokasi->nama:"Gedung Bursa Efek Indonesia" }}</li>
                                                                            <li><i class="fa fa-users"></i> Minimal Peserta {{ !empty($batch->minimal) ? $batch->minimal:10 }} Orang</li>
                                                                        </ul>
                                                                        <p>{!! substr($batch->keterangan,0,150) !!}</p>

                                                                        <a href="{{ url('daftar-batch/'.$batch->batch_id.'/'.$batch->nama) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $batch->nama }}', '{{ url()->current() }}' )" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>

                                                                        {{--@if($value->kd_type != 1)--}}
                                                                            {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $value->kd_type }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                                                        {{--@else--}}
                                                                            {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $value->cabang }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                                                        {{--@endif--}}
                                                                    </div><!-- Event Meta -->
                                                                    <br/>
                                                                </div><!-- Event details -->
                                                            </div><!-- Column -->
                                                        @endforeach
                                                    </div>
                                                @endif

                                        @endforeach
                                    @endif
                                @if($workshops->count() == 0 && $programs->count() == 0)
                                    <div class="col-sm-12">
                                        <div class="alert alert-warning" role="alert"><h4>Maaf, belum ada jadwal tersedia.</h4></div>
                                    </div>
                                @endif
                                {{-- Memasukkan array kode ke dalam select yang isinya akan diambil jquery --}}
                                <select id="kode" style="visibility: hidden;">
                                    @foreach($arr_kode as $kode)
                                    <option value="{{$kode}}"></option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <br>
                    </div>
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->


        <!-- Section -->
        <section class="bg-dark-red typo-light">
            <div class="container">
                <div class="row counter-sm">
                    <!-- Title -->
                    <div class="col-sm-12">
                        <div class="title-container">
                            <div class="title-wrap">
                                <h3 class="title sekilas-title">Sekilas tentang TICMI</h3>
                                <span class="separator line-separator"></span>
                            </div>
                            <p class="description">Berikut beberapa data kami </p>
                        </div>
                    </div>
                    <!-- Title -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WPPE</h5>
                            <h3 data-count="2315" class="count-number"><span class="counter">2315</span></h3>
                            <i class="uni-fountain-pen"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WPPE PT</h5>
                            <h3 data-count="1032" class="count-number"><span class="counter">1032</span></h3>
                            <i class="uni-talk-man"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan ASPM</h5>
                            <h3 data-count="119" class="count-number"><span class="counter">119</span></h3>
                            <i class="uni-fountain-pen"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WMI</h5>
                            <h3 data-count="997" class="count-number"><span class="counter">997</span></h3>
                            <i class="uni-download counter-data"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                </div><!-- Row -->
                <br>
                <div class="row counter-sm">
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white  color-dark">
                            <h5 style="">Lulusan WPPE Pemasaran</h5>
                            <h3 data-count="4140" class="count-number"><span class="counter">4140</span></h3>
                            <i class="uni-medal-3"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Total Pengunduh</h5>
                            <h3 data-count="3646" class="count-number"><span class="counter">3646</span></h3>
                            <i class="uni-talk-man"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Total Data Diunduh</h5>
                            <h3 data-count="598282" class="count-number"><span class="counter">598282</span></h3>
                            <i class="uni-download counter-data"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->
    {{-- <meta name="csrf-token" content="crsf_token"> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ url('assets/owlcarousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var array = [];
            $('#kode option').each(function() {
                array.push($(this).val());
            });
            
            $.each(array,function(key,val){
                $("#"+val).owlCarousel({
                    items:4,
                    nav:true,
                    margin:20,
                    // responsiveClass:true,
                    responsive:{
                        0:{
                            items:1,
                            nav:true
                        },
                        600:{
                            items:3,
                            nav:false
                        },
                        1000:{
                            items:4,
                            nav:true,
                            loop:false
                        }
                    }
                });
            })
        })
    </script>
@endsection


