@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Profile</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            {!! Form::open(['route' => ['profile.update', $peserta->peserta_id], 'id' => 'form_profile_peserta_edit', 'files'=> true]) !!}
                            {{method_field('PATCH')}}
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('nama', $peserta->nama, ['class' => 'form-control']) }}
                                                                {!! $errors->first('nama', '<span class="help-block form-messages">:message</span>') !!}
                                                                <small class="form-note">Masukkan nama sesuai dengan Kartu Identitas Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport" data-original-title="" title=""><span class="fa fa-question-circle"></span></a> </small>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Gender</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('sex', $sex, !empty($profiles->sex_id) ? $profiles->sex_id : null , ['class' => 'form-control select2 sex']) }}
                                                                {!! $errors->first('sex', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No Telp</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('no_telp', !empty($peserta->no_hp) ? $peserta->no_hp : null , ['class' => 'form-control']) }}
                                                                {!! $errors->first('tlp_rumah', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tanggal Lahir</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group date">
                                                                    {{ Form::text('tgl_lahir', $tgl_lahir, ['class' => 'form-control date-pickers']) }}
                                                                    <div class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-th"></span>
                                                                    </div>
                                                                </div>
                                                                {!! $errors->first('date_pickers', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tempat Lahir</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::textarea('tempat_lahir', !empty($profiles->tempat_lahir) ? $profiles->tempat_lahir : null , ['class' => 'form-control', 'size' => '-x-']) }}
                                                                {!! $errors->first('tempat_lahir', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Alamat Berdasarkan KTP</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::textarea('alamat_ktp', !empty($profiles->alamat_ktp) ? $profiles->alamat_ktp : null, ['class' => 'form-control', 'size' => '-x-']) }}
                                                                    {!! $errors->first('alamat_ktp', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Alamat Tempat Tinggal Sekarang</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::textarea('alamat_tinggal_skr', !empty($profiles->alamat_tinggal_skr) ? $profiles->alamat_tinggal_skr : null, ['class' => 'form-control', 'size' => '-x-']) }}
                                                                    {!! $errors->first('alamat', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div> 
                                            </div>

                                        <div class="col-md-12">
                                            <div class="form-group foto">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Foto</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                            <img src="{{ !empty($profiles->photo) ? 
                                                                                asset('assets/upload_files/peserta/photos/' .$profiles->photo) : asset('assets/images/default/no-image.png') }}" alt="foto"> </div>
                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;"> </div>
                                                                            
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="foto"> </span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                            <div class="clearfix margin-top-10">
                                                                            <span class="label label-danger">NOTE!</span> <span class="required" style="color:red;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                            <br />
                                                                            <small class="form-note">Foto yang Anda masukkan akan digunakan untuk sertifikat Anda<a href="#" onclick="return false" data-container="body" ></a> </small>
                                                                        </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                            <div class="form-group ktp">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>KTP</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                            <img src="{{ !empty($profiles->photo) ? 
                                                                                asset('assets/upload_files/peserta/ktp/' .$profiles->file_ktp) : asset('assets/images/default/no-image.png') }}" alt="ktp"> </div>
                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;"> </div>
                                                                            
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="ktp"> </span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE!</span> <span class="required" style="color:red;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                    </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--/row-->
                                </div><!--/@left-->

                                <!--@right-->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Lokasi</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('lokasi', $lokasi, !empty($profiles->lokasi_id) ? $profiles->lokasi_id : null, ['class' => 'form-control select2 lokasi']) }}
                                                                {!! $errors->first('lokasi', '<span class="help-block form-messages">:message</span>'); !!}
                                                                <span id="loading-lokasi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>KP</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('kp', $kp, !empty($profiles->lembg_pdkn_id) ? $profiles->lembg_pdkn_id : null, ['class' => 'form-control select2 kp']) }}
                                                                {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Warga Negara</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('negara', $negara, !empty($profiles->negara_id) ? $profiles->negara_id : null, ['class' => 'form-control select2 negara']) }}
                                                                {!! $errors->first('negara', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Pendidikan</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('pendidikan', !empty($profiles->pendidikan) ? $profiles->pendidikan : null, ['class' => 'form-control']) }}
                                                                    {!! $errors->first('pendidikan', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Jurusan</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('jurusan', !empty($profiles->jurusan) ? $profiles->jurusan : null, ['class' => 'form-control']) }}
                                                                    {!! $errors->first('jurusan', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                               <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Universitas</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('universitas', !empty($profiles->universitas) ? $profiles->universitas : null, ['class' => 'form-control']) }}
                                                                {!! $errors->first('universitas', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Lembaga</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('lembaga', $lembaga, !empty($profiles->lembaga_id) ? $profiles->lembaga_id : null, ['class' => 'form-control select2 lembaga']) }}
                                                                {!! $errors->first('lembaga', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Profesi</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('profesi', $profesi, $peserta->profesi_id, ['class' => 'form-control select2 profesi']) }}
                                                                {!! $errors->first('profesi', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <?php
                                            $hidden_profesi = $peserta->profesi_id == 1 ? '' : 'hidden';
                                        ?>
                                        <div class="col-md-12 {{ $hidden_profesi }}" id="hidden_profesi">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Profesi Lainnya</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('profesi_lainnya', !empty($peserta->profesi) ? $peserta->profesi : null, ['class' => 'form-control']) }}
                                                                {!! $errors->first('profesi_lainnya', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Perusahaan</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('perusahaan', $perusahaan, !empty($profiles->perusahaan_id) ? $profiles->perusahaan_id : null, ['class' => 'form-control select2 perusahaan']) }}
                                                                {!! $errors->first('perusahaan', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group perusahaan_lainnya hidden">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Perusahaan Lainnya</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('perusahaan_lainnya', !empty($profiles->perusahaan_lainnya) ? $profiles->perusahaan_lainnya : null, ['class' => 'form-control']) }}
                                                                {!! $errors->first('perusahaan_lainnya', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Pekerjaan</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('pekerjaan', !empty($profiles->pekerjaan) ? $profiles->pekerjaan : null , ['class' => 'form-control']) }}
                                                                {!! $errors->first('pekerjaan', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No SID</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('no_sid', $peserta->no_sid, ['class' => 'form-control', 'placeholder'=>'Contoh : IDD2601EB4556']) }}
                                                                {!! $errors->first('no_sid', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group sid_card">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>SID Card</td>
                                                            <td></td>
                                                            <td>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                        <img src="{{ !empty($peserta->sid_card) ? 
                                                                            asset('assets/upload_files/peserta/sid_card').'/'.$peserta->sid_card : asset('assets/ticmi/SID.jpg') }}" alt="sid_card"> </div>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="sid_card"> </span>
                                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                        <div class="clearfix margin-top-10"><span class="label label-danger">NOTE!</span>
                                                                            <small class="form-note text-left" style="text-align: left;color:red;">Masukkan scan bagian depan Kartu Akses Anda 
                                                                                <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-html="true" data-trigger="focus" data-content="Kartu Akses merupakan kartu yang diperoleh jika Anda menjadi Investor yang dikeluarkan oleh KSEI. <a href='http://akses.ksei.co.id/info/faq' target='_blank'>Info lebih lanjut</a> "><span class="fa fa-question-circle"></span></a> 
                                                                            </small>
                                                                        </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div><!--/@right-->
                                     </div>

                            <div class="clearfix"></div>
                            <br>

   
                            <div class="form-group">
                                <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                <button type="submit" name="simpan" class="btn red btn-block"><h4>Simpan Perubahan</h4></button>
                            </div>
                          {!! Form::close() !!}

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>


    {!! JsValidator::formRequest('App\Http\Requests\ProfileRequest', '#form_profile_peserta_edit'); !!}
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_profile_peserta_edit').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_profile_peserta_edit').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

        $(document).ready(function(){
            $("select[name='perusahaan']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_perusahaan') }}" });
            $("select[name='profesi']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_profesi') }}" });
            $("select[name='lembaga']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lembaga') }}" });
            $("select[name='lokasi']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lokasi') }}" });
            $("select[name='sex']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_gender') }}" });
            $("select[name='negara']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_negara') }}" });
            $("select[name='kp']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lembg_pdkn') }}" });

            $("select[name='lokasi']").on('change', function(){
                var lokasi = $(this).val().trim();

                $('#loading-lokasi').css({'display': 'block'});

                // reset if filled
                $("select[name='kp']").html('');

                $.ajax({
                    method: 'GET',
                    url: '{{ route('profile.ajax_get_kp') }}',
                    data: {lokasi: lokasi},
                    success: function(msg){
                        console.log(msg);
                        $('#loading-lokasi').css({'display': 'none'});
                        obj_kp  = JSON.parse(msg);
                        list_kp = "<option value=''></option>";

                        if(obj_kp.length > 0){
                            $.each(obj_kp, function(i,v){    
                                list_kp += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                            });
                        }else{
                            list_kp = "<option></option>";
                        }

                        
                        $("select[name='kp']").html(list_kp);
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                        $('#loading-lokasi').css({'display': 'none'});
                    }
                });
            });

            $('#form_profile_peserta_edit').on('submit', function (e) {
                // form is valid
                is_form_valid = $('#form_profile_peserta_edit').valid();
                if(is_form_valid == true){
                    perusahaan = $("select[name='perusahaan'] :selected").val().trim();
                    if(perusahaan == '1'){ // Perusahaan Lainnya
                        perusahaan_lainnya = $("input[name='perusahaan_lainnya']").val();
                        if(perusahaan_lainnya == ''){
                            alert('Mohon isi Nama Perusahaan!');
                            return false;
                        }
                    }

                    $('button[name="simpan"]').attr('disabled', 'disabled');
                    $('#execute-loading').css({'visibility': 'visible'});
                }
            });

            perusahaan_lainnya = '{{ !empty($profiles->perusahaan_lainnya) ? $profiles->perusahaan_lainnya : '' }}';
            if(perusahaan_lainnya != '')
            {
                //$("select[name='perusahaan']").val(0).trigger('change');
                $('.perusahaan_lainnya').removeClass('hidden');
            }

            $("select[name='perusahaan']").on('change', function(){
                perusahaan = $(this).val();

                if(perusahaan == 1){ // Perusahaan Lainnya
                    $('.perusahaan_lainnya').removeClass('hidden');
                }else{
                    $('.perusahaan_lainnya').addClass('hidden');
                    //$("input[name='username']").val('');
                }
            });

            $("select[name='profesi']").on('change', function(){
                profesi = $(this).val();

                if(profesi == 1){ // Perusahaan Lainnya
                    $('#hidden_profesi').removeClass('hidden');
                }else{
                    $('#hidden_profesi').addClass('hidden');
                }
            });

             $('[data-toggle="popover"]').popover(); 
            <?= \Helper::date_formats('$(".date-pickers")', 'js') ?>
        });
    </script>
@endsection