@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <style>
        fieldset.block-border {
            width:95%;
            border: 1px solid #e7505a!important;
            padding: 0 0.2em 0.2em 0.2em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
                    box-shadow:  0px 0px 0px 0px #000;
        }

        legend.block-border {
            width: auto !Important;
            border: none;
            font-size: 14px;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Verifikasi Data Seritifikat</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            {!! Form::open(['route' => 'profile.insert_sertifikat', 'id' => 'form_verify_sertifikat', 'files'=> true]) !!}
                                <div class="col-md-6">
                                    <div class="row">
                                        <fieldset class="block-border">
                                        <legend class="block-border font-red bold"><h4>[ Data Sertifikat ]</h4></legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('nama', $peserta->nama, ['class' => 'form-control']) }}
                                                                {!! $errors->first('nama', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tanggal Lahir</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group date">
                                                                    {{ Form::text('tgl_lahir', \Helper::date_formats($peserta->tanggal_lahir), ['class' => 'form-control date-pickers']) }}
                                                                    <div class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-th"></span>
                                                                    </div>
                                                                </div>
                                                                {!! $errors->first('date_pickers', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group foto">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Foto</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                            <img src="{{ !empty($profiles->photo) ? 
                                                                                asset('assets/upload_files/peserta/photos/' .$profiles->photo) : asset('assets/images/default/no-image.png') }}" alt="foto"> </div>
                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;"> </div>
                                                                            
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="foto"> </span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                            <div class="clearfix margin-top-10">
                                                                                <span class="label label-danger">NOTE!</span> <span class="required" style="color:red;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                            </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div><!--/row-->
                                </div><!--/@left-->
                                <input name="batch_id" type="hidden" value="{{ $batch_id }}">
                                <!--@right-->
                                <div class="col-md-6">
                                    <div class="row">
                                        <fieldset class="block-border">
                                        <legend class="block-border font-red bold"><h4>[ Tempat Pengambilan Sertifikat ]</h4></legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Lokasi</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('lokasi', $lokasi, $row_cabang->lokasi_id, ['class' => 'form-control select2 lokasi']) }}
                                                                {!! $errors->first('lokasi', '<span class="help-block form-messages">:message</span>'); !!}
                                                                <span id="loading-lokasi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                            </td>
                                                        </tr>   
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Cabang</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('cabang', $cabang, $row_cabang->cabang_id, ['class' => 'form-control select2 cabang']) }}
                                                                {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                                <span id="loading-cabang" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>KP</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('kp', $kp, $profiles->lembg_pdkn_id, ['class' => 'form-control select2 kp']) }}
                                                                {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </fieldset>
                                    </div>
                                </div><!--/@right-->
                                    
                            <div class="clearfix"></div>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2">
                                        <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                        <button type="submit" name="simpan" class="btn green">Simpan</button>
                                    </div>
                                </div>
                            </div>
                          {!! Form::close() !!}

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>


    {!! JsValidator::formRequest('App\Http\Requests\VerifySertifikatRequest', '#form_verify_sertifikat'); !!}
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_verify_sertifikat').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_verify_sertifikat').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

        $(document).ready(function(){
            $('.profile').addClass('active open');

            $("select[name='lokasi']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lokasi') }}" });
            $("select[name='cabang']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_cabang') }}" });
            $("select[name='kp']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lembg_pdkn') }}" });

            $("select[name='lokasi']").on('change', function(){
                var lokasi = $(this).val().trim();

                $('#loading-lokasi').css({'display': 'block'});

                    // reset if filled
                    $("select[name='cabang']").val('').trigger('change');
                    $("select[name='kp']").val('').trigger('change');

                    $.ajax({
                        method: 'GET',
                        url: '{{ route('ujian_ulang.ajax_get_cabang') }}',
                        data: {lokasi: lokasi},
                        success: function(msg){
                            console.log(msg);
                            $('#loading-lokasi').css({'display': 'none'});
                            obj_cabang  = JSON.parse(msg);
                            list_cabang = "<option value=''></option>";

                            if(obj_cabang.length > 0){
                                $.each(obj_cabang, function(i,v){    
                                    list_cabang += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                                });
                            }else{
                                list_cabang = "<option></option>";
                            }

                            
                            $("select[name='cabang']").html(list_cabang);
                        },
                        error: function(err){
                            alert(JSON.stringify(err));
                            $('#loading-lokasi').css({'display': 'none'});
                        }
                    });
            });

            $("select[name='cabang']").on('change', function(){
                var cabang = $(this).val().trim();

                $('#loading-cabang').css({'display': 'block'});

                    // reset if filled
                    $("select[name='kp']").val('').trigger('change')

                    $.ajax({
                        method: 'GET',
                        url: '{{ route('ujian_ulang.ajax_get_kp') }}',
                        data: {cabang: cabang},
                        success: function(msg){
                            console.log(msg);
                            $('#loading-cabang').css({'display': 'none'});
                            obj_kp  = JSON.parse(msg);
                            list_kp = "<option value=''></option>";

                            if(obj_kp.length > 0){
                                $.each(obj_kp, function(i,v){    
                                    list_kp += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                                });
                            }else{
                                list_kp = "<option></option>";
                            }

                            
                            $("select[name='kp']").html(list_kp);
                        },
                        error: function(err){
                            alert(JSON.stringify(err));
                            $('#loading-cabang').css({'display': 'none'});
                        }
                    });
            });
        });
    </script>
@endsection