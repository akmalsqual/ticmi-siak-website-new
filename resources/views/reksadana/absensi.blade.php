@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/images/ticmi/wisuda/banner2.jpg') }}" class="img-responsive" alt="Wisuda Akbar Periode II Tahun 2017">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center">Absensi Wisuda Akbar Periode II Tahun 2017</h5>
                            @include('errors.list')
                            @include('flash::message')
                            @if($pesertaWisuda && $pesertaWisuda->count() > 0)
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="30%">Nama</td>
                                        <td>: {{ $pesertaWisuda->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>: {{ $pesertaWisuda->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Program</td>
                                        <td>: {{ $pesertaWisuda->program }}</td>
                                    </tr>
                                    <tr>
                                        <td>No HP</td>
                                        <td>: {{ $pesertaWisuda->no_hp }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            @if(!empty($pesertaWisuda->is_hadir))
                                                <a href="#" class="btn btn-lg absenWisuda"  data-url="{{ route('wisuda.absensi.store',['emailwisuda'=>base64_encode($pesertaWisuda->email)]) }}" data-loading-text="Loading">ABSEN</a>
                                            @else
                                                <a href="#" class="btn btn-lg"  data-url="" data-loading-text="Loading">SUDAH ABSEN</a>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                <p class="text-center">Data Peserta Wisuda tidak ditemukan</p>
                            @endif
                        </div><!-- Blog Detail Wrapper -->
                </div><!-- Blog Wrapper -->

                <!-- Blog Share Post -->
                <div class="share">
                    <h5>Share Post : </h5>
                    <ul class="social-icons">
                        <li class="fb-share-button"  data-href="{{ route('wisuda') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('wisuda') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                        </li>
                    </ul><!-- Blog Social Share -->
                </div><!-- Blog Share Post -->

            </div><!-- Column -->
        </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection