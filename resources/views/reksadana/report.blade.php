@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/images/ticmi/wisuda/banner2.jpg') }}" class="img-responsive" alt="Wisuda Akbar Periode II Tahun 2017">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center">Daftar Hadir Wisuda Akbar Periode II Tahun 2017</h5>
                            @include('errors.list')
                            @include('flash::message')

                            <table class="table table-bordered table-hover table-striped table-wisuda">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Program</th>
                                    <th>Absensi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($pesertaWisuda && $pesertaWisuda->count() > 0)
                                    @foreach($pesertaWisuda as $item)
                                        <tr>
                                            <td>{{ strtoupper($item->nama) }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->program }}</td>
                                            <td class="text-center">
                                                @if($item->is_hadir == 1)
                                                    <span class="label label-success">Hadir</span>
                                                @else
                                                    <a href="" class="absenWisuda" data-url="{{ route('wisuda.absensi.store',['emailwisuda'=>base64_encode($item->email),'from'=>'report']) }}" data-loading-text="Loading"><span class="label label-danger">X</span></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection

@push('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table-wisuda').DataTable({
                pageLength: 20,
                lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
                order: [],
                pagingType : "full_numbers",
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline' },
//                    { extend: 'copy', className: 'btn red btn-outline' },
                    { extend: 'pdf', className: 'btn green btn-outline' },
                    { extend: 'excel', className: 'btn yellow btn-outline ' },
//                    { extend: 'csv', className: 'btn purple btn-outline ' },
//                    { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
                ],
//                dom: 'Bfrtip',
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            });
        });
    </script>
@endpush