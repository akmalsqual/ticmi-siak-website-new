@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/wisuda/banner.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">Wisuda Akbar TICMI Periode II 2017</a></h5>
                            <p class="text-justify">
                                <strong>The Indonesia Capital Market Intitute (TICMI)</strong> mengundang semua lulusan Program Sertifikasi dengan periode kelulusan
                                <strong>April 2017</strong> s/d <strong>Oktober 2017</strong> untuk mengikuti acara Wisuda Akbar TICMI Periode II yang akan di selenggarakan pada :
                            </p>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <table class="table table-bordered table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td align="right" width="30%">Hari / Tanggal</td>
                                            <td>Rabu, 25 April 2018</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Waktu</td>
                                            <td>Pukul 14:30 s/d 17:00 WIB</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Tempat</td>
                                            <td>Main Hall BEI, Gedung Bursa Efek Indonesia, Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190 </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <strong>Acara ini tidak dikenakan biaya</strong> <br>
                                    <br>
                                    <br>
                                </div>
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">Terima kasih telah melakukan konfirmasi kehadiran</div>
                                        </div>
                                        <div class="panel-body">
                                            @include('errors.list')
                                            @include('flash::message')
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('wisuda') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('wisuda') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection