@extends('layouts_ticmi.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">Form Pendaftaran Sekolah Reksadana</a></h5>
                             
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                     <table class="table table-bordered table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td align="right" width="30%">Nama Peserta</td>
                                            <td>{{ auth()->user()->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Nama Seminar / Workshop</td>
                                            <td>{{ $workshop->workshop_name }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Tanggal</td>
                                            <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }} </td>
                                        </tr>
										<tr>
                                            <td align="right">Waktu</td>
                                            <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }} </td>
                                        </tr>
										<tr>
                                            <td align="right">Lokasi - Ruangan</td>
                                            <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							 
                            <div class="row  hidden" id='form-reksadana'>
                              
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="panel-title">Form Pendaftaran</div>
                                            </div>
                                            <div class="panel-body">
                                                @include('errors.list')
                                                @include('flash::message')
                                                {!! Form::open(['url'=>route('reksadana.store',['workshopslug'=>$workshop->slugs]),'class'=>'form-horizontal','autocomplete'=>'off','files'=>true]) !!}
                                                <div class="panel-body">
												  <div class="container">
													<div class="stepwizard">
														<div class="stepwizard-row setup-panel">
															<div class="stepwizard-step">
																<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
																<p>Data Pribadi </p>
															</div>
															<div class="stepwizard-step">
																<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
																<p>Alamat </p>
															</div>
															<div class="stepwizard-step">
																<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
																<p>Alamat Domisili</p>
															</div>
															<div class="stepwizard-step">
																<a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
																<p>Data Bank</p>
															</div>
															<div class="stepwizard-step">
																<a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
																<p>Kontak   </p>
															</div>
															<div class="stepwizard-step">
																<a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
																<p>Data Pekerjaan   </p>
															</div>
														</div>
													</div>
													<form role="form">
														<div class="row setup-content" id="step-1">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5> Data Pribadi </h5>


																	<div class="form-group">
																		<label class="control-label">Nama Lengkap<em class="text-red">*</em></label>
																		  {{ Form::text('nama',auth()->user()->nama,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
																		  {{ Form::hidden('workshop_id',$workshop->id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
																			{{ Form::hidden('t_program_id',$workshop->t_program_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
																			{{ Form::hidden('am_id',$workshop->am_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
																	</div>

																	<div class="form-group">
																		<label class="control-label">Jenis Kelamin<em class="text-red">*</em></label>
																		<div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('kelamin','L',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Laki-Laki</label>
																		</div>
																		<div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('kelamin','P',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Perempuan</label>
																	</div>
																	</div>

																	<div class="form-group">
																		<label class="control-label">Nama Gadis Ibu Kandung<em class="text-red">*</em></label>
																		  {{ Form::text('nama_ibu',null,['class'=>'form-control','placeholder'=>'Nama Gadis Ibu Kandung','required'=>'required']) }}
																	</div>

																	<div class="form-group">
																		<label class="control-label">Kewarganegaraan<em class="text-red">*</em></label>
																	 <div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('nasional','WNI',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Warga Negara Indonesia</label>
																		</div>
																		<div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('nasional','WNA',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Warga Negara Asing</label>
																	</div>
																	</div>

																	<div class="form-group">
																		<label class="control-label">Jenis Tanda Pengenal<em class="text-red">*</em></label>
																		   {{Form::select('tipe_id',['KTP'=>'KTP','KITAS'=>'KITAS','Passport'=>'Passport','SIM'=>'SIM'],null,['class'=>'form-control','placeholder'=>'Pilih Tanda Pengenal','required'=>'required']) }}
																	</div>
																	  <div class="form-group">
																		{!! Form::label('upload_file','Upload File Tanda Pengenal') !!}
																		{!! Form::file('file_ktp') !!}
																			<small class="form-text text-danger text-muted">*jpg,jpeg,png</small>
																	</div>
																	<div class="form-group">
																		<label class="control-label">No. Tanda Pengenal<em class="text-red">*</em></label>
																		  {{ Form::text('no_ktp',null,['class'=>'form-control','placeholder'=>'No. Kartu Pengenal','required'=>'required']) }}
																	</div>

																	<div class="form-group">
																		<label class="control-label">Tempat Lahir<em class="text-red">*</em></label>
																		  {{ Form::text('tempat_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir','required'=>'required']) }}
																	</div>

																	<div class="form-group">
																		<label class="control-label">Tanggal Lahir<em class="text-red">*</em></label>
																		 {{ Form::text('tgl_lahir',null,['class'=>'form-control datepicker','placeholder'=>'Tanggal Lahir','required'=>'required']) }}
																	</div>

																	<div class="form-group">
																		<label class="control-label">Status Pernikahan<em class="text-red">*</em></label>
																		<div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('marital','M',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Menikah</label>
																		</div>
																		<div class="radio-inline">
																			<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('marital','S',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Tidak Menikah</label>
																	</div>
																	</div>

																	<div class="form-group">
																		<label class="control-label">Pendidikan Terakhir<em class="text-red">*</em></label>
																		{!! Form::select('pendidikan',$pendidikan,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Pendidikan','required'=>'required']) !!}
																</div>

																	<div class="form-group">
																		<label class="control-label">Agama<em class="text-red">*</em></label>
																		{!! Form::select('agama',$agama,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Agama','required'=>'required']) !!}

																	</div>

																	<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
																</div>
															</div>
														</div>
														<div class="row setup-content" id="step-2">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5> Alamat </h5><p><strong> (Alamat Sesuai KTP)</strong></p>
																	<div id="clone_ktp">
																	 <div class="col-sm-12">
																	 <div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Alamat Sesuai KTP <em class="text-red">*</em></label>
																				{{ Form::textarea('alamat_ktp',null,['class'=>'form-control','placeholder'=>'Alamat Sesuai KTP (masukkan alamat selengkap-lengkapnya)','rows'=>5,'required'=>'required']) }}
																			</div>
																	 </div>
																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Provinsi (Sesuai KTP) <em class="text-red">*</em></label>
																				{{ Form::select('provinsi_ktp',$provinsi,null,['class'=>'form-control cari-kabupaten','placeholder'=>'Pilih Provinsi Sesuai KTP','data-prefix-code'=>'ktp','required'=>'required']) }}
																			</div>
																		</div>

																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Kabupaten/Kota (Sesuai KTP) <em class="text-red">*</em></label>
																				{{ Form::select('kabupaten_ktp',[],null,['class'=>'form-control kabupaten_ktp cari-kecamatan','placeholder'=>'Pilih Kabupaten/Kota Sesuai KTP','data-prefix-code'=>'ktp','required'=>'required']) }}
																			</div>
																		</div>
																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Kecamatan (Sesuai KTP) <em class="text-red">*</em></label>
																				{{ Form::select('kecamatan_ktp',[],null,['class'=>'form-control kecamatan_ktp','placeholder'=>'Pilih Kecamatan Sesuai KTP','required'=>'required']) }}
																			</div>
																		</div>
																		</div>

																	<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
																</div>

															</div>
														</div>
														<div class="row setup-content" id="step-3">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5> Alamat Domisili </h5><p>(jika alamat sama dengan ktp cukup centang, namun jika berbeda harus mengisi)</p>
																	  {!! Form::checkbox('alamat_sesuai_ktp',1,null,['id'=>'sesuai_ktp']) !!} Sesuai KTP

																	<div class="cek_sesuai_ktp">
																	 <div class="col-sm-12">
																	 <div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Alamat Domisili <em class="text-red">*</em></label>
																				{{ Form::textarea('alamat_skrg',null,['class'=>'form-control','placeholder'=>'Alamat Sesuai Domisili (masukkan alamat selengkap-lengkapnya)','rows'=>5]) }}
																			</div>
																	 </div>
																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Provinsi (Domisili) <em class="text-red">*</em></label>
																				{{ Form::select('provinsi_skrg',$provinsi,null,['class'=>'form-control cari-kabupaten','placeholder'=>'Pilih Provinsi Sesuai Domisili','data-prefix-code'=>'skrg']) }}
																			</div>
																		</div>

																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Kabupaten/Kota (Domisili) <em class="text-red">*</em></label>
																				{{ Form::select('kabupaten_skrg',[],null,['class'=>'form-control kabupaten_skrg cari-kecamatan','placeholder'=>'Pilih Kabupaten/Kota Sesuai Domisili','data-prefix-code'=>'skrg']) }}
																			</div>
																	 </div>
																	 <div class="col-sm-12">
																			<div class="input-text form-group form-group-sm">
																				<label for="nama" class="hidden-xs">Kecamatan (Sekarang) <em class="text-red">*</em></label>
																				{{ Form::select('kecamatan_skrg',[],null,['class'=>'form-control kecamatan_skrg','placeholder'=>'Pilih Kecamatan Sesuai Domisili']) }}
																			</div>
																		</div>
																		</div>
																	<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>

																	</div>
																</div>
															</div>
														<div class="row setup-content" id="step-4">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5>Informasi Rekening Bank</h5>
																	<div class="form-group">
																		<label class="control-label">Bank<em class="text-red">*</em></label>
																		 {{ Form::text('bank',null,['class'=>'form-control','placeholder'=>'Nama Bank','required'=>'required']) }}
																	</div>
																	<div class="form-group">
																		<label class="control-label">Cabang<em class="text-red">*</em></label>
																		 {{ Form::text('cbg_bank',null,['class'=>'form-control','placeholder'=>'Cabang Bank','required'=>'required']) }}
																	</div>
																	<div class="form-group">
																		<label class="control-label">No. Rekening<em class="text-red">*</em></label>
																		 {{ Form::text('rekening',null,['class'=>'form-control','placeholder'=>'No. Rekening','required'=>'required']) }}
																	</div>
																	<div class="form-group">
																		<label class="control-label">Nama Pemilik Rekening<em class="text-red">*</em></label>
																		 {{ Form::text('nama_rekening',null,['class'=>'form-control','placeholder'=>'Nama Pemilik Rekening','required'=>'required']) }}
																	</div>
																	<div class="form-group">
																		<label class="control-label">Mata Uang <em class="text-red">*</em></label>
																		 {{ Form::text('nilai','Rupiah',['class'=>'form-control','placeholder'=>'Currency','readonly'=>'readonly']) }}
																	</div>
																	<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
																</div>
															</div>
														</div>
														<div class="row setup-content" id="step-5">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5> Contact Information</h5>
																	<div class="form-group">
																		<label class="control-label">Email<em class="text-red">*</em></label>
																			 {{ Form::email('email',auth()->user()->email,['class'=>'form-control','placeholder'=>'Email','readonly'=>'readonly']) }}
																	</div>
																	<div class="form-group">
																		<label class="control-label">No. HP<em class="text-red">*</em> </label>
																		 {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'No. HP','required'=>'required']) }}
																	</div>
																	<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
																</div>
															</div>
														</div>
														<div class="row setup-content" id="step-6">
															<div class="col-xs-12">
																<div class="col-md-12">
																	<h5> Job Data/Business Information </h5>
																	<div class="form-group">
																		<label class="control-label">Jenis Pekerjaan</label>
																		{!! Form::select('pekerjaan',$pekerjaan,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Pekerjaan','required'=>'required']) !!}

																	</div>

																	<div class="form-group">
																		<label class="control-label">Pendapatan Tahunan<em class="text-red">*</em></label>
																		{!! Form::select('pendapatan',$pendapatan,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Pendapatan','required'=>'required']) !!}

																	</div>

																	<div class="form-group">
																		<label class="control-label">Sumber Dana <em class="text-red">*</em></label>
																		{!! Form::select('sumber_dana',$sumber_dana,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Sumber Dana','required'=>'required']) !!}

																	</div>

																	<div class="form-group">
																		<label class="control-label">Tujuan Investasi<em class="text-red">*</em> </label>

																		  {!! Form::select('goal',$tujuan_investasi,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Tujuan Investasi','required'=>'required']) !!}

																	</div>

																	<button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
																</div>
															</div>
														</div>

													</form>
													</div>
												</div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

										<div class="row">
											<div class="col-sm-12 text-center">
												{{--<strong>Konfirmasi kehadiran sudah di tutup.</strong> <br>--}}
												Jika Anda mempunyai pertanyaan dan membutuhkan informasi lebih lanjut silahkan email ke :
												<a href="mailto:info@ticmi.co.id">info@ticmi.co.id</a> atau hubungi callcenter di : <strong>0800 100 9000 (bebas pulsa)</strong>
												<br>
											</div>
										</div>
                                    </div>
                                
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ url()->current() }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection