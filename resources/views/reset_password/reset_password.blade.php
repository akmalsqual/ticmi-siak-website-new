@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <h5>Reset Password</h5>
                                    </a>
                                </li><!-- Page Template Logo -->
                            </ul>
                            @if(Session::has('message'))
                                <div class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</div>
                            @endif

                                {{ Form::open(['url'=>route('resetpassword.proses',['useremail'=>trim($useremail),'userid'=>$userid]), 'id' => 'form_reset_password']) }}
                                <div class="alert alert-info">
                                    Untuk menjaga keamanan sistem dan akun Anda, Password Anda Kami telah reset. Silahkan cek email Anda untuk mendapatkan Token Password dan Masukkan Token dan Password baru Anda pada form dibawah ini. Cek folder Spam pada email Anda
                                    jika email reset password belum Anda terima.
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::text('token',null,['class'=>'input-name form-control','placeholder'=>'Masukkan Token Password Anda yang terdapat di email', 'required']) !!}

                                    @if ($errors->has('token'))
                                        <span class="help-block" style="color:red">
                                            <strong>{{ $errors->first('token') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::password('password',['class'=>'input-name form-control','placeholder'=>'Masukkan Password Baru Anda', 'minlength' => '6', 'required']) !!}

                                    @if ($errors->has('password'))
                                        <span class="help-block" style="color:red">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::password('password_confirmation',['class'=>'input-name form-control','placeholder'=>'Masukkan Kembali Password Baru Anda', 'minlength' => '6', 'required']) !!}
                                </div>
                                {!! Form::submit('Reset Password',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                {{ Form::close() }}
                            <br>
                            <br>
                            <ul class="text-center">
                                <li>
                                    Kembali ke halaman <a href="{{ route('login') }}">Login</a> atau <a href="{{ route('register.index') }}">Daftar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->

<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}" type="text/javascript"></script>

{!! JsValidator::formRequest('App\Http\Requests\ResetPasswordRequest', '#form_reset_password'); !!}
<script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_reset_password').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_reset_password').valid();
            if(is_form_valid == true){
                alert('failed');
            }
        });
</script>
@endsection