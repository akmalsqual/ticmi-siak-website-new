@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <h5>Anda belum melakukan verifikasi email</h5>
                                    </a>
                                </li><!-- Page Template Logo -->
                            </ul>

                            {{ Form::open(['url'=>route('register.resend-email-verification.proses'), 'id' => 'form_reset_password']) }}
                            <div class="alert alert-warning">
                                Anda tidak dapat login karena belum melakukan <strong>verifikasi email</strong>, silahkan cek email Anda dan klik link verifikasi. Jika Anda belum menerima email silahkan
                                klik tombol dibawah untuk mengirim kembali email verifikasi.
                                <p>Pastikan email Anda sudah benar</p>
                            </div>
                            @if(Session::has('alert-error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    {!! Session::get('alert-error') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                            @endif
                            @if(Session::has('message'))
                                <div class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</div>
                            @endif
                            <div class="input-text form-group text-left">
                                {!! Form::text('email',$user->email,['class'=>'input-name form-control','placeholder'=>'Masukkan alamat email Anda', 'required']) !!}

                                @if ($errors->has('email'))
                                    <span class="help-block" style="color:red">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                            {{ Form::hidden('userid', $user->peserta_id) }}
                            {!! Form::submit('Kirim ulang email verifikasi',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                            {{ Form::close() }}
                            <br>
                            <br>
                            <ul class="text-center">
                                <li>
                                    Kembali ke halaman <a href="{{ route('login') }}">Login</a> atau <a href="{{ route('register.index') }}">Daftar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->

    <script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}" type="text/javascript"></script>

    {!! JsValidator::formRequest('App\Http\Requests\ResetPasswordRequest', '#form_reset_password'); !!}
    <script type="text/javascript">
        /**
         * Action before submit form
         */
        $('#form_reset_password').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_reset_password').valid();
            if(is_form_valid == true){
                alert('failed');
            }
        });
    </script>
@endsection