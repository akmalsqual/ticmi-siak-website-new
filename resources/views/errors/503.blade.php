<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100i,200,200i,300,300i,400,400i,500,500i,600" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: #B0BEC5;*/
                color: #7b8589;
                display: table;
                font-weight: 200;
                font-family: 'Raleway';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }

            .text {
                font-size: 30px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img src="{{ asset('assets/images/default/logo-ticmi.jpg') }}" alt="TICMI">
                <div class="title">Kami sedang melakukan perbaikan pada sistem kami. Mohon maaf atas ketidaknyamanan Anda</div>
                <!--<div class="text">Akan selesai pada Minggu 23 Juli 2017 pukul 18:00 WIB</div>-->
            </div>
        </div>
    </body>
</html>
