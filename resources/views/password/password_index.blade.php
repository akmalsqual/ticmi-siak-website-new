@extends('layouts.app')
    @section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-key font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Ubah Password</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            {!! Form::open(['route' => ['password.update', Auth::user()->peserta_id ], 'id' => 'form_change_password']) !!}
                            {{method_field('PATCH')}}
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Password</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::password('password', ['class' => 'form-control']) }}
                                                            {!! $errors->first('password', '<span class="help-block form-messages">:message</span>'); !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Confirm Password</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                                                            {!! $errors->first('password_confirmation', '<span class="help-block form-messages">:message</span>'); !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>

                            <div class="form-group">
                                <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                <button type="submit" name="simpan" class="btn green">Simpan</button>
                            </div>
                            {!! Form::close() !!}
                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    {!! JsValidator::formRequest('App\Http\Requests\PasswordRequest', '#form_change_password'); !!}

    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_change_password').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_change_password').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

    </script>
@endsection