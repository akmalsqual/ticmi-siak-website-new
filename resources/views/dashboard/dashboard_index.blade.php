@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style>
    /*.ticmi {
        background-color: #e13138 !important;
    }*/
    .table-siak td:first-child {
        width: 20%!Important;
    }
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    @if (\Session::has('success-survey'))
                        <div class="alert alert-success">
                            {!! \Session::get('success-survey') !!}</li>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif

                    <div class="portlet light portlet-fit bordered">
                        <div class="content-box shadow bg-white">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <img src="{{ !empty(Auth::user()->profile->photo) ? asset('assets/upload_files/peserta/photos/'. Auth::user()->profile->photo) : asset('assets/images/default/no-image.png') }}" class="img-circle img-center" width="120px">
                                            <h3>{{ Auth::user()->nama }}</h3>
                                        </td>
                                        <td>
                                            <table class="table course-table no-margin">
                                                <tr>
                                                    <td>Email</td>
                                                    <td>: {{ Auth::user()->email }}</td>
                                                    <td>No HP</td>
                                                    <td>: {{ Auth::user()->no_hp }}</td>
                                                </tr>
                                                <tr>
                                                    <td>No SID</td>
                                                    <td>: {{ Auth::user()->no_sid }}</td>
                                                    <td>Profesi</td>
                                                    <td>: {{ Auth::user()->profesi }}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tagihan Pembayaran</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-siak-ajax" id="tagihan-table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Batch</th>
                                                <th>Jenis</th>
                                                <th>Tanggal</th>
                                                <th>Invoice</th>
                                                <th>No VA</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script>
    $(function() {
        var t = $('#tagihan-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ $url_ajax_datatable }}',
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama_batch', name: 'tagihan_tmp.nama_batch'},
                {data: 'jenis'},
                // {data: 'nama_batch', name: 'tagihan_tmp.nama_batch'},
                {data: 'tgl_daftar', name: 'tagihan_tmp.tgl_daftar'},
                {data: 'no_invoice', name: 'tagihan_tmp.no_invoice'},
                {data: 'account_no', name: 'tagihan_tmp.account_no'},
                {data: 'action', orderable:false, searchable: false},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 5,
            // stateSave: true,
            "order": [[ 1, 'asc' ]],
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });

    function confirm_destroy(form){
        var c = confirm("Hapus Data?");
        if(c){
            form.submit();
        }else{
            return false;
        }
    }
        // $( document ).ready(function() {
        //     $('.collapse').collapse('hide');
        // });
    </script>
@endsection