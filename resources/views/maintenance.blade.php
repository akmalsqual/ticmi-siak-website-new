<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->

<head>

	<!--- Basic Page Needs
   ================================================== -->
   <meta charset="utf-8">
	<title>TICMI | Maintenance</title>
   <meta name="description" content="">  
   <meta name="author" content="">

   <!-- Mobile Specific Metas
   ================================================== -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="https://ticmi.co.id/assets/css_um/base.css">
   <link rel="stylesheet" href="https://ticmi.co.id/assets/css_um/vendor.css">
   <link rel="stylesheet" href="https://ticmi.co.id/assets/css_um/main.css">    

   <!-- Modernizr
   =================================================== -->
   <script src="js/modernizr.js"></script>

   <!-- Favicons
   =================================================== -->
   <link rel="shortcut icon" href="https://ticmi.co.id/assets/favicon.png" >
    
</head>
    
<body>

   <!--[if lt IE 9]>
   	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. 
   	Please <a href="https://ticmi.co.id/assets/http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
   <![endif]-->   

   <!-- content-wrap -->
   <div id="content-wrap">

      <!-- main  -->
      <main class="row">

            <header class="site-header">
               <div class="logo">
               	<a href="https://ticmi.co.id/assets/index.html">Advent.</a>
               </div> 
            </header>

            <div id="main-content" class="twelve columns">

               <h1>This website is under construction</h1>

               <p> Estimated time remaining before launch
               </p>

               <hr>

               <div id="counter" class="group">
                  <span>0 <em>days</em></span> 
                  <span>2 <em>hours</em></span>
                  <span>00 <em>minutes</em></span>
                  <span>00 <em>seconds</em></span> 
               </div>                  

               <!-- MailChimp Signup Form -->
                

            </div><!-- /main-content form -->

             
            
      </main>	      

   </div><!-- /content-wrap --> 
    	

   <!-- modals
   =================================================== -->
	      
	 

    

 	<!-- footer
   =================================================== -->
    

   <div id="preloader"> 
    	<div id="loader">
     	</div>
   </div> 
   
<!-- Script
=================================================== -->
<script src="https://ticmi.co.id/assets/js_um/jquery-1.11.3.min.js"></script>
<script src="https://ticmi.co.id/assets/js_um/jquery-migrate-1.2.1.min.js"></script>
<script src="https://ticmi.co.id/assets/http://maps.google.com/maps/api/js_um?v=3.13&amp;sensor=false"></script>    
<script src="https://ticmi.co.id/assets/js_um/jquery.fittext.js"></script>
<script src="https://ticmi.co.id/assets/js_um/jquery.countdown.min.js"></script>
<script src="https://ticmi.co.id/assets/js_um/jquery.placeholder.min.js"></script>
<script src="https://ticmi.co.id/assets/js_um/owl.carousel.min.js"></script>
<script src="https://ticmi.co.id/assets/js_um/jquery.ajaxchimp.min.js"></script>
<script src="https://ticmi.co.id/assets/js_um/main.js"></script>    
        
</body>
</html>
