@extends('layouts.base')
    @section('content')
<style>
.login {

    background: 
    /* top, transparent red, faked with gradient */ 
    linear-gradient(
      rgba(255,255,255,.7), 
      rgba(255,255,255,.7)
    ), url({{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }});    
    background-position: center;
    background-repeat: no-repeat;
    background-size: 1300px 1300px;  /*cover*/
}
.login .logo {
    text-align: left;
}
</style>
<div class="page-header typo-dark" style="background: url('https://ticmi.co.id/assets/images/banner/profile.jpg') top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Login</h3>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div>
        <div class="logo">
            <!-- nothing here -->
        </div>
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" role="form" action="{{ route('login') }}" method="post" id="form_login">
                {{ csrf_field() }}
                <center>
                    <img src="{{ asset('assets/theme/global/img/logo-ticmi.png') }}" class="logo-default" width="150" alt="logo">
                </center>
                <br>
                <br>
                @if(Session::has('alert-error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    {!! Session::get('alert-error') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                @endif
                @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {!! Session::get('message') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                <div class="form-group">
                    <label for="email" class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" value="{{ old('username') }}" required autofocus/>
                    @if ($errors->has('email'))
                        <span class="help-block" style="color:red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input id="password" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required/>
                    @if ($errors->has('password'))
                        <span class="help-block" style="color:red">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <a class="pull-right" href="{{ route('forgot_password') }}">(Lupa Password?)</a>
                </div>
                <div class="form-actions">
                    <span id="execute-loading" style="display: none;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                    <button type="submit" name="login" class="btn green uppercase">Login</button>
                    <div class="pull-right" style="padding-top: 0px!important; text-align: right;">
                        Belum punya akun ?<br>
                        <a class="green" href="{{ route('register.index') }}">Silahkan Daftar</a>
                    </div>
                </div>
                <br>
                <br>
                <div class="create-account">
                <!-- nothing here -->
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2017 © TICMI</div>

{!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#form_login'); !!}
{{-- <script type="text/javascript" src="{{ asset('js/js.cookie.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            
            var cookies = Cookies.get();
            for(var cookie in cookies) {
               Cookies.remove(cookie);
            }
            console.log(Cookies.get());
        });
    </script>    --}}
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_login').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_login').valid();
            if(is_form_valid == true){
                $('button[name="login"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'display': 'block'});
            }
        });
    </script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b3aef6c6d961556373d5962/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endsection
