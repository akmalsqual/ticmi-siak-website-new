@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')

<div class="page-header typo-dark" style="background: url('https://ticmi.co.id/assets/images/banner/profile.jpg') top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Login</h3>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div>
         <!-- Page Main -->
    <section class="relative bg-light typo-dark parallax-bg bg-cover overlay white md"  data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.5">
        <!-- Container -->
        <div class="container  parent-has-overlay">

            @if(Session::has('alert-error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ Session::get('alert-error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            @endif
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            @endif
            <div class="row shop-forms">
                <div class="col-sm-12">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <img width="211" height="40" alt="The Indonesia Capital Market Institute" class="img-responsive" src="{{ url('/assets/images/default/logo-ticmi.jpg') }}">
                                    </a>
                                </li><!-- Page Template Logo -->
                                <!-- Page Template Content -->
                                <li class="template-content">
                                    <div class="contact-form">
                                        @include('errors.list')
                                        @include('flash::message')
                                        <!-- Form Begins -->
                                            {{ Form::open(['url'=>route('login'), 'id' => 'form_login']) }}
                                            <!-- Field 1 -->
                                            <div class="input-text form-group text-left">
                                                <label>Email</label>
                                                {!! Form::text('email',null,['class'=>'input-name form-control','placeholder'=>'Email']) !!}
                                                @if ($errors->has('email'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <!-- Field 2 -->
                                            <div class="input-email form-group text-left">
                                                <a class="pull-right" href="{{ route('forgot_password') }}">(Lupa Password?)</a>
                                                <label>Password</label>
                                                {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                                @if ($errors->has('password'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <!-- Button -->
                                            {!! Form::submit('Login',['name' => 'login', 'class'=>'btn','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                            {{ Form::close() }}
                                            <p>
                                                <br>
                                                Belum mempunyai akun? Silahkan daftar <a href="{{ url('register') }}">disini</a>
                                            </p>
                                    </div>
                                </li><!-- Page Template Content -->
                            </ul>
                        </div><!-- Column -->
                    </div><!-- Row -->
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </section><!-- Page Default -->
    <!-- Page Main -->



{!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#form_login'); !!}
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_login').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_login').valid();
            if(is_form_valid == true){
                $('button[name="login"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'display': 'block'});
            }
        });
    </script>
@endsection
