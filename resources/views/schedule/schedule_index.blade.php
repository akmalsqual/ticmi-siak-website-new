@extends('layouts.app')
    @section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-calendar font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Schedule</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                     <div class="form-group">
                                        <div class="col-md-12"></div>
                                        <div class="col-md-12">
                                    <!-- CONTENT HERE -->
                                            {!! $calendar->calendar() !!}
                                            {!! $calendar->script() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                    <!-- </div> -->
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
<!--     <script type="text/javascript">
        $(document).ready(function(){
            var calendar = $('#calendar').fullCalendar({
                eventClick:  function(event, jsEvent, view) {
                        console.log(sukses);
                },
            });
        });
    </script> -->
 <div id="calendarModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title"></h4>
        </div>
        <div id="modalBody" class="modal-body">
             <a id="eventUrl" target="_blank"><button class="btn btn-primary">Lihat Detail Jadwal</button></a>
        </div>
       <!--  <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a id="eventUrl" target="_blank"><button class="btn btn-primary">Lihat Detail Jadwal</button></a>
        </div> -->
    </div>
</div>
</div>
    

    
@endsection