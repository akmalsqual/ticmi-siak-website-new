
@extends('layouts.app')
    @section('content')
    <!-- BEGIN CONTENT -->
    <style>
    /* remove td border */
    table.borderless td{
        border:0px!Important;
    }
    /* first td */
    .table-pmis td:first-child{
        color:#333333;
        vertical-align:middle;
        width:30%;
    }
    /* second td */
    .table-pmis td:nth-child(2){
        vertical-align:middle;
        width:5%;
    }
    /* third td */
    .table-pmis td:nth-child(3){
        width:60%;
    }
    .table-pmis hr{
        margin:1px 0px!Important;
    }

    /* form > subfom */
    .form-section{
        color:#333333;
        font-weight:bold;
    }

    .table-pmis{
        margin-bottom:-12px !Important;
    }
    tbody tr td{
        font-size: 15px !Important;
    }
    .table td:first-child {
        width:50%;
    }
    table.project_information table td{

    }
    table.table td{
        vertical-align: middle !Important;
    }

</style>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-calendar font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Jadwal Detail</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                            <div class="form-body">
                                <br/>
                                <div class="row">
                                    <!--@left-->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group /*has-error">
                                                    <table class="table table-pmis borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Nama Pelatihan<hr/></td>
                                                                <td>:</td>
                                                                <td>
                                                                   <h5>{{$getdata->nama_pelatihan}}</h5> 
                                                                   
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--@right-->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group /*has-error">
                                                    <table class="table table-pmis borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Pengajar<hr/></td>
                                                                <td>:</td>
                                                                <td>
                                                                    <h5>
                                                                        @if(!empty($getpengajar))
                                                                            {{$getpengajar->nama}}
                                                                        @else
                                                                            Belum Ada Pengajar
                                                                        @endif
                                                                    </h5>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row project-pmis">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group /*has-error">
                                                    <table class="table table-pmis borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Keterangan<hr/></td>
                                                                <td>:</td>
                                                                <td>
                                                                    <h5>
                                                                       {{$getdata->keterangan}}
                                                                    </h5>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br/>
                                <table class="table project_information">
                                    <tbody>
                                        <tr>
                                            <td colspan="6" bgcolor="#eee"><font color="#000"><b>Informasi Jadwal</b></font></td>
                                        </tr>
                                    </tbody>     
                                </table>
                                <div class="row project-pmis">
                                    <!--@left-->
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group /*has-error">
                                                        <table class="table table-pmis borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Tanggal Pelatihan<hr/></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                       <h5>{{date('d F Y', strtotime($getdata->tgl_pelatihan))}}</h5> 
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--@right-->
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group /*has-error">
                                                        <table class="table table-pmis borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Waktu<hr/></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <h5>{{$getdata->jam_pelatihan}}</h5>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                <div class="row project-pmis">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group /*has-error">
                                                    <table class="table table-pmis borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Ruang<hr/></td>
                                                                <td>:</td>
                                                                <td>
                                                                    <h5>{{ $ruang != null ? $ruang->nama : 'Belum di set' }}</h5>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group /*has-error">
                                                    <table class="table table-pmis borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Keterangan Ruangan <hr/></td>
                                                                <td>:</td>
                                                                <td>
                                                                    <h5>{{ $ruang != null ? $ruang->keterangan : 'Belum di set' }}</h5>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
   

   
@endsection