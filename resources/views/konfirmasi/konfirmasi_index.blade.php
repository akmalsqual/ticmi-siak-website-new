@extends('layouts.app')
    @section('content')

    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-basket font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Konfirmasi Pembayaran</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            <table class="table table-bordered table-hover table-siak-ajax" id="konfirmasi-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Batch</th>
                                        <th>Jenis</th>
                                        <th>Tanggal Daftar</th>
                                        <th>No Invoice</th>
                                        <th>No VA</th>
                                        <th>Total Tagihan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="clearfix"> </div>

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
<script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>

    <script>
    $(function() {
        var t = $('#konfirmasi-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ $url_ajax_datatable }}',
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama_batch', name: 'tagihan_tmp.nama_batch'},
                {data: 'jenis'},
                {data: 'tgl_daftar', name: 'tagihan_tmp.tgl_daftar'},
                {data: 'no_invoice', name: 'tagihan_tmp.no_invoice'},
                {data: 'account_no', name: 'tagihan_tmp.account_no'},
                {data: 'total_tagihan', name: 'tagihan_tmp.total_tagihan'},
                {data: 'action', orderable:false, searchable: false},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 10,
            // stateSave: true,
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });

    function confirm_destroy(form){
        var c = confirm("Hapus Data?");
        if(c){
            form.submit();
        }else{
            return false;
        }
    }
    </script>

@endsection