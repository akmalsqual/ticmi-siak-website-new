@extends('layouts.app')
    @section('content')

    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
    .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group {
        height: auto;
    }   
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-basket font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Konfirmasi Pembayaran</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            
                            {!! Form::open(['route' => 'konfirmasi.store', 'id' => 'form_konfirmasi_create', 'files'=> true]) !!}
                             <div class="form-body">
                                <!--@left-->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No Invoice</td>
                                                            <td>:</td>
                                                            <td>
                                                               {{ Form::text('no_invoice', $tagihan->no_invoice, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No VA</td>
                                                            <td>:</td>
                                                            <td>
                                                               {{ Form::text('account_no', $tagihan->account_no, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                           </td>
                                                       </tr>
                                                   </tbody>
                                               </table>
                                           </div>
                                           <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Total Tagihan</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Rp</span>
                                                                    {{ Form::text('total_tagihan', $tagihan->total_tagihan, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                               </div>
                                                           </td>
                                                       </tr>
                                                   </tbody>
                                               </table>
                                           </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nominal Transfer</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Rp</span>
                                                                    {{ Form::text('nominal_transfer', $tagihan->total_tagihan, ['class' => 'form-control']) }}
                                                                </div>
                                                                {!! $errors->first('nominal_transfer', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Transfer dari no rekening ?</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('transfer_dari', '', ['class' => 'form-control']) }}
                                                                {!! $errors->first('transfer_dari', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama pemegang rekening</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('nama_rekening', '', ['class' => 'form-control']) }}
                                                                {!! $errors->first('nama_rekening', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tanggal Transfer</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group date">
                                                                    {{ Form::text('tanggal_transfer', date('d-m-Y'), ['class' => 'form-control']) }}
                                                                    <div class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-th"></span>
                                                                    </div>
                                                                </div>
                                                                {!! $errors->first('tanggal_transfer', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Jam Transfer</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    {{ Form::text('jam_transfer', '', ['class' => 'form-control timepicker timepicker-24']) }}
                                                                            {!! $errors->first('jam_transfer', '<span class="help-block form-messages">:message</span>') !!}
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                                
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--/row-->
                                </div><!--/@left-->

                                <!--@right-->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama Peserta</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('nama_peserta', $tagihan->nama_peserta, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td>
                                                             {{ Form::text('email_peserta', $peserta->email, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                         </td>
                                                     </tr>
                                                 </tbody>
                                             </table>
                                         </div>
                                         <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No HP</td>
                                                            <td>:</td>
                                                            <td>
                                                             {{ Form::text('no_hp', $peserta->no_hp, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                         </td>
                                                     </tr>
                                                 </tbody>
                                             </table>
                                         </div>
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Batch</td>
                                                        <td>:</td>
                                                        <td>
                                                         {{ Form::text('batch', $tagihan->nama_batch, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                     </td>
                                                 </tr>
                                             </tbody>
                                         </table>
                                     </div>
                                     <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Bukti Transfer</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <img src="" alt=""> </div>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="bukti_transfer"> </span>
                                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                        <div class="clearfix margin-top-10">
                                                                            <span class="label label-danger">NOTE!</span> <span class="required" style="color:red;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                        </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div><!--/@right-->
                         </div>
                         </div>
                         <input name="tagihan_tmp_id" type="hidden" value="{{ $tagihan->tagihan_tmp_id }}">

                            <div class="clearfix"></div>

                            <div class="form-actions" style="margin-top: 20px">
                                <div class="col-md-6" style="margin-top: 20px">
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                        <button type="submit" name="simpan" class="btn green">Simpan</button>
                                                        <a href="{{ route('konfirmasi.index') }}" class="btn default">Kembali</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                          {!! Form::close() !!}

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
<script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Konfirmasi_pembayaranRequest', '#form_konfirmasi_create'); !!}

    <script type="text/javascript">
        $(document).ready(function(){
            $('.konfirmasi').addClass('active open');
            $('.timepicker').timepicker({
                maxHours: '24',
                showMeridian: false
            });
        /**
        * Action before submit form
        */
        $('#form_konfirmasi_create').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_konfirmasi_create').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

        <?= \Helper::date_formats('$("input[name=\'tanggal_transfer\']")', 'js') ?>
        <?= \Helper::number_formats('$("input[name=\'nominal_transfer\']")', 'js', 0) ?>
        <?= \Helper::number_formats('$("input[name=\'total_tagihan\']")', 'js', 0) ?>
    });
    </script>
@endsection