@extends('layouts_ticmi.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark  hidden-xs"
         style="background: url({{ url('/assets/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 35px;">{{ $workshop->workshop_name }}</h3>
                        <h6 class="sub-title">&nbsp;</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Section -->
    
	<section class="about-us py-5 " id="about-us">
    <div class="container mt-5">
	<div class="row">
		<div class="col-md-6  ">
            <div class="alert alert-info">
                <h5 class='text-success'>Sukses!</h5>
                <p class="text-justify">Terima kasih telah melakukan pendaftaran. Kami akan melakukan verifikasi terlebih dahulu apakah Anda memenuhi syarat untuk mengikuti kelas sertifikasi sesuai dengan syarat dan
                ketentuan yang berlaku. Proses verifikasi akan memakan waktu paling lambat 1 x 24 Jam (hari kerja), kami akan mengirimkan email mengenai hasil verifikasi tersebut kepada Anda.</p>
            </div>
		</div>
		<div class="col-md-6  ">
		    <img class="col-xs-12  " src="{{ url('/assets/ticmi/images/ticmi/laptop.png') }} "alt="">
		</div>
	</div>
</div>
</section>
@endsection