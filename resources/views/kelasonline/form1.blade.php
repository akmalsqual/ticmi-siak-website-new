@extends('layouts_ticmi.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"
             data-stellar-background-ratio="0.8"
             class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                    {!! Form::open(['url'=>route('ujian.store',['workshopslug'=>$workshop->slugs]),'class'=>'form-horizontal','autocomplete'=>'on','files'=>true]) !!}

                    <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">Form Pendaftaran Kelas Online {{ $tipe }}</a></h5>

                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <table class="table table-bordered table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td align="right" width="30%">Nama Peserta</td>
                                            <td>{{ auth()->user()->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Nama Seminar / Workshop</td>
                                            <td>{{ $workshop->workshop_name }}</td>
                                        </tr>

                                        <tr>
                                            <td align="right">URL</td>
                                            <td>https://elearning.ticmi.co.id</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row " id='form-reksadana'>

                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">Form Pendaftaran</div>
                                        </div>
                                        <div class="panel-body">
                                            @include('errors.list')
                                            @include('flash::message')
                                            <div class="panel-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-md-12">

                                                                <label class="">Pilih Jadwal Akses Pelatihan<em class="text-red">*</em></label>
                                                                {!! Form::select('jadwal_kelas_online_id',$arrJadwalKelas,null,['class'=>'form-control show-error-msg','placeholder'=>'Jadwal akses kelas online','required'=>'required']) !!}

                                                                <label class="">Nama Lengkap<em class="text-red">*</em></label>
                                                                {{ Form::text('nm_peserta',auth()->user()->nama,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}

                                                                {{ Form::hidden('workshop_id',$workshop->id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
                                                                {{ Form::hidden('t_program_id',$workshop->t_program_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
                                                                {{ Form::hidden('t_kelas_id',$workshop->t_kelas_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
                                                                {{ Form::hidden('kd_kelas',$workshop->t_kelas_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}

                                                                {{ Form::hidden('hp',auth()->user()->no_hp,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}

                                                                {{ Form::hidden('email',auth()->user()->email,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
                                                                {{ Form::hidden('password',auth()->user()->password,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
                                                                {{ Form::hidden('tanggal',time(),['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}


                                                                <label class="">Jenis Kelamin<em class="text-red">*</em></label>
                                                                {!! Form::select('kelamin',[''=>'Pilih Jenis Kelamin','Laki-Laki'=>'Laki-Laki','Perempuan'=>'Perempuan'],null,['class'=>'form-control', 'required'=>'required']) !!}

                                                                {{--<div class="radio-inline">--}}
                                                                    {{--<label style="text-transform: capitalize;background: none;"--}}
                                                                           {{--class="radio-inline">{!! Form::radio('kelamin','Laki-Laki',null,['class'=>'show-error-msg input-sm','required'=>'required']) !!}--}}
                                                                        {{--Laki-Laki</label>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="radio-inline">--}}
                                                                    {{--<label style="text-transform: capitalize;background: none;"--}}
                                                                           {{--class="radio-inline">{!! Form::radio('kelamin','Perempuan',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!}--}}
                                                                        {{--Perempuan</label>--}}
                                                                {{--</div>--}}

                                                                {!! Form::label('upload_file','Upload Foto') !!}
                                                                {!! Form::file('foto',['class'=>'form-control']) !!}
                                                                <small class="form-text text-danger text-muted pull-right"> Format foto yang diterima hanya : .jpg,.jpeg,.png</small>

                                                                <label class="">Tempat Lahir<em class="text-red">*</em></label>
                                                                {{ Form::text('tempat_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir','required'=>'required']) }}

                                                                <label class="">Tanggal Lahir<em class="text-red">*</em></label>
                                                                {{ Form::text('tgl_lahir',$peserta->tanggal_lahir,['class'=>'form-control datepicker','readonly'=>'readonly','placeholder'=>'Tanggal Lahir']) }}

                                                                <label for="nama" class="">Alamat <em class="text-red">*</em></label>
                                                                {{ Form::textarea('alamat',null,['class'=>'form-control','placeholder'=>'Alamat ','rows'=>5,'required'=>'required']) }}

                                                                <label class="">Pendidikan Terakhir<em class="text-red">*</em></label>
                                                                {!! Form::select('pendidikan',$pendidikan,null,['class'=>'form-control show-error-msg','placeholder'=>'Pendidikan','required'=>'required']) !!}

                                                                <label class="">Jurusan<em class="text-red">*</em></label>
                                                                {{ Form::text('jurusan',null,['class'=>'form-control','placeholder'=>'Jurusan','required'=>'required']) }}

                                                                <label class="">Universitas<em class="text-red">*</em></label>
                                                                {{ Form::text('universitas',null,['class'=>'form-control','placeholder'=>'Universitas','required'=>'required']) }}

                                                                <label class="">Pekerjaan<em class="text-red">*</em></label>
                                                                {{ Form::text('pekerjaan',$peserta->profesi,['class'=>'form-control','placeholder'=>'Pekerjaan'  ]) }}

                                                                <label class="">Jenis Perusahaan<em class="text-red">*</em></label>
                                                                {!! Form::select('cek_sro',$perusahaan,null,['class'=>'form-control cek_sro show-error-msg','placeholder'=>'Jenis Perusahaan','required'=>'required']) !!}

                                                                <div class="nonlainnya">
                                                                    <label class="">Nama Perusahaan<em class="text-red">*</em></label>
                                                                    {!! Form::select('perusahaan',$nm_perusahaan,null,['class'=>'form-control nm_perusahaan show-error-msg','placeholder'=>'Jenis Perusahaan']) !!}
                                                                </div>

                                                                <div class="lainnya" style="display: none;">
                                                                    <label class="">Nama Perusahaan<em class="text-red">*</em></label>
                                                                    {{ Form::text('perusahaan',null,['class'=>'form-control','placeholder'=>'Nama Perusahaan']) }}
                                                                </div>

                                                                <label class="">Kelembagaan<em class="text-red">*</em></label>
                                                                {!! Form::select('kelembagaan',$kelembagaan,null,['class'=>'form-control show-error-msg','placeholder'=>'Nama Kelembagaan','required'=>'required']) !!}

                                                                <label class="">Status Pendaftar<em class="text-red">*</em></label>
                                                                {!! Form::select('status_pendaftar',$status_pendaftar,null,['class'=>'form-control pendaftar show-error-msg','placeholder'=>'Pilih Status Pendaftar','required'=>'required']) !!}

                                                                <label class="label_filependukung">Upload</label>
                                                                {!! Form::file('file_pendukung',['class'=>'form-control']) !!}
                                                                <small class="form-text text-danger text-muted pull-right">
                                                                    format file yang di terima yaitu :
                                                                    .*pdf
                                                                </small>
                                                                <div class="clearfix"></div>
                                                                <br>

                                                                <button class="btn btn-success btn-lg pull-right" type="submit">Daftar Kelas</button>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    {{--<strong>Konfirmasi kehadiran sudah di tutup.</strong> <br>--}}
                                    Jika Anda mempunyai pertanyaan dan membutuhkan informasi lebih lanjut silahkan email
                                    ke :
                                    <a href="mailto:info@ticmi.co.id">info@ticmi.co.id</a> atau hubungi callcenter di :
                                    <strong>0800 100 9000 (bebas pulsa)</strong>
                                    <br>
                                </div>
                            </div>

                        </div>

                    </div><!-- Blog Detail Wrapper -->
                </div><!-- Blog Wrapper -->

                <!-- Blog Share Post -->
                <div class="share">
                    <h5>Share Post : </h5>
                    <ul class="social-icons">
                        <li class="fb-share-button"
                            data-href="{{ route('kelasonline.checkout',['workshopslug'=>$workshop->slugs]) }}"
                            data-layout="button" data-size="small" data-mobile-iframe="true">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('kelasonline.checkout',['workshopslug'=>$workshop->slugs]) }}&src=sdkpreparse"
                               class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                        </li>
                    </ul><!-- Blog Social Share -->
                </div><!-- Blog Share Post -->

            </div><!-- Column -->
        </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
            type="text/javascript"></script>
    <script>

        $('.pendaftar').on('change', function () {
            var id = $(this).val();

            if (id == 'Mahasiswa' || id == 'Alumni') {
                $('.label_filependukung').text('Upload Transkrip Nilai');

            } else if (id == 'Umum') {
                $('.label_filependukung').text('Upload Sertifikat');
            } else {
                $('.label_filependukung').text('Upload Surat Referensi');
            }

        });

        $('.cek_sro').on('change', function () {
            var cek = this.value;


            if (cek != 3) {
                $('.nonlainnya').show();
                $('.lainnya').hide();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    url: '/getperusahaan',
                    data: {
                        id: cek
                    },
                    success: function (msg) {

                        $('.nm_perusahaan')
                            .find('option')
                            .remove()
                            .end()
                            .append(msg)

                        ;


                    }
                });

            } else {
                $('.nonlainnya').hide();
                $('.lainnya').show();


            }
        })
    </script>

    <script>
        $(document).ready(function () {

            $("#sesuai_ktp").change(function () {
                if (this.checked) {
                    $('.cek_sesuai_ktp').hide();
                } else {
                    $('.cek_sesuai_ktp').show();
                }
            });
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
            $("div#form-reksadana").removeClass("hidden");
        });

    </script>
@endsection
