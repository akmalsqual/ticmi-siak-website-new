@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">Form Pendaftaran Kelas Online</a></h5>
                             
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                     <table class="table table-bordered table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td align="right" width="30%">Nama Peserta</td>
                                            <td>{{ auth()->user()->name }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Nama Seminar / Workshop</td>
                                            <td>{{ $workshop->workshop_name }}</td>
                                        </tr>
                                         
										<tr>
                                            <td align="right">URL</td>
                                            <td>elearning.ticmi.co.id</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							 
                            <div class="row  hidden" id='form-reksadana'>
                                
                               
                              
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="panel-title">Form Pendaftaran</div>
                                            </div>
                                            <div class="panel-body">
                                                @include('errors.list')
                                                @include('flash::message')
                                                {!! Form::open(['url'=>route('ujian.store',['workshopslug'=>$workshop->slugs]),'class'=>'form-horizontal','autocomplete'=>'off','files'=>true]) !!}
                                                <div class="panel-body">
								<div class="container">
							 
							 
								<div class="row"  >
									<div class="col-xs-12">
										<div class="col-md-12">
										 
											
											<div class="form-group">
												<label class="control-label">Nama Lengkap<em class="text-red">*</em></label>
												  {{ Form::text('nm_peserta',auth()->user()->name,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
												  {{ Form::hidden('workshop_id',$workshop->id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
												    {{ Form::hidden('t_program_id',$workshop->t_program_id,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
												    
													 {{ Form::hidden('hp',auth()->user()->no_hp,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
													
													{{ Form::hidden('email',auth()->user()->email,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
													{{ Form::hidden('password',auth()->user()->password,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly','required'=>'required']) }}
													
											</div>
											
											
											
											<div class="form-group">
												<label class="control-label">Jenis Kelamin<em class="text-red">*</em></label>
												<div class="radio-inline">
													<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('kelamin','Laki-Laki',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Laki-Laki</label>
												</div>	
												<div class="radio-inline">
													<label style="text-transform: capitalize;background: none;" class="radio-inline">{!! Form::radio('kelamin','Perempuan',null,['class'=>'  show-error-msg input-sm','required'=>'required']) !!} Perempuan</label>
											</div>
											</div>
											
											  
											 
												<div class="form-group">
												{!! Form::label('upload_file','Upload Foto') !!}
												{!! Form::file('foto') !!}
													<small class="form-text text-danger text-muted">*jpg,jpeg,png</small>
												</div>
											 
											
											<div class="form-group">
												<label class="control-label">Tempat Lahir<em class="text-red">*</em></label>
												  {{ Form::text('tempat_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir','required'=>'required']) }}
											</div>
											
											<div class="form-group">
												<label class="control-label">Tanggal Lahir {{ $peserta }}<em class="text-red">*</em></label>
												 {{ Form::text('tgl_lahir',$peserta->tanggal_lahir,['class'=>'form-control  ','readonly'=>'readonly','placeholder'=>'Tanggal Lahir']) }}
											</div>
											<div class="form-group">
												  <label for="nama" class="hidden-xs">Alamat <em class="text-red">*</em></label>
                                                        {{ Form::textarea('alamat',null,['class'=>'form-control','placeholder'=>'Alamat ','rows'=>5,'required'=>'required']) }}
											</div>
											 
											
											<div class="form-group">
												<label class="control-label">Pendidikan Terakhir<em class="text-red">*</em></label>
												{!! Form::select('pendidikan',$pendidikan,null,['class'=>'form-control input-sm show-error-msg','placeholder'=>'Pendidikan','required'=>'required']) !!}
											</div>
											<div class="form-group">
												<label class="control-label">Jurusan<em class="text-red">*</em></label>
												  {{ Form::text('jurusan',null,['class'=>'form-control','placeholder'=>'Jurusan','required'=>'required']) }}
											</div>
											<div class="form-group">
												<label class="control-label">Universitas<em class="text-red">*</em></label>
												  {{ Form::text('universitas',null,['class'=>'form-control','placeholder'=>'Universitas','required'=>'required']) }}
											</div> 
											
											<div class="form-group">
												<label class="control-label">Pekerjaan<em class="text-red">*</em></label>
												 {{ Form::text('pekerjaan',$peserta->profesi,['class'=>'form-control','placeholder'=>'Nama Lengkap','readonly'=>'readonly' ]) }}
											</div>
												 
											<div class="form-group ">
												<label class="control-label">Jenis Perusahaan<em class="text-red">*</em></label>
												{!! Form::select('cek_sro',$perusahaan,null,['class'=>'form-control cek_sro input-sm show-error-msg','placeholder'=>'Jenis Perusahaan','required'=>'required']) !!}
											</div>
											
											
											 <div class="form-group nonlainnya">
												<label class="control-label">Nama Perusahaan<em class="text-red">*</em></label>
												{!! Form::select('perusahaan',$nm_perusahaan,null,['class'=>'form-control nm_perusahaan  input-sm show-error-msg','placeholder'=>'Jenis Perusahaan']) !!}
											 </div>
											 
											 <div class="form-group lainnya" style="display:none">
												<label class="control-label">Nama Perusahaan<em class="text-red">*</em></label>
												  {{ Form::text('perusahaan',null,['class'=>'form-control','placeholder'=>'Nama Perusahaan']) }}
											 </div>
											
											<div class="form-group">
												<label class="control-label">Kelembagaan<em class="text-red">*</em></label>
												{!! Form::select('kelembagaan',$kelembagaan,null,['class'=>'form-control   input-sm show-error-msg','placeholder'=>'Nama Kelembagaan','required'=>'required']) !!}
											</div>
											
											<div class="form-group">
												<label class="control-label">Status Pendaftar<em class="text-red">*</em></label>
												{!! Form::select('status_pendaftar',$status_pendaftar,null,['class'=>'form-control pendaftar  input-sm show-error-msg','placeholder'=>'Pilih Status Pendaftar','required'=>'required']) !!}
											</div>
											
											
											
											
											
											<div class="form-group form_upload_file">
												<label class="control-label label_filependukung">Upload</label>
												{!! Form::file('file_pendukung') !!}
													<small class="form-text text-danger text-muted">format file 
													.*pdf</small>
											</div>
											
											 
												<button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
										</div>
									</div>
								</div>
								 
								  
								 
								</div>
								 
								 
								 
								 
							 
							</div>
							</div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    {{--<strong>Konfirmasi kehadiran sudah di tutup.</strong> <br>--}}
                                    Jika Anda mempunyai pertanyaan dan membutuhkan informasi lebih lanjut silahkan email ke :
                                    <a href="mailto:info@ticmi.co.id">info@ticmi.co.id</a> atau hubungi callcenter di : <strong>0800 100 9000 (bebas pulsa)</strong>
                                    <br>
                                </div>
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('wisuda') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('wisuda') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

$('.pendaftar').on('change', function() {
	 var id = $(this).val();
	 
 if (id == 'Mahasiswa' || id=='Alumni') {
    $('.label_filependukung').text('Upload Transkrip Nilai');
	
} else if (id == 'Umum') {
      $('.label_filependukung').text('Upload Sertifikat');
} else {
      $('.label_filependukung').text('Upload Surat Referensi');
}
	
	});
	
$('.cek_sro').on('change', function() {
		var cek = this.value;
		 
		
		if(cek!=3){
			$('.nonlainnya').show();
			$('.lainnya').hide();
		$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});
		 
		$.ajax({
            type: "POST",
            url: '/getperusahaan',
            data: {
				id: cek
				},
            success: function( msg ) {
               
				$('.nm_perusahaan')
					.find('option')
					.remove()
					.end()
					.append(msg)
					
				;


            }
        });
  
		}else{
			$('.nonlainnya').hide();
			$('.lainnya').show();
			
			
			
		}
})
</script>
@endsection
