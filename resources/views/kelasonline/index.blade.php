@extends('layouts_ticmi.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark " style="background: url('/assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper ">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 25px; margin-bottom: 10px;">{{ $pelatihan->workshop_name }}</h3>
                        <h6 class="sub-title" style="font-size: 18px;">{{ $pelatihan->sub_title }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/ticmi/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            {{--<h4>{{ $pelatihan->workshop_name }}</h4>--}}
                            <span class="cat bg-yellow">{{ strtoupper($pelatihan->program->nm_program) }} ONLINE</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>Rp{{ number_format($pelatihan->biaya) }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Program</span><h5>{{ $pelatihan->lama_belajar }} Hari</h5></li>
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
                {{--<div class="col-sm-12 text-center">--}}
                    {{--<br>--}}
                  	{{--@if($pelatihan->is_ujian != 1)--}}
                        {{--<a href="{{ route('kelasonline.checkout',['workshopslug'=>$pelatihan->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Kelas Online (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>--}}
						{{--@else--}}
						 {{--<a href="{{ route('kelasonline.form',['workshopslug'=>$pelatihan->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Kelas Online (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg btn-setuju"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>--}}
						{{----}}
						{{--@endif--}}
                {{--</div>--}}
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! $pelatihan->deskripsi !!}
                    </div>
                    @if($pelatihan->is_ujian == 1)
                        <div class="panel panel-primary">

                            <div class="panel-body">{!! Form::checkbox('setuju','1',null,['class'=>'setuju','t_program_id'=>$pelatihan->t_program_id]) !!}
                                <strong>
                                    Saya bersedia tunduk dan patuh terhadap persyaratan program yang saya pilih, dan mengisi setiap data dan informasi pribadi dengan sebenarnya.
                                    TICMI tidak menerbitkan sertifikasi apabila ditemukan adanya perbedaan data dan informasi, sesuai dengan hukum yang berlaku di Republik Indonesia.
                                </strong></div>
                        </div>




                        <p class="text-center">
                            <br>
                            <a href="{{ route('kelasonline.form',['workshopslug'=>$pelatihan->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Kelas Online (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg btn-setuju  disabled"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>

                        </p>

                    @endif


                    <p class="text-center">
                        <br>

                        @if($pelatihan->is_ujian != 1)
                            <a href="{{ route('kelasonline.checkout',['workshopslug'=>$pelatihan->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Kelas Online (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>

                        @endif

                    </p>

                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h5>Program Kami lainnya : </h5>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="3" data-desktopsmall="3"  data-desktop="4" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        <img alt="Event" class="img-responsive" src="{{ $batch_path.$value->logo }}" width="600" height="220">
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar-batch/'.$value->batch_id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

 $(document).on("change", "input[name='setuju']", function () {
   
    if (this.checked) {
		 
		$('.btn-setuju').removeClass('disabled');
	}else{
		$('.btn-setuju').addClass('disabled');
		
	}
});


</script>