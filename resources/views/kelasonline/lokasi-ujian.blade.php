@extends('layout.modal.base')

@section('contentWrapper')
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Lokasi Ujian</th>
                    <th>Nama Tempat</th>
                    <th>Alamat Ujian</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Medan</td>
                    <td>Kantor Perwakilan BEI Medan</td>
                    <td>Jl. Asia No.182, Medan – Sumatera Utara</td>
                </tr>
                <tr>
                    <td>Bandung</td>
                    <td>Kantor Perwakilan BEI Bandung</td>
                    <td>Jl. P.H.H. Mustofa No. 33, Kota Bandung</td>
                </tr>
                <tr>
                    <td>Semarang</td>
                    <td>Kantor Perwakilan BEI Semarang</td>
                    <td>Jl. M.H. Thamrin No.152, Semarang 50134 - Jawa Tengah</td>
                </tr>
                <tr>
                    <td>Surabaya</td>
                    <td>Kantor Perwakilan BEI Surabaya</td>
                    <td>Gedung Bursa Efek Indonesia, Lantai 6, Jl. Taman Ade Irma Suryani (TAIS) Nasution No 21, Surabaya</td>
                </tr>
                <tr>
                    <td>Banda Aceh</td>
                    <td>Kantor Perwakilan BEI Banda Aceh</td>
                    <td>Jl. T. Imeum Lueng Bata No.84, Banda Aceh 23247 – Provinsi Aceh</td>
                </tr>
                <tr>
                    <td>Riau</td>
                    <td>Kantor Perwakilan BEI Riau</td>
                    <td> Jl. Jend. Sudirman No.73, Pekanbaru - Riau</td>
                </tr>
                <tr>
                    <td>Padang</td>
                    <td>Kantor Perwakilan BEI Padang</td>
                    <td>Jl. Pondok No.90A, Padang 25211 - Sumatera Barat</td>
                </tr>
                <tr>
                    <td>Batam</td>
                    <td>Kantor Perwakilan BEI Batam</td>
                    <td>Komplek Mahkota Raya Blok A. No.11, Jl. Raja H. Fisabilillah - Batam Center, Batam, 29456 - Prov. Kepulauan Riau</td>
                </tr>
                <tr>
                    <td>Lampung</td>
                    <td>Kantor Perwakilan BEI Lampung</td>
                    <td>Jl. Jend. Sudirman No.5D, Bandar Lampung 35118</td>
                </tr>
                <tr>
                    <td>Yogyakarta</td>
                    <td>Kantor Perwakilan BEI Yogyakarta</td>
                    <td>Jl. P. Mangkubumi No.111, Yogyakarta 55232</td>
                </tr>
                <tr>
                    <td>Palembang</td>
                    <td>Kantor Perwakilan BEI Palembang</td>
                    <td>Jl. Angkatan 45, No. 13-14, RT 0014/RW 004, Kel. Demang Lebar Daun Kec. Ilir Barat I, Kota Palembang, 30137</td>
                </tr>
                <tr>
                    <td>Jambi</td>
                    <td>Kantor Perwakilan BEI Jambi</td>
                    <td>Jl. Kolonel Abun Jani No.11A dan 11B, Kel. Selamat Kec. Telanaipura, Kota Jambi</td>
                </tr>
                <tr>
                    <td>Denpasar</td>
                    <td>Kantor Perwakilan BEI Denpasar</td>
                    <td>Jl. Cok Agung Tresna No. 163, Renon, Denpasar, Bali</td>
                </tr>
                <tr>
                    <td>Pontianak</td>
                    <td>Kantor Perwakilan BEI Pontianak</td>
                    <td>Komplek Perkantoran Central Perdana Blok A2-A3, Jl. Perdana – Kota Pontianak, 78124</td>
                </tr>
                <tr>
                    <td>Banjarmasin</td>
                    <td>Kantor Perwakilan BEI Banjarmasin</td>
                    <td>Jl. Ahmad Yani KM 1,5 No.103, Banjarmasin</td>
                </tr>
                <tr>
                    <td>Balikpapan</td>
                    <td>Kantor Perwakilan BEI Balikpapan</td>
                    <td>Jl. Jend. Sudirman No.33B, Balikpapan – Kalimantan Timur</td>
                </tr>
                <tr>
                    <td>Makassar</td>
                    <td>Kantor Perwakilan BEI Makassar</td>
                    <td>Jl. Dr. Sam Ratulangi No. 124, Makassar - Indonesia</td>
                </tr>
                <tr>
                    <td>Manado</td>
                    <td>Kantor Perwakilan BEI Manado</td>
                    <td>Ruko Mega Style Blok 1C No.9, Kompleks Mega Mas Jl. Piere Tendean - Boulevard, Manado 95111</td>
                </tr>
                <tr>
                    <td>Jayapura</td>
                    <td>Kantor Perwakilan BEI Jayapura</td>
                    <td>Komplek Perkantoran Ardipura Jl. Ardipura Polimak No 3, Kelurahan Ardipura Jayapura</td>
                </tr>
                <tr>
                    <td>Kendari</td>
                    <td>Kantor Perwakilan BEI Kendari</td>
                    <td>Jl. Syekh Yusuf No.20, Kota Kendari</td>
                </tr>
                <tr>
                    <td>Pangkalpinang</td>
                    <td>Kantor Perwakilan BEI Pangkalpinang</td>
                    <td>Ruko NIAGA CENTER Blok G Lt.2, Jl. Jend. Sudirman, Kel. Pasar Padi, Kec. Girimaya, Kota Pangkalpinang</td>
                </tr>
                <tr>
                    <td>Palangka Raya</td>
                    <td>Kantor Perwakilan BEI Palangkaraya</td>
                    <td>Jl. RTA Milono Km. 1.5 Ruko No. 1, , RT.004 RW.11, Kel. Langkai, Kec. Pahandut, Kota Palangka Raya</td>
                </tr>
                <tr>
                    <td>Bengkulu</td>
                    <td>Kantor Perwakilan BEI Bengkulu</td>
                    <td>Jl. Jend. Sudirman No. 219B, RT. 3 RW 1, Kel.Pintu Batu, Kec. Teluk Segara, Kota Bengkulu 38115</td>
                </tr>
                <tr>
                    <td>Manokwari</td>
                    <td>Kantor Perwakilan BEI Manokwari</td>
                    <td>Jl. Trikora Wosi No. 86, Kel. Wosi, Kec. Manokwari Barat, Kab. Manokwari, Prov. Papua Barat.</td>
                </tr>
                <tr>
                    <td>Ambon</td>
                    <td>Kantor Perwakilan BEI Ambon</td>
                    <td>Jl. Philip Latumahina No. 16, Kel. Honipopu, Kec. Sirimau. RT/RW 001/003, Kota Ambon.</td>
                </tr>
                <tr>
                    <td>Mataram</td>
                    <td>Kantor Perwakilan BEI Mataram</td>
                    <td>Jl. Pejanggik no. 47 C Mataram</td>
                </tr>
                <tr>
                    <td>Solo</td>
                    <td>Kantor Perwakilan BEI Solo</td>
                    <td>Gedung Graha Prioritas (Lantai 1 dan 2), Jl. Slamet Riyadi No. 302-304, Surakarta 57141</td>
                </tr>
                <tr>
                    <td>Serang</td>
                    <td>Kantor Perwakilan BEI Serang</td>
                    <td>Jl. Veteran No 39-40. Serang - Banten</td>
                </tr>
                <tr>
                    <td>Jakarta</td>
                    <td>TICMI </td>
                    <td>Indonesian Stock Exchange Building, Tower II, 1st Floor Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190
</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection