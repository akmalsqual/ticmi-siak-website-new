@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark  hidden-xs"
         style="background: asset('assets/ticmi/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 35px;">{{ $workshop->workshop_name }}</h3>
                        <h6 class="sub-title">&nbsp;</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Section -->
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"
             data-stellar-background-ratio="0.5" class="parallax-bg overlay sm">
        <div class="container parent-has-overlay">
            {{ Form::hidden('workshop','Corporate Secretary') }}
            <div class="row course-single content-box  bg-white shadow">
                <div class="col-md-8 col-md-offset-2">
                    @include('errors.list')
                    @include('flash::message')
                    <br>
                    @if($pesertaWorkshop->is_confirm == 0 && $pesertaWorkshop->is_payment_approve == 0)
                        @php
                            $total_bayar = $pesertaWorkshop->total_bayar;
                            $subtotal    = $pesertaWorkshop->biaya;
                            $diskontotal    = $pesertaWorkshop->diskon;
                        @endphp
                        @if(!empty($workshop->jml_peserta_diskon) && empty($workshop->jml_group_referral))
                            @if($pesertaWorkshop->diskon > 0)
                                <div class="alert alert-info">
                                    <strong>Selamat!!!</strong> Anda mendapatkan diskon sebesar
                                    <strong>Rp{{ number_format($pesertaWorkshop->diskon,0,',','.') }}</strong> untuk
                                    promo <em>Early Bird</em> {{ $workshop->jml_peserta_diskon }} pendaftar pertama.
                                </div>
                            @endif
                        @elseif(empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral))
                            @php
                                if ($referral) {
                                    foreach ($referral as $refValue) {
                                        $total_bayar += $refValue->total_bayar;
                                        $subtotal    += $refValue->biaya;
                                        $diskontotal    += $refValue->diskon;
                                    }
                                }
                            @endphp
                        @endif

                        @if($pesertaWorkshop->urutan <= $workshop->jml_peserta_diskon)
                            @if($workshop->pesan_email_khusus)
                                <div class="alert alert-info">
                                    {!! $workshop->pesan_email_khusus !!}
                                </div>
                            @endif
                        @endif
                        <p class="text-center">
                            Terima kasih telah melakukan pendaftaran untuk
                            <strong><em>{{ $workshop->workshop_name }}</em></strong>
                            <br>
                            Data Anda sudah kami terima dan tercatat dengan <strong>No Tagihan :
                                #{{ $pesertaWorkshop->invoice_no }}</strong> dan untuk proses selanjutnya silahkan Anda
                            transfer biaya investasi sebesar :
                            <br><br>
                            <span style="border: 1px solid #c4c4c4;background: #e2e2e2;font-size:45px;width: 100%;display:block;"
                                  class="text-center">
                                    Rp{{ number_format($total_bayar,0,',','.') }}
                                </span>
                        </p>
                        <br><br>

                        <h5>Berikut Detail Tagihan Anda : </h5>
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Nama Peserta</th>
                                <th>Email</th>
                                <th class="text-center">Biaya</th>
                                <th class="text-center">Diskon</th>
                                <th class="text-center">Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $pesertaWorkshop->nama }}</td>
                                <td>{{ $pesertaWorkshop->email }}</td>
                                <td class="text-right">{{ number_format($pesertaWorkshop->biaya,0,',','.')  }}</td>
                                <td class="text-right">{{ number_format($pesertaWorkshop->diskon,0,',','.')  }}</td>
                                <td class="text-right">{{ number_format($pesertaWorkshop->total_bayar,0,',','.')  }}</td>
                            </tr>
                            @if($referral)
                                @foreach($referral as $refValue)
                                    <tr>
                                        <td>{{ $refValue->nama }}</td>
                                        <td>{{ $refValue->email }}</td>
                                        <td class="text-right">{{ number_format($refValue->biaya,0,',','.')  }}</td>
                                        <td class="text-right">{{ number_format($refValue->diskon,0,',','.')  }}</td>
                                        <td class="text-right">{{ number_format($refValue->total_bayar,0,',','.')  }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-right" colspan="4"><strong>TOTAL</strong></td>
                                    <td class="text-right"><strong>Rp{{ number_format($total_bayar,0,',','.') }}</strong></td>
                                </tr>
                            </tfoot>
                        </table>

                        <p class="text-center">
                            Pembayaran dapat dilakukan ke nomor rekening Virtual Account Bank BCA <br>

                            <img src="{{ url('assets/images/shop/logo-bca.gif') }}" alt="BCA"> <br>
                            <span class="bank__name">TICMI</span> <br>
                            <span class="virtual__account">
                                               {{ $pesertaWorkshop->virtual_account }}
                                                        </span> <br>
                            <span class="account__owner">PEND SERT TICMI</span>
                        </p>
                        <br>
                        <p class="text-center">
                            Jika sudah melakukan pembayaran, mohon untuk melakukan <strong>Konfirmasi
                                Pembayaran</strong> Anda agar pendaftaran Anda bisa diproses.
                        </p>
                        <p class="text-center">
                            <a href="{{ route('pelatihan.konfirmasi',['workshopslug'=>$pesertaWorkshop->workshop->slugs,'pesertaworkshopinvoice'=>$pesertaWorkshop->invoice_no]) }}"
                               class="btn btn-lg">Konfirmasi Pembayaran</a>
                        </p>
                        <p class="text-center">
                            Kami juga sudah mengirimkan notifikasi pendaftaran ke Email Anda. Silahkan cek email Anda
                            (cek juga spam folder jika tidak menemukan email Kami di Inbox Anda)
                        </p>
                    @elseif($pesertaWorkshop->is_confirm == 1 && $pesertaWorkshop->is_payment_approve == 0)
                        <p>Terima kasih sudah melakukan konfirmasi atas pendaftaran Anda. Kami akan segera verifikasi
                            pembayaran Anda.</p>
                        <p>Kami juga sudah mengirimkan notifikasi konfirmasi ke Email Anda. Silahkan cek email Anda (cek
                            juga spam folder jika tidak menemukan email Kami di Inbox Anda)</p>
                    @elseif($pesertaWorkshop->is_confirm == 1 && $pesertaWorkshop->is_payment_approve == 1)
                        Pendaftaran Anda sudah berhasil, sampai jumpa di {{ $workshop->workshop_name }}
                    @endif
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

@endsection