@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark " style="background: asset('assets/ticmi/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper ">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 25px; margin-bottom: 10px;">{{ $pelatihan->workshop_name }}</h3>
                        <h6 class="sub-title" style="font-size: 18px;">{{ $pelatihan->sub_title }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            {{--<h4>{{ $pelatihan->workshop_name }}</h4>--}}
                            <span class="cat bg-yellow">{{ strtoupper($pelatihan->program->nama) }}</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>Rp{{ number_format($pelatihan->biaya) }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Waktu Pelaksanaan</span><h5>{{ GeneralHelper::waktuPelaksanaan($pelatihan->tgl_mulai, $pelatihan->tgl_selesai) }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>{{ $pelatihan->lokasi }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Ruangan</span><h5>{{ $pelatihan->ruangan }}</h5></li>
                                @if($pelatihan->minimal_peserta)
                                    <li><i class="fa fa-users"></i><span>Minimal Peserta</span><h5>{{ $pelatihan->minimal_peserta }} Peserta</h5></li>
                                @endif
                                <li><i class="fa fa-clock-o"></i><span>Waktu</span><h5>{{ $pelatihan->waktu }}</h5></li>
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
                <div class="col-sm-12 text-center">
                    <br>
                    @if($pelatihan->t_program_id==39)
                     <a href="{{ route('pelatihan.form',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Workshop (form)', '{{ $pelatihan->t_program_id }}', '{{ url()->current() }}' )"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>

                    @else
                    <a href="{{ route('pelatihan.checkout',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Workshop (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                    @endif
                </div>
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! str_replace("\\", "",$pelatihan->deskripsi) !!}
                    </div>


                </div><!-- Column -->
                <div class="col-sm-12 text-center">
                    <br>
                    @if($pelatihan->t_program_id==39)
                     <a href="{{ route('pelatihan.form',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Workshop (form)', '{{ $pelatihan->t_program_id }}', '{{ url()->current() }}' )"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>

                    @else
                    <a href="{{ route('pelatihan.checkout',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Workshop (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                    @endif
                </div>
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h4>Program Kami lainnya : </h4>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="2" data-desktopsmall="2"  data-desktop="3" data-autoplay="true" data-delay="3000" data-navigation="true">


                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $batch)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        <img alt="Event" class="img-responsive" src="{{ $batch_path.$batch->logo }}" width="600" height="220">
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar-batch/'.$batch->batch_id.'/'.$batch->nama) }}">+</a><span>{{ $batch->nama }}</span>
                                        <h5 class="title">{{ $batch->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                        <div class="col-sm-12">
                            <div class="alert alert-warning" role="alert"><h4>Maaf, belum ada jadwal tersedia.</h4></div>
                        </div>
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection