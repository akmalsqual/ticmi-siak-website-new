@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <h5>Verifikasi Email</h5>
                                    </a>
                                </li><!-- Page Template Logo -->
                            </ul>
                            @if(Session::has('verified'))
                                <div class="alert {{ Session::get('alert-class') }}">{{ Session::get('verified') }}</div>
                            @else
                                @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</div>
                                @endif
                                    {{ Form::open(['url'=>route('register.resend-email-verification.proses')]) }}
                                    <div class="input-text form-group text-left">
                                        {!! Form::email('email',null,['class'=>'input-name form-control','placeholder'=>'Masukkan Email Anda', 'required']) !!}
                                        <small class="form-note">Cek kembali Email Anda, pastikan email yang Anda masukkan benar</small>
                                    </div>
                                    {!! Form::submit('Verifikasi Email',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                    {{ Form::close() }}
                            @endif
                            <br>
                            <br>
                            <ul class="text-center">
                                <li>
                                    Kembali ke halaman <a href="{{ route('login') }}">Login</a> atau <a href="{{ route('register.index') }}">Daftar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->


@endsection