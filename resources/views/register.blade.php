@extends('layouts.base')
    @section('content')
<style>
.login {

    background: 
    /* top, transparent red, faked with gradient */ 
    linear-gradient(
      rgba(255,255,255,.7), 
      rgba(255,255,255,.7)
    ), url({{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }});    
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;  /*cover*/
}
.login .logo {
    text-align: left;
}
.login .content-register{
	background-color: #fff;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    -ms-border-radius: 7px;
    -o-border-radius: 7px;
    border-radius: 7px;
    max-width: 750px;
    margin: 40px auto 10px;
    padding: 10px 30px 30px;
    overflow: hidden;
    position: relative;
    margin-top: -20px;
}
.select2-container {
  width:100%;   
}

.select2-selection .select2-selection--single{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
.select2-selection{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
.select2-selection--single{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
.input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group {
    height: auto;
}
</style>

<div class="page-header typo-dark" style="background: url('https://ticmi.co.id/assets/images/banner/profile.jpg') top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran user baru</h3>
                        <h6 class="sub-title">Silahkan daftar disini</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div>

        <div class="logo">
            <!-- nothing here -->
        </div>
        <!-- BEGIN LOGIN -->
        <div class="content-register">
            <!-- BEGIN LOGIN FORM -->
            <div class="login-form">
                {{ csrf_field() }}
                <center>
                    <img src="{{ asset('assets/theme/global/img/logo-ticmi.png') }}" class="logo-default" width="150" alt="logo">
                </center>
                <br>
                <br>
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class') }}" style="font-size: 13px !important;">{{ Session::get('message') }}</p>
                @endif
				
				<!-- Tab Register -->
				<div class="portlet light bordered">
				    <div class="portlet-title tabbable-line">
				        <div class="caption">
				            <i class="fa fa-user-plus"></i>
				            <span class="caption-subject font-dark bold uppercase"> &nbsp;Pendaftaran User Baru</span>
				        </div>
				        <ul class="nav nav-tabs">
				            <li class="active">
				                <a href="#sid" data-toggle="tab"> Pemilik SID </a>
				            </li>
				            <li class="">
				                <a href="#non_sid" data-toggle="tab"> Non Pemilik SID </a>
				            </li>
				        </ul>
				    </div>
				    <div class="portlet-body">
				        <div class="tab-content">

				        	<!--
				            =================================================================================================
							====================== Form SID
				            ================================================================================================= 
				            -->
				            <div class="tab-pane active" id="sid">
				                {!! Form::open(array('route' => 'register.store','method'=>'POST', 'files'=>true)) !!}
				                	<div class="col-md-12 form-group">
				                		<?php $class_name = ErrorForm::errorColumn($errors->has('name')); ?>
				                		{!! Form::label('name', 'Nama Lengkap', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('name', null, ['class'=>$class_name, 'placeholder'=>'Masukkan Nama Anda']) !!}
				                		<small class="form-note">Masukkan nama sesuai dengan Kartu Identitas Anda 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport"><span class="fa fa-question-circle"></span></a> 
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('name'), $errors->first('name')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_no_hp = ErrorForm::errorColumn($errors->has('no_hp')); ?>
				                		{!! Form::label('no_hp', 'Nomor Handphone', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('no_hp', null, ['class'=>$class_no_hp, 'placeholder'=>'Masukkan Nomor Handphone Anda']) !!}
				                		<small class="form-note">Masukkan no HP Aktif Anda </small>
				                		<?= ErrorForm::errorLabel($errors->has('no_hp'), $errors->first('no_hp')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_tanggal_lahir = ErrorForm::errorColumn($errors->has('tanggal_lahir')); ?>
				                		{!! Form::label('tanggal_lahir', 'Tanggal Lahir', ['class'=>'color-maroon']) !!}
				                		<div class="input-group date datepicker"  >
				                		    {!! Form::text('tanggal_lahir', null, ['class'=>'datepika  '.$class_tanggal_lahir,'autocomplete'=>'off','readonly'=>'readonly', 'placeholder' => 'Masukkan Tanggal Lahir Anda']) !!}
				                		    <span class="input-group-btn">
				                		        <button class="btn default" type="button">
				                		            <i class="fa fa-calendar"></i>
				                		        </button>
				                		    </span>
				                		</div>
				                		<?= ErrorForm::errorLabel($errors->has('tanggal_lahir'), $errors->first('tanggal_lahir')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_provinsi = ErrorForm::errorColumn($errors->has('provinsi')) . ' select2'; ?>
				                		{!! Form::label('provinsi', 'Domisili (Provinsi)', ['class'=>'color-maroon']) !!}
				                		{!! Form::select('provinsi',$provinsi,null,['class'=>$class_provinsi]) !!}
				                		<?= ErrorForm::errorLabel($errors->has('provinsi'), $errors->first('provinsi')); ?>
				                		<small class="form-note">Masukkan Provinsi domisili Anda
				                		</small>
				                		<span id="loading-provinsi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_kabupaten = ErrorForm::errorColumn($errors->has('kabupaten')) . ' select2'; ?>
				                		{!! Form::label('kabupaten', 'Domisili (Kabupaten/Kota)', ['class'=>'color-maroon']) !!}
				                		{!! Form::select('kabupaten',[], null,['class'=>$class_kabupaten]) !!}
				                		<small class="form-note">Masukkan Kabupaten domisili Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('kabupaten'), $errors->first('kabupaten')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_instansi = ErrorForm::errorColumn($errors->has('instansi')); ?>
				                		{!! Form::label('instansi', 'Instansi/Universitas/Kantor', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('instansi', null, ['class'=>$class_instansi, 'placeholder'=>'Masukkan Instansi/Universitas/Kantor']) !!}
				                		<small class="form-note">Masukkan Nama Instansi/Universitas/Kantor Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('instansi'), $errors->first('instansi')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_profesi = ErrorForm::errorColumn($errors->has('profesi')) . ' select2'; ?>
				                		{!! Form::label('profesi', 'Profesi Anda', ['class'=>'color-maroon']) !!}
				                		{!! Form::select('profesi',$profesi,null,['class'=>$class_profesi]) !!}
				                		<?= ErrorForm::errorLabel($errors->has('profesi'), $errors->first('profesi')); ?>
				                	</div>

				                	<div class="col-md-12 form-group hidden" id="hidden_profesi">
				                		<?php $class_profesi_lainnya = ErrorForm::errorColumn($errors->has('profesi_lainnya')); ?>
				                		{!! Form::label('profesi_lainnya', 'Profesi Lainnya', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('profesi_lainnya', null, ['class'=>$class_profesi_lainnya, 'placeholder'=>'Isikan Profesi Anda']) !!}
				                		<small class="form-note">Masukkan Profesi Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('profesi_lainnya'), $errors->first('profesi_lainnya')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_email = ErrorForm::errorColumn($errors->has('email')); ?>
				                		{!! Form::label(null, 'Email', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('email', null, ['class'=>$class_email, 'placeholder'=>'Masukkan Email Anda']) !!}
				                		<small class="form-note">Pastikan e-mail yang Anda tuliskan valid 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> 
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('email'), $errors->first('email')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_email_repeat = ErrorForm::errorColumn($errors->has('email_repeat')); ?>
				                		{!! Form::label(null, 'Ulangi Email', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('email_repeat', null, ['class'=>$class_email_repeat, 'placeholder'=>'Masukkan Kembali Email Anda']) !!}
				                		<small class="form-note margin-bottom-5" style="padding-bottom: 10px;">Pastikan e-mail yang Anda tuliskan valid 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a>
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('email_repeat'), $errors->first('email_repeat')); ?>
				                	</div>

		                	        <div class="col-md-12">
		                	            <div class="row">
		                	                <div class="col-xs-12 col-sm-6">
												<div class="form-group">
													<?php $class_password = ErrorForm::errorColumn($errors->has('password')); ?>
													{!! Form::label('password', 'Password', ['class'=>'color-maroon']) !!}
													{{ Form::password('password', ['class'=>$class_password, 'placeholder'=>'Masukkan Password Anda']) }}
													<?= ErrorForm::errorLabel($errors->has('password'), $errors->first('password')); ?>
												</div>	
		                	                </div>
		                	                <div class="col-xs-12 col-sm-6">
		                	                    <div class="form-group">
		                	                    	<?php $class_password_repeat = ErrorForm::errorColumn($errors->has('password_repeat')); ?>
		                	                    	{!! Form::label('password_repeat', 'Ulangi Password', ['class'=>'color-maroon']) !!}
		                	                    	{!! Form::password('password_repeat', ['class'=>$class_password_repeat, 'placeholder'=>'Masukkan Kembali Password Anda']) !!}
		                	                    	<?= ErrorForm::errorLabel($errors->has('password_repeat'), $errors->first('password_repeat')); ?>
		                	                    </div>
		                	                </div>
		                	            </div>
		                	        </div>
									
		                	        <span class="border-top-14"></span>
									
									<p class="col-md-12" style="padding-top: 30px; padding-bottom: -10px;">Jika Anda memiliki SID, maka layanan data TICMI dapat diperoleh secara gratis. Apakah Anda sudah memiliki SID ? Silahkan masukkan SID Anda dan Upload Kartu Akses Anda.</p>

									<div class="col-md-12 form-group">
										<?php $class_no_sid = ErrorForm::errorColumn($errors->has('no_sid')); ?>
										{!! Form::label('no_sid', 'Nomor SID', ['class'=>'color-maroon']) !!}
										{!! Form::text('no_sid', null, ['class'=>$class_no_sid, 'placeholder'=>'Contoh : IDD2601EB4556']) !!}
										<?= ErrorForm::errorLabel($errors->has('no_sid'), $errors->first('no_sid')); ?>
									</div>

									<div class="col-md-12" style="margin-bottom: 20px;">
		                	            <div class="row">
		                	                <div class="col-xs-12 col-sm-12">
		                	                	<label class="color-maroon">Kartu Akses</label>
		                	                </div>
		                	                <div class="col-xs-12 col-sm-4">
												<div class="form-group">
													<img src="{{ asset('assets/ticmi/images/ticmi/SID.jpg') }}" class="img-responsive">
												</div>
		                	                </div>
		                	                <div class="col-xs-12 col-sm-8">
		                	                    <div class="form-group">
	                    							<?php $class_file_sid = ErrorForm::errorColumn($errors->has('file_sid')); ?>
		                	                    	{!! Form::file('file_sid', ['class'=>$class_file_sid]) !!}
		                	                    	<small class="form-note">Format : Jpeg, Jpg, Png</small>
		                	                    	<?= ErrorForm::errorLabel($errors->has('file_sid'), $errors->first('file_sid')); ?>
		                	                    </div>
		                	                </div>
		                	            </div>
		                	            <small class="form-note text-left" style="text-align: left;">Masukkan scan bagian depan Kartu Akses Anda 
		                	            	<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-html="true" data-trigger="focus" data-content="Kartu Akses merupakan kartu yang diperoleh jika Anda menjadi Investor yang dikeluarkan oleh KSEI. <a href='http://akses.ksei.co.id/info/faq' target='_blank'>Info lebih lanjut</a> "><span class="fa fa-question-circle"></span></a> 
		                	            </small>
		                	        </div>

		                	        <div class="col-md-12 form-group">
                                        <div class="row">
                                        	<div class="col-xs-12 col-sm-12">
	                                            <input checked="checked" name="newsletter" type="checkbox" value="1"> Saya ingin menerima newsletter melalui e-mail
	                                            <br>
	                                            <small>Dengan mendaftar Anda berarti sudah menyetujui <a href="#">Kebijakan privasi TICMI</a></small>
                                        	</div>
                                        </div>
                                    </div>

				                	<div class="form-group" style="padding-left: 15px;">
                                      	{!! Form::submit('Daftar', ['class'=>'btn btn-lg btn-block btn-form-sid']) !!}		
				                		<!-- <a class="btn btn-danger btn-block" href="<?= route('login'); ?>">Back</a> -->
				                	</div>

				                {!! Form::close() !!}
				            </div>

				            <!--
				            =================================================================================================
							====================== Form Non SID
				            ================================================================================================= 
				            -->
				            <div class="tab-pane" id="non_sid">
        		                {!! Form::open(array('route' => 'register.storeNonSID','method'=>'POST', 'files'=>true)) !!}
        		                	<div class="col-md-12 form-group">
        								<?php $class_name_non = ErrorForm::errorColumn($errors->has('name_non')); ?>
        		                		{!! Form::label(null, 'Nama Lengkap', ['class'=>'color-maroon']) !!}
        		                		{!! Form::text('name_non', null, ['class'=>$class_name_non, 'placeholder'=>'Masukkan Nama Anda']) !!}

        		                		<small class="form-note">Masukkan nama sesuai dengan Kartu Identitas Anda 
        		                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport"><span class="fa fa-question-circle"></span></a> 
        		                		</small>
        		                		<?= ErrorForm::errorLabel($errors->has('name_non'), $errors->first('name_non')); ?>
        		                	</div>

        		                	<div class="col-md-12 form-group">
        		                		<?php $class_no_hp_non = ErrorForm::errorColumn($errors->has('no_hp_non')); ?>
        		                		{!! Form::label(null, 'Nomor Handphone', ['class'=>'color-maroon']) !!}
        		                		{!! Form::text('no_hp_non', null, ['class'=>$class_no_hp_non, 'placeholder'=>'Masukkan Nomor Handphone Anda']) !!}
        		                		<small class="form-note">Masukkan no HP Aktif Anda </small>
        		                		<?= ErrorForm::errorLabel($errors->has('no_hp_non'), $errors->first('no_hp_non')); ?>
        		                	</div>

        		                	<div class="col-md-12 form-group">
        		                		<?php $class_tanggal_lahir_non = ErrorForm::errorColumn($errors->has('tanggal_lahir_non')); ?>
        		                		{!! Form::label(null, 'Tanggal Lahir', ['class'=>'color-maroon']) !!}
        		                		<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
        		                		    {!! Form::text('tanggal_lahir_non', null, ['class'=>$class_tanggal_lahir_non.' datepika', 'placeholder' => 'Masukkan Tanggal Lahir Anda']) !!}
        		                		    <span class="input-group-btn">
        		                		        <button class="btn default" type="button">
        		                		            <i class="fa fa-calendar"></i>
        		                		        </button>
        		                		    </span>
        		                		</div>
        		                		<?= ErrorForm::errorLabel($errors->has('tanggal_lahir_non'), $errors->first('tanggal_lahir_non')); ?>
        		                	</div>

        		                	<div class="col-md-12 form-group">
				                		<?php $class_provinsi_non = ErrorForm::errorColumn($errors->has('provinsi_non')) . ' select2'; ?>
				                		{!! Form::label('provinsi_non', 'Domisili (Provinsi)', ['class'=>'color-maroon']) !!}
				                		{!! Form::select('provinsi_non',$provinsi,null,['class'=>$class_provinsi_non]) !!}
				                		<small class="form-note">Masukkan Provinsi domisili Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('provinsi_non'), $errors->first('provinsi_non')); ?>
				                		<span id="loading-provinsi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_kabupaten_non = ErrorForm::errorColumn($errors->has('kabupaten_non')) . ' select2'; ?>
				                		{!! Form::label('kabupaten_non', 'Domisili (Kabupaten/Kota)', ['class'=>'color-maroon']) !!}
				                		{!! Form::select('kabupaten_non',[], null,['class'=>$class_kabupaten_non]) !!}
				                		<small class="form-note">Masukkan Kabupaten domisili Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('kabupaten_non'), $errors->first('kabupaten_non')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_instansi_non = ErrorForm::errorColumn($errors->has('instansi_non')); ?>
				                		{!! Form::label('instansi_non', 'Instansi/Universitas/Kantor', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('instansi_non', null, ['class'=>$class_instansi_non, 'placeholder'=>'Masukkan Instansi/Universitas/Kantor']) !!}
				                		<small class="form-note">Masukkan Nama Instansi/Universitas/Kantor Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('instansi_non'), $errors->first('instansi_non')); ?>
				                	</div>

        		                	<div class="col-md-12 form-group">
        		                		<?php $class_profesi_non = ErrorForm::errorColumn($errors->has('profesi_non')) ; ?>
        		                		{!! Form::label(null, 'Profesi Anda', ['class'=>'color-maroon']) !!}
        		                		{!! Form::select('profesi_non',$profesi,null,['class'=>$class_profesi_non]) !!}
        		                		<?= ErrorForm::errorLabel($errors->has('profesi_non'), $errors->first('profesi_non')); ?>
        		                	</div>

        		                	<div class="col-md-12 form-group hidden" id="hidden_profesi_non">
				                		<?php $class_profesi_non_lainnya = ErrorForm::errorColumn($errors->has('profesi_non_lainnya')); ?>
				                		{!! Form::label('profesi_non_lainnya', 'Profesi Lainnya', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('profesi_non_lainnya', null, ['class'=>$class_profesi_non_lainnya, 'placeholder'=>'Isikan Profesi Anda']) !!}
				                		<small class="form-note">Masukkan Profesi Anda
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('profesi_non_lainnya'), $errors->first('profesi_non_lainnya')); ?>
				                	</div>

        		                	<div class="col-md-12 form-group">
        		                		<?php $class_email_non = ErrorForm::errorColumn($errors->has('email_non')); ?>
				                		{!! Form::label(null, 'Email', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('email_non', null, ['class'=>$class_email_non, 'placeholder'=>'Masukkan Email Anda']) !!}
				                		<small class="form-note">Pastikan e-mail yang Anda tuliskan valid 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> 
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('email_non'), $errors->first('email_non')); ?>
				                	</div>

				                	<div class="col-md-12 form-group">
				                		<?php $class_email_repeat_non = ErrorForm::errorColumn($errors->has('email_repeat_non')); ?>
				                		{!! Form::label(null, 'Ulangi Email', ['class'=>'color-maroon']) !!}
				                		{!! Form::text('email_repeat_non', null, ['class'=>$class_email_repeat_non, 'placeholder'=>'Masukkan Kembali Email Anda']) !!}
				                		<small class="form-note margin-bottom-5" style="padding-bottom: 10px;">Pastikan e-mail yang Anda tuliskan valid 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a>
				                		</small>
				                		<?= ErrorForm::errorLabel($errors->has('email_repeat_non'), $errors->first('email_repeat_non')); ?>
				                	</div>

                        	        <div class="col-md-12 form-group">
                        	            <div class="row">
                        	                <div class="col-xs-12 col-sm-6">
        										<div class="form-group">
        											<?php $class_password_non = ErrorForm::errorColumn($errors->has('password_non')); ?>
        											{!! Form::label(null, 'Password', ['class'=>'color-maroon']) !!}
        											{{ Form::password('password_non', ['class'=>$class_password_non, 'placeholder'=>'Masukkan Password_non Anda']) }}
        											<?= ErrorForm::errorLabel($errors->has('password_non'), $errors->first('password_non')); ?>
        										</div>	
                        	                </div>
                        	                <div class="col-xs-12 col-sm-6">
                        	                    <div class="form-group">
                        	                    	<?php $class_password_repeat_non = ErrorForm::errorColumn($errors->has('password_repeat_non')); ?>
                        	                    	{!! Form::label(null, 'Ulangi Password', ['class'=>'color-maroon']) !!}
                        	                    	{!! Form::password('password_repeat_non', ['class'=>$class_password_repeat_non, 'placeholder'=>'Masukkan Kembali Password Anda']) !!}
                        	                    	<?= ErrorForm::errorLabel($errors->has('password_repeat_non'), $errors->first('password_repeat_non')); ?>
                        	                    </div>
                        	                </div>
                        	            </div>
                        	        </div>

                        	       <div class="col-md-12 form-group">
                                        <div class="row">
                                        	<div class="col-xs-12 col-sm-12">
	                                            <input checked="checked" name="newsletter" type="checkbox" value="1"> Saya ingin menerima newsletter melalui e-mail
	                                            <br>
	                                            <small>Dengan mendaftar Anda berarti sudah menyetujui <a href="#">Kebijakan privasi TICMI</a></small>
                                        	</div>
                                        </div>
                                    </div>

				                	<div class="form-group" style="padding-left: 15px;">
                                      	{!! Form::submit('Daftar', ['class'=>'btn btn-lg btn-block btn-form-sid']) !!}		
				                		<!-- <a class="btn btn-danger btn-block" href="<?= route('login'); ?>">Back</a> -->
				                	</div>

        		                {!! Form::close() !!}
				            </div>
				        </div>
				    </div>
				</div>
				<!-- /Tab Register -->

                <div class="create-account">
                <!-- nothing here -->
                </div>
            </div>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2017 © TICMI</div>
<script>
	$(document).ready(function(){
		$('#dashboard-banner').hide();
		$('select[name="profesi"]').select2({ width: '100%', placeholder: 'Pilih profesi Anda' });
		$('select[name="profesi_non"]').select2({ width: '100%', placeholder: 'Pilih profesi Anda' });
		$('select[name="provinsi"]').select2({ width: '100%', placeholder: 'Pilih Provinsi Sesuai Domisili' });
		$('select[name="provinsi"]').select2({ width: '100%', placeholder: 'Pilih Provinsi Sesuai Domisili' });
		$('select[name="kabupaten"]').select2({ width: '100%', placeholder: 'Pilih Kabupaten/Kota Sesuai Domisili' }); 
		$('select[name="provinsi_non"]').select2({ width: '100%', placeholder: 'Pilih Provinsi Sesuai Domisili' });
		$('select[name="kabupaten_non"]').select2({ width: '100%', placeholder: 'Pilih Kabupaten/Kota Sesuai Domisili' }); 
		

		// select kategori program, 
        // then, get program
        $("select[name='provinsi']").on('change', function(){
            var provinsi = $(this).val().trim();

            $('#loading-provinsi').css({'display': 'block'});

            // reset if filled
            $("select[name='kabupaten']").html('');
            
            $.ajax({
                method: 'GET',
                url: '{{ route('register.ajax_get_kabupaten') }}',
                data: {provinsi: provinsi},
                success: function(msg){
                    $('#loading-provinsi').css({'display': 'none'});

                    obj_kabupaten     = JSON.parse(msg);
                    list_kabupaten    = "<option value=''></option>";
                    if(obj_kabupaten.length > 0){
                        $.each(obj_kabupaten, function(i,v){
                            list_kabupaten += "<option value='"+ v.id +"'>"+ v.name +"</option>";
                        });
                    }else{
                        list_kabupaten = "<option></option>";
                    }

                    
                    $("select[name='kabupaten']").html(list_kabupaten);
                },
                error: function(err){
                    alert(JSON.stringify(err));
                    $('#loading-provinsi').css({'display': 'none'});
                }
            });
        });

        $("select[name='provinsi_non']").on('change', function(){
            var provinsi = $(this).val().trim();

            $('#loading-provinsi_non').css({'display': 'block'});

            // reset if filled
            $("select[name='kabupaten_non']").html('');
            
            $.ajax({
                method: 'GET',
                url: '{{ route('register.ajax_get_kabupaten') }}',
                data: {provinsi: provinsi},
                success: function(msg){
                    $('#loading-provinsi_non').css({'display': 'none'});

                    obj_kabupaten     = JSON.parse(msg);
                    list_kabupaten    = "<option value=''></option>";
                    if(obj_kabupaten.length > 0){
                        $.each(obj_kabupaten, function(i,v){
                            list_kabupaten += "<option value='"+ v.id +"'>"+ v.name +"</option>";
                        });
                    }else{
                        list_kabupaten = "<option></option>";
                    }

                    
                    $("select[name='kabupaten_non']").html(list_kabupaten);
                },
                error: function(err){
                    alert(JSON.stringify(err));
                    $('#loading-provinsi_non').css({'display': 'none'});
                }
            });
        });

        $("select[name='profesi']").on('change', function(){
        	var profesi = $(this).val();
        	if(profesi == '1'){ // profesi lainnya
        		$('#hidden_profesi').removeClass('hidden');
        	} else {
        		$('#hidden_profesi').addClass('hidden');
        	}
		});

		$("select[name='profesi_non']").on('change', function(){
        	var profesi_non = $(this).val();
        	if(profesi_non == '1'){ // profesi lainnya
        		$('#hidden_profesi_non').removeClass('hidden');
        	} else {
        		$('#hidden_profesi_non').addClass('hidden');
        	}
		});
	});
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b3aef6c6d961556373d5962/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endsection
