                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered">
                           <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-calendar-minus-o font-red"></i>
                                    <span class="caption-subject font-red bold uppercase">{{$survey->nama}}</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                            	{!! Form::open(['route' => 'survey.store', 'id' => 'form_survey', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                            	<input type="hidden" name="nama_survey" value="{{$survey->nama}}">
								<table class="table table-stripped">
									<?php $x=0; ?>
                                	@foreach($svy_pertanyaan as $pertanyaan)
                                    	@if($pertanyaan->svy_tipe_pertanyaan_id == $multiple->svy_tipe_pertanyaan_id)
		                                    <?php $x+=1; ?>
		                                @endif
	                                    <thead>
	                                        <tr>
	                                            <th colspan="3">{{$pertanyaan->name}}</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
		                                    <input type="hidden" name="arr_pertanyaan[]" value="{{$pertanyaan->svy_pertanyaan_id}}">
	                                      	@foreach($pertanyaan->svy_pilihan as $pilihan)
		                                      	<tr>
		                                      		@if($pertanyaan->svy_tipe_pertanyaan_id == $single->svy_tipe_pertanyaan_id)
		                                      			<td>
															<div class="pretty p-default p-round">
		                                      					<input type="radio" name="{{$single->nama}}[{{$pertanyaan->svy_pertanyaan_id}}]" value="{{$pilihan->svy_pilihan_id}}" required/>
		                                      					<div class="state p-info">
		                                      						<i class="icon mdi mdi-check"></i>
		                                      						<label>{{$pilihan->pilihan}}</label>
		                                      					</div>
		                                      				</div>
		                                      			</td>
		                                      		@elseif($pertanyaan->svy_tipe_pertanyaan_id == $multiple->svy_tipe_pertanyaan_id)
		                                      			<td>
		                                      				<div class="checkbox-group{{$x}} required">
			                                      				<div class="pretty p-default p-curve">
			                                      					<input type="checkbox" name="{{$multiple->nama}}[{{$pertanyaan->svy_pertanyaan_id}}][]" value="{{$pilihan->svy_pilihan_id}}" required />
			                                      					<div class="state p-info">
			                                      						<i class="icon mdi mdi-check"></i>
			                                      						<label>{{$pilihan->pilihan}}</label>
			                                      					</div>
			                                      				</div>
			                                      			</div>
		                                      			</td>
		                                      		@elseif($pertanyaan->svy_tipe_pertanyaan_id == $essay->svy_tipe_pertanyaan_id)
		                                      			<td><textarea name="{{$essay->nama}}[{{$pertanyaan->svy_pertanyaan_id}}]" rows="5" cols="80" required/></textarea></td>
		                                      		@endif
		                                      	</tr>
	                                      	@endforeach
	                                @endforeach
	                                      	<br>
		                                      	<tr>
		                                      		<td>
		                                      			<input type="hidden" name="pelatihan_aktual_dtl_id" value="{{ $pelatihan_aktual_dtl_id }}">
				                                  		<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
		                                      		</td>
		                                      	</tr>
		                                </tbody>
	                            	</table>
                                  	
                                {!! Form::close() !!}	

                                <div class="clearfix"> </div>
                            </div>
                        </div>

                    </div>
                </div>
                <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                <script type="text/javascript">
					$(function(){
						var chk = parseInt('{{$x}}');
						var i = 1;
						var j = 1;
						var func = [];
						for(i=1; i <= chk; i++){

							func[i] = check(i);
						}


						function check(num){
						    var checkboxGroup = $('.checkbox-group'+ num +' :checkbox[required]').change(function(){
								if(checkboxGroup.is(':checked')) {
						            checkboxGroup.removeAttr('required');
						        }else{
						        	checkboxGroup.attr('required', 'required');
						        }
						    });
						}
					});
                </script>