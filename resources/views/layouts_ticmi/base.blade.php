<!DOCTYPE html>
<html class="js no-touch csstransforms3d csstransitions webkit chrome win js js no-touch csstransforms3d csstransitions js no-touch csstransforms3d csstransitions boxed" style="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="google-site-verification" content="GJjVwb29TNebSloS7Kj-LGHLje-SUTFKVgY9QIBli10" />
    <!-- Basic -->
    <title>
        @if(isset($pageTitle) && !empty($pageTitle))
            {{ $pageTitle }}
        @else
            The Indonesia Capital Market Institute | Pusat Edukasi dan Data Pasar Modal Indonesia
        @endif
    </title>
    <meta name="keywords" content="Sertifikasi Pasar Modal, Pelatihan Pasar Modal, Training Pasar Modal, Edukasi Pasar Modal, Pasar Modal Indonesia, Laporan Keuangan Emiten, Laporan Keuangan, Sertifikasi WPPE, Sertifikasi WMI, Sertifikasi Pasar Modal Syariah, Pasar Modal Syariah, WPPE, WMI, Wakil Penjamin Pedagang Efek, Wakil Manajer Investasi, Sekolah Pasar Modal,ASPM, Ahli Syariah Pasar Modal,CMPDP, Capital Market Professional Development Program,Seminar Pasar Modal,Workshop Pasar Modal, Pusat Edukasi dan Data Pasar Modal Indonesia, {{ !empty($pageTitle) ? $pageTitle:"" }} ">
    <meta name="description" content="Sertifikasi Pasar Modal, Pelatihan Pasar Modal, Training Pasar Modal, Edukasi Pasar Modal, Pasar Modal Indonesia, Laporan Keuangan Emiten, Laporan Keuangan, Sertifikasi WPPE, Sertifikasi WMI, Sertifikasi Pasar Modal Syariah, Pasar Modal Syariah, WPPE, WMI, Wakil Penjamin Pedagang Efek, Wakil Manajer Investasi, Sekolah Pasar Modal,ASPM, Ahli Syariah Pasar Modal,CMPDP, Capital Market Professional Development Program,Seminar Pasar Modal,Workshop Pasar Modal, Pusat Edukasi dan Data Pasar Modal Indonesia, {{ !empty($pageTitle) ? $pageTitle:"" }}">
    <meta name="author" content="Akmal Bashan <akmal.squal@gmail.com>">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Social Media Tags -->
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="{{ (isset($ogtype) && !empty($ogtype)) ? $ogtype:"" }}" />
    <meta property="og:title" content="{{ (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle:"The Indonesia Capital Market Institute | Pusat Edukasi dan Data Pasar Modal Indonesia" }}" />
    <meta property="og:description"        content="{{ (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle:"The Indonesia Capital Market Institute | Pusat Edukasi dan Data Pasar Modal Indonesia" }}" />
    <meta property="og:image"              content="{{ (isset($ogimage) && !empty($ogimage)) ? $ogimage:url('/assets/images/ticmi/library-ticmi-small.jpg') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/ticmi/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/ticmi/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/ticmi/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/ticmi/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/ticmi/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/ticmi/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/ticmi/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/ticmi/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/ticmi/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/ticmi/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/ticmi/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/ticmi/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/ticmi/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/ticmi/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/ticmi/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">

    <!-- Lib CSS -->
<!--     <link rel="stylesheet" href="./assets/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/lib/animate.min.css">
    <link rel="stylesheet" href="./assets/css/lib/font-awesome.min.css">
    <link rel="stylesheet" href="./assets/css/lib/univershicon.css">
    <link rel="stylesheet" href="./assets/css/lib/owl.carousel.css">
    <link rel="stylesheet" href="./assets/css/lib/prettyPhoto.css">
    <link rel="stylesheet" href="./assets/css/lib/menu.css">
    <link rel="stylesheet" href="./assets/css/lib/timeline.css"> -->

    <link rel="stylesheet" href="{{ url('assets/ticmi/css/lib/libs.css') }}">

    <!-- Revolution CSS -->
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/layers.css') }}">
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/navigation.css') }}">


    <link rel="stylesheet" href="{{ url('assets/ticmi/css/style.css') }}">
    <!-- Theme CSS -->
    {{--<link rel="stylesheet" href="{{ url('assets/ticmi/css/theme.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ url('assets/ticmi/css/theme-responsive.css') }}">--}}
	{{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
    <!--[if IE]>
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/ie.css') }}">
    <![endif]-->

    <!-- Head Libs -->
    <script src="{{ url('assets/ticmi/js/lib/modernizr.js') }}"></script>
    <!-- Skins CSS -->
    <!-- <link rel="stylesheet" href="./assets/css/default.css"> -->

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/custom.css') }}">

    @yield('googlesnippet')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-78360717-1', 'auto');
        ga('send', 'pageview');

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <style>
        .stepwizard-step p {
            margin-top: 10px;
        }

        .stepwizard-row {
            display: table-row;
        }

        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }

        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
		
		/*#popup {*/
            /*z-index: 99999;*/
            /*width: 60%;*/
            /*height: auto;*/
            /*display:none;*/
            /*position:absolute;*/
            /*margin:0 auto;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/
            /*box-shadow: 0px 0px 50px 2px #000;*/

        /*}*/
    </style>
</head>
<body>
<div id="fb-root"></div>

{{--<div>--}}
    {{--<script>--}}
        {{--(function() {--}}
            {{--var cx = '003863218905538145526:iqdxuenqsi8';--}}
            {{--var gcse = document.createElement('script');--}}
            {{--gcse.type = 'text/javascript';--}}
            {{--gcse.async = true;--}}
            {{--gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;--}}
            {{--var s = document.getElementsByTagName('script')[0];--}}
            {{--s.parentNode.insertBefore(gcse, s);--}}
        {{--})();--}}
    {{--</script>--}}
    {{--<gcse:search></gcse:search>--}}
{{--</div>--}}
	
@if($routename=='home')
 <div class="popupbanner" id="popup"> 
    {{--<a href="#" class="btn">Klik Untuk Daftar</a>--}}
      
	   <img class="img-responsive" src="{{ url('assets/ticmi/images/ticmi/homebanner/Popup-Data.jpg') }}" alt="popup">
 </div> 
@endif

<!-- Page Loader -->
{{--<div id="pageloader" style="">--}}
    {{--<div class="loader-inner" style="">--}}
        {{--<img src="{{ url('assets/images/default/logo-ticmi.jpg') }}" alt="" width="250">--}}
        {{--<img src="{{ url('assets/images/preloader.gif') }}" alt="">--}}
    {{--</div>--}}
{{--</div>--}}
<!-- Page Loader -->


<!-- Back to top -->
<a href="#0" class="cd-top cd-is-visible">Top</a>

<!-- End Theme Panel Switcher -->
<div id="theme-panel" class="theme-panel close-theme-panel">
    <!-- Panel Button -->
    <a class="panel-button"><i class="fa uni-car-coins"></i> <span class="badge badge-red">{{ count(session()->get('cart')) }}</span></a>

    <!-- Panel Content -->
    <div class="panel-content">

        <!-- Options Content -->
        <div class="config panel-btns config-layout">
            <h5 class="title">Keranjang Belanja</h5>
            <ul class="config-options">
                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                    @php($item = session()->get('cart'))
                    <li><a id="layout-config-boxed" href="{{ route('cart.view') }}" onclick="ga('send', 'event', 'Pembelian Data', 'Lihat Keranjang', '{{ url()->current() }}' )">Lihat Keranjang</a></li>
                @endif
                {{--<li><a id="layout-config-boxed" href="#--}}{{-- {{ route('cart.view') }} --}}{{--" onclick="ga('send', 'event', 'Pembelian Data', 'Lihat Keranjang', '{{ url()->current() }}' )">Lihat Keranjang</a></li>--}}
                {{--<li><a id="layout-config-wide" href="#--}}{{-- {{ route('cart.checkout') }} --}}{{--"  onclick="ga('send', 'event', 'Pembelian Data', 'Checkout', '{{ url()->current() }}' )">Checkout</a></li>--}}
            </ul><!-- Options Config -->
        </div><!-- Options Content -->

        <!-- Options Content -->
        <div class="config config-bg">
            <h5 class="title">Ada {{ count(session()->get('cart')) }} item di keranjang</h5>
            <table class="table table-striped side-cart">
                <tbody>
                @php($total=0)
                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                    @foreach(session()->get('cart') as $key=>$item)
                        <tr>
                            <td>{!! $item['title']." <strong>@Rp".number_format($item['price'],0,',','.').'</strong>' !!}</td>
                            <td style="width:10%;text-align: right;"><a href="" class="delfromcart" data-id="{{ $key }}"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @php($total+=$item['price'])
                    @endforeach
                @else
                    <tr>
                        <td><p>Keranjang masih kosong</p></td>
                    </tr>

                @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td align="right"><strong>TOTAL</strong></td>
                        <td><strong>Rp{{ number_format($total,0,',','.') }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div><!-- Options Content -->

        {{--<!-- Options Content -->--}}
        {{--<div class="config config-demo">--}}
            {{--<h5 class="title">Demos</h5>--}}
            {{--<ul class="demos">--}}
                {{--<li><a href="index-events.html"><img class="img-responsive" src="images/panel/demo-events.jpg"></a></li>--}}
                {{--<li><a href="index-kids.html"><img class="img-responsive" src="images/panel/demo-kids.jpg"></a></li>--}}
                {{--<li><a href="index.html"><img class="img-responsive" src="images/panel/demo-default.jpg"></a></li>--}}
                {{--<li><a href="index-news.html"><img class="img-responsive" src="images/panel/demo-news.jpg"></a></li>--}}
                {{--<li><a href="index-one-page.html"><img class="img-responsive" src="images/panel/demo-onepage.jpg"></a></li>--}}
            {{--</ul><!-- Options Config -->--}}
        {{--</div><!-- Options Content -->--}}

        {{--<!-- Options Content -->--}}
        <div class="config panel-btns">
            <ul class="config-options">
                <li><a href="#{{-- {{ route('cart.removeall') }} --}}" class="delfromcart">Hapus Semua Item</a></li>
            </ul><!-- Options Config -->
        </div><!-- Options Content -->
    </div><!-- Panel Content -->
</div><!-- End Theme Panel Switcher -->


@yield('header')

<!-- Page Main -->
<div role="main" class="main">
    @yield('contentWrapper')

<!-- Section -->
    <section class="pad-tb-40">
    <div class="container">
        <div class="row">
            <!-- Column -->
            <div class="col-sm-11 col-sm-offset-1">
                <div class="callto-action text-center">
                    <div class="widget no-box text-center" style="margin: 0 auto;">
                        <ul class="sro-widget">
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ojk.go.id" target="_blank" title="OJK"><img width="100" height="60" src="{{ url('assets/ticmi/images/partner/ojk.jpg') }}" class="img-responsive" alt="OJK" style="margin-top: 30px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://idx.co.id" target="_blank" title="IDX BEI"><img width="60" height="60" src="{{ url('assets/ticmi/images/partner/idx.png') }}" class="img-responsive" alt="IDX" style="width: 90px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://kpei.co.id" target="_blank" title="KPEI"><img width="60" height="60" src="{{ url('assets/ticmi/images/partner/kpei.png') }}" class="img-responsive" alt="Thumb" style="width: 80px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ksei.co.id" target="_blank" title="KSEI"><img width="60" height="60" src="{{ url('assets/ticmi/images/partner/ksei.png') }}" class="img-responsive" alt="Thumb" style="width: 100px;margin-top: 35px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://www.pefindo.com" target="_blank" title="PEFINDO"><img width="60" height="60" src="{{ url('assets/ticmi/images/partner/pefindo.jpg') }}" class="img-responsive" alt="Thumb" style="width: 130px;margin-top: 5px"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ibpa.co.id" target="_blank" title="IBPA"><img width="60" height="60" src="{{ url('assets/ticmi/images/partner/ibpa.png') }}" class="img-responsive" alt="Thumb" style="width: 85px; margin-top: 40px"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://www.indonesiasipf.co.id" target="_blank" title="SIPF"> <img width="60" height="60" src="{{ url('assets/ticmi/images/partner/sipf.png') }}" class="img-responsive" alt="Thumb" style="width: 85px;margin-top: 25px;"></a>
                                </div>
                            </li>
                        </ul><!-- Thumbnail Widget -->
                    </div><!-- Widget -->

                    <!--<h5 class="title"><span class="call-title-text"><span class="call-title-text">Need a copy of this beautiful Html5 so what are you waiting?</span></span><a href="http://glorythemes.in/html/universh/demo/index.html#" class="btn">Buy now</a></h5>-->
                </div>
            </div><!-- Column -->
        </div><!-- row -->
    </div><!-- Container -->
</section><!-- Section -->
</div><!-- Page Main -->

<!-- Footer -->
<footer id="footer" class="footer-1">
    <div class="main-footer widgets typo-light">
        <div class="container">
            <div class="row">
                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget subscribe no-box  hidden-xs">
                        <h5 class="widget-title">Newsletter<span></span></h5>
                        <p class="form-message1" style="display: none;"></p>
                        <div class="clearfix"></div>
                        <p>Silahkan daftar Newsletter kami untuk mendapatkan berita terbaru mengenai TICMI.</p>
                        <form class="input-group subscribe-form bv-form" name="subscribe-form" method="post" action="" id="subscribe-form" novalidate="novalidate"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                            <div class="form-group has-feedback">
                                <input class="form-control" type="email" placeholder="Subscribe" value="" name="subscribe_email" data-bv-field="subscribe_email"><i class="form-control-feedback bv-no-label" data-bv-icon-for="subscribe_email" style="display: none;"></i>
                                <small class="help-block" data-bv-validator="notEmpty" data-bv-for="subscribe_email" data-bv-result="NOT_VALIDATED" style="display: none;">Email is required. Please enter email.</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="subscribe_email" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a correct email address.</small></div>
                            <span class="input-group-btn">
                                <a class="btn"><span class="glyphicon glyphicon-arrow-right"></span></a>
                            </span>
                        </form>
                    </div><!-- Widget -->
                    <!-- Widget -->
                    <div class="widget no-box text-center">
                        <a class="logo" href="https://ticmi.co.id">
                            <img src="{{ url('assets/ticmi/images/default/logo-ticmi.jpg') }}" width="190" height="0" class="img-rounded img-responsive" alt="Universh Education HTML5 Website Template">
                        </a>
                    </div><!-- Widget -->
                </div><!-- Column -->


                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget no-box">
                        <h5 class="widget-title">Alamat<span></span></h5>
                        <address>
                            <strong>PT. Indonesian Capital Market Electronic Library</strong> <br>
                            Indonesian Stock Exchange Building, Tower II, 1st Floor
                            Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190 <br>
                            <abbr title="Call Center">Call Center</abbr> : 0800 100 9000 (Toll Free) <br>
                            <abbr title="Phone">P </abbr> : (021) 515 23 18 <br>
                            <abbr title="Email">E </abbr> : info@ticmi.co.id

                        </address>
                    </div><!-- Widget -->
                    <!-- Widget -->
                    <div class="widget no-box">
                        <h5 class="widget-title">Follow Us<span></span></h5>
                        <!-- Social Icons Color -->
                        <ul class="social-icons color">
                            <li class="facebook"><a href="https://www.facebook.com/TICMInow" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="https://twitter.com/TICMInow" target="_blank" title="Twitter">Twitter</a></li>
                            <li class="youtube"><a href="https://www.youtube.com/channel/UCtJ15ddZV_JhHtPXa-Jj9Lw" target="_blank" title="Youtube">Youtube</a></li>
                            <li class="instagram"><a href="https://www.instagram.com/TICMInow" target="_blank" title="Instagram">Instagram</a></li>
                            <li class="telegram"><a href="http://t.me/sonyaticmibot" target="_blank" title="Sonya Ticmi Bot">Sonya Ticmi Bot</a></li>
                        </ul>
                    </div><!-- Widget -->
                </div><!-- Column -->

                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget gallery-widget no-box">
                        <h5 class="widget-title">Galleri TICMI<span></span></h5>
                        <!-- Gallery Widget -->
                        <ul>
                            <li><a href="{{ url('upload/gallery/2018/seleksi-cmpdp-ticmi-2018.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Seleksi CMPDP 2018"><img width="60" height="60" src="{{ url('upload/gallery/2018/seleksi-cmpdp-ticmi-2018.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Seleksi CMPDP 2018"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Wisuda Periode 1 TICMI 2018"><img width="60" height="60" src="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Wisuda Periode 1 TICMI 2018"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018-2.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Wisuda Periode 1 TICMI 2018"><img width="60" height="60" src="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018-2.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Wisuda Periode 1 TICMI 2018"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018-3.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Wisuda Periode 1 TICMI 2018"><img width="60" height="60" src="{{ url('upload/gallery/2018/wisuda-periode-1-ticmi-2018-3.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Wisuda Periode 1 TICMI 2018"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/interview-dan-FGD-CMPDP-2018-TICMI.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama DSN MUI & TICMI"><img width="60" height="60" src="{{ url('upload/gallery/2018/interview-dan-FGD-CMPDP-2018-TICMI.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Interview & FGD CMPDP 2018"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/workshop-value-investor-rivan-kurniawan-ticmi-2018.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Workshop Value Investor bersama Rivan Kurniawan"><img width="60" height="60" src="{{ url('upload/gallery/2018/workshop-value-investor-rivan-kurniawan-ticmi-2018.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Workshop Value Investor bersama Rivan Kurniawan"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/ticmi-corner-di-idx-chanel-ticmi-2018.jpg') }}" data-rel="prettyPhoto[portfolio]" title="TICMI Corner On IDX Chanel"><img width="60" height="60" src="{{ url('upload/gallery/2018/ticmi-corner-di-idx-chanel-ticmi-2018.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="TICMI Corner on IDX Chanel"></a></li>
                            <li><a href="{{ url('upload/gallery/2018/launching-sertifikasi-dan-spm-online-ticmi-2018.jpg') }}" data-rel="prettyPhoto[portfolio]" title="Launching Sertifikasi dan SPM Online TICMI 2018"><img width="60" height="60" src="{{ url('upload/gallery/2018/launching-sertifikasi-dan-spm-online-ticmi-2018.jpg') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Launching Sertifikasi dan SPM Online TICMI 2018"></a></li>
                        </ul>
                    </div><!-- Widget -->
                </div><!-- Column -->

            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Main Footer -->

    <!-- Footer Copyright -->
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <!-- Copy Right Logo -->
                <!--<div class="col-md-2">-->
                <!--<a class="logo" href="#">-->
                <!--<img src="./assets/images/default/logo-ticmi.jpg" width="120" height="0" class="img-rounded img-responsive" alt="Universh Education HTML5 Website Template">-->
                <!--</a>-->
                <!--</div>-->
                <!-- Copy Right Logo -->
                <!-- Copy Right Content -->
                <div class="col-md-9">
                    <p>© Copyright 2016. All Rights Reserved. | By <a href="https://ticmi.co.id" title="TICMI">The Indonesia Capital Market Institute</a></p>
                </div><!-- Copy Right Content -->
                <!-- Copy Right Content -->
                <div class="col-md-3">
                    <nav class="sm-menu text-center">
                        <ul>
                            <li><a href="{{ url('faq') }}">FAQ's</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="{{ url('hubungi-kami') }}">Hubungi Kami</a></li>
                        </ul>
                    </nav><!-- Nav -->
                </div><!-- Copy Right Content -->
            </div><!-- Footer Copyright -->
        </div><!-- Footer Copyright container -->
    </div><!-- Footer Copyright -->
</footer>
<!-- Footer -->


{{-- Modal --}}

<div class="modal fade bs-example-modal-lg" id="mod-iframe-large" aria-labelledby="mod-view-large" tabindex="-1" role="modal">
    <div class="modal-dialog " style="" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myModalLabel">Modal Title</h5>
            </div>
            <div class="modal-body" style="margin:0;padding:5px">
                <iframe src="" id="iframeModalContent" style="" frameborder="0" height="100%" width="100%"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade bs-example-modal-lg" id="mod-iframe-document" aria-labelledby="mod-view-large" tabindex="-1" role="modal">
    <div class="modal-dialog  modal-lg" style="width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="pull-right" data-dismiss="modal" >x</button>
                <h5 class="modal-title" id="myModalLabel">Modal Title</h5>
            </div>
            <div class="modal-body" style="margin:0;padding:5px">
                <div class="body-header">
                    <table class="table table-condensed" width="100%" style="border: none;margin-bottom: 0;">
                        <tr>
                            <td style="border: none;" width="50%">Perusahaan : <span id="nm-perusahaan"></span></td>
                            <td style="border: none;">Tipe Dokumen : <span id="tipe-dokumen"></span></td>
                        </tr>
                        <tr>
                            <td style="border: none;">Kode Perusahaan : <span id="kode-perusahaan"></span></td>
                            <td style="border: none;">Tahun : <span id="tahun-dokumen"></span></td>
                        </tr>
                    </table>
                </div>
                <iframe src="" id="iframeModalContent" style="" frameborder="0" height="100%" width="100%"></iframe>
            </div>
            <div class="modal-footer">
                <div class="mb10"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<input type="hidden" name="base_url" class="base_url" value="{!! url('/') !!}">
<input type="hidden" name="_token" class="_token" value="{!! csrf_token() !!}">
<!-- library -->
<script src="{{ url('assets/ticmi/libs/pdfjs/build/pdf.js') }}"></script>
<script src="{{ url('assets/ticmi/libs/pdfjs/build/pdf.worker.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/libs.js') }}"></script>
<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.2.0/scrollreveal.min.js"></script>

<!-- Revolution Js -->
<script src="{{ url('assets/ticmi/js/lib/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/theme-rs.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/theme.js') }}"></script>
{{--<script src="{{ url('assets/ticmi/js/lib/jquery-ui.js') }}"></script>--}}
<!-- Theme Base, Components and Settings -->
<script src="{{ url('assets/ticmi/js/theme.js') }}"></script>
 
  <script>
  // $( function() {
  //   $( ".datepickers" ).datepicker({
  //     changeMonth: true,
  //     changeYear: true,
  //    dateFormat: "yy-mm-dd"
  //   });
  // } );
  </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b3aef6c6d961556373d5962/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

<script type="text/javascript">

    $(document).ready(function(){
		$("#popup").hide().fadeIn(1000);
        $('.popupbanner').on('click', function(e){
            $('.popupbanner').fadeOut(500);
        });
		$(document).click(function(event) { 
			 $('.popupbanner').fadeOut(50);
		});
    });

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault(); 
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    $("div#form-reksadana").removeClass("hidden");
</script>


</body></html>