@extends('layouts_ticmi.base')
@include('layouts_ticmi.header')

@section('contentWrapper')

    @yield('content')

@endsection