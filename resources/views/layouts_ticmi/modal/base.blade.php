<!DOCTYPE html>
<html class="js no-touch csstransforms3d csstransitions webkit chrome win js js no-touch csstransforms3d csstransitions js no-touch csstransforms3d csstransitions boxed" style="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Basic -->
    <title>
        @if(isset($pageTitle) && !empty($pageTitle))
            {{ $pageTitle }}
        @else
            The Indonesia Capital Market Institute | Pusat Edukasi dan Data Pasar Modal Indonesia
        @endif
    </title>
    <meta name="keywords" content="Sertifikasi Pasar Modal, Pelatihan Pasar Modal, Training Pasar Modal, Edukasi Pasar Modal, Pasar Modal Indonesia, Laporan Keuangan Emiten, Laporan Keuangan, Sertifikasi WPPE, Sertifikasi WMI, Sertifikasi Pasar Modal Syariah, Pasar Modal Syariah, WPPE, WMI, Wakil Penjamin Pedagang Efek, Wakil Manajer Investasi, Sekolah Pasar Modal,ASPM, Ahli Syariah Pasar Modal,CMPDP, Capital Market Professional Development Program,Seminar Pasar Modal,Workshop Pasar Modal, Pusat Edukasi dan Data Pasar Modal Indonesia ">
    <meta name="description" content="Sertifikasi Pasar Modal, Pelatihan Pasar Modal, Training Pasar Modal, Edukasi Pasar Modal, Pasar Modal Indonesia, Laporan Keuangan Emiten, Laporan Keuangan, Sertifikasi WPPE, Sertifikasi WMI, Sertifikasi Pasar Modal Syariah, Pasar Modal Syariah, WPPE, WMI, Wakil Penjamin Pedagang Efek, Wakil Manajer Investasi, Sekolah Pasar Modal,ASPM, Ahli Syariah Pasar Modal,CMPDP, Capital Market Professional Development Program,Seminar Pasar Modal,Workshop Pasar Modal, Pusat Edukasi dan Data Pasar Modal Indonesia, {{ !empty($pageTitle) ? $pageTitle:"" }}">
    <meta name="author" content="Akmal Bashan <akmal.squal@gmail.com>">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">

    <!-- Lib CSS -->
    <!--     <link rel="stylesheet" href="./assets/css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/css/lib/animate.min.css">
        <link rel="stylesheet" href="./assets/css/lib/font-awesome.min.css">
        <link rel="stylesheet" href="./assets/css/lib/univershicon.css">
        <link rel="stylesheet" href="./assets/css/lib/owl.carousel.css">
        <link rel="stylesheet" href="./assets/css/lib/prettyPhoto.css">
        <link rel="stylesheet" href="./assets/css/lib/menu.css">
        <link rel="stylesheet" href="./assets/css/lib/timeline.css"> -->

    <link rel="stylesheet" href="{{ url('assets/ticmi/css/lib/libs.css') }}">

    <!-- Revolution CSS -->
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/layers.css') }}">
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/navigation.css') }}">


    <link rel="stylesheet" href="{{ url('assets/ticmi/css/style.css') }}">
    <!-- Theme CSS -->
    <!-- <link rel="stylesheet" href="assets/ticmi/css/theme.css"> -->
    <!-- <link rel="stylesheet" href="assets/ticmi/css/theme-responsive.css"> -->

    <!--[if IE]>
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/ie.css') }}">
    <![endif]-->

    <!-- Head Libs -->
    <script src="{{ url('assets/ticmi/js/lib/modernizr.js') }}"></script>

    <!-- Skins CSS -->
    <!-- <link rel="stylesheet" href="./assets/ticmi/css/default.css"> -->

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ url('assets/ticmi/css/custom.css') }}">

    <script type="text/css">
        html.boxed body {
            max-width: 1900px;
        }
    </script>

    @yield('googlesnippet')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-78360717-1', 'auto');
        ga('send', 'pageview');

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>
<body style="max-width: 90%;">
<div id="fb-root"></div>

{{--<div>--}}
{{--<script>--}}
{{--(function() {--}}
{{--var cx = '003863218905538145526:iqdxuenqsi8';--}}
{{--var gcse = document.createElement('script');--}}
{{--gcse.type = 'text/javascript';--}}
{{--gcse.async = true;--}}
{{--gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;--}}
{{--var s = document.getElementsByTagName('script')[0];--}}
{{--s.parentNode.insertBefore(gcse, s);--}}
{{--})();--}}
{{--</script>--}}
{{--<gcse:search></gcse:search>--}}
{{--</div>--}}

@if(empty($routename))
    <!--<div class="popupbanner">
    <img src="{{ url('/assets/ticmi/images/course/notifikasi-data-ticmi.jpeg') }}" alt="">
</div>-->
@endif

<!-- Page Loader -->
<div id="pageloader" style="">
    <div class="loader-inner" style="">
        <img src="{{ url('assets/ticmi/images/default/logo-ticmi.jpg') }}" alt="" width="250">
        <img src="{{ url('assets/ticmi/images/preloader.gif') }}" alt="">
    </div>
</div><!-- Page Loader -->


@yield('header')

<!-- Page Main -->
<div role="main" class="">
<!-- Section -->
    {{--<section class="pad-tb-10">--}}
        @yield('contentWrapper')
    {{--</section><!-- Section -->--}}
</div><!-- Page Main -->


{{-- Modal --}}

<div class="modal fade bs-example-modal-lg" id="mod-iframe-large" aria-labelledby="mod-view-large" tabindex="-1" role="modal">
    <div class="modal-dialog " style="" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myModalLabel">Notifikasi</h5>
            </div>
            <div class="modal-body" style="margin:0;padding:5px">
                <iframe src="" id="iframeModalContent" style="" frameborder="0" height="100%" width="100%"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade bs-example-modal-lg" id="mod-iframe-document" aria-labelledby="mod-view-large" tabindex="-1" role="modal">
    <div class="modal-dialog  modal-lg" style="width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="pull-right" data-dismiss="modal" >x</button>
                <h5 class="modal-title" id="myModalLabel">Modal Title</h5>
            </div>
            <div class="modal-body" style="margin:0;padding:5px">
                <div class="body-header">
                    <table class="table table-condensed" width="100%" style="border: none;margin-bottom: 0;">
                        <tr>
                            <td style="border: none;" width="50%">Perusahaan : <span id="nm-perusahaan"></span></td>
                            <td style="border: none;">Tipe Dokumen : <span id="tipe-dokumen"></span></td>
                        </tr>
                        <tr>
                            <td style="border: none;">Kode Perusahaan : <span id="kode-perusahaan"></span></td>
                            <td style="border: none;">Tahun : <span id="tahun-dokumen"></span></td>
                        </tr>
                    </table>
                </div>
                <iframe src="" id="iframeModalContent" style="" frameborder="0" height="100%" width="100%"></iframe>
            </div>
            <div class="modal-footer">
                <div class="mb10"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<input type="hidden" name="base_url" class="base_url" value="{!! url('/') !!}">
<input type="hidden" name="_token" class="_token" value="{!! csrf_token() !!}">
<!-- library -->
<script src="{{ GeneralHelper::base_url('libs/pdfjs/build/pdf.js') }}"></script>
<script src="{{ GeneralHelper::base_url('libs/pdfjs/build/pdf.worker.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/libs.js') }}"></script>
<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.2.0/scrollreveal.min.js"></script>

<!-- Revolution Js -->
<script src="{{ url('assets/ticmi/js/lib/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ url('assets/ticmi/js/lib/theme-rs.js') }}"></script>
<!-- Theme Base, Components and Settings -->
<script src="{{ url('assets/ticmi/js/theme.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('.popupbanner').on('click', function(e){
            $('.popupbanner').fadeOut(500);
        });
    });
</script>


</body></html>