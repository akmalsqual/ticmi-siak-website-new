<link href="{{ asset('assets/ticmi/css/custom.css') }}" rel="stylesheet">
<style>
a.plain{
    background-color: none;
}
</style>

    <!-- Header Begins -->
    <header id="header" class="default-header colored flat-menu">
        <div class="header-top">
            <div class="container">
                <nav>
                    <ul class="nav nav-pills nav-top">
                        <li class="phone">
                            <span><i class="fa fa-envelope"></i> <a href="mailto:info@ticmi.co.id?subject=Hello From Website">info@ticmi.co.id</a> </span>
                        </li>
                        <li class="phone">
                            <span><i class="fa fa-phone"></i>0800 100 9000 (Toll Free)</span>
                        </li>
                    </ul>
                </nav>
                <ul class="social-icons color">
                    <li class="facebook"><a href="http://www.facebook.com/TICMInow" target="_blank" title="Facebook">Facebook</a></li>
                    <li class="twitter"><a href="https://twitter.com/TICMInow" target="_blank" title="Twitter">Twitter</a></li>
                    <li class="youtube"><a href="https://www.youtube.com/channel/UCtJ15ddZV_JhHtPXa-Jj9Lw" target="_blank" title="Youtube">Youtube</a></li>
                    <li class="instagram"><a href="https://www.instagram.com/TICMInow" target="_blank" title="Instagram">Instagram</a></li>
                    <li class="telegram"><a href="http://t.me/sonyaticmibot" target="_blank" title="Sonya Ticmi Bot">Sonya Ticmi Bot</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="logo">
                <a href="{{ url('/') }}">
                    <img alt="TICMI" width="103" height="60" data-sticky-width="95" data-sticky-height="55" data-sticky-padding="20" src="{{ url('assets/images/default/logo-ticmi.png') }}" >
                </a>
            </div>
            <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse"><i class="fa fa-bars"></i></button>
        </div>

        <div class="navbar-collapse nav-main-collapse collapse">
            <div class="container">
                <nav class="nav-main mega-menu">
                    <ul class="nav nav-pills nav-main" id="mainMenu">
                        @php $routename = 'temp'; @endphp
                        <li class="dropdown  {{ empty($routename) ? "active":"" }}">
                            <a class="disabled sticky-menu-active {{ empty($routename) ? "active":"" }}" href="{{ url('/') }}" onclick="ga('send', 'event', 'Header Menu', 'home', '{{ url()->current() }}' );"> Home </a>
                        </li>
                        <li class="dropdown {{ ($routename == 'profil-perusahaan' || $routename == 'visi-misi' || $routename == 'sejarah' || $routename == 'struktur') ? "active":"" }}">
                            <a class="dropdown-toggle sticky-menu-active {{ ($routename == 'profil-perusahaan' || $routename == 'visi-misi' || $routename == 'sejarah' || $routename == 'struktur') ? "active":"" }}" href="#">Tentang Kami<i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('profil-perusahaan') }}" onclick="ga('send', 'event', 'Header Menu', 'profil-perusahaan', '{{ url()->current() }}' );">Profil Perusahaan</a></li>
                                <li><a href="{{ url('visi-misi') }}" onclick="ga('send', 'event', 'Header Menu', 'visi-misi', '{{ url()->current() }}' );">Visi & Misi</a></li>
                                <li><a href="{{ url('sejarah') }}" onclick="ga('send', 'event', 'Header Menu', 'sejarah', '{{ url()->current() }}' );">Sejarah </a></li>
                                <li><a href="{{ url('struktur') }}" onclick="ga('send', 'event', 'Header Menu', 'struktur', '{{ url()->current() }}' );">Struktur Organisasi</a></li>
                            </ul>
                        </li>
                        <li class="dropdown mega-menu-item mega-menu-fullwidth {{ ($routename == 'wppe-product' || $routename == 'wmi-product' || $routename == 'aspm-product' || $routename == 'waiver-product' || $routename == 'data-historis' || $routename == 'perpustakaan' || $routename == 'permintaan-data') ? 'active':"" }}">
                            <a class="dropdown-toggle sticky-menu-active" href="#">Produk & Layanan<i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">Sertifikasi Profesi</span>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{ url('wppe') }}" onclick="ga('send', 'event', 'Header Menu', 'wppe-product', '{{ url()->current() }}' );">WPPE (Wakil Perantara Pedagang Efek)</a></li>
                                                            <li><a href="{{ url('wmi') }}" onclick="ga('send', 'event', 'Header Menu', 'wmi-product', '{{ url()->current() }}' );">WMI (Wakil Manajer Investasi)</a></li>
                                                            <li><a href="{{ url('aspm') }}" onclick="ga('send', 'event', 'Header Menu', 'aspm-product', '{{ url()->current() }}' );">ASPM (Ahli Syariah Pasar Modal)</a></li>
                                                            <li><a href="{{ url('wpee') }}" onclick="ga('send', 'event', 'Header Menu', 'wpee-product', '{{ url()->current() }}' );">WPEE (Wakil Penjamin Emisi Efek)</a></li>
                                                            {{--<li><a href="{{ url('waiver-product') }}">Program Waiver 2016 <span class="tip yellow">promo</span></a></li>--}}
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">Data Pasar Modal</span>
                                                        <ul class="sub-menu">
                                                            {{-- <li><a href="{{ url('data-historis') }}" onclick="ga('send', 'event', 'Header Menu', 'data-historis', '{{ url()->current() }}' );">Data Historis</a></li> --}}
                                                            <li><a href="{{ url('datapasarmodal') }}" onclick="ga('send', 'event', 'Header Menu', 'layanan-data', '{{ url()->current() }}' );">Layanan Data Pasar Modal</a></li>
                                                            <li><a href="{{ url('perpustakaan') }}" onclick="ga('send', 'event', 'Header Menu', 'perpustakaan', '{{ url()->current() }}' );">Perpustakaan Pasar Modal</a></li>
                                                            <li><a href="{{ url('permintaan-data') }}" onclick="ga('send', 'event', 'Header Menu', 'permintaan-data', '{{ url()->current() }}' );">Permintaan Data</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">Pengembangan Profesi</span>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{ route('cmpdp') }}" onclick="ga('send', 'event', 'Header Menu', 'cmpdp', '{{ url()->current() }}' );">Capital Market Profesional Development Program</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">Edukasi Investasi</span>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{ route('cmk') }}" onclick="ga('send', 'event', 'Header Menu', 'cmk', '{{ url()->current() }}' );">Cerdas Mengelola Keuangan</a></li>
                                                            <li><a href="{{ route('rivan') }}" onclick="ga('send', 'event', 'Header Menu', 'rivan', '{{ url()->current() }}' );">Seminar Bersama Rivan Kurniawan</a></li>
                                                            <li><a href="{{ route('ppl') }}" onclick="ga('send', 'event', 'Header Menu', 'ppl', '{{ url()->current() }}' );">Program Pendidikan Lanjutan (PPL)</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle sticky-menu-active" href="#">Akademik<i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('jadwal-kegiatan') }}" onclick="ga('send', 'event', 'Header Menu', 'konfirmasi-pembayaran', '{{ url()->current() }}' );">Jadwal Kegiatan</a></li>
                                <li><a href="http://akademik.ticmi.co.id/index/confirm.html" onclick="ga('send', 'event', 'Header Menu', 'konfirmasi-pembayaran', '{{ url()->current() }}' );">Konfirmasi Pembayaran</a></li>
                                <li><a href="https://old.ticmi.co.id/daftarujianulang" onclick="ga('send', 'event', 'Header Menu', 'daftar-ujian-ulang', '{{ url()->current() }}' );">Daftar Ujian Ulang</a></li>
                                <li><a href="{{ url('standar-kelulusan') }}" onclick="ga('send', 'event', 'Header Menu', 'standar-kelulusan', '{{ url()->current() }}' );">Standar Kelulusan Sertifikasi</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle sticky-menu-active" href="#">
                                Data Pasar Modal
                                <i class="fa fa-caret-down"></i>                        
                            </a>
                            <ul class="dropdown-menu">
                                <li class=""><a href="{{ route('dataemiten',['doctype'=>'ED','type'=>'ir']) }}" onclick="ga('send', 'event', 'Header Menu', 'ed-interim-report', '{{ url()->current() }}' );">Interim Report</a></li>
                                {{--<li class=""><a href="http://cmeds.ticmi.co.id/EmitenNew/AnnualReport">Annual Report</a></li>--}}
                                <li class=""><a href="{{ route('dataemiten',['doctype'=>'ED','type'=>'ar']) }}" onclick="ga('send', 'event', 'Header Menu', 'ed-annual-report', '{{ url()->current() }}' );">Annual Report</a></li>
                                <li><a href="{{ route('dataemiten',['doctype'=>'ED','type'=>'fr']) }}" onclick="ga('send', 'event', 'Header Menu', 'ed-financial-report', '{{ url()->current() }}' );">Financial Report</a></li>
                                <li><a href="{{ route('dataemiten',['doctype'=>'ED','type'=>'pr']) }}" onclick="ga('send', 'event', 'Header Menu', 'ed-prospectus', '{{ url()->current() }}' );">Prospectus</a></li>
                                <li class="dropdown-submenu">
                                    <a href="#">Financial Highlights</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'financial']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-financial-highlights', '{{ url()->current() }}' );">Financial Highlights</a></li>
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'ringkasan']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-ringkasan-perusahaan', '{{ url()->current() }}' );">Ringkasan Perusahaan</a></li>
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'statistics']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-idx-statistic', '{{ url()->current() }}' );">IDX Statistics</a></li>
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'fact']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-idx-factbook', '{{ url()->current() }}' );">IDX Factbook</a></li>
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'lq']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-lq45', '{{ url()->current() }}' );">IDX LQ45</a></li>
                                        <li><a href="{{ route('dataemiten',['doctype'=>'FH','type'=>'fh','title'=>'bond']) }}" onclick="ga('send', 'event', 'Header Menu', 'fh-bond-book', '{{ url()->current() }}' )">Bond Book</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('dataemiten.pengumuman',['doctype'=>'AN','type'=>'an']) }}" onclick="ga('send', 'event', 'Header Menu', 'an-pengumuman', '{{ url()->current() }}' )">Pengumuman</a></li>
                                <li><a href="{{ route('dataemiten.peraturan',['doctype'=>'RR','type'=>'rr']) }}" onclick="ga('send', 'event', 'Header Menu', 'rr-peraturan', '{{ url()->current() }}' )">Peraturan</a></li>
                            </ul>
                        </li>
                        <li class="dropdown mega-menu-item mega-menu-fullwidth {{ ($routename == 'faq') ? 'active':'' }}">
                            <a href="{{ url('faq') }}" class="sticky-menu-active">FAQ<i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">FAQ</span>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{ url('faq#cara-daftar-program-sertifikasi') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#cara-daftar-program-sertifikasi', '{{ url()->current() }}' )">Cara Pendaftaran Program Sertifikasi</a></li>
                                                            <li><a href="{{ url('faq#penundaan-keikutsertaan') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#penundaan-keikutsertaan', '{{ url()->current() }}' )">Penundaan Keikutsertaan Program Sertifikasi</a></li>
                                                            <li><a href="{{ url('faq#pengunduran-diri') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#pengunduran-diri', '{{ url()->current() }}' )">Pengunduran Diri dan Pengembalian Biaya Keikut-sertaan</a></li>
                                                            <li><a href="{{ url('faq#penundaan-training') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#penundaan-training', '{{ url()->current() }}' )">Penundaan Periode Training Sertifikasi</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="sub-menu menu-border">
                                                    <li>
                                                        <span class="mega-menu-sub-title">&nbsp;</span>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{ url('faq#syarat-keikutsertaan') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#syarat-keikutsertaan', '{{ url()->current() }}' )">Syarat Keikut-sertaan Program Sertifikasi</a></li>
                                                            <li><a href="{{ url('faq#pengajuan-sertifikat-ticmi') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#pengajuan-sertifikat-ticmi', '{{ url()->current() }}' )">Proses Pengajuan Sertifikat TICMI</a></li>
                                                            <li><a href="{{ url('faq#pengajuan-licensi-ojk') }}" onclick="ga('send', 'event', 'Header Menu', 'faq#pengajuan-licensi-ojk', '{{ url()->current() }}' )">Cara Mengajukan Lisensi ke OJK</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown {{ ($routename == 'karir') ? 'active':'' }}">
                            <a href="{{ url('karir') }}" class="disabled active sticky-menu-active" onclick="ga('send', 'event', 'Header Menu', 'karir', '{{ url()->current() }}' )">Karir</a>
                        </li>
                        <li class="dropdown {{ ($routename == 'hubungi-kami') ? 'active':'' }}">
                            <a href="{{ url('hubungi-kami') }}" class="disabled active sticky-menu-active" onclick="ga('send', 'event', 'Header Menu', 'hubungi-kami', '{{ url()->current() }}' )">Hubungi Kami</a>
                        </li>
                        @if(Auth::check())
                            <li class="dropdown ">
                                <a class="dropdown-toggle mobile-redirect" href="#">
                                    <i class="fa fa-user"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('dashboard') }}" onclick="ga('send', 'event', 'Header Menu', 'dashboard', '{{ url()->current() }}' )">Dashboard</a></li>
                                    <li><a href="{{ url('profile') }}" onclick="ga('send', 'event', 'Header Menu', 'profile', '{{ url()->current() }}' )">Profile</a></li>
                                    <li><a href="{{ url('password') }}" onclick="ga('send', 'event', 'Header Menu', 'password', '{{ url()->current() }}' )">Ubah Password</a></li>
                                    <li><a href="{{ url('schedule') }}" onclick="ga('send', 'event', 'Header Menu', 'sertifikasi', '{{ url()->current() }}' )">Schedule</a></li>
                                     <li><a href="{{ route('member.seminar') }}" onclick="ga('send', 'event', 'Header Menu', 'seminar-workshop', '{{ url()->current() }}' )">Seminar & Workshop</a></li>
                                    <li><a href="{{ url('sertifikasi') }}" onclick="ga('send', 'event', 'Header Menu', 'sertifikasi', '{{ url()->current() }}' )">Sertifikasi</a></li>
                                    <li><a href="{{ url('tagihan') }}" onclick="ga('send', 'event', 'Header Menu', 'sertifikasi', '{{ url()->current() }}' )">Tagihan</a></li>
                                    <li><a href="{{ route('member.paketdata') }}" onclick="ga('send', 'event', 'Header Menu', 'pembeliandata', '{{ url()->current() }}' )">Paket Data</a></li>
                                    <li><a href="{{ route('member.pembeliandata') }}" onclick="ga('send', 'event', 'Header Menu', 'pembeliandata', '{{ url()->current() }}' )">Pembelian Data Satuan</a></li>
                                    <li><a href="{{ route('member.suratriset') }}" onclick="ga('send', 'event', 'Header Menu', 'surat-riset', '{{ url()->current() }}' )">Surat Riset</a></li>
                                    <li><a href="{{ url('karir') }}" onclick="ga('send', 'event', 'Header Menu', 'sertifikasi', '{{ url()->current() }}' )">Daftar Lowongan</a></li>
                                    <li><a href="{{ url('konfirmasi') }}" onclick="ga('send', 'event', 'Header Menu', 'sertifikasi', '{{ url()->current() }}' )">Konfirmasi Pembayaran</a></li>
                                    {{--<li><a href="{{ url('karir') }}" onclick="ga('send', 'event', 'Header Menu', 'konfirmasi', '{{ url()->current() }}' )">Daftar Lowongan</a></li>--}}
                                    {{--<li><a href="{{ url('konfirmasi') }}" onclick="ga('send', 'event', 'Header Menu', 'konfirmasi', '{{ url()->current() }}' )">Konfirmasi Pembayaran</a></li>--}}
                                    {{--<li><a href="#" onclick="ga('send', 'event', 'Header Menu', 'simulasi-saham', '{{ url()->current() }}' )">Online Trading Simulation</a></li> --}}
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a> </li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </ul>
                            </li>
                        @else
                            <li class="dropdown mega-menu-item mega-menu-shop">
                                <a class="dropdown-toggle mobile-redirect" href="#">
                                    <i class="fa fa-user"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="mega-menu-content">
                                            {{ Form::open(['url'=>route('login')]) }}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        {{ Form::text('email','', ['class'=>'form-control','placeholder'=>'Email']) }}
                                                    </div>
                                                    <div class="form-group">
                                                        {{ Form::password('password', ['class'=>'form-control','placeholder'=>'Password']) }}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Login',['class'=>'btn btn-lg btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="plain" onclick="ga('send', 'event', 'Header Menu', 'forgot-password', '{{ url()->current() }}' )"><span class="text-danger">Lupa Pasword ?</span></a>
                                                    </div>
                                                    <div>
                                                        Belum punya akun ?
                                                        <a href="#" class="plain"  onclick="ga('send', 'event', 'Header Menu', 'registration', '{{ url()->current() }}' )"> <span class="text-danger">Silahkan Daftar</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>


                </nav>
            </div>
        </div>
    </header><!-- Header Ends -->
@if(Auth::check())
    <div id="dashboard-banner" class="page-header typo-dark" style="background: url('{{ asset("assets/images/banner/profile.jpg") }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Dashboard</h3>
                        <h4 class="sub-title">Hallo {{ Auth::user()->nama }}</h4>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div>
@endif
