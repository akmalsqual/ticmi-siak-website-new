<ul class="page-breadcrumb breadcrumb the-breadcrumb">

    <script>
    $(document).ready(function(){

        var menu_open   = $('.the-sidebar').find('li.open');
        var len_menu    = $(menu_open).length;
        var breadcumb   = '';


        // FIX BUGS : submenu2 not 'open'
    	// check have 3 menu
    	// if have, add class 'open' in submenu2
    	ishave_3menu = $('.the-sidebar').find('li.active > ul.sub-menu').css('display') == 'block';
    	if(ishave_3menu == true){
        	menu_no2 	= $('.the-sidebar').find('li.active ul.sub-menu[style="display:block"]').parent();
        	li	 		= $(menu_no2).closest('li').addClass('open active');

        	// refresh var, because previously we add class 'open'
    	    var menu_open   = $('.the-sidebar').find('li.open');
    	    var len_menu    = $(menu_open).length;
    	}

        $.each(menu_open, function(i, v){ // i = 0
            i1      = (parseInt(i) + 1);
            href    = $(v).find('a').attr('href');
            text    = $(v).find('a:eq(0)').text().trim();

            // first
            if(i1 == 1){
            	// only have 1 menu
            	// without icon '>' at the end of breadcumb
            	if(len_menu == 1){
            		have_menu = '<a href="'+ href +'">'+ text +'</a>';
            	}else if(len_menu > 1){
            		have_menu = '<a href="#">'+ text +'&nbsp;</a><i class="fa fa-circle"></i>&nbsp;';
            	}

                breadcumb += '<li>'+
                                have_menu +
                            '</li>';
            }
            // middle
            else if(i1 > 1 && i1 < len_menu){
                breadcumb += '<li>'+
                                '<a href="#">'+ text +'&nbsp;</a><i class="fa fa-circle"></i>&nbsp;'+
                            '</li>';
            }
            // last
            else if(i1 == len_menu){
                breadcumb += '<li>'+
                                '<a href="'+ href +'">'+ text +'</a>'+
                            '</li>';
            }
        });

    	// delete first tmp icon
    	$('.the-breadcrumb i.fa.fa-home').replaceWith('');

        // append breadcumb list to first - breadcumb
        $('.the-breadcrumb').prepend(breadcumb);
    });        
    </script>

</ul>