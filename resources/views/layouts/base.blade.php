<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ config('app.locale') }}">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', '') }}</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Aplikasi SIAK - TICMI" name="description" />
    <meta content="" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- TICMI ASSETS -->
    <link href="{{ asset('assets/ticmi/css/lib/libs.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/ticmi/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/ticmi/css/default.css') }}" rel="stylesheet">
    <!-- END TICMI ASSETS -->
    <!-- FAVICON -->
    <link rel="shortcut icon" href="favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/ticmi/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/ticmi/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/ticmi/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/ticmi/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/ticmi/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/ticmi/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/ticmi/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/ticmi/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/ticmi/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/ticmi/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/ticmi/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/ticmi/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/ticmi/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/ticmi/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/ticmi/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- END FAVICON -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/css/pikaday.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/theme/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/ticmi/css/lib/pickaday/pikaday.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/ticmi/css/lib/pickaday/theme.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/ticmi/css/lib/pickaday/triangle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/theme/global/css/custom.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/theme/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/theme/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('assets/theme/pages/css/login.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- jquery -->
    <script src="{{ asset('assets/theme/global/plugins/jquery.min.js') }}"></script>
	
            <!--[if lt IE 9]>
            <script src="{{ asset('assets/theme/global/plugins/respond.min.js') }}"></script>
            <script src="{{ asset('assets/theme/global/plugins/excanvas.min.js') }}"></script>
            <script src="{{ asset('assets/theme/global/plugins/ie8.fix.min.js') }}"></script>
        <![endif]-->

        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('assets/theme/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- END CORE PLUGINS -->
        <script src="{{ asset('assets/theme/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}" type="text/javascript"></script>
        
        
        <script src="{{ asset('assets/theme/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/ticmi/js/lib/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/ticmi/js/lib/pikaday.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/ticmi/js/lib/pikaday.jquery.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}" type="text/javascript"></script>
        {{--<script src="{{ url('assets/ticmi/js/lib/libs.js') }}"></script>--}}
		<script>
		 
		
		</script>
        <script>
            $("document").ready(function() {
                 $('[data-toggle="popover"]').popover();

                 if ($().select2) {
                     $('.select2, .select2-copy').select2({
                         placeholder: "Pilih Profesi Anda",
                         allowClear: true
                     });
                 }
                $('.datepika').pikaday({
                    format: 'YYYY-MM-DD',
                    firstDay: 1,
                    minDate: new Date(1940, 0, 1),
                    maxDate: new Date(2020, 12, 31),
                    yearRange: [1940,2020],
                    theme: 'triangle-theme'
                });
            });
     </script>
     </head>
     @include('layouts.header')
     <!-- @before login -->
     <body class="login">

     @yield('content')

     @include('layouts.footer')
 </body>
 </html>