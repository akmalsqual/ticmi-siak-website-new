<!-- BEGIN FOOTER -->
<style>
body {
    background: #ffffff;
}

footer .widget:last-child {
    margin-bottom: 0;
}
.login .logo {
    margin: 0; 
    padding: 15px; 
    text-align: none;
}
.pad-tb-40 {
    /*margin-left: 100px !important;*/
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #ffffff !important;
    
}
.widget.no-box{
    padding: 0;
    margin-bottom: 40px;
    box-shadow: none; -webkit-box-shadow: none; -moz-box-shadow: none; -ms-box-shadow: none; -o-box-shadow: none;
}
.sro-widget {
    list-style: none outside none;
    margin-bottom: 0;
    margin-top: 0;
    padding: 0;
}
.sro-widget li {
    padding: 5px 15px;
}
footer .main-footer{
    padding: 15px 0; /* 55px 0 */
    /*padding: 63px 0 55px;*/
    /*background: #252525;*/
    background: #e13138;
    color: #fff;
}
.widget-title {
    color: #fff;
}
h5.widget-title {
    font-size: 24px;
    line-height: 27px;
    margin: 0 0 14px 0;
}
.widget-title span {
    background-color: #2196f3;
    display: block;
    height: 1px;
    margin-top: 25px;
    position: relative;
    width: 20%;
}
.btn.subs {
    background-color: #2196f3;
    color: #fff;
    height: 39px !important;
    line-height: 25px !important;
}
footer ul{
    padding-left: 0;
    list-style: none;
}
.gallery-widget li {
    float: left;
    padding: 0 2px 4px;
    width: 25%;
}
ul.social-icons {
    margin-left: 0;
    padding: 0;
    width: auto;
}
.social-icons a i {
     margin-left: 5px !important;
    color: #fff !important;
    font-size: 30px;
}
/* Copy Right Footer */
.footer-copyright {
    background: #222;
    padding: 10px 0;
    font-size: 14px;
}
.footer-copyright .logo {
    display: inherit;
}
.footer-copyright .logo img {
    position: relative;
    margin-top: -10px;
}
.footer-copyright nav {
    float: right;
    margin-top: 5px;
}

.footer-copyright nav ul {
    list-style: none;
    margin: 0;
    padding: 0;
}

.footer-copyright nav ul li {
    border-left: 1px solid #505050;
    display: inline-block;
    line-height: 12px;
    margin: 0;
    padding: 0 8px;
}
.footer-copyright nav ul li a{
    color: #969696;
}
.footer-copyright nav ul li:first-child {
    border: medium none;
    padding-left: 0;
}
.footer-copyright p {
    color: #969696;
    margin: 2px 0 0;
}

</style>
     <section class="pad-tb-40">
    <div class="container">
        <div class="row-footer">
            <!-- Column -->
            <div class="col-sm-11 col-sm-offset-1">
                <div class="callto-action text-center">
                    <div class="widget no-box text-center" style="margin: 0 auto;">
                        <ul class="sro-widget">
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ojk.go.id" target="_blank" title="OJK"><img width="100" height="60" src="{{ url('assets/images/partner/ojk.jpg') }}" class="img-responsive" alt="OJK" style="margin-top: 30px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://idx.co.id" target="_blank" title="IDX BEI"><img width="60" height="60" src="{{ url('assets/images/partner/idx.png') }}" class="img-responsive" alt="IDX" style="width: 90px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://kpei.co.id" target="_blank" title="KPEI"><img width="60" height="60" src="{{ url('assets/images/partner/kpei.png') }}" class="img-responsive" alt="Thumb" style="width: 80px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ksei.co.id" target="_blank" title="KSEI"><img width="60" height="60" src="{{ url('assets/images/partner/ksei.png') }}" class="img-responsive" alt="Thumb" style="width: 100px;margin-top: 35px;"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://www.pefindo.com" target="_blank" title="PEFINDO"><img width="60" height="60" src="{{ url('assets/images/partner/pefindo.jpg') }}" class="img-responsive" alt="Thumb" style="width: 130px;margin-top: 5px"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://ibpa.co.id" target="_blank" title="IBPA"><img width="60" height="60" src="{{ url('assets/images/partner/ibpa.png') }}" class="img-responsive" alt="Thumb" style="width: 85px; margin-top: 40px"></a>
                                </div>
                            </li>
                            <li style="float: left;">
                                <div class="thumb-wrap">
                                    <a href="http://www.indonesiasipf.co.id" target="_blank" title="SIPF"> <img width="60" height="60" src="{{ url('assets/images/partner/sipf.png') }}" class="img-responsive" alt="Thumb" style="width: 85px;margin-top: 25px;"></a>
                                </div>
                            </li>
                        </ul><!-- Thumbnail Widget -->
                    </div><!-- Widget -->

                </div>
            </div><!-- Column -->
        </div><!-- row -->
    </div><!-- Container -->
</section><!-- Section -->


<footer id="footer" class="footer-1">
    <div class="main-footer widgets typo-light">
        <div class="container">
            <div class="row">
                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget subscribe no-box  hidden-xs">
                        <h5 class="widget-title">Newsletter<span></span></h5>
                        <p class="form-message1" style="display: none;"></p>
                        <div class="clearfix"></div>
                        <p>Silahkan daftar Newsletter kami untuk mendapatkan berita terbaru mengenai TICMI.</p>
                        <form class="input-group subscribe-form bv-form" name="subscribe-form" method="post" action="" id="subscribe-form" novalidate="novalidate"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                            <div class="form-group has-feedback">
                                <input class="form-control" type="email" placeholder="Subscribe" value="" name="subscribe_email" data-bv-field="subscribe_email"><i class="form-control-feedback bv-no-label" data-bv-icon-for="subscribe_email" style="display: none;"></i>
                                <small class="help-block" data-bv-validator="notEmpty" data-bv-for="subscribe_email" data-bv-result="NOT_VALIDATED" style="display: none;">Email is required. Please enter email.</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="subscribe_email" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a correct email address.</small></div>
                            <span class="input-group-btn">
                                <a class="btn subs"><span class="glyphicon glyphicon-arrow-right"></span></a>
                            </span>
                        </form>
                    </div><!-- Widget -->
                    <!-- Widget -->
                    <div class="widget no-box text-center">
                        <a class="logo" href="http://ticmi.co.id">
                            <img src="{{ url('assets/images/default/logo-ticmi.jpg') }}" width="190" height="0" class="img-rounded img-responsive" alt="Universh Education HTML5 Website Template">
                        </a>
                    </div><!-- Widget -->
                </div><!-- Column -->


                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget no-box">
                        <h5 class="widget-title">Alamat<span></span></h5>
                        <address>
                            <strong>PT. Indonesian Capital Market Electronic Library</strong> <br>
                            Indonesian Stock Exchange Building, Tower II, 1st Floor
                            Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190 <br>
                            <abbr title="Call Center">Call Center</abbr> : 0800 100 9000 (Toll Free) <br>
                            <abbr title="Phone">P </abbr> : (021) 515 23 18 <br>
                            <abbr title="Email">E </abbr> : info@ticmi.co.id

                        </address>
                    </div><!-- Widget -->
                    <!-- Widget -->
                    <div class="widget no-box">
                        <h5 class="widget-title">Follow Us<span></span></h5>
                        <!-- Social Icons Color -->
                        <ul class="social-icons color">
                            <a href="http://www.facebook.com/TICMInow" target="_blank" title="Facebook"><i class="fa fa-facebook-square"></i></a>
                            <a href="https://twitter.com/TICMInow" target="_blank" title="Twitter"><i class="fa fa-twitter-square"></i></a>
                            <a href="https://www.youtube.com/channel/UCtJ15ddZV_JhHtPXa-Jj9Lw" target="_blank" title="Youtube"><i class="fa fa-youtube-square"></i></a>
                            <a href="https://www.instagram.com/TICMInow" target="_blank" title="Instagram"><i class="fa fa-instagram-square"></i></a>
                            <li class="telegram"><a href="http://t.me/sonyaticmibot" target="_blank" title="Sonya Ticmi Bot">Sonya Ticmi Bot</a></li>
                        </ul>
                    </div><!-- Widget -->
                </div><!-- Column -->

                <!-- Widget Column -->
                <div class="col-md-4">
                    <!-- Widget -->
                    <div class="widget gallery-widget no-box">
                        <h5 class="widget-title">Galleri TICMI<span></span></h5>
                        <!-- Gallery Widget -->
                        <ul>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3824.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Reuni Alumni TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3824.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Temu kangen alumni TICMI"></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3758a.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama DSN MUI & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3758a.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Konfrensi Pers & Penandatanganan Perjanjian kerjasama DSN MUI dengan TICMI"></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3720.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama DSN MUI & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/SAM_3720.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Sambutan Direktur TICMI Yulianto Aji Sadono dalam acara Penandatanganan Perjanjian kerjasama DSN MUI dengan TICMI "></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_1014.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama DSN MUI & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_1014.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Foto bersama perwakilan DSN MUI & Direksi TICMI"></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_1004.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama UNISKA & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_1004.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Foto Bersama perwakilan UNISKA & Direksi TICMI "></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0997.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama UNISKA & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0997.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Penandatanganan MOU Kerjasama antara UNISKA & TICMI"></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0996.JPG') }}" data-rel="prettyPhoto[portfolio]" title="Kerjasama UNISKA & TICMI"><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0996.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt="Pembukaan Perdagangan oleh UNISKA"></a></li>
                            <li><a href="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0991.JPG') }}" data-rel="prettyPhoto[portfolio]" title=""><img width="60" height="60" src="{{ url('assets/images/gallery/wisuda-mei-2016/IMG_0991.JPG') }}" style="width: 100px; height: 60px;" class="img-responsive" alt=""></a></li>
                        </ul>
                        <p><a href="#" title="glorythemes">@TICMIJAKARTA</a></p>
                    </div><!-- Widget -->
                </div><!-- Column -->

            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Main Footer -->

    <!-- Footer Copyright -->
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <!-- Copy Right Logo -->
                <!--<div class="col-md-2">-->
                <!--<a class="logo" href="#">-->
                <!--<img src="./assets/images/default/logo-ticmi.jpg" width="120" height="0" class="img-rounded img-responsive" alt="Universh Education HTML5 Website Template">-->
                <!--</a>-->
                <!--</div>-->
                <!-- Copy Right Logo -->
                <!-- Copy Right Content -->
                <div class="col-md-9">
                    <p>© Copyright 2016. All Rights Reserved. | By <a href="http://ticmi.co.id" title="TICMI">The Indonesia Capital Market Institute</a></p>
                </div><!-- Copy Right Content -->
                <!-- Copy Right Content -->
                <div class="col-md-3">
                    <nav class="sm-menu text-center">
                        <ul>
                            <li><a href="{{ url('faq') }}">FAQ's</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="{{ url('hubungi-kami') }}">Hubungi Kami</a></li>
                        </ul>
                    </nav><!-- Nav -->
                </div><!-- Copy Right Content -->
            </div><!-- Footer Copyright -->
        </div><!-- Footer Copyright container -->
    </div><!-- Footer Copyright -->
</footer>
<!-- Footer -->
<!-- END FOOTER -->

<script type="text/javascript">

	// ========
	// GLOBAL
	// ========

    // auto width select2 (text wrapping)
    // $(document).ready(function(){
        // $("select").select2({ width: 'resolve' });
    // });
</script>