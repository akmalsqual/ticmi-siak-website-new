            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar the-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                        <li class="nav-item {{ Request::path() == 'dashboard' ? 'active open' : '' }}">
                            <a href="{{ URL::to('dashboard') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                {{-- <span class="selected"></span> --}}
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'profile' ? 'active open' : '' }}">
                            <a href="{{ URL::to('profile') }}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'password' ? 'active open' : '' }}">
                            <a href="{{ URL::to('password') }}" class="nav-link nav-toggle">
                                <i class="icon-key"></i>
                                <span class="title">Ubah Password</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'schedule' ? 'active open' : '' }}">
                            <a href="{{ URL::to('schedule') }}" class="nav-link nav-toggle">
                                <i class="glyphicon glyphicon-calendar"></i>
                                <span class="title">Schedule</span>
                            </a>
                        </li>
                         <li class="nav-item {{ Request::path() == 'member/seminar' ? 'active open' : '' }}">
                            <a href="{{ route('member.seminar') }}" class="nav-link nav-toggle">
                                <i class="icon-tag"></i>
                                <span class="title">Seminar dan Workshop</span>
                            </a>
                        </li>
                        <li class="nav-item sertifikasi {{ Request::path() == 'sertifikasi' ? 'active open' : '' }}">
                            <a href="{{ URL::to('sertifikasi') }}" class="nav-link nav-toggle">
                                <i class="icon-graduation"></i>
                                <span class="title">Sertifikasi</span>
                            </a>
                        </li>  
                        <li class="nav-item tagihan {{ Request::path() == 'tagihan' ? 'active open' : '' }}">
                            <a href="{{ URL::to('tagihan') }}" class="nav-link nav-toggle">
                                <i class="icon-notebook"></i>
                                <span class="title">Tagihan</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'paket-data' ? 'active open' : '' }}">
                            <a href="{{ route('member.paketdata') }}" class="nav-link nav-toggle">
                                <i class="icon-list"></i>
                                <span class="title">Paket Data</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'member/riwayat-pembelian-data' ? 'active open' : '' }}">
                            <a href="{{ route('member.pembeliandata') }}" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Pembelian Data Satuan</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'member/surat-riset' ? 'active open' : '' }}">
                            <a href="{{ route('member.suratriset') }}" class="nav-link nav-toggle">
                                <i class="icon-list"></i>
                                <span class="title">Surat Riset</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::path() == 'karir' ? 'active open' : '' }}">
                            <a href="{{ URL::to('karir') }}" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Daftar Lowongan</span>
                            </a>
                        </li>
                        <li class="nav-item konfirmasi{{ Request::path() == 'konfirmasi' ? 'active open' : '' }}">
                            <a href="{{ URL::to('konfirmasi') }}" class="nav-link nav-toggle">
                                <i class="icon-basket"></i>
                                <span class="title">Konfirmasi Pembayaran</span>
                            </a>
                        </li>  
                        <li class="nav-item  ">
                         <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        	<i class="icon-logout"></i>
                        	<span class="title">Logout</span>
	                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                        	{{ csrf_field() }}
	                        </form>
                                
                         </a>
                        </li>          
                    </ul>
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->