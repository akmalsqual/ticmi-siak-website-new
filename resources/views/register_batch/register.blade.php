@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <style>
    .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group {
        height: 38px!important;
    }


    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Pendaftaran Batch : {{$batch->nama}}</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            {!! Form::open(['route' => ['register-batch.save', $batch_id], 'id' => 'form_pendaftaran_batch', 'files'=> true]) !!}
                            {{method_field('PATCH')}}
                            	<h4 class="title" style="margin-top: 0px !important; margin-bottom: 20px;"><i class="fa fa-angle-double-right"></i> &nbsp;Informasi Peserta</h4>
                            	<hr>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text('nama', $peserta->nama, ['class' => 'form-control', 'data-toggle' => 'tooltip','readonly'=>'readonly']) }}
                                                                <small class="form-note" style="color: black;">Masukkan nama sesuai dengan Kartu Identitas Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport" data-original-title="" title=""><span class="fa fa-question-circle"></span></a> </small>
                                                                {{-- {{ Form::text('nama', $peserta->nama, ['class' => 'form-control', 'data-container' => 'body', 'data-placement' => 'top', 'data-original-title' => 'Nama Sesuai KTP']) }} --}}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Gender</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::select('sex', $sex, !empty($profiles->sex_id) ? $profiles->sex_id : null , ['class' => 'form-control select2 sex','readonly'=>'readonly']) }}
                                                                    {!! $errors->first('status', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Handphone</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('no_hp', !empty($peserta->no_hp) ? $peserta->no_hp : null , ['class' => 'form-control','readonly'=>'readonly']) }}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Tanggal Lahir</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <div class="input-group date">
                                                                        {{ Form::text('tgl_lahir', $tgl_lahir, ['class' => 'form-control date-pickers','readonly'=>'readonly']) }}
                                                                        <div class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-th"></span>
                                                                        </div>
                                                                    </div>
                                                                    {!! $errors->first('date_pickers', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group foto">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Foto</td>
                                                                <td>:</td>
                                                                <td>
                                                                    
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                            <img src="{{ !empty($profiles->photo) ? 
                                                                                asset('assets/upload_files/peserta/photos/' .$profiles->photo) : asset('assets/images/default/no-image.png') }}" alt="foto">
                                                                        </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;">
                                                                            
                                                                        </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="foto"> </span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                        <div class="clearfix margin-top-10">
                                                                            <span class="label label-danger">NOTE!</span> <span class="required" style="color: black;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                            <br />
                                                                            <small class="form-note" style="color: black;">Foto yang Anda masukkan akan digunakan untuk sertifikat Anda<a href="#" onclick="return false" data-container="body" ></a> </small>
                                                                        </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Alamat Sekarang</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::textarea(null, !empty($peserta->alamat_tinggal_skr) ? $peserta->alamat_tinggal_skr : null, ['class' => 'form-control', 'readonly' => 'readonly', 'size' => '-x-']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> --}}
                                    </div><!--/row-->
                                </div><!--/@left-->

                                <!--@right-->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Tempat Lahir</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::textarea('tempat_lahir', !empty($profiles->tempat_lahir) ? $profiles->tempat_lahir : null , ['class' => 'form-control', 'size' => '-x-']) }}
                                                                    {!! $errors->first('tempat_lahir', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Alamat Berdasarkan KTP</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::textarea('alamat_ktp', !empty($profiles->alamat_ktp) ? $profiles->alamat_ktp : null, ['class' => 'form-control', 'size' => '-x-']) }}
                                                                    {!! $errors->first('alamat_ktp', '<span class="help-block form-messages">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Lokasi</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('lokasi', $lokasi, !empty($profiles->lokasi_id) ? $profiles->lokasi_id : null, ['class' => 'form-control select2 lokasi']) }}
                                                                {!! $errors->first('lokasi', '<span class="help-block form-messages">:message</span>'); !!}
                                                                <span id="loading-lokasi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>KP</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('kp', $kp, !empty($profiles->lembg_pdkn_id) ? $profiles->lembg_pdkn_id : null, ['class' => 'form-control select2 kp']) }}
                                                                {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group ktp">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>KTP</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                            <img src="{{ !empty($profiles->photo) ? 
                                                                                asset('assets/upload_files/peserta/ktp/' .$profiles->file_ktp) : asset('assets/images/default/no-image.png') }}" alt="ktp"> </div>
                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;"> </div>
                                                                            
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="ktp"> </span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE!</span> <span class="required" style="color:black;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                    </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tempat Lahir</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::textarea(null, !empty($peserta->tempat_lahir) ? $peserta->tempat_lahir : null , ['class' => 'form-control', 'readonly' => 'readonly', 'size' => '-x-']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    	<div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text(null, $peserta->email, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> --}}
                                       {{--  <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No Telp</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text(null, !empty($peserta->tlp_rumah) ? $peserta->tlp_rumah : null , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> --}}
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Pendidikan Terakhir</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text(null, !empty($peserta->pendidikan) ? $peserta->pendidikan : null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                {!! $errors->first('pendidikan', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Jurusan</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text(null, !empty($peserta->jurusan) ? $peserta->jurusan : null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                {!! $errors->first('jurusan', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                           <div class="form-group">
                                           		<table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Universitas</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::text(null, !empty($peserta->universitas) ? $peserta->universitas : null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                {!! $errors->first('universitas', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> --}}
                                    </div><!--/@right-->
                                 </div>
							    <div class="clearfix"></div>
                                <br />

                                <div class="row">
                                <h4 class="title" style="margin-top: 0px !important; margin-bottom: 20px;"><i class="fa fa-angle-double-right"></i> &nbsp;Batch</h4>
                                <hr>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Nama</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('nama_batch', $batch->nama , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                    {!! $errors->first('nama_batch', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Harga</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('harga', \Helper::number_formats($batch->harga, 'view', 0) , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                    {!! $errors->first('harga', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><!--@right-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Program</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text(null, $program , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                    {!! $errors->first('program', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><!--@right-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Kantor Perwakilan</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text(null, $lembg_pdkn , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                    {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><!--@right-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-siak borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td>Tanggal Mulai Pelatihan</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ Form::text('tgl_mulai', $tgl_mulai , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                                    {!! $errors->first('tgl_mulai', '<span class="help-block form-messages">:message</span>'); !!}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <br />


                            <div class="row">

								<h4 class="title" style="margin-top: 0px !important; margin-bottom: 20px;"><i class="fa fa-angle-double-right"></i> &nbsp;Detail Pendaftaran</h4>
                            	<hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Status Pendaftar</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::select('status', $status, null , ['class' => 'form-control select2 sex']) }}
                                                                {!! $errors->first('status', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="syarat hidden">
                                                @foreach($persyaratan as $pst)
                                                        <div class="form-group">
                                                            <table class="table table-siak borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>{{ $pst->nama }}</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            {{ Form::select($pst->persyaratan_id, $jawaban, null , ['class' => 'form-control select2 form-syarat']) }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group foto">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Ijazah</td>
                                                            <td>:</td>
                                                            <td>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                                        <img src="{{ asset('assets/images/default/no-image.png') }}" alt="ijazah">
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 130px;">
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="ijazah"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix margin-top-10">
                                                                    <span class="label label-danger">NOTE!</span> <span class="required" style="color:black;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        @if($batch->program_id == 4) {{-- jika program batch tersebut wppe --}}
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>No Reg WPPE</td>
                                                            <td>:</td>
                                                            <td>
                                                                   {{ Form::text('no_reg_wppe', null , ['class' => 'form-control']) }}
                                                                    {!! $errors->first('no_reg_wppe', '<span class="help-block form-messages">:message</span>'); !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                        <div class="form-group form-voucher">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Voucher</td>
                                                        <td>:</td>
                                                        <td>
                                                                <div class="input-group">
                                                                    <input class="form-control" type="text" name="voucher" placeholder="Masukkan Kode">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" id="voucher-submit" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-left">
                                                                            {{-- <i class="fa fa-arrow-left fa-fw"></i> --}}
                                                                            <span class="ladda-label"> Check </span>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="berhasil hidden alert alert-success">
                                            <div id="ket_voucher">
                                                <strong>Anda Mendapatkan Voucher :</strong> Rp <span id="total_voucher"></span>
                                                <input type="hidden" name="time_voucher" id="time_voucher"></input>
                                                <input type="hidden" name="tipe_inserted"></input>
                                                <input type="hidden" name="id_inserted"></input>
                                            </div>
                                            <input type="hidden" name="voucher_id"> 
                                        </div>

                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td><strong>Total yang dibayar</strong></td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::text('total', \Helper::number_formats($batch->harga, 'view', 0) , ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            {{ Form::hidden('status_bayar', '' , ['class' => 'form-control']) }}
                                                            {!! $errors->first('status_bayar', '<span class="help-block form-messages">:message</span>'); !!}

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
							

                            <div class="clearfix"></div>
                        </div>
                            <br>

   
                            <div class="form-group">
                                <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                <button type="submit" name="simpan" class="btn red btn-block"><h4>Setuju</h4></button>
                            </div>
                          {!! Form::close() !!}

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/theme/global/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/theme/global/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

    {!! JsValidator::formRequest('App\Http\Requests\Daftar_batchRequest', '#form_pendaftaran_batch'); !!}
    
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_pendaftaran_batch').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_pendaftaran_batch').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

        $(document).ready(function(){
            var voucherTime = 120; // WAKTU YANG DIGUNAKAN UNTUK RESET VOUCHER (PER DETIK)//
            var realPrice   = $('input[name="harga"]').val(); // menyimpan harga asli batch //

            $("select[name='sex']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_gender') }}" });
            $("select[name='status']").select2({ placeholder: "{{ '-- Pilih Status Pendaftar --' }}" });
            $("select[name='lokasi']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lokasi') }}" });
            $("select[name='kp']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lembg_pdkn') }}" });

            
              $('[data-toggle="popover"]').popover();
            

            $("select[name='lokasi']").on('change', function(){
                var lokasi = $(this).val().trim();

                $('#loading-lokasi').css({'display': 'block'});

                // reset if filled
                $("select[name='kp']").html('');

                $.ajax({
                    method: 'GET',
                    url: '{{ route('profile.ajax_get_kp') }}',
                    data: {lokasi: lokasi},
                    success: function(msg){
                        $('#loading-lokasi').css({'display': 'none'});
                        obj_kp  = JSON.parse(msg);
                        list_kp = "<option value=''></option>";

                        if(obj_kp.length > 0){
                            $.each(obj_kp, function(i,v){    
                                list_kp += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                            });
                        }else{
                            list_kp = "<option></option>";
                        }

                        
                        $("select[name='kp']").html(list_kp);
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                        $('#loading-lokasi').css({'display': 'none'});
                    }
                });
            });

            $('input[name="voucher"]').on('input', function(e){
                // keyword = $(this).val().trim();
                this.value = this.value.toLocaleUpperCase();
            })

            $('input[name="voucher"]').on('keydown', function(e){
                if (e.keyCode == 32) {
                    alert('Nama voucher tidak boleh menggunakan spasi');
                    return false;
                }
            })

            $(function() {
                $('#voucher-submit').click(function(e){
                    e.preventDefault();
                    var data     = $('input[name="voucher"]').val();
                    var batch_id = {{ $batch_id }};
                    $('input[name="voucher"]').attr('disabled', 'disabled');
                    var l = Ladda.create(this);
                    l.start();
                    $.get("{{ route('register-batch.cek_voucher') }}", 
                        { 
                            data : data,
                            batch_id : batch_id
                        },
                      function(response){
                        $('input[name="voucher"]').attr('disabled', false);
                        if('success' in response) {

                            $('.form-voucher').addClass('hidden');
                            $('.berhasil').removeClass('hidden');
                            harga_batch = $('input[name="harga"]').val(). <?= \Helper::number_formats('', 'clear_formatted_js', 0, '.'); ?>;
                            /* jika nilai voucher menggunakan potongan persen */
                            if(response.success.nilai_persen != null){
                                total_potongan = (parseInt(harga_batch)*parseInt(response.success.nilai_persen))/100;
                                total_bayar    = harga_batch - total_potongan;
                            /* jika nilai voucher menggunakan potongan rupiah */
                            } else {
                                total_potongan = parseInt(response.success.nilai);
                                total_bayar    = harga_batch - total_potongan;
                            }
                            
                            $('input[name="tipe_inserted"]').val(response.tipe);
                            $('input[name="id_inserted"]').val(response.idInserted);

                            $('#total_voucher').html(total_potongan);
                            $('input[name="voucher_id"]').val(response.success.voucher_id);
                            $('#total_voucher'). <?= \Helper::number_formats('', 'dynamic_var_jquery', 0) ?>
                            $('input[name="total"]').prop('value', total_bayar);
                            $('input[name="total"]'). <?= \Helper::number_formats('', 'dynamic_var_jquery', 0) ?>
                            //$('#time_voucher').val(voucherTime).trigger('change');

                            resetVoucher(voucherTime, response.tipe, response.idInserted);

                            if('status_bayar' in response) {
                                if(response.status_bayar == 'gratis')
                                {
                                    $('#ket_voucher').html('<strong>Anda Menggunakan Voucher Perusahaan</strong>');
                                    $('input[name="status_bayar"]').val('gratis');
                                    $('input[name="total"]').prop('value', '0');

                                    resetVoucher(voucherTime, response.tipe, response.idInserted);
                                }
                            }
                        } else {
                            swal(response.error, "", "error");
                        }

                      }, "json")
                    .always(function() { l.stop(); });
                    return false;
                });
            });

            $("select[name='status']").on('change', function(){

                var status = $(this).val().trim();
                /** jika status pendaftar mahasiswa atau dosen tampilkan form nilai **/
                if(status == 2 || status == 3){
                    $('.syarat').removeClass('hidden');
                    $('.form-syarat').attr('disabled', false);
                } else {
                    $('.syarat').addClass('hidden');
                    $('.form-syarat').attr('disabled', 'disabled');
                }
    
            });


            function resetVoucher(time, type, idInserted){
                var interval = setInterval(function(){
                    var totalWaktu = time;
                    var jam        = Math.floor(totalWaktu / 3600);
                    totalWaktu    %= 3600;
                    menit          = Math.floor(totalWaktu / 60);
                    detik          = totalWaktu % 60; 

                    $('#jam').text(jam);
                    $('#menit').text(menit);
                    $('#detik').text(detik);
                    time -= 1;

                    if(time == 0){
                        $('#total_voucher').html('');
                        $('input[name="voucher_id"]').val('');
                        $('#total_voucher').html('');
                        $('input[name="total"]').prop('value', realPrice);
                        $('input[name="total"]'). <?= \Helper::number_formats('', 'dynamic_var_jquery', 0) ?>
                        $('#time_voucher').val('');
                        $('#time_voucher').val(voucherTime).trigger('change');
                        $('#ket_voucher').html('<strong>Anda Mendapatkan Voucher :</strong> Rp <span id="total_voucher"></span><input type="hidden" name="time_voucher" id="time_voucher">');
                        $('input[name="status_bayar"]').val('');
                        $('input[name="voucher"]').val('');
                        $('.form-voucher').removeClass('hidden');
                        $('.berhasil').addClass('hidden');

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'get',
                            dataType: 'json',
                            url: "{{ route('register-batch.reset_voucher') }}",
                            data: {
                                type: type,
                                idInserted: idInserted
                            },
                            success: function(data){
                                if(data == 'failed'){
                                    alert('Failed reset voucher');
                                }
                            }
                        });
                    }
                }, 1000);
            }

            // $( window ).on('beforeunload', function(){
            //     var typeInserted = $('input[name="tipe_inserted"]').val();
            //     var idInserted   = $('input[name="id_inserted"]').val();
            //     resetVoucher(voucherTime, typeInserted, idInserted);
            // });
            // window.addEventListener("beforeunload", function (e) {
            //   var typeInserted = $('input[name="tipe_inserted"]').val();
            //   var idInserted   = $('input[name="id_inserted"]').val();

            //   // if(typeInserted != '' && idInserted != ''){
            //     var reset        = resetVoucher(voucherTime, typeInserted, idInserted);
            //     (e || window.event).returnValue = reset;
            //     return reset;
            //   //}
              
            // });
            /** reset voucher  when user leave page **/
            window.onbeforeunload = function(){
                if(!window.isRefresh) {
                    var typeInserted = $('input[name="tipe_inserted"]').val();
                    var idInserted   = $('input[name="id_inserted"]').val();

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        dataType: 'json',
                        async: true,
                        url: "{{ route('register-batch.reset_voucher') }}",
                        data: {
                            type: typeInserted,
                            idInserted: idInserted
                        },
                        success: function(data){
                            if(data == 'failed'){
                                alert('Failed reset voucher');
                            }
                        }
                    });
                }

                window.isRefresh = false;
            }

        });
    
        <?= \Helper::date_formats('$(".date-pickers")', 'js') ?>
    </script>
@endsection