@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Pendaftaran Batch : {{$batch->nama}}</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            {!! Form::open(['route' => ['register-batch.save', $batch_id], 'id' => 'form_pendaftaran_batch', 'files'=> true]) !!}
                            {{method_field('PATCH')}}
                            	<h4 class="title" style="margin-top: 0px !important; margin-bottom: 20px;"><i class="fa fa-angle-double-right"></i> &nbsp;Detail</h4>
                            	<hr>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: left; width: 180px !important;">Cabang</td>
                                                            <td style="width: 10px !important; text-align: center;">:</td>
                                                            <td>
                                                                {{ Form::text(null, null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: left; width: 180px !important;">Email</td>
                                                            <td style="width: 10px !important; text-align: center;">:</td>
                                                            <td>
                                                                {{ Form::text(null, $peserta->email, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: left; width: 180px !important;">Status Pendaftar</td>
                                                            <td style="width: 10px !important; text-align: center;">:</td>
                                                            <td>
                                                                {{ Form::text(null, null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: left; width: 180px !important;">Memiliki No. Reg WPPE</td>
                                                            <td style="width: 10px !important; text-align: center;">:</td>
                                                            <td>
                                                                {{ Form::text(null, null, ['class' => 'form-control', 'readonly' => 'readonly']) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: left; width: 180px !important;"></td>
                                                            <td style="width: 10px !important; text-align: center;"></td>
                                                            <td>
                                                                <span id="execute-loading" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                                <button type="submit" name="simpan" class="btn red pull-right"><h4>NEXT &nbsp;<i class="fa fa-angle-double-right" style="font-size: 1em;"></i></h4></button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--/row-->
                                </div>

                            <div class="clearfix"></div>
                            <br>
                          {!! Form::close() !!}

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>


    {!! JsValidator::formRequest('App\Http\Requests\ProfileRequest', '#form_pendaftaran_batch'); !!}
    <script type="text/javascript">
        /**
        * Action before submit form
        */
        $('#form_pendaftaran_batch').on('submit', function (e) {
            // form is valid
            is_form_valid = $('#form_pendaftaran_batch').valid();
            if(is_form_valid == true){
                $('button[name="simpan"]').attr('disabled', 'disabled');
                $('#execute-loading').css({'visibility': 'visible'});
            }
        });

        $(document).ready(function(){
            
        });
    </script>
@endsection