@extends('layouts.base')
    @section('content')
<style>
.login {

    background: 
    /* top, transparent red, faked with gradient */ 
    linear-gradient(
      rgba(255,255,255,.7), 
      rgba(255,255,255,.7)
    ), url({{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }});    
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;  /*cover*/
}
.login .content-register{
	background-color: #fff;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    -ms-border-radius: 7px;
    -o-border-radius: 7px;
    border-radius: 7px;
    max-width: 750px;
    margin: 40px auto 10px;
    padding: 10px 30px 30px;
    overflow: hidden;
    position: relative;
    margin-top: -20px;
}
.select2-container {
  width:100%;   
}

.select2-selection .select2-selection--single{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
.select2-selection{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
.select2-selection--single{
	padding: 3px 0px 0px 5px !important;
	height: 35px !important;
}
</style>
        <div class="logo">
            <!-- nothing here -->
        </div>
        <!-- BEGIN LOGIN -->
        <div class="content-register">
            <!-- BEGIN LOGIN FORM -->
            <div class="login-form">
                {{ csrf_field() }}
                <center>
                    <img src="{{ asset('assets/theme/global/img/logo-ticmi.png') }}" class="logo-default" width="150" alt="logo">
                </center>
                <br>
                <br>
				
				<!-- Tab Register -->
				<div class="portlet light bordered">
				    <div class="portlet-title tabbable-line">
				        <div class="caption">
				            <i class="fa fa-user-plus"></i>
				            <span class="caption-subject font-dark bold uppercase"> &nbsp;Complete Your Profile</span>
				        </div>
				    </div>
				    <div class="portlet-body">
				        <div class="tab-content">

				        	<!--
				            =================================================================================================
							====================== Form SID
				            ================================================================================================= 
				            -->
				            <div class="tab-pane active" id="sid">
				                {!! Form::open(array('url' => 'complete-profile','method'=>'POST', 'files'=>true, 'id'=>'complete_profile')) !!}

				                	<!-- Perusahaan -->
				                	<div class="col-md-12 form-group">
				                		<label name="perusahaan_id" class="color-maroon">Perusahaan Anda</label>

				                		<select name="perusahaan_id" class="form-control select2">
				                			<option></option>
				                			@foreach($perusahaan as $ps)
				                				<option value="{{ $ps->perusahaan_id }}">{{ $ps->nama }}</option>
				                			@endforeach
				                			<option value="perusahaan_lainnya">Perusahaan Lainnya</option>
				                		</select>

				                		{!! $errors->first('perusahaan_id', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Perusahaan Lainnya -->
				                	<div class="col-md-12 form-group" style="display: none;" id="perusahaan-lainnya">
				                		<label class="color-maroon">Perusahaan Lainnya</label>
				                		{!! Form::text('perusahaan_lainnya',null,['class'=>'form-control', 'placeholder'=>'-- Perusahaan Lainnya --']) !!}
				                		{!! $errors->first('perusahaan_lainnya', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Lembaga -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Lembaga Anda</label>
				                		{!! Form::select('lembaga_id',$lembaga,null,['class'=>'form-control select2']) !!}
				                		{!! $errors->first('lembaga_id', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Lokasi -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Lokasi Anda</label>
				                		{!! Form::select('lokasi_id',$lokasi,null,['class'=>'form-control select2']) !!}
				                		{!! $errors->first('lokasi_id', '<span class="help-block form-messages">:message</span>') !!}
				                		<span id="lokasi-loading" style="display: none;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
				                	</div>

				                	<!-- Lembaga Pendidikan -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Lembaga Pendidikan</label>
				                		{!! Form::select('lembg_pdkn_id',$lembaga_pdkn,null,['class'=>'form-control select2', 'disabled']) !!}
				                		{!! $errors->first('lembg_pdkn_id', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Jenis Kelamin -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Gender</label>
				                		{!! Form::select('sex_id',$jenis_kelamin,null,['class'=>'form-control select2']) !!}
				                		{!! $errors->first('sex_id', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Negara -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Negara Anda</label>
				                		{!! Form::select('negara_id',$negara,null,['class'=>'form-control select2']) !!}
				                		{!! $errors->first('negara_id', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Alamat KTP -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Alamat KTP</label>
				                		{!! Form::textarea('alamat_ktp',null,['class'=>'form-control', 'rows'=>'5']) !!}
				                		{!! $errors->first('alamat_ktp', '<span class="help-block form-messages">:message</span>') !!}
				                		<small class="form-note">Masukkan alamat sesuai dengan KTP Anda 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP"><span class="fa fa-question-circle"></span></a> 
				                		</small>
				                	</div>

				                	<!-- Pekerjaan -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Pekerjaan Anda</label>
				                		{!! Form::text('pekerjaan',null,['class'=>'form-control', 'placeholder'=>'-- Pekerjaan Anda --']) !!}
				                		{!! $errors->first('pekerjaan', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Jurusan -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Jurusan Anda</label>
				                		{!! Form::text('jurusan',null,['class'=>'form-control', 'placeholder'=>'-- Jurusan Anda --']) !!}
				                		{!! $errors->first('jurusan', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Universitas -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Universitas Anda</label>
				                		{!! Form::text('universitas',null,['class'=>'form-control', 'placeholder'=>'-- Universitas Anda --']) !!}
				                		{!! $errors->first('universitas', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Tempat Lahir -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Tempat Lahir</label>
				                		{!! Form::text('tempat_lahir',null,['class'=>'form-control', 'placeholder'=>'-- Tempat Lahir --']) !!}
				                		{!! $errors->first('tempat_lahir', '<span class="help-block form-messages">:message</span>') !!}
				                		<small class="form-note">Masukkan tempat lahir sesuai dengan Kartu Identitas Anda 
				                			<a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport"><span class="fa fa-question-circle"></span></a> 
				                		</small>
				                	</div>

				                	<!-- Telepon Rumah -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Telepon Rumah</label>
				                		{!! Form::number('tlp_rumah',null,['class'=>'form-control', 'placeholder'=>'-- No. Telp --']) !!}
				                		{!! $errors->first('tlp_rumah', '<span class="help-block form-messages">:message</span>') !!}
				                		<small class="form-note">Masukkan no Telepon yang dapat di hubungi
				                		</small>
				                	</div>

				                	<!-- Pendidikan -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Pendidikan</label>
				                		{!! Form::text('pendidikan',null,['class'=>'form-control', 'placeholder'=>'-- Pendidikan Terakhir --']) !!}
				                		{!! $errors->first('pendidikan', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Alamat Tinggal Sekarang -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Alamat Tinggal Sekarang</label>
				                		{!! Form::textarea('alamat_tinggal_skr',null,['class'=>'form-control', 'placeholder'=>'-- Alamat Tinggal Sekarang --','rows'=>'5']) !!}
				                		{!! $errors->first('alamat_tinggal_skr', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>

				                	<!-- Keterangan -->
				                	<div class="col-md-12 form-group">
				                		<label class="color-maroon">Keterangan</label>
				                		{!! Form::textarea('keterangan',null,['class'=>'form-control', 'placeholder'=>'-- Keterangan --','rows'=>'5']) !!}
				                		{!! $errors->first('keterangan', '<span class="help-block form-messages">:message</span>') !!}
				                	</div>
									
		                	        <span class="border-top-14"></span>

									<div class="col-md-12" style="margin-bottom: 20px;">
		                	            <div class="row">
		                	                <div class="col-xs-12 col-sm-12">
		                	                	<label class="color-maroon">Photo</label>
		                	                </div>
		                	                <div class="col-xs-12 col-sm-8">
		                	                    <div class="form-group">
		                	                    	{!! Form::file('photo', ['class'=>'form-control']) !!}
		                	                    	<small class="form-note">Format : Jpeg, Jpg, Png</small>
		                	                    	{!! $errors->first('photo', '<span class="help-block form-messages">:message</span>') !!}
		                	                    </div>
		                	                </div>
		                	            </div>
		                	        </div>

		                	        <div class="col-md-12 form-group">
                                        <div class="row">
                                        	<div class="col-xs-12 col-sm-12">
	                                            <small>Dengan mendaftar Anda berarti sudah menyetujui <a href="#">Kebijakan privasi TICMI</a></small>
                                        	</div>
                                        </div>
                                    </div>

				                	<div class="form-group" style="padding-left: 15px;">
                                      	{!! Form::submit('Daftar', ['class'=>'btn btn-lg btn-default btn-block']) !!}
                                      	<span id="execute-loading" style="display: none;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
				                		<a class="btn btn-danger btn-block" href="#" onclick="window.history.go(-1)">Back</a>
				                	</div>

				                {!! Form::close() !!}
				            </div>
				        </div>
				    </div>
				</div>

                <div class="create-account">
                <!-- nothing here -->
                </div>
            </div>
            <!-- END LOGIN FORM -->
            <meta name="csrf-token" content="{{ csrf_token() }}">
        </div>
        <div class="copyright"> 2017 © TICMI</div>

        {!! JsValidator::formRequest('App\Http\Requests\CompleteProfileRequest', '#complete_profile'); !!}

        <script>
        	$(document).ready(function(){

        		$("select[name='perusahaan_id']").select2({placeholder:'-- Perusahaan Anda --'});
        		$("select[name='lembaga_id']").select2({placeholder:'-- Lembaga Anda --'});
        		$("select[name='lokasi_id']").select2({placeholder:'-- Lokasi Anda --'});
        		$("select[name='sex_id']").select2({placeholder:'-- Jenis Kelamin --'});
        		$("select[name='negara_id']").select2({placeholder:'-- Negara --'});
        		$("select[name='lembg_pdkn_id']").select2({placeholder:'-- Lembaga Pendidikan --'});

        		var isi_lembg_pdkn = $("select[name='lembg_pdkn_id']").val();

        		if(isi_lembg_pdkn != null)
        		{
        			$("select[name='lembg_pdkn_id']").removeAttr('disabled');
        			$("select[name='lokasi_id']").val('');
        		}

        		// Ketika memilih perusahaan
        		$("select[name='perusahaan_id']").change(function(){
        			var isi_perusahaan = $(this).val();
        			console.log(isi_perusahaan);
        			if(isi_perusahaan == 0){
        				$('#perusahaan-lainnya').show();
        			};
        		});

        		// Ketika memilih lokasi
        		$("select[name='lokasi_id']").change(function(){
        			var lokasi = $(this).val();
        			
        			$("select[name='lembg_pdkn_id']").empty().prop('disabled', 'true');
        			$('#lokasi-loading').show();

        			$.ajax({
        				headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    type: 'POST',
					    url: "{{ url('complete-profile/select-lembg') }}",
					    data: {
					    	lokasi: lokasi
					    },
					    success: function(data){
					    	$("select[name='lembg_pdkn_id']").removeAttr('disabled');
					    	$('#lokasi-loading').hide();

					    	$.each(data.lembaga, function(i, v){
					    		console.log(v);

					    		var lembaga = '';
					    		lembaga += '<option></option>';
					    		lembaga += "<option value="+v.lembg_pdkn_id+">";
					    		lembaga += v.nama;
					    		lembaga += '</option>';

					    		$("select[name='lembg_pdkn_id']").append(lembaga);
					    	});
					    }
        			});
        		});

        		// Ketika form di submit
        		$('#complete_profile').on('submit', function (e) {
        			var perusahaan_lainnya = $("select[name='perusahaan_lainnya']").val();
        			var perusahaan_id = $("select[name='perusahaan_id']").val();

        			if((perusahaan_id == 'perusahaan_lainnya') && (perusahaan_lainnya == null)){
        				alert('perusahaan lainnya wajib di isi');
        			}

		            // form is valid
		            is_form_valid = $('#complete_profile').valid();
		            if(is_form_valid == true){
		                $('button[name="simpan"]').attr('disabled', 'disabled');
		                $('#execute-loading').show();
		            }
		        });

        	});
        </script>

@endsection
