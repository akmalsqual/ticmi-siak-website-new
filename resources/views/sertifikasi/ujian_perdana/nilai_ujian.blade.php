@extends('layouts.app')
    @section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-graduation font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Nilai Ujian Perdana</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- CONTENT HERE -->
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($ujian_detail as $dtl)
                                        @if($loop->first)
                                            <span class="label label-danger">
                                                <strong>
                                                    {{ \Helper::tgl_indo($dtl->tgl_ujian) }}
                                                </strong>
                                            </span>
                                            <div class="alert alert-info">
                                        @endif
                                            {{ $dtl->nama_modul }}::{{ $dtl->nilai }}
                                            <br>
                                        @if($loop->last)
                                            </div>
                                        @endif
                                    @endforeach
                                    
                                </div>
                                
                                    
                            </div><!--/row-->
                        </div>

                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">

        $(document).ready(function(){
            $('.sertifikasi').addClass('active open');
        });
    </script>
@endsection