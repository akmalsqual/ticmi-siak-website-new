@extends('layouts.app')
    @section('content')

    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <style>
</style>
    <!-- BEGIN CONTENT -->  
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-calendar-minus-o font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Jadwal Ujian Ulang</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Lokasi</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::select('lokasi', $lokasi, !empty($profile->lokasi_id) ? $profile->lokasi_id : null, ['class' => 'form-control select2 lokasi']) }}
                                                            {!! $errors->first('lokasi', '<span class="help-block form-messages">:message</span>'); !!}
                                                            <span id="loading-lokasi" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                        </td>
                                                    </tr>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Cabang</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::select('cabang', $cabang, $cabang_id, ['class' => 'form-control select2 cabang']) }}
                                                            {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-siak borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>KP</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{ Form::select('kp', $kp, !empty($profile->lembg_pdkn_id) ? $profile->lembg_pdkn_id : null, ['class' => 'form-control select2 kp']) }}
                                                            {!! $errors->first('kp', '<span class="help-block form-messages">:message</span>'); !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <table class="table table-bordered table-hover table-siak-ajax" id="ulang-table">
                                <thead>
                                    <tr>
                                        
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kantor Perwakilan</th>
                                        <th>Tanggal</th>
                                        <th>Ruang</th>
                                        <th>Hari</th>
                                        <th>Jam</th>
                                        <th>Kuota</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="clearfix"> </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script>
    $(function() {
        var t = $('#ulang-table').DataTable({
            processing: true,
            serverSide: true,
           ajax:    {
                    url: '{{ $url_ajax_datatable }}',
                    data: function (d) {
                        d.kp = $(".kp").val();
                        d.batch = {{ $batch_id }};
                    }
                },
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama', name: 'ulang_jadwal.nama', class:'nama_sesi'},
                {data: 'nama_kp', name: 'lembg_pdkn.nama'},
                {data: 'tgl_ulang', name: 'ulang_jadwal.tgl_ulang'},
                {data: 'nama_ruang', name: 'ruang.nama'},
                {data: 'nama_hari', name: 'hari.nama'},
                {data: 'nama_jam', name: 'jam.nama'},
                {data: 'kuota'},
                {data: 'action', orderable:false, searchable: false},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 5,
            // stateSave: true,
            "order": [[ 3 /*urutan kolom */, 'asc' /* ascending or descending */]],  
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });
    
    $(document).ready(function(){
        $('.sertifikasi').addClass('active open');

        $("select[name='lokasi']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lokasi') }}" });
        $("select[name='cabang']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_cabang') }}" });
        $("select[name='kp']").select2({ width: 'auto', placeholder: "{{ \Lang::get('select2.select_lembg_pdkn') }}" });

        $("select[name='lokasi']").on('change', function(){
            var lokasi = $(this).val().trim();

            $('#loading-lokasi').css({'display': 'block'});

                // reset if filled
                $("select[name='cabang']").val('').trigger('change')
                $("select[name='kp']").val('').trigger('change')

                $.ajax({
                    method: 'GET',
                    url: '{{ route('ujian_ulang.ajax_get_cabang') }}',
                    data: {lokasi: lokasi},
                    success: function(msg){
                        console.log(msg);
                        $('#loading-lokasi').css({'display': 'none'});
                        obj_cabang  = JSON.parse(msg);
                        list_cabang = "<option value=''></option>";

                        if(obj_cabang.length > 0){
                            $.each(obj_cabang, function(i,v){    
                                list_cabang += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                            });
                        }else{
                            list_cabang = "<option></option>";
                        }

                        
                        $("select[name='cabang']").html(list_cabang);
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                        $('#loading-lokasi').css({'display': 'none'});
                    }
                });
        });

        $("select[name='cabang']").on('change', function(){
            var cabang = $(this).val().trim();

            $('#loading-cabang').css({'display': 'block'});

                // reset if filled
                $("select[name='kp']").val('').trigger('change')

                $.ajax({
                    method: 'GET',
                    url: '{{ route('ujian_ulang.ajax_get_kp') }}',
                    data: {cabang: cabang},
                    success: function(msg){
                        console.log(msg);
                        $('#loading-cabang').css({'display': 'none'});
                        obj_kp  = JSON.parse(msg);
                        list_kp = "<option value=''></option>";

                        if(obj_kp.length > 0){
                            $.each(obj_kp, function(i,v){    
                                list_kp += "<option value='"+ v._id +"'>"+ v.nama +"</option>";
                            });
                        }else{
                            list_kp = "<option></option>";
                        }

                        
                        $("select[name='kp']").html(list_kp);
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                        $('#loading-cabang').css({'display': 'none'});
                    }
                });
        });

        $("select[name='kp']").on('change', function(){
            $('#ulang-table').DataTable().ajax.reload();
        });

        $('body').on('click', '.daftar_ulang', function(){
            nama_sesi  = $(this).parent().parent().find('.nama_sesi').html();
            sesi_id    = $(this).attr('data-jadwal');
            batch_id   = {{ $batch_id }};
            peserta_id = {{ Auth::user()->peserta_id }};

            swal({
                    title: "Anda yakin ?",
                    text: "Anda akan mendaftar "+nama_sesi+"",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: 'Batal',
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ya, saya yakin!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'post',
                        dataType: 'json',
                        url: "{{ url('sertifikasi/ulang/save_pendaftaran_ulang') }}",
                        data: {
                            batch_id: batch_id,
                            sesi_id: sesi_id,
                            peserta_id: peserta_id
                        },
                        success: function(data){
                            swal("Silahkan menunggu konfirmasi melalui email dari pihak TICMI untuk proses selanjutnya", "", "success");
                            $('#ulang-table').DataTable().ajax.reload();
                        },
                        error: function(err){
                            alert(JSON.stringify(err));
                            swal.close();
                        }
                    });
                });
            });

    });
    </script>

@endsection