@extends('layouts.app')
    @section('content')

    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <style>
</style>
    <!-- BEGIN CONTENT -->  
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-calendar-minus-o font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Info Pendaftaran</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-bordered table-hover table-siak-ajax" id="ulang-table">
                                <thead>
                                    <tr>
                                        
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kantor Perwakilan</th>
                                        <th>Tanggal</th>
                                        <th>Ruang</th>
                                        <th>Hari</th>
                                        <th>Jam</th>
                                        <th>Pembayaran</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="clearfix"> </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <!-- MODAL DETAIL PENDAFTARAN-->
    <div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Detail Pendaftaran</h4>
        </div>
        <div class="modal-body">
                <span id="loading-modal-peserta" style="display:none"><img src="{{ asset('assets/theme/global/img/loading-spinner-grey.gif') }}"><span> &nbsp;&nbsp;Loading... </span></span>

                <div class="content_modal">
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Batch</td>
                                                    <td>:</td>
                                                    <td class="nama_batch">

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Peserta</td>
                                                    <td>:</td>
                                                    <td class="nama_peserta">
                                                        
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td class="email_peserta">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Lokasi</td>
                                                    <td>:</td>
                                                    <td class="lokasi">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Cabang</td>
                                                    <td>:</td>
                                                    <td class="cabang">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Kantor Perwakilan</td>
                                                    <td>:</td>
                                                    <td class="kp">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Ujian Ulang Ke</td>
                                                    <td>:</td>
                                                    <td class="ulang_ke">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>No Invoice</td>
                                                    <td>:</td>
                                                    <td class="no_invoice">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>No Virtual Account</td>
                                                    <td>:</td>
                                                    <td class="no_va">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>:</td>
                                                    <td>
                                                        <select class="form-control select2 is_bayar" name="aktif" disabled>
                                                         <option value=""></option>
                                                          <option value="1">Bayar</option>
                                                          <option value="0">Gratis</option>
                                                      </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Harga Pendaftaran</td>
                                                    <td>:</td>
                                                    <td class="harga_pendaftaran">
                                                       <span class="loading-harga" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="text" class="hidden peserta_id">
                                    <input type="text" class="hidden batch_id">
                                    <input type="text" class="hidden ujian_pendaftaran_id">

                </div>
       </div>
            <div class="modal-footer">
                <div class="form-group">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                {{-- <button type="button" class="btn green simpan_peserta">Simpan</button>
                <span id="loading-peserta" class="pull-right" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span> --}}
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL DETAIL PEMBAYARAN-->

    <!-- MODAL BUKTI PEMBAYARAN -->
    <div class="modal fade" id="modal_upload" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Upload Bukti Pembayaran</h4>
        </div>
        <div class="modal-body">
                <span id="loading-modal-upload" style="display:none"><img src="{{ asset('assets/theme/global/img/loading-spinner-grey.gif') }}"><span> &nbsp;&nbsp;Loading... </span></span>

                <div class="content_modal_upload">
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Batch</td>
                                                    <td>:</td>
                                                    <td class="nama_batch">

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Peserta</td>
                                                    <td>:</td>
                                                    <td class="nama_peserta">
                                                        
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td class="email_peserta">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Lokasi</td>
                                                    <td>:</td>
                                                    <td class="lokasi">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Cabang</td>
                                                    <td>:</td>
                                                    <td class="cabang">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Kantor Perwakilan</td>
                                                    <td>:</td>
                                                    <td class="kp">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Ujian Ulang Ke</td>
                                                    <td>:</td>
                                                    <td class="ulang_ke">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>No Invoice</td>
                                                    <td>:</td>
                                                    <td class="no_invoice">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>No Virtual Account</td>
                                                    <td>:</td>
                                                    <td class="no_va">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>:</td>
                                                    <td>
                                                        <select class="form-control select2 is_bayar" name="aktif" disabled>
                                                         <option value=""></option>
                                                          <option value="1">Bayar</option>
                                                          <option value="0">Gratis</option>
                                                      </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Harga Pendaftaran</td>
                                                    <td>:</td>
                                                    <td class="harga_pendaftaran">
                                                       <span class="loading-harga" style="display:none"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Tanggal Pembayaran</td>
                                                    <td>:</td>
                                                    <td class="tanggal_pembayaran">
                                                       
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <form enctype="multipart/form-data" id="form_upload_pembayaran" role="form" method="POST" action="" >
                                    <div class="alert alert-danger print-error-msg" style="display:none">
                                      <ul></ul>
                                    </div>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <table class="table table-siak borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Foto</td>
                                                    <td>:</td>
                                                    <td>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="" alt=""> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="foto" class="foto"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix margin-top-10">
                                                                    <span class="label label-danger">NOTE!</span> <span class="required" style="color:red;">(Max: 2 MB | Type: .jpg .png)</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <table class="table table-siak borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Keterangan</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ Form::textarea('keterangan', '', ['class' => 'form-control keterangan', 'size' => '-x-', 'placeholder' => 'Opsional']) }}
                                                                {!! $errors->first('keterangan', '<span class="help-block form-messages">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                    <input type="text" class="hidden peserta_id">
                                    <input type="text" class="hidden batch_id">
                                    <input type="text" name="ujian_pendaftaran_id" class="hidden ujian_pendaftaran_id">
                                    
                                    {!! Form::close() !!}
                </div>
       </div>
            <div class="modal-footer">
                <div class="form-group">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                <button class="btn green simpan_upload">Simpan</button>
                <span id="execute-loading" class="pull-right" style="visibility: hidden;"><img src="{{ asset('assets/theme/global/img/loading.gif') }}"></span>
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL DETAIL PEMBAYARAN-->
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

    <script>
    $(function() {
        var t = $('#ulang-table').DataTable({
            processing: true,
            serverSide: true,
           ajax:    {
                    url: '{{ $url_ajax_info_ulang }}',
                    data: function (d) {
                        d.batch = {{ $id_batch }};
                        d.peserta = {{ Auth::user()->peserta_id }};
                    }
                },
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama', name: 'ulang_jadwal.nama', class:'nama_sesi'},
                {data: 'nama_kp', name: 'lembg_pdkn.nama'},
                {data: 'tgl_ulang', name: 'ulang_jadwal.tgl_ulang'},
                {data: 'nama_ruang', name: 'ruang.nama'},
                {data: 'nama_hari', name: 'hari.nama'},
                {data: 'nama_jam', name: 'jam.nama'},
                {data: 'pembayaran', name: 'ujian_pendaftaran.is_bayar'},
                {data: 'status'},
                {data: 'action', orderable:false, searchable: false},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 5,
            // stateSave: true,
            "order": [[ 3 /*urutan kolom */, 'asc' /* ascending or descending */]],  
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });


    $(document).ready(function(){
        $('.sertifikasi').addClass('active open');

        

        $('body').on('click', '.detail_pendaftaran', function() {
            ujian_pendaftaran_id = $(this).attr('data-jadwal');

            $(".content_modal").hide();
            $('#loading-modal-peserta').css({'display': 'block'});
            $('#modal_detail').modal('toggle');

            $.ajax({
                    method: 'GET',
                    url: '{{ $url_ajax_get_detail_pendaftaran }}',
                    data: {ujian_pendaftaran_id: ujian_pendaftaran_id},
                    success: function(msg){
                        obj = JSON.parse(msg);
                        if(obj.is_bayar == true){
                            var status = 1;
                        } else {
                            var status = 0;
                        }
                        $('.peserta_id').val(obj.peserta_id);
                        $('.batch_id').val(obj.batch_id);
                        $('.ujian_pendaftaran_id').val(obj._id);
                        $('.is_bayar').val(status);
                        $('.nama_batch').html(obj.nama_batch);
                        $('.nama_peserta').html(obj.nama_peserta);
                        $('.email_peserta').html(obj.email_peserta);
                        $('.lokasi').html(obj.nama_lokasi);
                        $('.cabang').html(obj.nama_cabang);
                        $('.kp').html(obj.nama_kp);
                        $('.ulang_ke').html(obj.ulang_ke);
                        $('.no_invoice').html(obj.no_invoice);
                        $('.no_va').html(obj.no_va);
                        $('.is_bayar').val(status).trigger('change.select2');
                        $('.harga_pendaftaran').html(obj.harga_pendaftaran). <?= \Helper::number_formats('', 'dynamic_var_jquery', 0) ?>

                        $('#loading-modal-peserta').css({'display': 'none'});
                        $(".content_modal").show();
                        
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                    }
                });
        });

        $('body').on('click', '.upload_bukti', function() {
            var ujian_pendaftaran_id = $(this).attr('data-jadwal');
            var d = new Date();

            var month = d.getMonth()+1;
            var day = d.getDate();

            var today = (day<10 ? '0' : '') + day + '-' +
                (month<10 ? '0' : '') + month + '-' +
                d.getFullYear();

            $(".content_modal_upload").hide();
            $('#loading-modal-upload').css({'display': 'block'});
            $('#modal_upload').modal('toggle');

            $.ajax({
                    method: 'GET',
                    url: '{{ $url_ajax_get_detail_pendaftaran }}',
                    data: {ujian_pendaftaran_id: ujian_pendaftaran_id},
                    success: function(msg){
                        obj = JSON.parse(msg);
                        if(obj.is_bayar == true){
                            var status = 1;
                        } else {
                            var status = 0;
                        }
                        $('.peserta_id').val(obj.peserta_id);
                        $('.batch_id').val(obj.batch_id);
                        $('.ujian_pendaftaran_id').val(obj._id);
                        $('.is_bayar').val(status);
                        $('.nama_batch').html(obj.nama_batch);
                        $('.nama_peserta').html(obj.nama_peserta);
                        $('.email_peserta').html(obj.email_peserta);
                        $('.lokasi').html(obj.nama_lokasi);
                        $('.cabang').html(obj.nama_cabang);
                        $('.kp').html(obj.nama_kp);
                        $('.ulang_ke').html(obj.ulang_ke);
                        $('.no_invoice').html(obj.no_invoice);
                        $('.no_va').html(obj.no_va);
                        $('.is_bayar').val(status).trigger('change.select2');
                        $('.harga_pendaftaran').html(obj.harga_pendaftaran). <?= \Helper::number_formats('', 'dynamic_var_jquery', 0) ?>
                        $('.tanggal_pembayaran').html(today);
                        $('.fileinput').fileinput('clear');
                        $('.keterangan').val('');
                        $('#loading-modal-upload').css({'display': 'none'});
                        $(".content_modal_upload").show();
                        
                    },
                    error: function(err){
                        alert(JSON.stringify(err));
                    }
                });

        });

        $('body').on('click', '.simpan_upload', function() {
             var tombol  = $(this);
             var loading = $('#execute-loading');
             var foto    = $('.foto').val();
             if(foto == '')
             {
                alert('mohon upload bukti konfirmasi pembayaran Anda');
             } else {
                tombol.attr('disabled', 'disabled');
                loading.css({'visibility': 'visible'});

                $.ajax({
                  type: "POST",
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: '{{ route("sertifikasi.upload_pembayaran") }}',
                  data:new FormData($("#form_upload_pembayaran")[0]),
                  dataType:'json',
                  async:false,
                  type:'post',
                  processData: false,
                  contentType: false,
                  success: function(msg){
                    loading.css({'visibility': 'hidden'});
                    tombol.removeAttr('disabled');
                    if(msg.status == "Data Updated") {
                        swal("Data Updated!", "", "success");
                    } else {
                        swal("Data Failed to Updated!", "", "error");
                    }
                    $('#modal_upload').modal('toggle');
                    $('#ulang-table').DataTable().ajax.reload();
                  },
                  error: function(err){
                    $('button[name="simpan"]').removeAttr('disabled');
                    loading.css({'visibility': 'hidden'});
                    tombol.removeAttr('disabled');
                    $('#modal_upload').modal('toggle');
                    alert(JSON.stringify(err));
                  }
                });
             }

        });
    });
    </script>

@endsection