@extends('layouts.app')
    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <!-- BEGIN CONTENT -->  
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div id="konten">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered">
                           <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-calendar-minus-o font-red"></i>
                                    <span class="caption-subject font-red bold uppercase">Jadwal Pelatihan</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-bordered table-hover table-siak-ajax" id="ulang-table">
                                    <thead>
                                        <tr>
                                            
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Modul</th>
                                            <th>Ruang</th>
                                            <th>Tanggal</th>
                                            <th>Jam</th>
                                            <th>Survey</th>
                                        </tr>
                                    </thead>
                                </table>

                                <div class="clearfix"> </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <meta name="crsf-token" content="{{csrf_token()}}">
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script>
    $(function() {
        var t = $('#ulang-table').DataTable({
            processing: true,
            serverSide: true,
           ajax:    {
                    url: '{{ $url_ajax_get_jadwal }}',
                    data: function (d) {
                        d.batch = {{ $batch_id }};
                    }
                },
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama', name: 'pelatihan_aktual_dtl.nama'},
                {data: 'modul', name: 'modul.nama'},
                {data: 'ruangan', name: 'ruang.nama'},
                {data: 'tanggal', name: 'pelatihan_aktual_dtl.tgl_pelatihan'},
                {data: 'jam', name: 'pelatihan_aktual_dtl.jam_pelatihan'},
                {data: 'survey', name: 'svy.nama_survey'},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 5,
            // stateSave: true,
            "order": [[ 6 /*urutan kolom */, 'asc' /* ascending or descending */]],  
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });

    function survey(svy_id,pelatihan_aktual_dtl_id){
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : $('meta[name=crsf-token]').attr('content')
            }
        })

        $.ajax({
            url : '{{$url_ajax_survey}}/'+svy_id+'/'+pelatihan_aktual_dtl_id,
            type : 'GET',
            success:function(data){
                $('#konten').html(data);
            }
        })
    }

    
    $(document).ready(function(){
        $('.sertifikasi').addClass('active open');
    });
    </script>

@endsection