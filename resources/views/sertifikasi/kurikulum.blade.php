<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:9.0pt;
	font-family:"Segoe UI","sans-serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Segoe UI","sans-serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:841.9pt 595.3pt;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
-->

#modul {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 70%;
      margin: auto;
  }

 #modul td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
      font-size: 18px;
  }

  #modul td.text {
      font-size: 15px;
  }

  #modul td, th .thin{
      font-weight: normal;
      font-style: italic;
  }

 #modul tr:nth-child(odd) {
      background-color: #dddddd;
  }

</style>

</head>

<body lang=EN-US>

<div class=WordSection1>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='position:relative;
z-index:251658240'><span style='left:0px;position:absolute;left:772px;
top:-74px;width:213px;height:132px'><img width=213 height=132
src="image001.jpg"></span></span><b><span
lang=IN style='font-size:16.0pt;font-family:"Arial","sans-serif"'>KURIKULUM</span></b><span
lang=IN style='font-size:10.0pt;font-family:"Arial","sans-serif"'> </span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><i><span lang=IN style='font-size:16.0pt;
font-family:"Arial","sans-serif"'>CURRICULUM</span></i></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><i><span lang=IN style='font-size:16.0pt;
font-family:"Arial","sans-serif"'>&nbsp;</span></i></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=IN style='font-size:16.0pt;
font-family:"Arial","sans-serif"'>WAKIL PERANTARA PEDAGANG EFEK</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><i><span lang=IN style='font-size:16.0pt;
font-family:"Arial","sans-serif"'>BROKER DEALER REPRESENTATIVE</span></i></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=IN style='font-size:18.0pt'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=IN style='font-size:18.0pt'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=IN style='font-size:18.0pt'>
</span></b></p>

<table id="modul">
  <tr>
    <th>
    	MODUL
    	<br />
    	<span class="thin">MODULE</span>
    </th>
    <th>
    	JUMLAH SESI
    	<br />
    	<span class="thin">TOTAL SESSIONS
    </th>
    <th>
    	JUMLAH JAM
    	<br />
    	<span class="thin">TOTAL HOURS
    </th>
  </tr>
  @foreach($modul as $value)
  <tr>
    <td class="text">
    	<strong>{{ $value->nama }}</strong>
    	<br />
    	{{ $value->singkatan_eng }}
    </td>
    <td class="text">
    	<strong>7</strong>
    </td>
    <td class="text">
    	<strong>10</strong>
    </td>
  </tr>
  @endforeach
</table>
<br clear=ALL>
<br />
<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;margin-left: 2.1in;'>
<img width=150 height=150
  src="{{ asset('assets/upload_files/peserta/photos').'/'.$profile->photo}}"></td>

<br clear=ALL>
<br clear=ALL>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.6in;margin-bottom:.0001pt;text-indent:.5in;line-height:normal'><span
lang=IN style='font-size:14.0pt'>Nama / <i style="margin-right:180px;">Name</i>
: {{ $peserta->nama }}</span></p>

@php
setlocale(LC_TIME, config('app.locale'));
$date = Carbon\Carbon::createFromTimeStamp(strtotime($peserta->tanggal_lahir));
$date_only = $date->formatLocalized('%d %B %Y');
@endphp

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;margin-left:2.1in;'><span lang=IN style='font-size:14.0pt'>Tanggal
Lahir / <i style="margin-right:75px;">Date of Birth</i>
: {{ $date_only }}</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=IN style='font-size:14.0pt'>                                                No.
Sertifikat / <i>Certificate</i></span><i><span lang=IN style='font-size:14.0pt'>
</span></i><i><span lang=IN style='font-size:14.0pt'>Numbe</span></i><i><span
lang=EN-ID style='font-size:14.0pt'>r</span></i><span lang=IN style='font-size:
14.0pt'>       : WPPE-002260</span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=IN style='font-size:18.0pt'>&nbsp;</span></b></p>

</div>

</body>

</html>
