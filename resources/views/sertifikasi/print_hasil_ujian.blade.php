<html>
<head>
<title>{{ $hasil_ujian[0]['nama_peserta'] }}</title>
<!-- For print -->
<style type="text/css" media="print">
*{ padding: 0px !important; margin: 0px !important;  }
@page {
	size: A4;
	margin: 0;
  }
@media print {
  html, body {
    width: 210mm;
    height: 297mm;
  }
}
</style>

<!-- For Styling -->
<style type="text/css">
	/* Code in here */
	.margin-left-20{
		margin-left: 20px !important;
	}
</style>

</head>
<body>
	<center>
		<img src="{{ public_path('assets/images/default/logo-ticmi.jpg') }}" width="200px" height="140px" style="margin-top: 20px !important;">
	</center>
	<div class="container" style="/*outline: 1px solid #E1DFE8; */box-sizing: border-box; padding: 10px 50px; text-align: center; margin-top: 50px !important;">
	   <p>HASIL UJIAN SERTIFIKASI PROFESI</p>
	   @php
	   setlocale(LC_TIME, config('app.locale'));
	   $date = \Carbon\Carbon::now()->formatLocalized('%d %B %Y');
	   @endphp
	   <p><b>{{ $hasil_ujian[0]['singkatan_ind'] }}</b></p>

	   <p>{{ \Helper::tgl_indo(date('Y-m-d')) }}</p>

	   <p><b>{{ $hasil_ujian[0]['nama_peserta'] }}</b></p>
	   <p>Tanggal Lahir : {{ \Helper::date_formats($hasil_ujian[0]['tgl_lahir'], 'view') }}</p>
	   <br />

	   @foreach($hasil_ujian as $hasil)
		   <p><b>{{ $hasil['nama_modul'] }}</b></p>
		   <p>Nilai : {{ $hasil['nilai'] }} - <b>{{ $hasil['lulus'] == true ? 'Lulus' : 'Gagal' }}</b></p>
		@endforeach

		<br />
		@if($hasil_ujian[0]['reg_wppe'] != '')
	  		<p><b>Reg.No. WPPE - {{ $hasil_ujian[0]['reg_wppe'] }}</b></p>
	  	@endif

	</div>
	<div class="container" style="/*outline: 1px solid #E1DFE8; */box-sizing: border-box; padding: 10px 50px; text-align: left; top : 30px;bottom: 100px;"> 
	   <div style="padding-left: 50 !important;">
	   	   Catatan:
	   	   <ul>
	   		   	<li class="margin-left-20">Lembar ini hanya merupakan review hasil ujian.</li>
	   		   	<li class="margin-left-20">Simpan sebagai tanda bukti penukaran Sertifikat Kelulusan dari TICMI.</li>
	   	   </ul>
	   </div>
	</div>
</body>
</html>