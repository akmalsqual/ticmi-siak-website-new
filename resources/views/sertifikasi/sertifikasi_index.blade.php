@extends('layouts.app')
    @section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/sweetalerts/sweetalert.css') }}">

    <style>
        .table-siak td:first-child{
            width: 15%!Important;
        }
    </style>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-graduation-cap font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Sertifikasi</span>
                            </div>
                        </div>
                        <div class="portlet-body">
            
                            <!-- CONTENT HERE -->


                            <div id="pelatihan-accordion">


                                <!-- JADWAL PELATIHAN -->
                                <h3>Jadwal Pelatihan</h3>
                                    <div>
                                        {{-- @foreach($pelatihan_detail as $pdl)
                                        <div class="list-pelatihan alert alert-success" style="cursor:pointer;">
                                        <strong>{{$pdl['nama_pelatihan']}}</strong>
                                        </div>
                                        @endforeach --}}
                                        <table class="table table-siak table-pelatihan table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Nama Batch</th>
                                                    <th style="padding-left: 20px;">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>                                                        
                                                @if($batch_aktif->count() > 0)
                                                    @foreach($batch_aktif as $batch)
                                                        <tr>
                                                            <td>{{ $batch->nama }}</td>
                                                            <td>
                                                                <a name="mulai" href="{{ url('sertifikasi/jadwal_pelatihan/'.$batch->batch_id) }}" class="btn btn-sm green btn-outline">Lihat Jadwal</a>
                                                                <a href="{{ url('sertifikasi/materi_pelatihan/'.$batch->program_id) }}" class="btn btn-sm red btn-outline">Lihat Materi</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <td colspan="2"><center><h4>{{ $alert }}</h4></center></td>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                <!-- // JADWAL PELATIHAN -->

                                <!-- TATA TERTIB PESERTA UJIAN -->
                                <h3>Tata Tertib Peserta Ujian</h3>
                                    <div>
                                        <ul class="list-unstyled">
                                        <li> 1. Diharuskan hadir 15 (lima belas) menit sebelum ujian dimulai. Apabila terlambat, merupakan hak panitia untuk menentukan keikutsertaan peserta</li>
                                        <li>2. Diharuskan membawa identitas pribadi, berupa KTP/SIM/Paspor/KITAS asli (hardcopy/softcopy) untuk keperluan registrasi ujian</li> 
                                        <li>3. Diharuskan berpakaian sopan dan rapi, tidak mengenakan celana pendek dan sandal</li>
                                        <li>4. Registrasi ujian dilakukan paling lambat 5 (lima) menit sebelum ujian dimulai</li>
                                        <li>5. Barang pribadi milik peserta, seperti tas, dompet, handphone, gadget, dan lain-lain harus diletakkan di tempat yang disediakan oleh petugas;</li>
                                        <li>6. Selama ujian berlangsung, peserta tidak diperkenankan:</li>
                                        <ul class="list-unstyled" style="margin-left: 20px;">
                                            <li >a. Membawa handphone atau gadget apapun ke dalam ruang ujian</li>
                                            <li>b. Membawa alarm dalam bentuk apapun, termasuk wrist watch (jam tangan yang memakai alarm) ke dalam ruang ujian</li>
                                            <li>c. Membawa catatan kecil atau bahan pelajaran ke dalam ruangan</li>
                                            <li>d. Membawa makanan/minuman ke dalam ruang ujian</li>
                                            <li>e. Berbicara, bekerjasama, melihat soal atau jawaban peserta lainnya dan membuat keributan yang dapat mengganggu konsentrasi peserta lain</li>
                                            <li>f. Menyalin atau memfoto soal ujian;</li>
                                        </ul>
                                        <li>7. Melakukan kecurangan merupakan suatu pelanggaran serius dan dapat mengakibatkan peserta yang bersangkutan dikeluarkan dan didiskualifikasi atau ditindak secara hukum;</li>
                                        <li>8. Diperbolehkan menggunakan kalkulator dan alat tulis pribadi untuk menunjang pelaksanaan ujian;</li>
                                        <li>9. TICMI hanya menyediakan fasilitas personal computer/Laptop, jaringan internet, dan kertas buram;</li>
                                        <li>10. Dilarang meninggalkan tempat ujian dengan alasan apapun setelah melakukan log in;</li>
                                        <li>11. Pada saat ujian selesai, peserta diharuskan:</li>
                                        <ul class="list-unstyled" style="margin-left: 20px;">
                                            <li>a. Segera meninggalkan tempat duduk</li>
                                            <li>b. Memastikan kertas buram tidak dibawa ke luar ruang ujian</li>
                                        </ul>
                                        </ul>
                                    </div>
                                <!-- TATA TERTIB PESERTA UJIAN -->

                                <!-- UJIAN PERDANA -->
                                <h3>Ujian Perdana</h3>
                                    <div>
                                        <div id="perdana-accordion">
                                            <!-- JADWAL -->
                                            <h3>Jadwal</h3>
                                                <div>
                                                    <table class="table table-siak borderless table-pelatihan">
                                                            <thead>
                                                                <tr>
                                                                    <th width="30%">Nama Batch</th>
                                                                    <th width="15%">Tanggal</th>
                                                                    <th width="15%">Ruang</th>
                                                                    <th width="15%">Jam</th>
                                                                    <th width="15%">Lama Ujian</th>
                                                                    <th width="10%">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(!empty($nama_batch))
                                                                @foreach($nama_batch as $nb)
                                                                {!! Form::open(['url'=>'sertifikasi/perdana/index/'.$nb['id'] ]) !!}
                                                                <tr>
                                                                    <input type="hidden" name="emp_id" value="{{ $nb['emp_id'] }}">
                                                                    <td>{{ $nb['nama'] }}</td>
                                                                    <td>{{ \Helper::date_formats($nb['tanggal'], 'view') }}</td>
                                                                    <td>{{ $nb['ruang'] }}</td>
                                                                    <td>{{ $nb['jam'] }}</td>
                                                                    <td>{{ $nb['lama_ujian']. ' Menit' }}</td>
                                                                    @if($nb['aktivasi'] === FALSE)
                                                                        <td><button class="btn default" disabled>Mulai Ujian</button></td>
                                                                    @elseif($nb['aktivasi'] === TRUE)
                                                                        <td><button type="submit" name="mulai" class="btn green" {{ ($status_ujian == false) ? 'disabled' : '' }}>Mulai Ujian</button></td>
                                                                    @endif
                                                                    <input type="hidden" name="keyboard_lock" value="{{ $nb['keyboard_lock'] }}">
                                                                </tr>
                                                                {{ Form::close() }}
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="5"><center><h4>Tidak ada Jadwal</h4></center></td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                    </table>
                                                </div>
                                            <!-- NILAI -->    
                                            <h3>Nilai</h3>
                                                <div>
                                                    <table class="table table-siak table-pelatihan table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Batch</th>
                                                                <th style="padding-left: 20px;">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                        
                                                            @if($batch_nilai->count() > 0)
                                                                @foreach($batch_nilai as $batch)
                                                                    <tr>
                                                                        <td>{{ $batch->nama }}</td>
                                                                        <td>
                                                                            <a name="mulai" href="{{ url('sertifikasi/nilai_perdana/'.$batch->batch_id) }}" class="btn btn-sm green btn-outline">Lihat Nilai</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <td colspan="2"><center><h4>{{ $alert }}</h4></center></td>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- // UJIAN PERDANA -->

                                    <!-- UJIAN ULANG -->
                                    <h3>Ujian Ulang</h3>
                                        <div>

                                        <div id="ulang-accordion">
                                        <!-- PENDAFTARAN -->
                                            <h3>Pendaftaran</h3>
                                                <div>
                                                    <table class="table table-siak table-pelatihan table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Batch</th>
                                                                <th style="padding-left: 17px;">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                  
                                                            @if(isset($batch_ulang) && count($batch_ulang) > 0)
                                                                @foreach($batch_ulang as $ulang)
                                                                    @php
                                                                    // cari ujian ulang yang telah terdaftar dan belum dilaksanakan
                                                                    $cek_ujian = DB::table('ujian_pendaftaran')
                                                                                  ->where('peserta_id', Auth::user()->peserta_id)
                                                                                  ->where('batch_id', $ulang->batch_id)
                                                                                  ->whereIn('ulang_jadwal_id', function($query){
                                                                                        $query->select('ulang_jadwal_id')
                                                                                              ->from('ulang_jadwal')
                                                                                              ->where('tgl_ulang', '>=', date('Y-m-d'));
                                                                                    })
                                                                                  ->whereIn('ujian_pendaftaran_id', function($query){
                                                                                        $query->select('ujian_pendaftaran_id')
                                                                                              ->from('ujian_peserta')
                                                                                              ->where('selesai', false);
                                                                                    });
                                                                    if($cek_ujian->count() > 0) {
                                                                        $button = '<a class="btn btn-sm btn-default tooltips" data-toogle="tooltip" data-placement="top" title="Anda sedang dalam masa ujian ulang" disabled>Daftar</a>';
                                                                        // $button = '<button class="btn btn-sm btn-default popovers disabled" data-container="body" data-trigger="hover" data-placement="top" data-content="Popover body goes here! Popover body goes here!" >Daftar</a>';
                                                                    } else {
                                                                        $button = '<a name="mulai" href="'.url('sertifikasi/ulang/'.$ulang->batch_id).'" class="btn btn-sm green btn-outline">Daftar</a>';
                                                                    }
                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $ulang->nama }}</td>
                                                                        <td>
                                                                                {!! $button !!}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="2"><center><h4>Tidak Ada Jadwal</h4></center></td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                        <!-- // PENDAFTARAN -->

                                        <!-- STATUS PENDAFTARAN -->
                                            <h3>Status Pendaftaran</h3>
                                                <div>
                                                    <table class="table table-siak table-pelatihan table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Batch</th>
                                                                <th style="padding-left: 17px;">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                        
                                                            @if(isset($batch_ulang) && count($batch_ulang) > 0)
                                                                @foreach($batch_ulang as $ulang)
                                                                    <tr>
                                                                        <td>{{ $ulang->nama }}</td>
                                                                        <td>
                                                                            <a name="mulai" href="{{ url('sertifikasi/info_ulang/'.$ulang->batch_id) }}" class="btn btn-sm green btn-outline">Lihat Info</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <td colspan="2"><center><h4>Anda belum mendaftar ujian ulang</h4></center></td>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                        <!-- // STATUS PENDAFTARAN -->

                                        <!-- JADWAL -->
                                            <h3>Jadwal</h3>
                                                <div>
                                                    <table class="table table-siak table-pelatihan table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th width="20%">Batch</th>
                                                                <th width="10%">KP</th>
                                                                <th width="10%">Tanggal Daftar</th>
                                                                <th width="10%">Ruang</th>
                                                                <th width="10%">Tanggal Ujian</th>
                                                                <th width="10%">Jam</th>
                                                                <th width="10%">Ulang Ke</th>
                                                                <th width="10%">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                        
                                                            @if($jadwal_uji_ulang->count() > 0)
                                                                @foreach($jadwal_uji_ulang as $jadwal)
                                                                    {!! Form::open(['route'=>'ulang.start_ujian' ]) !!}
                                                                    <tr>
                                                                        <td>{{ $jadwal->nama_batch }}</td>
                                                                        <td>{{ $jadwal->nama_lembg_pdkn }}</td>
                                                                        <td>{{ \Helper::date_formats($jadwal->tgl_daftar, 'view') }}</td>
                                                                        <td>{{ $jadwal->ruang }}</td>
                                                                        <td>{{ \Helper::date_formats($jadwal->tgl_ulang, 'view') }}</td>
                                                                        <td>{{ $jadwal->jam }}</td>
                                                                        <td>{{ $jadwal->ulang_ke }} </td>
                                                                        <td>
                                                                        @if($jadwal->is_aktivasi === TRUE)
                                                                            <button type="submit" class="btn btn-sm green btn-outline">Ujian</button>&nbsp;
                                                                        @else
                                                                            <a class="btn btn-sm btn-default tooltips" data-toogle="tooltip" data-placement="top" title="Belum di aktivasi" disabled>Ujian</a>
                                                                        @endif
                                                                        </td>
                                                                    </tr>
                                                                    <input type="text" class="hidden" name="ujian_pendaftaran_id" value="{{ $jadwal->_id }}">
                                                                    <input type="text" class="hidden" name="batch_id" value="{{ $jadwal->batch_id }}">
                                                                    {{ Form::close() }}
                                                                @endforeach
                                                            @else
                                                                <td colspan="8"><center><h4>Tidak ada jadwal</h4></center></td>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <!-- // JADWAL -->
                                            <!-- NILAI -->    
                                            <h3>Nilai</h3>
                                                <div>
                                                    <table class="table table-siak table-pelatihan table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Batch</th>
                                                                <th style="padding-left: 20px;">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                        
                                                            @if($batch_nilai->count() > 0)
                                                                @foreach($batch_nilai as $batch)
                                                                    <tr>
                                                                        <td>{{ $batch->nama }}</td>
                                                                        <td>
                                                                            <a name="mulai" href="{{ url('sertifikasi/nilai_ulang/'.$batch->batch_id) }}" class="btn btn-sm green btn-outline">Lihat Nilai</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <td colspan="2"><center><h4>{{ $alert }}</h4></center></td>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <!-- modal ITEM -->
                            <div class="modal fade" id="modal_list_pelatihan" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <b><h4 class="modal-title">Pelatihan Detail</h4></b>
                                            <span class="tag label label-primary" style="font-size: 9pt;padding: 0px 6px;">Pertemuan Ke #</span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="portlet light">
                                                <div class="portlet-body">
                                                    
                                                    <table class="table table-bordered">
                                                        
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn red" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div><!-- /modal ITEM -->


                            <!-- END CONTENT -->                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('assets/sweetalerts/sweetalert.js') }}"></script>

    <script>
        $(document).ready(function(){
            $("select[name='batch']").select2({ placeholder: "--Select Batch--", width: "auto" });
            $("select[name='ujian-perdana']").select2({ placeholder: "--Select Batch--", width: "auto" });

            $('[data-toggle="tooltip"]').tooltip(); 

            $('#pelatihan-accordion').accordion({
                heightStyle: "content",
                collapsible: true,
                icons: {
                    header: 'ui-icon-circle-arrow-e',
                    activeHeader: 'ui-icon-circle-arrow-s'
                },
                animated: 'easeslide'
            });
            $('#perdana-accordion').accordion({
                heightStyle: "content",
                collapsible: true,
                icons: {
                    header: 'ui-icon-circle-arrow-e',
                    activeHeader: 'ui-icon-circle-arrow-s'
                }
            });
            $('#ulang-accordion').accordion({
                heightStyle: "content",
                collapsible: true,
                icons: {
                    header: 'ui-icon-circle-arrow-e',
                    activeHeader: 'ui-icon-circle-arrow-s'
                }
            });

        });
    </script>

@endsection