@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <!-- BEGIN CONTENT -->  
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div id="konten">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered">
                           <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-calendar-minus-o font-red"></i>
                                    <span class="caption-subject font-red bold uppercase">Materi Pelatihan</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-bordered table-hover table-siak-ajax" id="materi-table">
                                    <thead>
                                        <tr>
                                            
                                            <th>No</th>
                                            <th>Nama Materi</th>
                                            <th>Program</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>

                                <div class="clearfix"> </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script>
    $(function() {
        var t = $('#materi-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{{ $url_ajax_get_materi }}',
                    data: function (d) {
                        d.program = {{ $program_id }};
                  }
            },
            columns: [
                {data: 'rownum', searchable: false},
                {data: 'nama', name: 'materi.nama'},
                {data: 'program', name: 'program.nama'},
                {data: 'keterangan', name: 'materi.keterangan'},
                {data: 'action', orderable:false, searchable: false},
            ],
            "drawCallback": function(settings) {
                //
            },            
            pageLength: 5,
            // stateSave: true,
            "order": [[ 1 /*urutan kolom */, 'asc' /* ascending or descending */]],  
        });

        // responsive table
        $('.table-siak-ajax').wrap('<div class="table-scrollable"></div>');
        $('.dataTables_wrapper .col-xs-12:first .col-xs-6').attr('class', 'col-md-6');
        $('.dataTables_wrapper .col-xs-12:first').removeClass();
    });

    
    $(document).ready(function(){
        $('.sertifikasi').addClass('active open');
    });
    </script>

@endsection