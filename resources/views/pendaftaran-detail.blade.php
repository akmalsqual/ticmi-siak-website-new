@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ url('assets/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $batch->nama }}</h3>
                        <h6 class="sub-title">{{ $batch->keterangan }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <script type="application/ld+json">
    {
      "@@context": "http://schema.org",
      "@@type": "Course",
      "name": "{{ $batch->nama }}",
      "description": "{{ $batch->keterangan }} {{ $batch->nama }}",
      "provider": {
        "@@type": "EducationalOrganization",
        "name": "The Indonesia Capital Market Institute",
        "sameAs": "http://ticmi.co.id"
      }
    }
    </script>


    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                <!-- Course Banner Image -->
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/ticmi/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>{{ $batch->program->nama." ".$batch->kelas->type->nama }}</h4>
                            <span class="cat bg-yellow">{{ $batch->program->nama }}</span><span class="cat bg-green">Sertifikasi</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>IDR{{ number_format($batch->harga,0,".",",") }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Tanggal Mulai Pelatihan</span><h5>{{ $batch->tgl_mulai->formatLocalized('%A, %#d %B %Y') }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>{{ $batch->lembaga->nama }}</h5></li>
                                <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->min_peserta }} Orang</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>{{ $batch->rab_batch->durasi_pelatihan_hari }} Hari</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>{{ $batch->waktuUjian->durasi_masa_aktif_ujian }} Bulan</h5></li>
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! $batch->program->keterangan !!}

                        <p class="text-center">
                        @if($data['is_loggin'] == TRUE)
                            {{-- @if($data['peserta'] == TRUE) --}}
                            @if($data['terdaftar'] == TRUE)
                                <a href="#" onclick="alert('Anda telah terdaftar di dalam batch ini')" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                            @else
                                <a href="{{ url('register-batch/'.Crypt::encrypt($batch->batch_id).'/daftar') }}" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                            @endif
                            {{-- @else
                                Redirect ke route yang dibuat fadli
                                <a href="{{ url('profile') }}" onclick="alert('Silahkan lengkapi user profile')" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                            @endif --}}
                        @else
                            <a href="{{ url('register-batch/'.Crypt::encrypt($batch->batch_id).'/daftar') }}" onclick="alert('Silahkan login terlebih dahulu')" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                        @endif
                        </p>
                    </div>
                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h5>Program Kami lainnya : </h5>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="3" data-desktopsmall="3"  data-desktop="4" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        <img alt="Event" class="img-responsive" src="{{ $batch_path.$value->logo }}" width="600" height="220">
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar-batch/'.$value->batch_id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>

        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection