@extends('layouts_ticmi.home')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ url('/assets/images/ticmi/header-overview-data-service.jpg') }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">DATA PASAR MODAL</h4>

                            <p style="text-align:justify">
                                The Indonesia Capital Market Institute bekerja sama denga Bursa Efek Indonesia menyediakan referensi pasar modal berupa
                                data dan informasi yang dapat diakses kapanpun dan dimanapun. Nikmati akses ke data-data pasar modal Indonesia mulai tahun 1977
                                dalam formta <em>Excel (.xls)</em>, <em>pdf</em>, dan <em>teks (.txt)</em> yang didesain khusus untuk pengolahan data Anda.
                            </p>

                            <p style="text-align:justify">
                                Selain dari data yang sudah ada di website TICMI yang bisa didapatkan melalui berlangganan
                                <a href="{{ route('paket-data.index') }}" class="">Paket Data</a>, TICMI juga mempunyai data-data pasar modal
                                lainnya yang bisa didapatkan melalui <a href="{{ route('permintaan-data') }}">Permintaan Data Khusus</a>.
                                Untuk jenis-jenis data yang TICMI miliki bisa di lihat pada daftar dibawah, berikut tahapan untuk melakukan permintaan data pasar modal
                                ke TICMI :

                                <ol>
                                    <li>Silahkan tentukan data pasar modal apa yang akan dibeli, buat daftar detail data berisi Jenis Data, Emiten, Tahun dll yang diperlukan</li>
                                    <li>Anda bisa datang langsung ke TICMI atau bisa email pesanan Anda ke <a href="mailto:info@ticmi.co.id?subject=Permintaan Data Khusus - [isi nama Anda disini]">info@ticmi.co.id</a> dengan mencantumkan daftar detail pesanan data yang diinginkan  </li>
                                    <li>TICMI akan mengecek ketersediaan data pesanan Anda, jika data yang dipesan tersedia maka TICMI akan kirim sample data beserta dengan rincian biaya yang diperlukan</li>
                                    <li>Jika sudah sesuai dengan keperluan Anda maka Anda bisa langsung melakukan pembayaran dan konfirmasi pembayaran Anda dengan reply email dari TICMI tersebut</li>
                                    <li>TICMI akan melakukan verifikasi pembayaran Anda, jika sudah diverifikasi maka data akan kami kirimkan melalui email dengan estimasi 3- 7 hari kerja, namun apabila ukuran data yang di request terlalu besar maka data tersebut dapat kami kirimkan dengan menggunakan jasa pengiriman ekspedisi.</li>
                                </ol>
                            </p>
                            <br>
                            <h5>Daftar data yang tersedia</h5>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th>Jenis Data</th>
                                    <th>Format Data</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th width="40">Sample</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td>1</td>
                                    <td colspan="5"><strong>Statistic</strong></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Financial Data & Ratio</td>
                                    <td>Excel</td>
                                    <td>All Emiten per tahun</td>
                                    <td class="text-right">Rp140.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/statistic/1.Financial-Data-&-Ratio.jpg') }}" rel="prettyPhoto" title="Sample data Financial Data & Ratio"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Beta Saham</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/statistic/2.Sample-Beta-Saham.jpg') }}" rel="prettyPhoto" title="Sample data Beta Saham"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr class="info">
                                    <td>2</td>
                                    <td colspan="5"><strong>Historical Data</strong></td>
                                </tr>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td rowspan="2">Perdagangan Obligasi</td>
                                    <td rowspan="2">Excel</td>
                                    <td>per emiten per tahun</td>
                                    <td class="text-right">Rp5.000</td>
                                    <td rowspan="2" class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/1.Perdagangan-Obligasi.jpg') }}" rel="prettyPhoto" title="Sample data Perdagangan Obligasi"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td>All Emiten per tahun</td>
                                    <td class="text-right">Rp700.000</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Foreign Buy & Sell</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/2.Foreign-Buy-Sample.jpg') }}" rel="prettyPhoto" title="Sample data Foreign Buy & Sell"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Domestic Buy & Sell</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/3.Domestic-Buy-Sample.jpg') }}" rel="prettyPhoto" title="Sample data Domestic Buy & Sell"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Nett Domestic Buy & Sell</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/4.SAmple_Net-Foreign-Domectic.jpg') }}" rel="prettyPhoto" title="Sample data Nett Domestic Buy & Sell"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Nett Foreign Buy & Sell</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/4.SAmple_Net-Foreign-Domectic.jpg') }}" rel="prettyPhoto" title="Sample data Nett Foreign Buy & Sell"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Perdagangan Anggota Bursa</td>
                                    <td>Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/5.Sample Data Daily AB.png') }}" rel="prettyPhoto" title="Sample data Perdagangan Anggota Bursa"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td rowspan="2">Listed Shares</td>
                                    <td rowspan="2">Excel</td>
                                    <td>per emiten per tahun</td>
                                    <td class="text-right">Rp5.000</td>
                                    <td rowspan="2" class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/historical/6.Listed-Share.jpg') }}" rel="prettyPhoto" title="Sample data Listed Share"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td>All Emiten per tahun</td>
                                    <td class="text-right">Rp700.000</td>
                                </tr>
                                <tr class="info">
                                    <td>3</td>
                                    <td colspan="5"><strong>Indeks</strong></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>IHSG</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/1.Sample Data Indeks IHSG.png') }}" rel="prettyPhoto" title="Sample data Indeks IHSG"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Indeks Sektoral</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/2.Sample Data Indeks Sektoral.png') }}" rel="prettyPhoto" title="Sample data Indeks Sektoral"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Indeks LQ45</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/3.Sample-Indeks-LQ45.png') }}" rel="prettyPhoto" title="Sample data Indeks LQ45"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>IDX30</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Indeks Kompas 100</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/4.Sample_Index-kompas-100.png') }}" rel="prettyPhoto" title="Sample data Indeks Kompas 100"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>JII</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/5.Sample-Indeks-JII.png') }}" rel="prettyPhoto" title="Sample data Indeks JII"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Pefindo</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kompas 100</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Sri Kehati</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Sm Infra</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>MNC 36</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td rowspan="2">Indeks Individual</td>
                                    <td rowspan="2">Excel</td>
                                    <td>per indeks per tahun</td>
                                    <td class="text-right">Rp5.000</td>
                                    <td rowspan="2" class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/indeks/6.Sample Data Daily Indeks.png') }}" rel="prettyPhoto" title="Sample data Daily Indeks"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td>All indeks per tahun</td>
                                    <td class="text-right">Rp700.000</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>IGBX</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr class="info">
                                    <td>4</td>
                                    <td colspan="5"><strong>Corporate Action</strong></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Merger & Akuisisi</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/1.Sample Merger & Akuisisi.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Merger & Akuisisi"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Delisting</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/2.Sample Delisted Perusahaan.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Delisting"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List IPO & Underwriter</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/3.Sample list of IPO.png') }}" rel="prettyPhoto" title="Sample data Corporate Action List IPO & Underwriter"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Right Issue (IDX)</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/4.Sample Right Issue.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Right Issue"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Stock Split</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/5.Sample Stock Split IDX.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Stock Split"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Dividen (KSEI) Dividen Saham</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/6.Sample-Dividen-Saham.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Dividen Saham"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Dividen (KSEI) Dividen Tunai</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/7. Sample Dividen Tunai.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Dividen Tunai"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Reverse Stock (KSEI)</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/8.Sample Reverse Stock.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Reverse Stock"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kepemilikan Saham (KSEI)</td>
                                    <td>Text</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/11.Sample Komposisi Kepemilikan Balance KSEI.png') }}" rel="prettyPhoto" title="Sample data Kompisisi Kepemilikan Saham"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Tanggal Penyampaian LK</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/9.Sample Tgl Penyampaian Lk.png') }}" rel="prettyPhoto" title="Sample data Corporate Action Tanggal Pemyampaian LK"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Jadwal Corporate Action</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp40.000</td>
                                    <td class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/10.Sample Jadwal Corp Actions.png') }}" rel="prettyPhoto" title="Sample data Jadwal Corporate Action"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Ringkasan Kinerja</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp30.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Market Cap Yearly</td>
                                    <td>PDF/Excel</td>
                                    <td>per tahun</td>
                                    <td class="text-right">Rp30.000</td>
                                    <td rowspan="2" class="text-center"><a href="{{ asset('assets/ticmi/images/sample-data/corporate-action/12.Sample Market Cap Tahunan.png') }}" rel="prettyPhoto" title="Sample data Market Cap Yearly"><span class="fa fa-file-excel-o"></span></a></td>
                                </tr>
                                <tr class="info">
                                    <td>5</td>
                                    <td colspan="5"><strong>List Index Konstituen</strong></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen LQ45</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen JII</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Bisnis 27</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen IDX 30</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Pefindo 25</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Sri kehati</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Indeks Syariah</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Sminfra</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen MNC 36</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Kompas 100</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Investor 33</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>List Konstituen Infobank</td>
                                    <td>PDF/Excel</td>
                                    <td>per dokumen per tahun</td>
                                    <td class="text-right">Rp8.000</td>
                                    <td class="text-center"></td>
                                </tr>
                                </tbody>
                            </table>
                            <p>* Harga dapat berubah sewaktu-waktu, harap konfirmasi terlebih dahulu ketersediaan data dan harga di <a href="mailto:info@ticmi.co.id?subject=Permintaan Data Khusus - [isi nama Anda disini]">info@ticmi.co.id</a></p>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
        <br>
    </section><!-- Page Default -->
@endsection