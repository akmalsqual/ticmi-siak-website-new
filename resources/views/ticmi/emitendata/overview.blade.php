@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ url('/assets/images/ticmi/header-overview-data-service.jpg') }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">STAY AHEAD IN CAPITAL MARKET WITH OUR RELIABLE DATA</h4>

                            <p style="text-align:justify">
                                The Indonesia Capital Market Institute bekerja sama denga Bursa Efek Indonesia menyediakan referensi pasar modal berupa
                                data dan informasi yang dapat diakses kapanpun dan dimanapun. Nikmati akses ke data-data pasar modal Indonesia mulai tahun 1977
                                dalam formta <em>Excel (.xls)</em>, <em>pdf</em>, dan <em>teks (.txt)</em> yang didesain khusus untuk pengolahan data Anda.
                            </p>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
        <br>
        <div class="container parent-has-overlay">


            <div class="row clearfix">

                <!-- Column -->
                <div class="col-sm-6">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-white icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-financial"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Koleksi data yang lengkap</h4>
                            <ul class="go-widget">
                                <li><a href="#">Tersedia sejak tahun 1977</a></li>
                                <li><a href="#">Sudah termasuk jasa pembuatan surat riset</a></li>
                                <li><a href="#">Dapat dikustomisasi</a></li>
                            </ul>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->

                <!-- Column -->
                <div class="col-sm-6">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-white icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-hand-touch2"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Menu paket yang <em>user friendly</em></h4>
                            <ul class="go-widget">
                                <li><a href="#">Data daily per tahun</a></li>
                                <li><a href="#">Data per emiten</a></li>
                                <li><a href="#">Ekstra data disetiap paket</a></li>
                            </ul>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->
            </div>
            <br>
            <div class="row">
                <!-- Column -->
                <div class="col-sm-6">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-white icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-box-withfolders"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Pilihan format yang variatif</h4>
                            <ul class="go-widget">
                                <li><a href="#">Format Excel (.xls)</a></li>
                                <li><a href="#">Format PDF</a></li>
                                <li><a href="#">Format Text (.txt)</a></li>
                            </ul>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->
                <!-- Column -->
                <div class="col-sm-6">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-white icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-globe"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Akses tanpa batas</h4>
                            <ul class="go-widget">
                                <li><a href="#">Desktop (PC & Mac)</a></li>
                                <li><a href="#">Mobile (handphone & tablet)</a></li>
                                <li><a href="#">Apps (RIDMI)</a></li>
                            </ul>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->
            </div>


            <div class="row pricing-sm-col">
                <!-- Title Block -->
                <div class="col-md-12 clearfix">
                    <div class="course-full-detail content-box  bg-white shadow">
                        <div class="title-container" style="margin-bottom: 0;">
                            <div class="title-wrap" style="margin-bottom: 0;">
                                <h4 class="title">Harga Paket Data</h4>
                                <span class="separator line-separator"></span>
                            </div>
                        </div><!-- Title Container -->
                        <div class="alert alert-info">
                            Untuk dapat melakukan unduh data silahkan berlangganan Paket Data kami. Discount 10% bagi Anda pemegang SID dan sudah mendaftar sebagai Pemegang SID.
                        </div>
                    </div>
                </div><!-- Title Block -->
            </div>

            <div class="row pricing-sm-col">
                <div class="clearfix"><br></div>
                @if($paketDataAll && $paketDataAll->count() > 0)
                    @foreach($paketDataAll as $item)
                        <div class="col-md-3 col-sm-6">
                            <!-- Pricing Wrapper -->
                            <div class="pricing-wrap">
                                <!-- Pricing Header -->
                                <div class="pricing-header">
                                    <div class="pricing-icon">
                                        <img src="{{ asset('assets/images/default/icon/'.$item->icon) }}" alt="Pricing Icon" class="img-responsive" width="70" height="70">
                                    </div><!-- Pricing Icon -->
                                    <!-- Pricing Title -->
                                    <div class="pricing-title">
                                        <h5><span class="color">{{ $item->name }}</span></h5>
                                        <span>Langganan Perbulan</span>
                                    </div>
                                </div><!-- Pricing Header -->
                                <!-- Pricing Content -->
                                <ul class="pricing-body">
                                    <li>Rp {{ number_format(trim($item->price))}} <span class="duration">/1 Bulan</span> </li>
                                    <li>{{ ($item->jml_download == 9999) ? "Unlimited":$item->jml_download }} File <span class="duration">Unduhan</span> </li>
                                    <li> 1 Kali <span class="duration">Surat Riset</span> </li>
                                    <li>
                                        <div class="file-list">File yang di dapat di unduh</div>

                                        @if($dataTypes && $dataTypes->count() > 0)
                                            @php($arrDataType=[])
                                            @if(is_object($item->data_type) && $item->data_type->count() > 0)
                                                @php
                                                    $arrDataType = $item->data_type()->get()->pluck('id')->toArray();
                                                @endphp
                                            @endif
                                            <ul class="datatype">
                                                @foreach($dataTypes->where('parent_id',0) as $dataType)
                                                    <li> <i class="fa fa-arrow-right"></i> <a href="javascript:return false;">{{ $dataType->name }}</a>
                                                        @php
                                                            $dataTypeChilds = $dataType->where('parent_id', $dataType->id)->where('is_active',1)->get();
                                                        @endphp
                                                        @if($dataTypeChilds && $dataTypeChilds->count() > 0)
                                                            <ul class="datatype-child">
                                                                @foreach($dataTypeChilds as $dataTypeChild)
                                                                    <li>{{ $dataTypeChild->name }}
                                                                        @if(in_array($dataTypeChild->id, $arrDataType))
                                                                            <span class="label label-success pull-right"><i class="fa fa-check"></i></span>
                                                                        @else
                                                                            <span class="label label-danger pull-right"><i class="fa fa-times"></i></span>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                </ul><!-- Pricing Content -->
                                <!-- Pricing Footer (Book Button) -->
                                <div class="pricing-footer text-center">
                                    @if(auth()->check())
                                    {{ $paketUserActive = ''}}
                                        @if($paketUserActive && $paketUserActive->count() > 0)
                                            <a href="#" class="btn btn-sm btn-warning"> Anda sudah berlangganan Paket Data </a>
                                        @else
                                            <a href="{{ route('paket-data.checkout',['code'=>$item->slug]) }}" class="btn btn-block">Berlangganan +</a>
                                        @endif
                                    @else
                                        <a href="{{ route('paket-data.checkout',['paket-data-code'=>$item->slug]) }}" class="btn btn-block">Berlangganan +</a>
                                    @endif
                                </div><!-- Pricing Footer -->
                            </div><!-- Pricing Wrapper -->
                        </div><!-- Column -->
                    @endforeach
                @endif
            </div><!-- row -->

            <div class="row pricing-sm-col">
                <!-- Title Block -->
                <div class="col-md-12 clearfix">
                    <div class="course-full-detail content-box  bg-white shadow">
                        <div class="alert alert-info ">
                            <strong>Informasi</strong>
                            <ol>
                                <li>Mohon cek terlebih dahulu ketersediaan data yang Anda butuhkan di menu Data Pasar Modal sebelum memilih paket di atas.</li>
                                <li>Data yang tersedia di paket tidak dapat dibeli secara satuan tanpa berlangganan paket.</li>
                                <li>Data yang tidak tersedia di paket dapat dibeli secara satuan di menu Data Pasar Modal Lainnya</li>
                                <li>Pelanggan yang memiliki SID berhak mendapatkan diskon sebesar 10% untuk pembelian data</li>
                                <li>Untuk pembelian extra data satuan dikenakan biaya sebesar Rp25.000/data.</li>
                                <li>Minimum masa berlangganan paket data: 1 bulan</li>
                                <li>Anda dapat mengganti paket langganan 1 bulan 1 kali</li>
                                <li>Pengajuan surat riset publik daapt dilakukan dengan berlangganan data minimal "Paket Value"</li>
                                <li>Revisi surat riset dikenakan biaya sebesar Rp100.000</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- Title Block -->
            </div>


            <!-- Blog Share Post -->
            <div class="share">
                <h5>Share Post : </h5>
                <ul class="social-icons">
                    <li class="fb-share-button"  data-href="{{ route('cmk') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmk') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                        {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                        {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                    </li>
                </ul><!-- Blog Social Share -->
            </div><!-- Blog Share Post -->

        </div>
    </section><!-- Page Default -->
@endsection