@extends('layouts.app')
@section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Konfirmasi Surat Riset</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            @include('errors.list')
                            @include('flash::message')


                            <div class="content-box bg-white">
                                @if(empty($suratRiset->isPaymentApproved))
                                    @if($suratRiset->isConfirm)
                                        <div class="alert alert-warning">
                                            Anda sudah melakukan konfirmasi sebelumnya. Konfirmasi Anda sedang dalam proses verifikasi.
                                        </div>
                                    @endif
                                    {!! Form::open(['url'=>route('member.suratriset.konfirmasi.store',['suratriset'=>$suratRiset]),'files'=>true]) !!}
                                    <div class="form-group">
                                        {!! Form::label('invoice_no','No Invoice') !!} <strong class="text-red">*</strong>
                                        {!! Form::text('invoice_no',$suratRiset->invoiceNumber,['class'=>'form-control','readonly'=>'readonly']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('virtual_account','No Virtual Account BCA') !!}  <strong class="text-red">*</strong>
                                        {!! Form::text('virtual_account',$suratRiset->virtual_account,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nominal_transfer','Nominal Transfer ') !!}  <strong class="text-red">*</strong> ( <small>Sesuai dengan jumlah yang ditransfer</small> )
                                        {!! Form::text('nominal',$suratRiset->price,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('transfer_rek','Transfer dari no rekening ?') !!}  <strong class="text-red">*</strong>
                                        {!! Form::text('no_rekening',$suratRiset->confirmRekening,['class'=>'form-control','placeholder'=>'No rekening yang digunakan untuk transfer']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('transfer_rek_holder','Nama pemegang rekening') !!} <strong class="text-red">*</strong>
                                        {!! Form::text('nama_rekening',$suratRiset->confirmRekeningNama,['class'=>'form-control','placeholder'=>'Nama pemegang rekening yang digunakan untuk transfer']) !!}
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            {!! Form::label('tgl_transfer','Tanggal dan Waktu Transfer') !!} <strong class="text-red">*</strong>
                                            {!! Form::text('tgl_transfer',((is_object($suratRiset) && $suratRiset->confirmDate != '') ? $suratRiset->confirmDate:null),['class'=>'form-control datepicker','placeholder'=>'yyyy-mm-dd']) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::label('jam_transfer','&nbsp;') !!}
                                            {!! Form::text('jam_transfer',$suratRiset->confirmTime,['class'=>'form-control','placeholder'=>'hh:mm']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('transfer_rek_holder','Bukti Transfer') !!}
                                        {!! Form::file('bukti_transfer') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Konfirmasi Surat Riset',['class'=>'btn btn-block','data-toggle'=>'loading','data-loading-text'=>'Loading...']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                @else
                                    <div class="alert alert-info">Permohonan Anda sedang dalam proses</div>
                                @endif
                            </div>

                        <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection