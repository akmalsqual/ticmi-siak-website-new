@extends('layouts_ticmi.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h5 class="title">Konfirmasi Surat Riset</h5>
                        <h6 class="sub-title">&nbsp;</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-md-6">

                    <div class="content-box shadow bg-white">
                        <h4>Konfirmasi</h4>
                        @include('errors.list')
                        @include('flash::message')
                        @if(empty($suratRiset->isPaymentApproved))
                            @if($suratRiset->isConfirm)
                                <div class="alert alert-warning">
                                    Anda sudah melakukan konfirmasi sebelumnya. Konfirmasi Anda sedang dalam proses verifikasi.
                                </div>
                            @endif
                            {!! Form::open(['url'=>route('member.suratriset.konfirmasi.store',['suratriset'=>$suratRiset]),'files'=>true]) !!}
                            <div class="form-group">
                                {!! Form::label('invoice_no','No Invoice') !!} <strong class="text-red">*</strong>
                                {!! Form::text('invoice_no',$suratRiset->invoiceNumber,['class'=>'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('virtual_account','No Virtual Account BCA') !!}  <strong class="text-red">*</strong>
                                {!! Form::text('virtual_account',$suratRiset->virtual_account,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('nominal_transfer','Nominal Transfer ') !!}  <strong class="text-red">*</strong> ( <small>Sesuai dengan jumlah yang ditransfer</small> )
                                {!! Form::text('nominal',$suratRiset->price,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('transfer_rek','Transfer dari no rekening ?') !!}  <strong class="text-red">*</strong>
                                {!! Form::text('no_rekening',$suratRiset->confirmRekening,['class'=>'form-control','placeholder'=>'No rekening yang digunakan untuk transfer']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('transfer_rek_holder','Nama pemegang rekening') !!} <strong class="text-red">*</strong>
                                {!! Form::text('nama_rekening',$suratRiset->confirmRekeningNama,['class'=>'form-control','placeholder'=>'Nama pemegang rekening yang digunakan untuk transfer']) !!}
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    {!! Form::label('tgl_transfer','Tanggal dan Waktu Transfer') !!} <strong class="text-red">*</strong>
                                    {!! Form::text('tgl_transfer',((is_object($suratRiset) && $suratRiset->confirmDate != '') ? $suratRiset->confirmDate:null),['class'=>'form-control datepicker','placeholder'=>'yyyy-mm-dd']) !!}
                                </div>
                                <div class="col-sm-6">
                                    {!! Form::label('jam_transfer','&nbsp;') !!}
                                    {!! Form::text('jam_transfer',$suratRiset->confirmTime,['class'=>'form-control','placeholder'=>'hh:mm']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('transfer_rek_holder','Bukti Transfer') !!}
                                {!! Form::file('bukti_transfer') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Konfirmasi Surat Riset',['class'=>'btn btn-block','data-toggle'=>'loading','data-loading-text'=>'Loading...']) !!}
                            </div>
                            {!! Form::close() !!}
                        @else
                            <div class="alert alert-info">Permohonan Anda sedang dalam Proses</div>
                        @endif

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Detail Surat Riset
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <table cellspacing="0" class="shop_table cart table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Nama</td>
                                            <td class="">{{ $suratRiset->namaMhs }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Surat Pengantar</td>
                                            <td class="">{{ $suratRiset->noSuratPengantar }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">Tanggal Surat Pengantar</td>
                                            <td style="">{{ date('d F Y', strtotime($suratRiset->tglSuratPengantar)) }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">Nama Universitas</td>
                                            <td style="">{{ $suratRiset->universitas }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">No Induk Mahasiswa</td>
                                            <td style="">{{ $suratRiset->noIndukMhs }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">Jenis Tugas Akhir</td>
                                            <td style="">{{ $suratRiset->jenisTugasAkhir }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">Jurusan</td>
                                            <td style="">{{ $suratRiset->programStudi }}</td>
                                        </tr>
                                        <tr>
                                            <td style="">Judul</td>
                                            <td style="">{{ $suratRiset->judulTugasAkhir }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- Panel Group -->
                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection