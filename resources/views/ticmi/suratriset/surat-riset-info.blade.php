@extends('layouts.app')
@section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Surat Riset</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            @include('errors.list')
                            @include('flash::message')

                            <table class="table table-bordered bg-google-red typo-light" style="">
                                <tr>
                                    <td width="25%"><span>No Pengajuan <br> <strong>#{{ $suratRiset->invoiceNumber }}</strong></span></td>
                                    <td width="25%"><span>Biaya <br> <strong>Rp{{ number_format($suratRiset->price,0,',','.') }}</strong></span></td>
                                    <td width="25%">
                                        <span>Status Pengajuan <br>
                                            @if(empty($suratRiset->approveSuperAdmin))
                                                @if($suratRiset->isConfirm)
                                                    <strong>Menunggu verifikasi pembayaran</strong>
                                                @else
                                                    <strong>Diajukan</strong>
                                                @endif
                                            @else
                                                @if($suratRiset->sendEmailDate)
                                                    <strong>Dikirim pada tanggal : {{ date("d F Y", strtotime($suratRiset->sendEmailDate)) }}</strong>
                                                @else
                                                    <strong>Diproses</strong>
                                                @endif
                                            @endif
                                        </span>
                                    </td>
                                    <td width="25%"><span>Metode Pembayaran <br> {!! ($suratRiset->price) ? '<strong>Transfer</strong>':'-' !!}</span></td>
                                </tr>
                                @if($suratRiset->price)
                                <tr>
                                    <td colspan="2">
                                        <span>No Invoice <br>
                                            <strong>{{ $suratRiset->invoiceNumber }}</strong>
                                        </span>
                                    </td>
                                    <td colspan="2">
                                        <span>Virtual Account <br>
                                            <strong>BCA : {{ $suratRiset->virtual_account }}</strong>
                                        </span>
                                    </td>
                                </tr>
                                @endif
                            </table>

                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td>Nama Mahasiswa</td>
                                    <td>{{ $suratRiset->namaMhs }}</td>
                                </tr>
                                <tr>
                                    <td>No Surat Pengantar</td>
                                    <td class="">{{ $suratRiset->noSuratPengantar }}</td>
                                </tr>
                                <tr>
                                    <td style="">Tanggal Surat Pengantar</td>
                                    <td style="">{{ date('d F Y', strtotime($suratRiset->tglSuratPengantar)) }}</td>
                                </tr>
                                <tr>
                                    <td style="">Nama Universitas</td>
                                    <td style="">{{ $suratRiset->universitas }}</td>
                                </tr>
                                <tr>
                                    <td style="">No Induk Mahasiswa</td>
                                    <td style="">{{ $suratRiset->noIndukMhs }}</td>
                                </tr>
                                <tr>
                                    <td style="">Jenis Tugas Akhir</td>
                                    <td style="">{{ $suratRiset->jenisTugasAkhir }}</td>
                                </tr>
                                <tr>
                                    <td style="">Jurusan</td>
                                    <td style="">{{ $suratRiset->programStudi }}</td>
                                </tr>
                                <tr>
                                    <td style="">Judul</td>
                                    <td style="">{{ $suratRiset->judulTugasAkhir }}</td>
                                </tr>
                                </tbody>
                            </table>

                            <table>
                                <tr>
                                    <td>
                                        <a href="{{ route('member.suratriset') }}" class="btn btn-inverse">Kembali</a>
                                        @if(empty($suratRiset->approveSuperAdmin))
                                            @if($suratRiset->isConfirm == 0)
                                                <a href="{{ route('member.suratriset.konfirmasi',['invoicesuratriset'=>$suratRiset->invoiceNumber]) }}" class="btn btn-inverse">Konfirmasi Pembayaran</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            </table>

                            <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection