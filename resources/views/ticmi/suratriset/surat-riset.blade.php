@extends('layouts.app')
@section('content')
    <link href="{{ asset('assets/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Surat Riset</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            @include('errors.list')
                            @include('flash::message')

                            @if($paketUserActive && $paketUserActive->count() > 0)
                                <div class="alert alert-info">
                                    <ol>
                                        <li>Anda bisa mengajukan <strong>Permohonan Surat Riset</strong> sebanyak <strong>1 (satu) kali</strong> tanpa dipungut biaya.</li>
                                        <li>Pastikan isian Permohonan Surat Riset Anda terisi dengan benar dan sesuai dengan surat pengantar. </li>
                                        <li>TICMI tidak melayani untuk perubahan data baik itu dikarenakan kesalahan pengisian data atau perubahan data isian permohonan setelah permohonan diproses atau dikirim.</li>
                                        <li>Jika ingin melakukan permohonan surat riset kembali maka akan dikenakan biaya <strong>Rp100.000,- (seratus ribu rupiah)</strong>.</li>
                                    </ol>
                                </div>
                                @if($jmlSuratRiset && $jmlSuratRiset->count() >= 1)
                                    <a href="{{ route('member.suratriset.create') }}"  class="btn btn-info pull-right btnSuratRisetBerbayar">Buat Permohonan Surat Riset Bayar</a>
                                @else
                                    <a href="{{ route('member.suratriset.create') }}" class="btn btn-info pull-right">Buat Permohonan Surat Riset</a>
                                @endif
                                <div class="clearfix"></div>
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No Surat Pengantar</th>
                                        <th>Tanggal</th>
                                        <th>Judul T.A/Skripsi/Thesis</th>
                                        <th>Aksi/Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($suratRiset && $suratRiset->count() > 0)
                                        @foreach($suratRiset as $item)
                                            <tr>
                                                <td>{{ $item->noSuratPengantar }}</td>
                                                <td>{{ $item->createdDate }}</td>
                                                <td>{{ $item->judulTugasAkhir }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        @if(empty($item->approveSuperAdmin))
                                                            <a href="{{ route('member.suratriset.info',['invoicesuratriset'=>$item->invoiceNumber]) }}" class="btn btn-success btn-xs" title="Lihat Pengajuan Surat Riset"><i class="fa fa-search"></i></a>
                                                            <a href="{{ route('member.suratriset.edit',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-info btn-xs" title="Ubah Pengajuan Surat Riset"><i class="fa fa-pencil"></i></a>
                                                            {{--                                            <a href="{{ route('member.suratriset.destroy',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-danger btn-xs delconfirm" data-del-message="Apakah Anda yakin untuk hapus pengajuan ini ? Data yang sudah dihapus tidak bisa di kembalikan." title="Hapus Pengajuan Surat Riset"><i class="fa fa-times"></i></a>--}}
                                                            @if($item->price != 0 && $item->isConfirm == 0)
                                                                <a href="{{ route('member.suratriset.konfirmasi',['invoicesuratriset'=>$item->invoiceNumber]) }}" class="btn btn-info btn-xs" title="Konfirmasi Pengajuan Surat Riset"><i class="fa fa-money"></i></a>
                                                            @endif
                                                        @else
                                                            @if($item->sendEmailDate)
                                                                <span class="label label-info">Dikirim pada tanggal : {{ date("d F Y", strtotime($item->sendEmailDate)) }}</span>
                                                            @else
                                                                <span class="label label-warning">Diproses</span>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">Belum ada pengajuan surat riset</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                @else
                                    <div class="alert alert-info">
                                        <p>
                                            Untuk dapat mengajukan surat riset, silahkan melakukan pembelian <strong>Paket Data</strong> terlebih dahulu. Klik <a href="{{ route('paket-data.index') }}">disini</a> untuk mendapatkan Paket Data.
                                        </p>
                                        <p>
                                            Jika sudah mempunyai Paket Data yang Aktif maka Anda bisa melakukan Pengajuan Permohonan Surat Riset melalui menu ini
                                        </p>
                                    </div>
                                @endif

                            <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection