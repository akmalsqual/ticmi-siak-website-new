<div class="alert alert-info" role="alert">
    <p>
        <h5>Informasi</h5>
        <ol>
            <li>
                Silahkan isi form di bawah untuk mengajukan permohonan surat riset kepada TICMI. Isi form dibawah sesuai dengan
                data yang sebenarnya.
            </li>
            <li>
                Kami akan melakukan verifikasi terhadap data yang diajukan. Periksa kembali data yang sudah dimasukkan, pastikan tidak ada <strong style="color: #cf0404;"><em>typo error</em></strong>
                untuk mempermudah proses pembuatan surat riset.
            </li>
        </ol>
    </p>
</div>
<div class="form-group">
    {!! Form::label('noSuratPengantar','No Surat Pengantar Universitas',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::text('noSuratPengantar',null,['class'=>'form-control','placeholder'=>'Masukkan No Surat Pengantar dari Universitas']) !!}
    </div>
</div>
{{--<div class="form-group">--}}
    {{--{!! Form::label('tglSuratPengantar','Tanggal Surat Pengantar Universitas dibuat',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::text('tglSuratPengantar',null,['class'=>'form-control datepicker','placeholder'=>'Tanggal surat pengantar dibuat','autocomplete'=>'off']) !!}--}}
    {{--</div>--}}
{{--</div>--}}
<div class="form-group">
    {!! Form::label('tglSuratPengantar','Tanggal Surat Pengantar Universitas dibuat',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-5">
        <div class="input-group date">
            {{ Form::text('tglSuratPengantar', null, ['class' => 'form-control date-pickers']) }}
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('universitas','Nama Universitas',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::text('universitas',null,['class'=>'form-control','placeholder'=>'Nama Universitas']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('noIndukMhs','No Induk Mahasiswa',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::text('noIndukMhs',null,['class'=>'form-control','placeholder'=>'No Induk Mahasiswa']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('jenisTugasAkhir','Jenis Tugas Akhir',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::select('jenisTugasAkhir',$jnsTugasAkhir,null,['class'=>'form-control','placeholder'=>'Pilih Jenis Tugas Akhir']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('programStudi','Jurusan / Program Studi',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::text('programStudi',null,['class'=>'form-control','placeholder'=>'Jurusan / Program Studi (contoh : S1 Akuntansi)']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('judulTugasAkhir','Judul T.A / Skripsi / Thesis / Disertasi',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::textarea('judulTugasAkhir',null,['class'=>'form-control','placeholder'=>'Masukkan Judul T.A / Skripsi / Thesis / Disertasi sesuai dengan yang tertera di Surat Pengantar']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('surat_pengantar','Surat Pengantar (JPG,PNG)',['class'=>'col-sm-3 control-label']) !!} <strong class="text-red">*</strong>
    <div class="col-sm-8">
        {!! Form::file('surat_pengantar') !!}
        <small class="text-red">Format file yang diterima yaitu <strong>JPG</strong> dan <strong>PNG</strong> dengan ukuran maksimal <strong>1 (satu) Mb</strong></small>
    </div>
    @if(isset($suratRiset) && !empty($suratRiset->suratPengantar))
        <div class="col-sm-8 col-sm-offset-3 bg-lgrey">
            <p>Di bawah ini merupakan surat pengantar yang sudah di upload, jika ingin melakukan perubahan surat pengantar silahkan untuk upload kembali.</p>
            <a href="{{ url('upload/surat-pengantar/'.$suratRiset->suratPengantar) }}"  data-rel="prettyPhoto[suratpengantar]">
                <img src="{{ url('upload/surat-pengantar/'.$suratRiset->suratPengantar) }}" class="img-responsive" alt="Surat Pengantar" width="100">
            </a>
        </div>
    @endif
</div>
<div class="form-group">
    {!! Form::label('dokumenskripsi','Dokumen T.A / Skripsi / Thesis / Disertasi (Opsional) (PDF)',['class'=>'col-sm-3 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::file('dokumenskripsi') !!}
        <small class="text-red">Format file yang diterima yaitu <strong>PDF</strong></small>
    </div>
    @if(isset($suratRiset) && !empty($suratRiset->skripsi))
        <div class="col-sm-8 col-sm-offset-3 bg-lgrey">
            <p>Di bawah ini merupakan Data T.A / Skripsi / Thesis / Disertasi yang sudah di upload, jika ingin melakukan perubahan Data T.A / Skripsi / Thesis / Disertasi silahkan untuk upload kembali.</p>
            <a href="{{ url('upload/skripsi/'.$suratRiset->skripsi) }}"  data-rel="prettyPhoto[skripsi]">
                <img src="{{ url('upload/skripsi/'.$suratRiset->skripsi) }}" class="img-responsive" alt="Data T.A / Skripsi / Thesis / Disertasi" width="100">
            </a>
        </div>
    @endif
</div>
<div class="alert alert-info" style="margin-top: 20px;">
    Jika Anda submit Form Permohonan ini maka Anda menyatakan <strong>bersedia</strong> memberikan salinan hasil Riset atau Penelitian yang telah diuji oleh pihak Akademik kepada <strong>PT Indonesian Capital Market Electronic Library</strong>, Sebagai bahan referensi penelitian yang dapat dimanfaatkan oleh publik.
</div>
<div class="form-group">
    <div class="col-sm-12">
        <small><em>* Surat riset akan dikirimkan melalui email yang telah diregistrasikan dengan maksimal 3 (tiga) hari kerja.</em></small> <br><br>
        {{ Form::submit('Submit Permohonan',['class'=>'btn btn-primary btn-block btn-lg','data-toggle'=>'loading','data-loading-text'=>'Loading ...']) }}
    </div>
</div>

<script>
	<?= \Helper::date_formats('$(".date-pickers")', 'js',['format'=>'yyyy-mm-dd','autoclose'=>true], $date_db = 'Y-m-d', $date_php = 'Y-m-d') ?>
</script>