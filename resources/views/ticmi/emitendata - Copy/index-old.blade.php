@extends('layout.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/library.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $jenisData }}</h3>
                        <h6 class="sub-title">Data Pasar Modal</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark shop-sm-col">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                {{ Form::open(['url'=>route('dataemiten',['doctype'=>$doctype,'type'=>$type,'title'=>$title]),'method'=>'get']) }}
                    {{ Form::hidden('title',$title) }}
                    <div class="form-group">
                        <div class="col-sm-6">
                            {{ Form::select('code',$emiten,$code,['class'=>'form-control','placeholder'=>'Cari Emiten']) }}
                        </div>
                        <div class="col-sm-3">
                            {{ Form::selectRange('year',date('Y'),1977,$year,['class'=>'form-control','placeholder'=>'Tahun']) }}
                        </div>
                        <div class="col-sm-3">
                            {{ Form::button('CARI',['type'=>'submit','class'=>'btn btn-block']) }}
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
            <br>
            {{--<div class="row">--}}
                @php
                    $i = 1;
                    $limitYear = date('Y') - 5
                @endphp
                @if($emitenData && $emitenData->count() > 0)
                    @foreach($emitenData as $item)
                        @if($i%4 == 0)
                            <div class="row">
                        @endif
                        <!-- Item Begins -->
                        <div class="col-sm-6 col-md-3">
                            <!-- Shop Grid Wrapper -->
                            <div class="shop-wrap">
                                <!-- Shop Image Wrapper -->
                                <div class="shop-img-wrap">
                                    <!-- <canvas id="{{ $item->idEmitenData }}" data-toggle="loadpdf" data-id-data="{{ $item->idEmitenData }}" data-doctype="{{ $item->docType }}" data-type="{{ $item->type }}" class="canvas-shop" width="262" height="262"></canvas> -->
                                    {{--<img width="500" height="500" src="http://glorytheme.com/template/universh/demo/images/shop/shop-01.jpg" class="img-responsive img-def" alt="Shop">--}}
                                    <img width="500" height="500" style="height: 300px;" src="http://ticmi.co.id{{ GeneralHelper::getThumbPath($item->docType,$item->type,$item->year,$item->filename) }}" class="img-responsive img-def" alt="Shop">
                                    <h6 class="product-label bg-pink">{{ $item->year }}</h6>
                                    <div class="shop-img-loader"></div>
                                </div><!-- Shop Wraper -->
                                <!-- Shop Detail Wrapper -->
                                <div class="product-details">
                                    <div class="shop-title-wrap">
					
                                        <a href="{{ route('dataemiten.viewsingle',['emitendata'=>$item->idEmitenData,'doctype'=>$doctype,'type'=>$type,'title'=> Slugify::slugify($item->title)]) }}">
                                        <h6 class="product-cat">{{ (strlen($item->name) >= 30) ? substr($item->name,0,30).'...':$item->name }}</h6>
                                        <h5 class="product-name">
                                            {{ $item->code.' - '.$item->year }}
                                            @if($item->year < $limitYear)
                                                @if(auth()->check())
                                                    @if($roles->name != 'member')
                                                        <span class="pull-right">Rp{{ number_format(GeneralHelper::getPriceList($item->docType,$item->type),0,',','.') }}</span>
                                                    @endif
                                                @else
                                                    <span class="pull-right">Rp{{ number_format(GeneralHelper::getPriceList($item->docType,$item->type),0,',','.') }}</span>
                                                @endif
                                            @endif
                                        </h5>
					                        </a>
                                    </div><!-- Shop Detail Wrapper -->
                                    <div class="shop-btns">
                                        <a class="option-btn" href="#" onclick="return false"> &plus; </a>
                                        <ul class="shop-meta">
                                            @if(Auth::check())
                                                @if($item->year >= $limitYear)
                                                    <li><a href="{{ route('dataemiten.getfile',['doctype'=>$item->docType,'type'=>$item->type,'emitendataencrypt'=>encrypt($item->idEmitenData)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-title="{{ $item->title }}"  data-id="{{ $item->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                @else
                                                    @if(Auth::user()->can('download-data-emiten'))
                                                        <li><a href="{{ route('dataemiten.getfile',['doctype'=>$item->docType,'type'=>$item->type,'emitendataencrypt'=>encrypt($item->idEmitenData)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-title="{{ $item->title }}"  data-id="{{ $item->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                    @else
                                                        <li><a href="#" class="addDocToCart" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-title="{{ $item->title }}"  data-id="{{ $item->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}" title="Tambah ke keranjang {{ $item->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                    @endif
                                                @endif
                                            @else
                                                @if($item->year >= $limitYear)
                                                    <li><a href="{{ route('dataemiten.getfile',['doctype'=>$item->docType,'type'=>$item->type,'emitendataencrypt'=>encrypt($item->idEmitenData)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-title="{{ $item->title }}"  data-id="{{ $item->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                @else
                                                    <li><a href="#" class="addDocToCart" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-title="{{ $item->title }}"  data-id="{{ $item->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}" title="Tambah ke keranjang {{ $item->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                @endif
                                            @endif
                                            <li><a href="{{ route('dataemiten.viewpdf',['emitendata'=>$item->idEmitenData,'doctype'=>$doctype,'type'=>$type]) }}" class="modalIframeDocument" title="Lihat {{ $item->title }}" onclick="ga('send', 'event', 'View Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idEmitenData }}" data-reftable="1" data-emiten="{{ (is_object($item->emiten) ? $item->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($item->emiten) ? $item->emiten->code:"-") }}" data-year="{{ $item->year }}"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                            {{--<li><a href="#" title="Added to wishlist"><i class="fa fa-heart" aria-hidden="true"></i></a></li>--}}
                                        </ul><!-- Blog Meta -->
                                    </div><!-- Shop btns -->
                                </div><!-- Blog Detail Wrapper -->
                            </div><!-- Blog Wrapper -->
                        </div><!-- Column -->
                        @if($i%4 == 0)
                            </div>
                        @endif
                        @php
                            $i++;
                        @endphp
                    @endforeach
                @endif

            {{--</div>--}}


            <!-- Pagination -->
            <div class="row">
                <div class="col-sm-12">
                    <nav class="text-center">
                        {{ $emitenData->appends(['code'=>$code,'year'=>$year,'title'=>$title])->links() }}
                    </nav><!-- Pagination -->
                </div><!-- Column -->
            </div><!-- Row -->

        </div>
    </div>
@endsection
