@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Main -->
    <section class="relative bg-light typo-dark parallax-bg bg-cover overlay white md shop-sm-col"  data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.5">
    <div class="page-default">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="content-box">
                    <ul class="template-box box-comingsoon parent-has-overlay bg-white" style="background: #fff;">
                        <!--Page Template Logo -->
                        <li class="logo-wrap">
                            <a href="" class="logo">
                                <p class="slogan">File Anda akan segera muncul sebentar lagi</p>
                            </a>
                        </li><!-- Page Template Logo -->
                        <!-- Page Template Content -->
                        <li class="template-content">
                            @if(file_exists($path))
                                <div id="daycounter-comingsooon" class="daycounter clearfix" data-counter="down" data-year="{{ date('Y') }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-month="{{ date('m') }}" data-date="{{ date('d') }}" data-emiten="{{ $idEncrypt }}"></div>
                            @else
                                <p>Maaf File yang Anda cari tidak ditemukan</p>
                            @endif
                        </li><!-- Page Template Content -->
                        <!-- Page Template Descriptin -->
                        <li class="template-content">
                        </li><!-- Page Template Descriptin -->
                    </ul>
                    </div>
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
    </section>
@endsection