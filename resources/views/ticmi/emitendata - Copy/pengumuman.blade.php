@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/library.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $jenisData }}</h3>
                        <h6 class="sub-title">Data Pasar Modal</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <div class="page-default bg-grey typo-dark shop-sm-col">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                {{ Form::open(['url'=>route('dataemiten.pengumuman',['doctype'=>$doctype,'type'=>$type]),'method'=>'get']) }}
                <div class="form-group">
                    <div class="col-sm-6">
                        {!! Form::text('title',$title,['class'=>'form-control','placeholder'=>'Cari Berdasarkan Judul']) !!}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::selectRange('year',date('Y'),1977,$year,['class'=>'form-control','placeholder'=>'Tahun']) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::button('CARI',['type'=>'submit','class'=>'btn btn-block']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <br>
            {{--<div class="row">--}}
            @php
                $i = 1;
            @endphp
            @if($pengumuman && $pengumuman->count() > 0)
                @foreach($pengumuman as $item)
                    @if($i%4 == 0)
                        <div class="row">
                        @endif
                        <!-- Item Begins -->
                            <div class="col-sm-6 col-md-3">
                                <!-- Shop Grid Wrapper -->
                                <div class="shop-wrap">
                                    <!-- Shop Image Wrapper -->
                                    <div class="shop-img-wrap">
                                        <img width="500" height="500" style="height: 300px;" src="http://dev.ticmi.co.id{{ GeneralHelper::getThumbPath($item->docType,$type,date('Y',strtotime($item->date)),$item->filename) }}" class="img-responsive img-def" alt="Shop">
                                        <h6 class="product-label bg-pink">{{ date('Y',strtotime($item->date)) }}</h6>
                                        <div class="shop-img-loader"></div>
                                    </div><!-- Shop Wraper -->
                                    <!-- Shop Detail Wrapper -->
                                    <div class="product-details">
                                        <div class="shop-title-wrap">
                                            <h6 class="product-cat"><a href="{{ route('dataemiten.pengumuman.viewpdf',['pengumuman'=>$item->idPeraturan,'doctype'=>$doctype,'type'=>$type]) }}"  class="modalIframeDocument" title="{{ $item->title }}" onclick="ga('send', 'event', 'View Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y', strtotime($item->date)) }}">{{ (strlen($item->title) >= 30) ? substr($item->title,0,30).'...':$item->title }}</a></h6>
                                            <h5 class="product-name">
                                                <a href="{{ route('dataemiten.viewpdf',['emitendata'=>$item->idEmitenData,'doctype'=>$doctype,'type'=>$type]) }}"  class="modalIframeDocument" title="{{ $item->title }}" onclick="ga('send', 'event', 'View Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="">{{ $item->institusi}}</a>
                                                <span class="pull-right">Rp{{ number_format(GeneralHelper::getPriceList($item->docType,$type),0,',','.') }}</span>
                                            </h5>
                                        </div><!-- Shop Detail Wrapper -->
                                        <div class="shop-btns">
                                            <a class="option-btn" href="#" onclick="return false"> &plus; </a>
                                            <ul class="shop-meta">
                                                @php($limitYear = date('Y') - 5)
                                                @if(Auth::check())
                                                    @if(date('Y',strtotime($item->date)) >= $limitYear)
                                                        <li><a href="{{ route('dataemiten.pengumuman.getfile',['doctype'=>$item->docType,'type'=>$type,'emitendataencrypt'=>encrypt($item->idPeraturan)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-title="{{ $item->title }}"  data-id="{{ $item->idPeraturan }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                    @else
                                                        @if(Auth::user()->can('download-data-emiten'))
                                                            <li><a href="{{ route('dataemiten.pengumuman.getfile',['doctype'=>$item->docType,'type'=>$type,'emitendataencrypt'=>encrypt($item->idPeraturan)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-title="{{ $item->title }}"  data-id="{{ $item->idPeraturan }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                        @else
                                                            <li><a href="#" class="addDocToCart" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-title="{{ $item->title }}"  data-id="{{ $item->idPeraturan }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ $item->institusi }}" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}" title="Tambah ke keranjang {{ $item->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                        @endif
                                                    @endif
                                                @else
                                                    @if(date('Y',strtotime($item->date)) >= $limitYear)
                                                        <li><a href="{{ route('dataemiten.pengumuman.getfile',['doctype'=>$item->docType,'type'=>$type,'emitendataencrypt'=>encrypt($item->idPeraturan)]) }}" class="" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-title="{{ $item->title }}"  data-id="{{ $item->idPeraturan }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}" title="Download {{ $item->title }}"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                                    @else
                                                        <li><a href="#" class="addDocToCart" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-title="{{ $item->title }}"  data-id="{{ $item->idPeraturan }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}" title="Tambah ke keranjang {{ $item->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                    @endif
                                                @endif
                                                <li><a href="{{ route('dataemiten.pengumuman.viewpdf',['pengumuman'=>$item->idPeraturan,'doctype'=>$doctype,'type'=>$type]) }}" class="modalIframeDocument" title="Lihat {{ $item->title }}" onclick="ga('send', 'event', 'View Data', '{{ $jenisData }}', '{{ $item->title }}' )" data-id="{{ $item->idPeraturan }}" data-reftable="1" data-emiten="" data-type="{{ GeneralHelper::getJenisData($type,$doctype) }}" data-code="" data-year="{{ date('Y',strtotime($item->date)) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                {{--<li><a href="#" title="Added to wishlist"><i class="fa fa-heart" aria-hidden="true"></i></a></li>--}}
                                            </ul><!-- Blog Meta -->
                                        </div><!-- Shop btns -->
                                    </div><!-- Blog Detail Wrapper -->
                                </div><!-- Blog Wrapper -->
                            </div><!-- Column -->
                            @if($i%4 == 0)
                        </div>
                @endif
                @php
                    $i++;
                @endphp
            @endforeach
        @endif

        {{--</div>--}}


        <!-- Pagination -->
            <div class="row">
                <div class="col-sm-12">
                    <nav class="text-center">
                        {{ $pengumuman->appends(['year'=>$year,'title'=>$title])->links() }}
                    </nav><!-- Pagination -->
                </div><!-- Column -->
            </div><!-- Row -->

        </div>
    </div>
@endsection