@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/library.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $jenisData }}</h3>
                        <h6 class="sub-title">Data Pasar Modal</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->
    <!-- Page Main -->
    <div role="main" class="main">
        <div class="page-default bg-white shop-single">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Member Image Column -->
                    <div class="col-md-5">
                        <div class="owl-carousel dots-inline"
                             data-animatein=""
                             data-animateout=""
                             data-items="1" data-margin=""
                             data-loop="true"
                             data-merge="true"
                             data-nav="false"
                             data-dots="true"
                             data-stagepadding=""
                             data-mobile="1"
                             data-tablet="1"
                             data-desktopsmall="1"
                             data-desktop="1"
                             data-autoplay="false"
                             data-delay="3000"
                             data-navigation="true">
                            <div class="item"><img class="img-responsive" src="{{ asset(GeneralHelper::getThumbPath($emitenData->docType,$emitenData->type,$emitenData->year,$emitenData->filename)) }}" alt="{{ $emitenData->title }}" width="600" height="500"></div>
                            <div class="item"><img class="img-responsive" src="{{ asset(GeneralHelper::getThumbPath($emitenData->docType,$emitenData->type,$emitenData->year,$emitenData->filename)) }}" alt="{{ $emitenData->title }}" width="600" height="500"></div>
                        </div><!-- carousel -->
                    </div><!-- Coloumn -->
                    <!-- Coloumn -->
                    <div class="col-md-7 bg-white shadow" style="padding: 20px;">
                        @php
                            $limitYear =  (int) date('Y') - 3;
                        @endphp

                        <div class="shop-detail-wrap">
                            <h5 class="product-name">{{ $emitenData->title }}</h5>
                            {{--<a class="author" href="teacher-single-left.html"><span class="">By Stefan Deo</span></a>--}}
                            {{--<ul class="review-box">--}}
                                {{--<li class="rating"><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></li>--}}
                                {{--<li class="readers">15,35,236 Votes</li>--}}
                                {{--<li class="readers"><a href="#" class="link">Add to Read</a></li>--}}
                            {{--</ul>--}}
                            <div class="share" style="margin-top: 0;">


                                @if(auth()->check())
                                    @if($roles->name == 'member')
                                        @if($emitenData->year < $limitYear)
                                            <h5 style="">Harga : </h5>
                                            <h5 class="">Rp{{ number_format(GeneralHelper::getPriceList($emitenData->docType,$emitenData->type),0,',','.') }}</h5>
                                        @endif
                                    @else
                                        @if(!empty($arrDataType) && in_array($type,$arrDataType) == false)
                                            <h5 style="">Harga : </h5>
                                            <h5 class="">Rp{{ number_format(GeneralHelper::getPriceList($emitenData->docType,$emitenData->type),0,',','.') }}</h5>
                                        @endif
                                    @endif
                                @endif

                                {{-- OLD LOGIC --}}

                                {{--@if($emitenData->year < $limitYear)--}}
                                    {{--@if(auth()->check())--}}
                                        {{--@if($roles->name != 'member')--}}
                                            {{--<h5 class="">Rp{{ number_format(GeneralHelper::getPriceList($emitenData->docType,$emitenData->type),0,',','.') }}</h5>--}}
                                        {{--@endif--}}
                                    {{--@else--}}
                                        {{--<h5 class="">Rp{{ number_format(GeneralHelper::getPriceList($emitenData->docType,$emitenData->type),0,',','.') }}</h5>--}}
                                    {{--@endif--}}
                                {{--@endif--}}

                            </div>
                            <div class="share" style="font-size: 16px;margin: 0;font-weight: bold;">
                                <h5 style="font-size: 16px;">Bagikan : </h5>
                                <ul class="social-icons color round">
                                    <li class="facebook"><a href="#" title="Facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('{{ url()->current() }}'),'facebook-share-dialog','width=626,height=436');return false;"></a></li>
                                </ul><!-- Blog Social Share -->
                            </div>
                            <div class="button">


                            @if(Auth::check())
                                @if($emitenData->year > $limitYear && $roles->name == 'member')
                                    <a href="{{ route('dataemiten.getfile',['doctype'=>$emitenData->docType,'type'=>$emitenData->type,'emitendataencrypt'=>encrypt($emitenData->idEmitenData)]) }}" class="btn" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Download {{ $emitenData->title }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh</a>
                                @else
                                    @if($paketUserActive && $paketUserActive->count() > 0)
                                        @if($downloadCounter < $paketUserActive->paket_data->jml_download)
                                            @if(!empty($arrDataType) && in_array($type,$arrDataType))
                                                <a class="btn" href="{{ route('dataemiten.getfile',['doctype'=>$emitenData->docType,'type'=>$emitenData->type,'emitendataencrypt'=>encrypt($emitenData->idEmitenData)]) }}" class="option-btn" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Download {{ $emitenData->title }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh</a>
                                            @else
                                                <a class="btn addDocToCart" href="#" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Tambah ke keranjang {{ $emitenData->title }}"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Tambah ke keranjang</a>
                                            @endif
                                        @else
                                            <a class="btn addDocToCart" href="#" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Tambah ke keranjang {{ $emitenData->title }}"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Tambah ke keranjang</a>
                                        @endif
                                    @else
                                        <a class="btn" href="{{ route('paket-data.index') }}" onclick=""> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Berlangganan</a>
                                    @endif
                                @endif
                            @else
                                <a class="btn" href="{{ route('paket-data.index') }}" onclick=""> <i class="fa fa-shopping-cart" aria-hidden="true"></i>  Berlangganan</a>
                            @endif



                                {{-- OLD BUTTON --}}
                                {{--@if(Auth::check())--}}
                                    {{--@if($emitenData->year < $limitYear && $roles->name == 'member')--}}
                                        {{--<a href="{{ route('dataemiten.getfile',['doctype'=>$emitenData->docType,'type'=>$emitenData->type,'emitendataencrypt'=>encrypt($emitenData->idEmitenData)]) }}" class="btn btn-md" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Download {{ $emitenData->title }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh</a>--}}
                                    {{--@else--}}
                                        {{--@if(Auth::user()->can('download-data-emiten'))--}}
                                            {{--<a href="{{ route('dataemiten.getfile',['doctype'=>$emitenData->docType,'type'=>$emitenData->type,'emitendataencrypt'=>encrypt($emitenData->idEmitenData)]) }}" class="btn btn-md" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Download {{ $emitenData->title }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh</a>--}}
                                        {{--@else--}}
                                            {{--<a href="#" class="addDocToCart btn" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Tambah ke keranjang {{ $emitenData->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Tambah ke Keranjang</a>--}}
                                        {{--@endif--}}
                                    {{--@endif--}}
                                {{--@else--}}
                                    {{--@if($emitenData->year > $limitYear)--}}
                                        {{--<a href="{{ route('dataemiten.getfile',['doctype'=>$emitenData->docType,'type'=>$emitenData->type,'emitendataencrypt'=>encrypt($emitenData->idEmitenData)]) }}" class="btn btn-md" onclick="ga('send', 'event', 'Download Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Download {{ $emitenData->title }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh</a>--}}
                                    {{--@else--}}
                                        {{--<a href="#" class="btn btn-md addDocToCart" onclick="ga('send', 'event', 'Add To Cart', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-title="{{ $emitenData->title }}"  data-id="{{ $emitenData->idEmitenData }}" data-doctype="{{ $doctype }}" data-type="{{ $type }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}" title="Tambah ke keranjang {{ $emitenData->title }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Tambah ke Keranjang</a>--}}
                                    {{--@endif--}}
                                {{--@endif--}}
                                {{--<a href="{{ route('dataemiten.viewpdf',['emitendata'=>$emitenData->idEmitenData,'doctype'=>$doctype,'type'=>$type]) }}" class="btn btn-md modalIframeDocument" title="Lihat {{ $emitenData->title }}" onclick="ga('send', 'event', 'View Data', '{{ $jenisData }}', '{{ $emitenData->title }}' )" data-id="{{ $emitenData->idEmitenData }}" data-reftable="1" data-emiten="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->name:"-") }}" data-type="{{ $jenisData }}" data-code="{{ (is_object($emitenData->emiten) ? $emitenData->emiten->code:"-") }}" data-year="{{ $emitenData->year }}"><i class="fa fa-eye" aria-hidden="true"></i> Lihat Dokumen</a>--}}
                            </div>
                            <p class="margin-top-30">
                                {{ $emitenData->description }}
                            </p>
                        </div><!-- Member Detail Wrapper -->
                    </div><!-- Member Detail Column -->
                </div><!-- Row -->


            </div><!-- Container -->
        </div><!-- Page Default -->
    </div><!-- Page Main -->

@endsection