@extends('layouts.app')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-tag font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Seminar & Workshop</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            @include('errors.list')
                            @include('flash::message')

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Seminar/Workshop</th>
                                    <th class="text-right">Biaya</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($workshops && $workshops->count() > 0)
                                    @foreach($workshops as $item)
                                        @php
                                            $peserta = $item->peserta->where('email',auth()->user()->email)->first();
                                        @endphp
                                        <tr>
                                            <td>
                                                @if($item->is_online == 1)
                                                    @if($peserta && $peserta->is_payment_approved == 1)
                                                        {{ strftime('%d %B %Y',strtotime($peserta->course_start_date)) }} s/d {{ strftime('%d %B %Y',strtotime($peserta->course_end_date)) }}
                                                    @else
                                                        -
                                                    @endif
                                                @else
                                                    {{ strftime('%d %B %Y',strtotime($item->tgl_mulai)) }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(strlen($item->workshop_name) > 50)
                                                    <abbr title="{{ $item->workshop_name }}" class="showtooltip">{{ substr($item->workshop_name,0,50) }}</abbr>
                                                @else
                                                    {{ $item->workshop_name }}
                                                @endif
                                            </td>
                                            <td class="text-right">Rp{{ (!empty($item->peserta) ? number_format($peserta->total_bayar,0):0)  }}</td>
                                            @if(is_object($peserta) && $peserta->is_payment_approved)
                                                <td><label for="" class="label label-success">Sudah di bayar</label></td>
                                                <td>
                                                    @if($item->is_online == 1)
                                                        <a href="http://elearning.ticmi.co.id" title="Akses elearning TICMI" class="btn btn-success btn-xs showtooltip" ><i class="fa fa-university"></i></a>
                                                    @else
                                                        <a href="{{ route('member.seminar.view',['workshopslug'=>$item->slugs]) }}" class="btn btn-success btn-xs showtooltip" title="Detail Seminar/Workshop & Unduh Materi Dan Modul"><i class="fa fa-book"></i></a>
                                                    @endif
                                                </td>
                                            @else
                                                @if($peserta->is_confirm)
                                                    <td><label for="" class="label label-warning">Proses Konfirmasi</label></td>
                                                    <td>
                                                        <a href="#" class="btn btn-default btn-xs disabled"><i class="fa fa-book"></i></a>
                                                    </td>
                                                @else
                                                    <td><label for="" class="label label-danger">Belum di bayar</label></td>
                                                    <td>
                                                        <a href="{{ route('pelatihan.konfirmasi',['workshopslug'=>$item->slugs,'pesertaworkshopinvoice'=>$peserta->invoice_no]) }}" class="btn btn-warning btn-xs"><i class="fa fa-money"></i></a>
                                                    </td>
                                                @endif
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>

                            <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection