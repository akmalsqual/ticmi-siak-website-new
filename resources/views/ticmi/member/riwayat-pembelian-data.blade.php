@extends('layouts.app')
@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Riwayat Pembelian Data Satuan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                        @include('errors.list')
                        @include('flash::message')

                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>No Invoice</th>
                                    <th>Status</th>
                                    <th class="text-right">Nominal</th>
                                    <th>Valid s/d</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php(\Carbon\Carbon::setLocale('id'))
                                @if($keranjang && $keranjang->count() > 0)
                                    @foreach($keranjang as $item)
                                        <tr>
                                            <td>{{ $item->created_at->format('d F Y') }}</td>
                                            <td>#{{ $item->invoice_no }}</td>
                                            <td>
                                                @if($item->is_confirm == 0)
                                                    <span class="label label-warning">Belum Konfirmasi</span>
                                                @else
                                                    @if($item->is_payment_approve == 0)
                                                        <span class="label label-success">Menunggu Verifikasi Pembayaran</span>
                                                    @else
                                                        <span class="label label-info">Sudah dibayar</span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-right">Rp{{ number_format($item->grandtotal,0,',','.') }}</td>
                                            <td>{{ (!empty($keranjang->valid_until)) ? date('d F Y - H:i', strtotime($item->valid_until)):'' }}</td>
                                            <td class="text-center">
                                                @if($item->is_confirm == 0)
                                                    <div class="btn-group" role="group">
                                                        <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$item->id]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                                    </div>
                                                @endif
                                                @if($item->is_payment_approve == 1)
                                                    <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Download Data" class="btn btn-xs btn-info showtootltip"><span class="fa fa-download"></span> </a>
                                                @else
                                                    <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Lihat Data Transaksi" class="btn btn-xs btn-warning showtootltip"><span class="fa fa-search"></span> </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">Belum ada tagihan terbaru</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection