@extends('layouts.app')
@section('content')



    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Riwayat Pembelian Data Satuan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- CONTENT HERE -->
                            @include('errors.list')
                            @include('flash::message')


                            @if($workshop->is_online == 1)
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="25%">Tanggal Akses</td>
                                        <td width="25%">
                                            @if($peserta && $peserta->is_payment_approved == 1)
                                                {{ strftime('%d %B %Y',strtotime($peserta->course_start_date)) }} s/d {{ strftime('%d %B %Y',strtotime($peserta->course_end_date)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td width="25%">Biaya</td>
                                        <td>Rp{{ number_format($peserta->total_bayar,0) }}</td>
                                    </tr>
                                    <tr>
                                        <td>No Invoice</td>
                                        <td>#{{ $peserta->invoice_no }}</td>
                                        <td>Virtual Account</td>
                                        <td>BCA {{ $peserta->virtual_account }}</td>
                                    </tr>
                                    <tr>

                                        @if($peserta && $peserta->is_payment_approved)
                                            <td>Status</td>
                                            <td><label for="" class="label label-success">Sudah di bayar</label></td>
                                            <td>Akses</td>
                                            <td>
                                                <a href="http://elearning.ticmi.co.id" title="Akses elearning TICMI" class="btn btn-success btn-xs showtooltip" ><i class="fa fa-university"></i> Akses Online Learning</a>
                                            </td>
                                        @else
                                            @if($peserta->is_confirm)
                                                <td>Status</td>
                                                <td><label for="" class="label label-warning">Proses Konfirmasi</label></td>
                                                <td>Akses</td>
                                                <td>
                                                    <a href="#" class="btn btn-default btn-xs disabled"><i class="fa fa-book"></i></a>
                                                </td>
                                            @else
                                                <td>Status</td>
                                                <td><label for="" class="label label-danger">Belum di bayar</label></td>
                                                <td>Akses</td>
                                                <td>
                                                    <a href="#" class="btn btn-default btn-xs disabled"><i class="fa fa-book"></i></a>
                                                </td>
                                            @endif
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="25%">Tanggal</td>
                                        <td width="25%">{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                        <td width="25%">Biaya</td>
                                        <td>Rp{{ number_format($peserta->total_bayar,0) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Waktu</td>
                                        <td>{{ $workshop->waktu }}</td>
                                        <td>Lokasi / Ruangan</td>
                                        <td>{{ $workshop->lokasi }} / {{ $workshop->ruangan }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <h6 class="title-bg-line">Materi dan Dokumen</h6>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Materi</th>
                                        <th>Unduh</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($workshop && $workshop->materi->where('is_publish',1)->count() > 0)
                                        @foreach($workshop->materi->where('is_publish',1) as $item)
                                            <tr>
                                                <td>
                                                    <strong>{{ $item->nama_materi }}</strong>
                                                    <p style="font-size: 12px;">{{ $item->keterangan }}</p>
                                                </td>
                                                <td width="15%">
                                                    <a href="{{ $item->link_file }}" class="btn btn-xs" target="_blank">Unduh <i class="fa fa-download"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                        @endif

                            <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection