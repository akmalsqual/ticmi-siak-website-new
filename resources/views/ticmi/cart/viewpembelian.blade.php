@extends('layouts_ticmi.home')
@section('content')

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="content-box shadow bg-white">
                        <h5>Tagihan Pembelian <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$keranjang->id]) }}"><span class="text-red">#{{ $keranjang->invoice_no }}</span></a></h5>
                        <br>
                        <div class="text-center invoice-view">
                            Pembelian data Anda dicatat dengan No Tagihan <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$keranjang->id]) }}"><strong>#{{ $keranjang->invoice_no }}</strong></a> <br>
                            Mohon lakukan pembayaran sebesar : <br><br>

                            <div class="inv-amount">Rp{{ number_format($keranjang->grandtotal,0,',','.') }}</div>

                            <p class="text-center">
                                <br>
                                Pembayaran dapat dilakukan melalui transfer ke <em>Virtual Account</em> A/N TICMI ke :
                                <br>

                                <img src="{{ url('assets/images/shop/logo-bca.gif') }}" alt="BCA"> <br>
                                <span class="bank__name">Bank BCA, Bursa Efek Indonesia</span> <br>
                                <span class="virtual__account">
                                    @php
                                        $vc = !empty($keranjang->virtual_account) ? str_split($keranjang->virtual_account,3):$keranjang->virtual_account;
                                        $vc = implode(" ",$vc);
                                    @endphp
                                    No : {{ $vc }}
                                </span> <br>
                                <span class="account__owner">a/n TICMI</span>
                            </p>

                            <p>
                                Terima kasih telah melakukan pembelian data di The Indonesia Capital Market Institute.
                                <br>
                                Jika sudah melakukan pembayaran, mohon untuk <strong>Konfirmasi Pembayaran</strong> Anda agar transaksi Anda bisa diproses.
                            </p>
                            <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$keranjang->id]) }}" class="btn btn-danger">Lihat Tagihan</a>
                            <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$keranjang->id]) }}" class="btn btn-primary">Konfirmasi Pembayaran</a>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection