@extends('layouts_ticmi.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Konfirmasi Pembayaran</h3>
                        <h6 class="sub-title">&nbsp;</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-md-6">

                    <div class="content-box shadow bg-white">
                        <h4>Konfirmasi</h4>
                        @include('errors.list')
                        @include('flash::message')
                        {!! Form::open(['url'=>route('transaksi.konfirmasi.store',['keranjang'=>$keranjang]),'files'=>true]) !!}
                        <div class="form-group">
                            {!! Form::label('invoice_no','No Invoice') !!} <strong class="text-red">*</strong>
                            {!! Form::text('invoice_no',$keranjang->invoice_no,['class'=>'form-control','readonly'=>'readonly']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('virtual_account','No Virtual Account BCA') !!}  <strong class="text-red">*</strong>
                            {!! Form::text('virtual_account',$keranjang->virtual_account,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nominal_transfer','Nominal Transfer ') !!}  <strong class="text-red">*</strong> ( <small>Sesuai dengan jumlah yang ditransfer</small> )
                            {!! Form::text('nominal',$keranjang->grandtotal,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek','Transfer dari no rekening ?') !!}  <strong class="text-red">*</strong>
                            {!! Form::text('no_rekening',null,['class'=>'form-control','placeholder'=>'No rekening yang digunakan untuk transfer']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek_holder','Nama pemegang rekening') !!} <strong class="text-red">*</strong>
                            {!! Form::text('nama_rekening',null,['class'=>'form-control','placeholder'=>'Nama pemegang rekening yang digunakan untuk transfer']) !!}
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                {!! Form::label('tgl_transfer','Tanggal dan Waktu Transfer') !!} <strong class="text-red">*</strong>
                                {!! Form::text('tgl_transfer',null,['class'=>'form-control datepicker','placeholder'=>'yyyy-mm-dd','autocomplete'=>'false','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Form::label('jam_transfer','&nbsp;') !!}
                                {!! Form::text('jam_transfer',null,['class'=>'form-control','placeholder'=>'hh:mm']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek_holder','Bukti Transfer') !!}
                            {!! Form::file('bukti_transfer') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Konfirmasi Pembayaran',['class'=>'btn btn-block','data-toggle'=>'loading','data-loading-text'=>'Loading...']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Konfirmasi Pembelian Data
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <table cellspacing="0" class="shop_table cart table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="product-thumbnail">&nbsp;

                                            </th>
                                            <th class="product-name">
                                                Nama
                                            </th>
                                            {{--<th class="product-price">--}}
                                                {{--Price--}}
                                            {{--</th>--}}
                                            {{--<th class="product-quantity">--}}
                                                {{--Quantity--}}
                                            {{--</th>--}}
                                            <th class="product-subtotal">
                                                Total
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php($total=0)
                                        @if($keranjang->keranjang_detail && $keranjang->keranjang_detail->count() > 0)
                                            @foreach($keranjang->keranjang_detail as $item)
                                                <tr>
                                                    <td>
                                                        <div class="shop-wrap no-margin">
                                                            <!-- Shop Image Wrapper -->
                                                            <div class="shop-img-wrap" style="width: 60px;">

                                                                <img width="55" height="55" src="http://dev.ticmi.co.id{{ GeneralHelper::getThumbPath($item->item_doctype,$item->item_type,$item->item_year,$item->item_filename) }}" class="img-responsive img-def" alt="Shop">
                                                                <div class="shop-img-loader" style="width: 60px;"></div>
                                                            </div><!-- Shop Wraper -->
                                                        </div>
                                                    </td>
                                                    <td>{{ $item->item_name }}</td>
                                                    {{--<td>{{ number_format($item->price) }}</td>--}}
                                                    {{--<td>1</td>--}}
                                                    <td>Rp{{ number_format($item->total_price) }}</td>
                                                </tr>
                                                @php($total += $item['price'])
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center">Keranjang Masih Kosong</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- Panel Group -->
                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection