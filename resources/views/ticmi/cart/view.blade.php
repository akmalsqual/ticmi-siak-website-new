@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Keranjang Belanja</h3>
                        <h6 class="sub-title">&nbsp;</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">


            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="content-box shadow bg-white">
                        @include('errors.list')
                        @include('flash::message')
                        <form method="post" action="">
                            <table cellspacing="0" class="shop_table cart table-bordered">
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">&nbsp;

                                    </th>
                                    <th class="product-name">
                                        Nama
                                    </th>
                                    <th class="product-price">
                                        Price
                                    </th>
                                    <th class="product-quantity">
                                        Quantity
                                    </th>
                                    <th class="product-subtotal">
                                        Total
                                    </th>
                                    <th class="">
                                        &nbsp;
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($total=0)
                                @if(session()->has('cart'))
                                    @foreach(session()->get('cart') as $key => $item)
                                        <tr>
                                            <td>
                                                <div class="shop-wrap">
                                                    <!-- Shop Image Wrapper -->
                                                    <div class="shop-img-wrap" style="width: 60px;">
                                                        <img width="55" height="55" src="http://dev.ticmi.co.id{{ GeneralHelper::getThumbPath($item['doctype'],$item['type'],$item['year'],$item['filename']) }}" class="img-responsive img-def" alt="{{ $item['title'] }}">
                                                        <div class="shop-img-loader" style="width: 60px;"></div>
                                                    </div><!-- Shop Wraper -->
                                                </div>
                                            </td>
                                            <td>{{ $item['title'] }}</td>
                                            <td>{{ number_format($item['price']) }}</td>
                                            <td>1</td>
                                            <td>{{ number_format($item['price']) }}</td>
                                            <td>
                                                <a href="{{ route('cart.remove',['id'=>$key]) }}" class="delfromcart" title="Hapus dari keranjang" onclick="ga('send', 'event', 'Pembelian Data', 'Hapus item dari keranjang', '{{ $item['title'] }}' )"><i class="fa fa-trash" style="font-size: 16px;"></i> </a>
                                            </td>
                                        </tr>
                                        @php($total += $item['price'])
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Keranjang Masih Kosong</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4" align="right"><strong>TOTAL</strong></td>
                                    <td colspan="2" align="right"><strong>Rp{{ number_format($total) }}</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>

                    </div>
                    <div class="content-box shadow bg-white">
                        <a href="{{ url('/') }}" class="btn">Lanjutkan Pembelian Data</a>
                        <a href="{{ route('cart.removeall') }}" class="btn btn-warning delfromcart" onclick="ga('send', 'event', 'Pembelian Data', 'Hapus Semua Keranjang', '{{ url()->current() }}' )">Hapus semua item di keranjang</a>
                        <a href="{{ route('cart.checkout') }}" class="btn btn-danger pull-right" onclick="ga('send', 'event', 'Pembelian Data', 'Checkout', '{{ url()->current() }}' )">Checkout</a>
                    </div>
                </div>

            </div><!-- Row -->


        </div>
    </div>
@endsection