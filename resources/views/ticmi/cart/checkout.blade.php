@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Checkout</h3>
                        <h6 class="sub-title">Konfirmasi Pembelian Data</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-md-8">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Item Pembelian
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <table cellspacing="0" class="shop_table cart table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="product-thumbnail">&nbsp;

                                            </th>
                                            <th class="product-name">
                                                Nama
                                            </th>
                                            <th class="product-price">
                                                Price
                                            </th>
                                            <th class="product-quantity">
                                                Quantity
                                            </th>
                                            <th class="product-subtotal">
                                                Total
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php($total=0)
                                        @if(session()->has('cart'))
                                            @foreach(session()->get('cart') as $key => $item)
                                                <tr>
                                                    <td>
                                                        <div class="shop-wrap">
                                                            <!-- Shop Image Wrapper -->
                                                            <div class="shop-img-wrap" style="width: 60px;">

                                                                <img width="55" height="55" src="http://dev.ticmi.co.id{{ GeneralHelper::getThumbPath($item['doctype'],$item['type'],$item['year'],$item['filename']) }}" class="img-responsive img-def" alt="Shop">
                                                                <div class="shop-img-loader" style="width: 60px;"></div>
                                                            </div><!-- Shop Wraper -->
                                                        </div>
                                                    </td>
                                                    <td>{{ $item['title'] }}</td>
                                                    <td>{{ number_format($item['price']) }}</td>
                                                    <td>1</td>
                                                    <td>{{ number_format($item['price']) }}</td>
                                                </tr>
                                                @php($total += $item['price'])
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center">Keranjang Masih Kosong</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- Panel Group -->
                </div>
                <div class="col-md-4">

                    <div class="content-box shadow bg-white">
                        <h4>Total Pembayaran</h4>
                        <table cellspacing="0" class="cart-totals table">
                            <tbody>
                            <tr class="total">
                                <th>
                                    <strong>Sub Total</strong>
                                </th>
                                <td class="text-right">
                                    <span class="amount">Rp{{ number_format($total) }}</span>
                                </td>
                            </tr>
                            <tr class="total">
                                <th>
                                    <strong>Biaya Administrasi</strong>
                                </th>
                                <td class="text-right">
                                    <span class="amount">Rp{{ number_format(3000) }}</span>
                                </td>
                            </tr>
                            <tr class="total">
                                <th>
                                    <strong>Total Bayar</strong>
                                </th>
                                <td class="text-right">
                                    <strong><span class="amount">Rp{{ number_format($total+3000) }}</span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    {!! Form::open(['url'=>route('cart.store'),'name'=>'formCheckout']) !!}
                                    {!! Form::hidden('subtotal',$total) !!}
                                    {!! Form::hidden('diskon',0) !!}
                                    {!! Form::hidden('biaya_admin',3000) !!}
                                    {!! Form::hidden('grandtotal',($total+3000)) !!}
                                    <button type="button" class="btn btn-block buttonCheckout">Konfirmasi Pembelian</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection