<table class="table table-bordered">
    <tbody>
    <tr>
        <td>
            {{ $dataCart[$idCart]['title'] }} berhasil ditambahkan kedalam keranjang
        </td>
        <td>
            @if(Session::has('cart'))
                @php
                    $totalprice = 0;
                @endphp
                @foreach(Session::get('cart') as $item)
                    <div>{{ $item['title'] }} {{ $item['price'] }}</div>
                    @php
                        $totalprice =+ $item['price'];
                    @endphp
                @endforeach

                <div>Total : {{ $totalprice }}</div>
            @else
            @endif
        </td>
    </tr>
    </tbody>
</table>