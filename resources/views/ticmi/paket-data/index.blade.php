@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- END PAGE HEAD-->
        
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Paket Data</h3>
                    </div>
                    <div class="panel-body">
                        @include('errors.list')
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Paket</th>
                                <th>Tanggal</th>
                                <th>No Invoice</th>
                                <th>Pembayaran</th>
                                <th>Status</th>
                                <th class="text-right">Nominal</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php(\Carbon\Carbon::setLocale('id'))
                            @if($paketUsers && $paketUsers->count() > 0)
                                @foreach($paketUsers as  $item)
                                    <tr>
                                        <td>{{ (is_object($item->paket_data) && !empty($item->paket_data)) ? $item->paket_data->name:'' }}</td>
                                        <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                        <td>{{ $item->invoice_no }}</td>
                                        <td>
                                            @if($item->is_active)
                                                <span class="label label-success">Sudah Bayar</span>
                                            @else
                                                @if(!empty($item->confirm_date))
                                                    <span class="label label-warning">Menunggu Verifikasi</span>
                                                @else
                                                    <a href="{{ route('paket-data.konfirmasi',['paketdatainvoice'=>$item->invoice_no]) }}" class="label label-default">Belum Konfirmasi</a>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->is_active)
                                                @if($item->valid_until >= date('Y-m-d H:i:s'))
                                                    <span class="label label-success">Aktif s/d {{ date('d-m-Y', strtotime($item->valid_until)) }}</span>
                                                @else
                                                    <span class="label label-danger">Berakhir pada {{ date('d-m-Y', strtotime($item->valid_until)) }}</span>
                                                @endif
                                            @else
                                                <span class="label label-danger">Tidak Aktif</span>
                                            @endif
                                        </td>
                                        <td class="text-right">Rp{{ number_format($item->total,0,',','.') }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group">
                                                @if($item->is_active)
                                                    <a href="{{ route('paket-data.view',['paketuser'=>$item->id]) }}" title="Lihat Paket" class="btn btn-xs showtootltip"><span class="fa fa-search"></span> </a>
                                                @else
                                                    @if(!empty($item->confirm_date))
                                                        <a href="{{ route('paket-data.view',['paketuser'=>$item->id]) }}" title="Lihat Paket" class="btn btn-xs showtootltip"><span class="fa fa-search"></span> </a>
                                                    @else
                                                        <a href="{{ route('paket-data.konfirmasi',['paketdatainvoice'=>$item->invoice_no]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7" class="text-center">Saat ini Anda tidak mempunyai paket data yang aktif. Silahkan berlangganan paket data <a href="{{ route('dataemiten.overview') }}">disini</a> </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection