@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h5 class="title">Konfirmasi Pembayaran Paket Data</h5>
                        <h6 class="sub-title">&nbsp;</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-md-6">

                    <div class="content-box shadow bg-white">
                        <h4>Konfirmasi</h4>
                        @include('errors.list')
                        @if($paketUser->due_date >= date('Y-m-d H:i:s'))
                        {!! Form::open(['url'=>route('paket-data.konfirmasi.store',['paketuser'=>$paketUser]),'files'=>true]) !!}
                        <div class="form-group">
                            {!! Form::label('invoice_no','No Invoice') !!} <strong class="text-red">*</strong>
                            {!! Form::text('invoice_no',$paketUser->invoice_no,['class'=>'form-control','readonly'=>'readonly']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('virtual_account','No Virtual Account BCA') !!}  <strong class="text-red">*</strong>
                            {!! Form::text('virtual_account',$paketUser->virtual_account,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nominal_transfer','Nominal Transfer ') !!}  <strong class="text-red">*</strong> ( <small>Sesuai dengan jumlah yang ditransfer</small> )
                            {!! Form::text('nominal',$paketUser->total,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek','Transfer dari no rekening ?') !!}  <strong class="text-red">*</strong>
                            {!! Form::text('no_rekening',null,['class'=>'form-control','placeholder'=>'No rekening yang digunakan untuk transfer']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek_holder','Nama pemegang rekening') !!} <strong class="text-red">*</strong>
                            {!! Form::text('nama_rekening',null,['class'=>'form-control','placeholder'=>'Nama pemegang rekening yang digunakan untuk transfer']) !!}
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                {!! Form::label('tgl_transfer','Tanggal dan Waktu Transfer') !!} <strong class="text-red">*</strong>
                                {!! Form::text('tgl_transfer',null,['class'=>'form-control datepicker','placeholder'=>'yyyy-mm-dd']) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Form::label('jam_transfer','&nbsp;') !!}
                                {!! Form::text('jam_transfer',null,['class'=>'form-control','placeholder'=>'hh:mm']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('transfer_rek_holder','Bukti Transfer') !!}
                            {!! Form::file('bukti_transfer') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Konfirmasi Pembayaran',['class'=>'btn btn-block','data-toggle'=>'loading','data-loading-text'=>'Loading...']) !!}
                        </div>
                        {!! Form::close() !!}
                        @else
                            <div class="alert alert-warning">
                                Transaksi Anda sudah melewati batas waktu pembayaran dan sudah dibatalkan, silahkan lakukan pembelian data kembali.
                            </div>
                        @endif
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Konfirmasi Pembelian Paket Data
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <table cellspacing="0" class="shop_table cart table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="product-name">
                                                Nama
                                            </th>
                                            <th class="product-subtotal text-right">
                                                Total
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $paketUser->paket_data->name }}</td>
                                            <td class="text-right"><strong>Rp{{ number_format($paketUser->total,0,',','.') }}</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- Panel Group -->
                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection