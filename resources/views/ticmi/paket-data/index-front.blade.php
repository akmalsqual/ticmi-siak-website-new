@extends('layouts_ticmi.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Langganan Paket Data</h3>
                        <h6 class="sub-title">Data Pasar Modal</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <div class="page-default bg-grey typo-dark shop-sm-col" style="padding-top: 0;">
        <!-- Container -->
        <div class="container">
            <div class="row pricing-sm-col">
                <!-- Title Block -->
                <div class="col-md-12 clearfix">
                    <div class="course-full-detail content-box  bg-white shadow">
                        <div class="title-container" style="margin-bottom: 0;">
                            <div class="title-wrap" style="margin-bottom: 0;">
                                <h4 class="title">Harga Paket Data</h4>
                                <span class="separator line-separator"></span>
                            </div>
                        </div><!-- Title Container -->
                        <div class="alert alert-info">
                            Untuk dapat melakukan unduh data silahkan berlangganan Paket Data kami. Discount 10% bagi Anda pemegang SID dan sudah mendaftar sebagai Pemegang SID.
                        </div>
                    </div>
                </div><!-- Title Block -->
            </div>
            <div class="row pricing-sm-col">
                <div class="clearfix"><br></div>
                @if($paketDataAll && $paketDataAll->count() > 0)
                    @foreach($paketDataAll as $item)
                        <div class="col-md-3 col-sm-6">
                            <!-- Pricing Wrapper -->
                            <div class="pricing-wrap">
                                <!-- Pricing Header -->
                                <div class="pricing-header">
                                    <div class="pricing-icon">
                                        <img src="{{ asset('assets/images/default/icon/'.$item->icon) }}" alt="Pricing Icon" class="img-responsive" width="70" height="70">
                                    </div><!-- Pricing Icon -->
                                    <!-- Pricing Title -->
                                    <div class="pricing-title">
                                        {{--<h5>Paket <span class="color">Value</span></h5>--}}
                                        <h5><span class="color">{{ $item->name }}</span></h5>
                                        <span>Langganan Perbulan</span>
                                    </div>
                                </div><!-- Pricing Header -->
                                <!-- Pricing Content -->
                                <ul class="pricing-body">
                                    <li>Rp{{ number_format($item->price) }} <span class="duration">/1 Bulan</span> </li>
                                    <li>{{ ($item->jml_download == 0) ? "Unlimited":$item->jml_download }} File <span class="duration">Unduhan</span> </li>
                                    <li> 1 Kali <span class="duration">Surat Riset</span> </li>
                                    <li>
                                        <div class="file-list">File yang di dapat di unduh</div>

                                        {{--@if(is_object($item->data_type) && $item->data_type->count() > 0)--}}
                                            {{--@foreach($item->data_type as $data_type)--}}
                                                {{--<ul>--}}
                                                    {{--<li> {{ $data_type->name }} </li>--}}
                                                {{--</ul>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                        @if($dataTypes && $dataTypes->count() > 0)
                                            @php($arrDataType=[])
                                            @if(is_object($item->data_type) && $item->data_type->count() > 0)
                                                @php
                                                    $arrDataType = $item->data_type()->get()->pluck('id')->toArray();
                                                @endphp
                                            @endif
                                            <ul class="datatype">
                                            @foreach($dataTypes->where('parent_id',0) as $dataType)
                                                <li> <i class="fa fa-arrow-right"></i> <a href="javascript:return false;">{{ $dataType->name }}</a>
                                                @php
                                                    $dataTypeChilds = $dataType->where('parent_id', $dataType->id)->where('is_active',1)->get();
                                                @endphp
                                                @if($dataTypeChilds && $dataTypeChilds->count() > 0)
                                                    <ul class="datatype-child">
                                                        @foreach($dataTypeChilds as $dataTypeChild)
                                                            <li>{{ $dataTypeChild->name }}
                                                                @if(in_array($dataTypeChild->id, $arrDataType))
                                                                    <span class="label label-success pull-right"><i class="fa fa-check"></i></span>
                                                                @else
                                                                    <span class="label label-danger pull-right"><i class="fa fa-times"></i></span>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                                </li>
                                            @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                </ul><!-- Pricing Content -->
                                <!-- Pricing Footer (Book Button) -->
                                <div class="pricing-footer text-center">
                                    @if(auth()->check())
                                        @if($paketUserActive && $paketUserActive->count() > 0)
                                            <a href="#" class="btn btn-sm btn-warning"> Anda sudah berlangganan Paket Data </a>
                                        @else
                                            <a href="{{ route('paket-data.checkout',['paket-data-code'=>$item->slug]) }}" class="btn btn-block">Berlangganan +</a>
                                        @endif
                                    @else
                                        <a href="{{ route('paket-data.checkout',['paket-data-code'=>$item->slug]) }}" class="btn btn-block">Berlangganan +</a>
                                    @endif
                                </div><!-- Pricing Footer -->
                            </div><!-- Pricing Wrapper -->
                        </div><!-- Column -->
                    @endforeach
                @endif
            </div><!-- row -->

        </div>


    </div>
@endsection