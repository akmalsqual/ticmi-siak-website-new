@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Checkout</h3>
                        <h6 class="sub-title">Konfirmasi Pembelian Data</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                <div class="col-md-8">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Item Pembelian
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <table cellspacing="0" class="shop_table cart table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="product-thumbnail">&nbsp;

                                            </th>
                                            <th class="product-name">
                                                Nama
                                            </th>
                                            <th class="product-price">
                                                Price
                                            </th>
                                            <th class="product-quantity">
                                                Quantity
                                            </th>
                                            <th class="product-subtotal">
                                                Total
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php($total=0)
                                        @php($diskon=0)
                                        @if(session()->has('cart'))
                                            @php($item = session()->get('cart'))
                                            <tr>
                                                <td>
                                                    <div class="shop-wrap">
                                                        <!-- Shop Image Wrapper -->
                                                        <div class="shop-img-wrap" style="width: 60px;">

                                                            <img width="55" height="55" src="{{ asset('assets/images/default/icon/'.$item['icon']) }}" class="img-responsive img-def" alt="Shop">
                                                            <div class="shop-img-loader" style="width: 60px;"></div>
                                                        </div><!-- Shop Wraper -->
                                                    </div>
                                                </td>
                                                <td>
                                                    <strong>{{ $item['name'] }}</strong>
                                                    <ul>
                                                        <li>Berlangganan data 1 bulan (30 Hari)</li>
                                                        <li>{{ ($item['jml_download'] == 0) ? "Unlimited":$item['jml_download'] }} File Unduhan </li>
                                                        <li> 1 Kali Surat Riset </li>
                                                        <li>{!! $item['description'] !!}</li>
                                                    </ul>
                                                </td>
                                                <td>{{ number_format(trim($item['price'])) }}</td>
                                                <td>1</td>
                                                <td>{{ number_format(trim($item['price'])) }}</td>
                                            </tr>
                                            @php($total += $item['price'])
                                            @if(auth()->user()->is_valid_sid == 1)
                                                @php
                                                    $diskon = ( 10 / 100 ) * $total;
                                                @endphp
                                            @endif
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center">Keranjang Masih Kosong</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- Panel Group -->
                </div>
                <div class="col-md-4">

                    <div class="content-box shadow bg-white">
                        <h4>Total Pembayaran</h4>
                        <table cellspacing="0" class="cart-totals table">
                            <tbody>
                            <tr class="total">
                                <th>
                                    <strong>Sub Total</strong>
                                </th>
                                <td class="text-right">
                                    <span class="amount">Rp{{ number_format(trim($total)) }}</span>
                                </td>
                            </tr>
                            <tr class="total">
                                <th>
                                    <strong>Biaya Admin</strong>
                                </th>
                                <td class="text-right">
                                    <span class="amount">Rp{{ number_format(trim($biaya_admin)) }}</span>
                                </td>
                            </tr>
                            @if(auth()->user()->is_valid_sid == 1)
                            <tr class="total">
                                <th>
                                    <strong>Diskon Pemilik SID</strong>
                                </th>
                                <td class="text-right">
                                    <span class="amount">Rp{{ number_format(trim($diskon)) }}</span>
                                </td>
                            </tr>
                            @endif
                            @php
                                $total_bayar = ($total+$biaya_admin) - $diskon;
                            @endphp
                            <tr class="total" style="background-color: #e2e2e2;">
                                <th>
                                    <strong>Total Bayar</strong>
                                </th>
                                <td class="text-right">
                                    <strong><span class="amount">Rp{{ number_format(trim($total_bayar)) }}</span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    {!! Form::open(['url'=>route('paket-data.store',['paketdatacode'=>$paketData->slug]),'name'=>'formCheckout']) !!}
                                    {!! Form::hidden('price',$total) !!}
                                    {!! Form::hidden('diskon',$diskon) !!}
                                    {!! Form::hidden('biaya_admin',$biaya_admin) !!}
                                    {!! Form::hidden('total',($total_bayar)) !!}
                                    <button type="button" class="btn btn-block buttonCheckout" data-message="">Konfirmasi Pembelian</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->

@endsection