@extends('layouts.app')
    @section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- END PAGE HEAD-->
        
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Paket Data</h3>
                    </div>
                    <div class="panel-body">
                        @include('errors.list')
                        @include('flash::message')
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td valign="middle" class="text-center" width="20%">
                                    <img src="{{ asset('assets/images/default/icon/'.$paketUser->paket_data->icon) }}" align="center" alt="{{ $paketUser->paket_data->name }}" class="img-responsive img-circle img-center">
                                </td>
                                <td>
                                    <table class="table table-bordered no-margin">
                                        <tr>
                                            <td width="25%"><strong>Nama Paket</strong></td>
                                            <td>{{ $paketUser->paket_data->name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>No Invoice</strong></td>
                                            <td>#{{ $paketUser->invoice_no }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Harga</strong></td>
                                            <td>Rp{{ number_format($paketUser->total,0,',','.') }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pembayaran</strong></td>
                                            <td>
                                                @if($paketUser->payment_approve == 1)
                                                    <span class="label label-success">Pembayaran sudah disetujui</span>
                                                @else
                                                    @if(!empty($paketUser->confirm_date))
                                                        <span class="label label-warning">Menunggu verifikasi pembayaran</span>
                                                    @else
                                                        <span class="label label-danger">Belum melakukan konfirmasi</span> <br> <br>
                                                        <div class="alert alert-info">
                                                            Lakukan pembayaran sebelum tanggal <strong>{{ date('d F Y', strtotime($paketUser->due_date)) }}</strong> jam <strong>{{ date('H:i', strtotime($paketUser->due_date)) }}</strong> atau transaksi akan dibatalkan.
                                                            <a href="{{ route('paket-data.konfirmasi',['paketdatainvoice'=>$paketUser->invoice_no]) }}" class="btn btn-xs">Konfirmasi disini</a>
                                                        </div>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @if($paketUser->payment_approve == 1)
                                            <tr>
                                                <td><strong>Aktif S/D </strong></td>
                                                <td>{{ date('d-M-Y H:i:s', strtotime($paketUser->valid_until)) }} </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td><strong>No Virtual Account BCA</strong></td>
                                            <td>{{ $paketUser->virtual_account }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h5 class="heading">Daftar Data yang sudah di unduh</h5>
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Jenis</th>
                                <th>Data</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php(\Carbon\Carbon::setLocale('id'))
                            @if($paketUser && $paketUser->download_log->count() > 0)
                                @php($no=1)
                                @foreach($paketUser->download_log as $download_log)
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ \GeneralHelper::getJenisData($download_log->file_type, $download_log->file_doctype) }}</td>
                                        <td>{{ $download_log->file_title }}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('dataemiten.viewsingle',['emitendata'=>$download_log->idEmitenData,'doctype'=>$download_log->file_doctype,'type'=>$download_log->file_type,'title'=> Slugify::slugify($download_log->file_title)]) }}" class="btn btn-xs" title="View {{ $download_log->file_title }}"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @php($no++)
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Belum ada riwayat unduhan</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection