@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">FAQ</h3>
                        <h6 class="sub-title">Frequently Asked Question</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default  typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">

                <!-- Page Content -->
                <div class="col-md-12">
                    <!-- Course Wrapper -->
                    <div class="row course-single">
                        <!-- Course Banner Image -->
                        <div class="col-sm-12">
                            <div class="owl-crousel">
                                <!--                                <img alt="Course" class="img-responsive" src="images/course/course-single-01.jpg" width="1920" height="966">-->
                            </div>
                        </div><!-- Column -->

                    </div><!-- Course Wrapper -->

                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <div  id="cara-daftar-program-sertifikasi" class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#pendaftaranSertifikasi" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Pendaftaran Program Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="pendaftaranSertifikasi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    {{--<ol>--}}
                                        {{--<li>Calon peserta melakukan pendaftaran peserta program sertifikasi secara <em>online</em> di <a href="www.ticmi.co.id">www.ticmi.co.id</a> </li>--}}
                                        {{--<li>Setelah melakukan pendaftaran Calon peserta akan menerima Automatic Email Confirmation dari TICMI (periksa email Anda dan juga periksa folder Spam pada email Anda).</li>--}}
                                        {{--<li>Calon peserta melakukan pembayaran melalui <strong>Bank BCA Cab. Bursa Efek Indonesia No Rekening: 4581.34.9992</strong>  atas nama <strong>PT Indonesian Capital Market Electronic Library</strong> sesuai jadwal yang telah ditentukan</li>--}}
                                        {{--<li>Calon peserta melakukan konfirmasi pembayaran secara online di <a href="www.ticmi.co.id">www.ticmi.co.id</a> (pada menu konfirmasi pembayaran)</li>--}}
                                        {{--<li>Setelah melakukan konfirmasi Calon peserta akan menerima email konfirmasi dari TICMI (periksa inbox email Anda dan juga folder spam email Anda)</li>--}}
                                        {{--<li>--}}
                                            {{--Calon peserta melakukan <em>Registrasi Administrasi</em> di Kantor TICMI dengan membawa persyaratan, untuk <em>Registrasi Administrasi</em> detailnya seperti berikut :--}}
                                            {{--<ul type="disc" style="list-style: disc;margin-left: 30px;">--}}
                                                {{--<li>Peserta menyerahkan bukti transfer/setor asli, fotokopi KTP, fotokopi ijazah pendidikan terakhir dan pas foto (ukuran 3x4) sebanyak 2 lembar kepada bagian Administrasi Akademik TICMI</li>--}}
                                                {{--<li>Peserta akan menerima kuitansi pembayaran 2 (dua) rangkap dari TICMI untuk kemudian ditukarkan dengan bahan dan modul pengajaran</li>--}}
                                                {{--<li>Peserta hadir untuk mengikuti program pendidikan dan pelatihan sesuai jadwal pelatihan yang telah ditentukan</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}

                                    {{--</ol>--}}
                                    <p class="text-center">

                                        <strong class="text-center">Gambar Alur Proses Pendaftaran Sertifikasi TICMI</strong>
                                        <br>

                                        <img src="{{ asset('assets/ticmi/images/ticmi/pendaftaran-sertifikasi-ticmi.png') }}" alt="Proses Pendaftaran Sertifikasi TICMI">
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{--<div class="panel panel-default" id="penundaan-keikutsertaan">--}}
                            {{--<div class="panel-heading" role="tab" id="headingTwo">--}}
                                {{--<h4 class="panel-title">--}}
                                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#penundaanIkutserta" aria-expanded="false" aria-controls="collapseTwo">--}}
                                        {{--Penundaan Keikutsertaan Program Sertifikasi--}}
                                    {{--</a>--}}
                                {{--</h4>--}}
                            {{--</div>--}}
                            {{--<div id="penundaanIkutserta" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">--}}
                                {{--<div class="panel-body">--}}
                                    {{--<ol>--}}
                                        {{--<li>Penundaan dapat dilakukan apabila peserta menderita sakit dan diharuskan untuk  menjalani perawatan atau mendapat tugas kantor dan/atau mutasi ke luar kota/negeri dalam jangka waktu yang cukup lama.</li>--}}
                                        {{--<li>Penundaan berlaku maksimal selama <strong>1 (satu) tahun</strong>, lebih dari itu dianggap hangus</li>--}}
                                        {{--<li>Peserta akan dikenakan biaya keikut-sertaan baru yang berlaku saat kembali mendaftarkan diri</li>--}}
                                    {{--</ol>--}}
                                    {{--<p>Peserta wajib membuat surat permohonan yang melampirkan surat penugasan/ surat keterangan sakit dari institusi terkait yang berwenang dan mengisi formulir permohonan penundaan keikut-sertaan yang sudah disediakan</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="panel panel-default" id="pelaksanaan-kelas">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pelaksanaanKelas" aria-expanded="false" aria-controls="collapseTwo">
                                        Pelaksanaan Kelas dan Persyaratan Peserta
                                    </a>
                                </h4>
                            </div>
                            <div id="pelaksanaanKelas" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Kelas akan dibuka di website hingga 7 hari sebelum pelaksanaan</li>
                                        <li>Peserta yang telah melakukan pendaftaran diwajibkan melakukan pembayaran selambat-lambatnya 7 hari sebelum pelaksanaan kelas</li>
                                        <li>Penundaan dapat dilakukan apabila peserta menderita (dengan perawatan intensif) dengan syarat menyerahkan surat keterangan dokter</li>
                                        <li>Penundaan berlaku maksimal selama 6 bulan, lebih dari itu dianggap hangus dan biaya pendaftaran tidak dapat dikembalikan</li>
                                        <li>TICMI berhak menunda pelatihan jika minimal peserta setiap kelasnya belum terpenuhi</li>
                                        <li>Persyaratan peserta program sertifikasi profesi dapat dilihat di halaman program terkait</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengunduran-diri">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengunduranDiri" aria-expanded="false" aria-controls="collapseThree">
                                        Pengunduran Diri dan Pengembalian Biaya Keikut-sertaan Pelatihan
                                    </a>
                                </h4>
                            </div>
                            <div id="pengunduranDiri" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <ol>
                                        <li>Pengunduran diri maksimal 8 hari sebelum pelatihan maka biaya pendaftaran dapat dikembalikan</li>
                                        <li>Pengunduran diri lebih dari 7 hari sebelum pelatihan maka biaya pendaftaran tidak dapat dikembalikan (non refundable)</li>
                                        <li>Dua ketentuan di atas tidak berlaku bagi kelas tertentu yang akan ditetapkan sebagai kelas yang <em>non refundable</em></li>
                                        <li>Jika pengunduran diri disampaikan setelah program dimulai, tidak ada pengembalian biaya pelatihan, dan tidak bisa digantikan dengan orang lain</li>
                                        <li>Biaya pengunduran diri pelatihan/pengembalian dana akan dikenakan biaya administrasi <strong>Rp50.000,-</strong> (kecuali pembatalan kelas yang diadakan TICMI)</li>
                                        <li>Peserta wajib melakukan konfirmasi pengembalian dana melalui email info@ticmi.co.id dan mengisi formulir permohonan pengembalian biaya pelatihan</li>
                                        <li>Periode waktu pengembalian selambat-lambatnya 14 hari kerja setelah konfirmasi diterima</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="ketentuan-ujian">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#penundaanTraining" aria-expanded="false" aria-controls="collapseThree">
                                        Ujian dan Batas Waktu Ujian Ulang
                                    </a>
                                </h4>
                            </div>
                            <div id="penundaanTraining" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <ol>
                                        <li>
                                            TICMI memberikan 3 kali ujian gratis dengan ketentuan :
                                            <ul>
                                                <li>1 kali ujian perdana</li>
                                                <li>2 kali ujian ulang</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Jika peserta telah melewati batas 3 kali ujian ulang, peserta dapat melakukan ujian ulang kembali dengan biaya sebagai berikut :
                                            <br>
                                            <br>
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Jenis Pelatihan</th>
                                                    <th colspan="2">Biaya Ujian Ulang (per Modul)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>WPPE/WMI/WPEE/ASPM Reguler & Waiver</td>
                                                    <td>Rp200.000</td>
                                                    <td>Max Rp500.000,-</td>
                                                </tr>
                                                <tr>
                                                    <td>WPPE Pemasaran Reguler & Waiver</td>
                                                    <td>Rp100.000</td>
                                                    <td>Max Rp250.000,-</td>
                                                </tr>
                                                <tr>
                                                    <td>WPPE Pemasaran Terbatas</td>
                                                    <td>Rp50.000</td>
                                                    <td>Max Rp125.000,-</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </li>
                                    </ol>
                                    <p>Batas waktu peserta melakukan ujian, baik ujian perdana maupun ujian ulang adalah 6 (enam) bulan setelah kelas berakhir. Apabila sudah melewati batas waktu 6 (enam) bulan dan peserta belum lulus maka peserta diharuskan untuk mengikuti pelatihan kembali</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default" id="syarat-keikutsertaan">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#syaratKeikutsertaan" aria-expanded="false" aria-controls="collapseThree">
                                        Syarat Keikut-sertaan Program Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="syaratKeikutsertaan" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <p>Syarat Keikutsertaan untuk WPPE, WMI dan ASPM:</p>
                                    <ol>
                                        <li>Berusia minimal 18 tahun</li>
                                        <li>Minimum telah lulus SMTA dan /atau yang sederajat Untuk WPPE</li>
                                        <li>Minimum Lulusan Diploma III dan /atau yang sederajat untuk WMI</li>
                                        <li>Minimum Lulusan S1 untuk ASPM</li>
                                        <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengajuan-licensi-ojk">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengajuanLicensiOjk" aria-expanded="false" aria-controls="collapseThree">
                                        Cara Mengajukan Lisensi ke OJK
                                    </a>
                                </h4>
                            </div>
                            <div id="pengajuanLicensiOjk" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <img src="{{ asset('assets/ticmi/images/ticmi/pengajuan-lisensi-ojk2.jpg') }}" alt="Cara pengajuan lisensi OJK">
                                    <img src="{{ asset('assets/ticmi/images/ticmi/Langkah-langkah pengajuan lisensi.png') }}" class="img-responsive" alt="Proses pengajuan Lisensi OJK">
                                </div>
				<div class="panel-body">
                                    <p> Informasi Lebih Lanjut:</p>
                                    <ol>
                                        <li><a target="_blank" href="/faq/Langkah-langkah Pengajuan Lisensi ke OJK.pdf"> Langkah-langkah Pengajuan ke OJK </a></li>
                                        <li><a target="_blank" href="/faq/POJK 16 Tentang Ahli Syariah Pasar Modal.pdf"> POJK 16 Tentang Ahli Syariah Pasar Modal</a></li>
                                        <li><a target="_blank" href="/faq/POJK 25 Lampiran.pdf"> POJK 25 Lampiran</a></li>
                                        <li><a target="_blank" href="/faq/POJK 25 Perizinan WMI.pdf">POJK 25 Perizinan WMI</a></li>
                                        <li><a target="_blank" href="/faq/POJK 27 Lampiran.pdf">POJK 27 Lampiran</a></li>
                                        <li><a target="_blank" href="/faq/POJK 27 Perizinan WPEE dan WPPEk.pdf">POJK 27 Perizinan WPEE dan WPPEk</a></li>
                                        
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengajuan-sertifikat-ticmi">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengajuanSertifikatTicmi" aria-expanded="false" aria-controls="collapseThree">
                                        Proses Pengajuan Sertifikat TICMI
                                    </a>
                                </h4>
                            </div>
                            <div id="pengajuanSertifikatTicmi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <img src="{{ asset('assets/ticmi/images/ticmi/pengajuan-sertifikasi-ticmi.JPG') }}" alt="Proses pengajuan sertifikat TICMI">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default" id="syarat-keikutsertaan">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#bataswaktuujian" aria-expanded="false" aria-controls="collapseThree">
                                        Batas Waktu Ujian
                                    </a>
                                </h4>
                            </div>
                            <div id="bataswaktuujian" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    Batas waktu peserta melakukan ujian, baik ujian perdana maupun ujian ulang adalah 6 (enam) bulan setelah kelas berakhir. Apabila sudah melewati batas waktu 6 (enam) bulan dan peserta belum lulus maka peserta diharuskan untuk mengikuti pelatihan kembali.
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection