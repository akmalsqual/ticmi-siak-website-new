@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/library.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Standar Kelulusan</h3>
                        <h6 class="sub-title">Minimal nilai untuk kelulusan</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/library-header.jpg') }}" class="img-responsive" alt="Event">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class="blog-single-details">
                            <h4><a href="#">Standar Kelulusan</a></h4>
                            <p>
                                Berdasarkan hasil keputusan Komite Standar Pengajaran (KSP) Bapepam LK, maka kelulusan peserta dalam Ujian Sertifikasi Profesi Pasar Modal di TICMI ditentukan oleh syarat minimum kelulusan untuk setiap modul.
                                Passing grade untuk setiap modul pada setiap program adalah sebagai berikut:

                            <ol>
                                <li>Program Wakil Perantara Pedagang Efek (WPPE): 65%</li>
                                <li>Program Wakil Manajer Investasi (WMI): 60%</li>
                            </ol>
                            </p>


                            <h4>Standar Pemenuhan Kompetensi Program WPPE</h4>
                            <h6>Minimum nilai kelulusan: 65% untuk setiap Modul</h6>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th width="70%">Modul</th>
                                    <th>Soal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <th scope="row">1</th>
                                    <td>Hukum & Perundangan Pasar Modal</td>
                                    <td>20</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">2</th>
                                    <td>Etika & Peraturan Pasar Modal</td>
                                    <td>8</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">3</th>
                                    <td>Kasus Hukum dan Etika</td>
                                    <td>8</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">4</th>
                                    <td>Analisis Ekonomi, Keuangan Perusahaan & Investasi</td>
                                    <td>20</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">5</th>
                                    <td>Pengetahuan Pasar Modal</td>
                                    <td>8</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">6</th>
                                    <td>Mekanisme & Peraturan Perdagangan Efek</td>
                                    <td>20</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">7</th>
                                    <td>Pengetahuan Efek</td>
                                    <td>16</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td colspan="2">TOTAL</td>
                                    <td>100</td>
                                </tr>
                                </tfoot>
                            </table>

                            <h4>Standar Pemenuhan Kompetensi Program WMI </h4>
                            <h6>Minimum nilai kelulusan: 60% untuk setiap Modul</h6>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th width="70%">Modul</th>
                                    <th>Soal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <th scope="row">1</th>
                                    <td>Perekonomian</td>
                                    <td>10</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">2</th>
                                    <td>Analisa Kuantitatif</td>
                                    <td>10</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">3</th>
                                    <td>Analisa Laporan Keuangan Perusahaan</td>
                                    <td>15</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">4</th>
                                    <td>Analisa Pendapatan Tetap</td>
                                    <td>15</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">5</th>
                                    <td>Analisa Ekuitas</td>
                                    <td>15</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">6</th>
                                    <td>Manajemen Portofolio</td>
                                    <td>15</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">7</th>
                                    <td>Hukum dan Etika</td>
                                    <td>20</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td colspan="2">TOTAL</td>
                                    <td>100</td>
                                </tr>
                                </tfoot>
                            </table>

                            <h4>Standar Pemenuhan Kompetensi Program ASPM </h4>
                            <h6>Minimum nilai kelulusan: 70 % untuk modul Syariah Muamalah & modul Pasar Modal Syariah, 65% untuk modul Pasar Modal Konvensional</h6>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th width="70%">Modul</th>
                                    <th>Soal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <th scope="row">1</th>
                                    <td>Syariah Muamalah</td>
                                    <td>30</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">2</th>
                                    <td>Pasar Modal Syariah</td>
                                    <td>30</td>
                                </tr>
                                <tr class="info">
                                    <th scope="row">3</th>
                                    <td>Pasar Modal Konvensional</td>
                                    <td>40</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td colspan="2">TOTAL</td>
                                    <td>100</td>
                                </tr>
                                </tfoot>
                            </table>




                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->


                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection