@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ url('/assets/ticmi/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ url('/assets/ticmi/images/ticmi/header-jadwal-kegiatan.jpg') }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">JADWAL KEGIATAN TICMI</h4>

                            <h5>I. Jadwal Sertifikasi</h5>
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th width="6%">Jan</th>
                                    <th width="6%">Feb</th>
                                    <th width="6%">Mar</th>
                                    <th width="6%">Apr</th>
                                    <th width="6%">Mei</th>
                                    <th width="6%">Jun</th>
                                    <th width="6%">Jul</th>
                                    <th width="6%">Agu</th>
                                    <th width="6%">Sep</th>
                                    <th width="6%">Okt</th>
                                    <th width="6%">Nov</th>
                                    <th width="6%">Des</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>WPPE Reguler</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 1,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WPPE Waiver</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,1, 2,'12',$year) !!}</td>
                                </tr>
                                {{--<tr>--}}
                                {{--<td>WPPE Pemasaran Reguler</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'01',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'02',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'03',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'04',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'05',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'06',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'07',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'08',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'09',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'10',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'11',$year) !!}</td>--}}
                                {{--<td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 1,'12',$year) !!}</td>--}}
                                {{--</tr>--}}
                                <tr>
                                    <td>WPPE Pemasaran Waiver</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,19, 2,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WPPE Pemasaran Terbatas</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,18, null,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WMI Reguler</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 1,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WMI Waiver</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,2, 2,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WPEE Reguler</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 1,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>WPEE Waiver</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,24, 2,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>ASPM Reguler</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 1,'12',$year) !!}</td>
                                </tr>
                                <tr>
                                    <td>ASPM Waiver</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'01',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'02',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'03',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'04',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'05',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'06',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'07',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'08',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'09',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'10',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'11',$year) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalSertifikasi,22, 2,'12',$year) !!}</td>
                                </tr>
                                </tbody>
                            </table>

                            <h5>II. Jadwal Seminar / Workshop</h5>
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th width="6%">Jan</th>
                                    <th width="6%">Feb</th>
                                    <th width="6%">Mar</th>
                                    <th width="6%">Apr</th>
                                    <th width="6%">Mei</th>
                                    <th width="6%">Jun</th>
                                    <th width="6%">Jul</th>
                                    <th width="6%">Agu</th>
                                    <th width="6%">Sep</th>
                                    <th width="6%">Okt</th>
                                    <th width="6%">Nov</th>
                                    <th width="6%">Des</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>PPL - WPPE</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'01',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'02',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'03',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'04',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'05',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'06',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'07',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'08',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'09',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'10',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'11',$year,1) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'12',$year,1) !!}</td>
                                </tr>
                                <tr>
                                    <td>PPL - WPEE</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'01',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'02',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'03',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'04',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'05',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'06',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'07',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'08',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'09',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'10',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'11',$year,24) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,34, null,'12',$year,24) !!}</td>
                                </tr>
                                <tr>
                                    <td>Cerdas Mengelola Keuangan (CMK)</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,500) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,500) !!}</td>
                                </tr>
                                <tr>
                                    <td>Risk Management</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,501) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,501) !!}</td>
                                </tr>
                                <tr>
                                    <td>Workshop GCG / Corporate Secretary</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,502) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,502) !!}</td>
                                </tr>
                                <tr>
                                    <td>Accounting For Non Accountant / Financial Report Analysis</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,503) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,503) !!}</td>
                                </tr>
                                <tr>
                                    <td>Financial Planner</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,504) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,504) !!}</td>
                                </tr>
                                <tr>
                                    <td>Sekolah Reksadana</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,505) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,505) !!}</td>
                                </tr>
                                <tr>
                                    <td>Technical Investing</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,506) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,506) !!}</td>
                                </tr>
                                <tr>
                                    <td>Value Investing</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'01',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'02',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'03',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'04',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'05',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'06',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'07',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'08',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'09',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'10',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'11',$year,507) !!}</td>
                                    <td class="text-center">{!! GeneralHelper::checkJadwalKegiatan($jadwalNonSertifikasi,[29,30], null,'12',$year,507) !!}</td>
                                </tr>
                                </tbody>
                            </table>
                            <div>
                                * Jadwal dapat berubah sewaktu-waktu, silahkan hubungi Callcenter kami di 0800 100 9000 (bebas pulsa) atau kirim email ke
                                <a href="mailto:info@ticmi.co.id">info@ticmi.co.id</a> untuk keterangan lebih lengkap.
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
        <br>
    </section><!-- Page Default -->
@endsection