@extends('layouts_ticmi.modal.base')
@section('contentWrapper')
    {{--<div class="bg-grey typo-dark">--}}
        <!-- Container -->
        <div class="container-fluid">
            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">
                    <h5 class="text-center text-info">{{ $jadwalKegiatan->nama_kegiatan }}</h5>
                    <p class="text-center">
                        {{ $jadwalKegiatan->keterangan }}
                    </p>
                    @if($jadwalKegiatan->link)
                    <div class="text-center">
                        <a href="{{ $jadwalKegiatan->link }}" class="btn btn-lg btn-info" target="_parent">Daftar</a>
                    </div>
                    @endif
                </div>
            </div><!-- Row -->
        </div>
    {{--</div>--}}
@endsection