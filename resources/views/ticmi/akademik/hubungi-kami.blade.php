@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Hubungi Kami</h3>
                        <h6 class="sub-title">Informasi lebih lanjut mengenai TICMI</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <!-- Section -->
    <section class="bg-grey typo-dark">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-6">
                    <div class="contact-info">
                        <div class="info-icon bg-dark">
                            <i class="uni-map-marker"></i>
                        </div>
                        <h5 class="title">Alamat Kantor</h5>
                        <address>
                            <strong>PT. Indonesian Capital Market Electronic Library</strong> <br>
                            Indonesia Stock Exchange Building, Tower II, 1st Floor
                            Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190 <br>
                            <abbr title="Phone">P </abbr> : (021) 515 23 18 <br>
                            <abbr title="Email">E </abbr> : info@ticmi.co.id

                        </address>
                    </div><!-- Contact Info -->
                </div><!-- Column -->

                <!-- Column -->
                <div class="col-sm-3">
                    <div class="contact-info">
                        <div class="info-icon bg-dark">
                            <i class="uni-mail"></i>
                        </div>
                        <h5 class="title">E-Mail</h5>
                        <p><a href="mailto:info@ticmi.co.id">info@ticmi.co.id</a></p>
                    </div><!-- Contact Info -->
                </div><!-- Column -->

                <!-- Column -->
                <div class="col-sm-3">
                    <div class="contact-info">
                        <div class="info-icon bg-dark">
                            <i class="uni-clock"></i>
                        </div>
                        <h5 class="title">Operation Hours</h5>
                        <p>
                            TICMI hours : Senin s.d Jumat 08:00 – 17:00 WIB <br>
                            Call Center hours : Senin s.d Jumat 07.00 – 18.00 WIB
                        </p>
                    </div><!-- Contact Info -->
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

    <!-- Section -->
    <section class="pad-none typo-dark">
        <div class="container-fluid">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-12 col-eq-height map-canvas"
                     style="min-height: 400px;"
                     data-zoom="18"
                     data-lat="-6.2233945"
                     data-lng="106.8082617"
                     data-title="The Indonesia Capital Market Institute"
                     data-type="roadmap"
                     data-hue="#2196F3"
                     data-content="The Indonesia Capital Market Institute &lt;br&gt; info@ticmi.co.id">
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->
@endsection