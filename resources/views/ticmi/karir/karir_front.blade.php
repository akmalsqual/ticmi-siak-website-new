@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
<div class="page-header typo-dark" style="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Karir</h3>
                        <h4 class="sub-title">TICMI Career Development Center</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">
                    <h5 class="text-center text-info">Selamat bagi Anda yang telah lulus ujian sertifikasi TICMI.</h5>
                    <p class="text-center">
                        Untuk meningkatkan pelayanan TICMI kepada industri Pasar Modal, kami telah meluncurkan pelayanan baru yaitu <strong>Career Development Center (CDC)</strong>.
                        CDC membantu alumni TICMI untuk mendapatkan informasi lowongan kerja di Perusahaan Efek atau Manajer Investasi.
						
                    </p>
                </div>
            </div><!-- Row -->
            <br>
            <div class="row course-single pad-tb-40 content-box bg-white shadow">
                <div class="col-sm-12">
                    @foreach($list_perusahaan as $perusahaan)
                            <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <h4 class="text-center"><a href="#" target="_blank">{{ $perusahaan['nama'] }}</a> </h4>
                                @foreach($perusahaan->lowongan as $key => $lowongan)
                                    @if($lowongan['is_publish'])
                                    <!-- Panel -->
                                    <div class="panel panel-default active">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $lowongan['lowongan_id'] }}" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    {{ $lowongan['nama_pekerjaan'] }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{ $lowongan['lowongan_id'] }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <div class="list-group">
                                                    <span <?php if($lowongan['tgl_close'] < date('Y-m-d')){ echo 'style="color:red"'; } ?> class="pull-right">Expired : {{ date('d F Y', strtotime($lowongan['tgl_close'])) }}</span>
                                                    <h6 class="uppercase bold">
                                                        <a href="javascript:;">Kualifikasi</a>
                                                    </h6>
                                                    <p>{!! $lowongan['keterangan'] !!}</p>
                                                    <h6 class="uppercase bold">
                                                        <a href="javascript:;">Deskripsi Pekerjaan</a>
                                                    </h6>
                                                    <p>{!! $lowongan['deskripsi'] !!}</p>
                                                </div>
                                                <center>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <center>Silahkan kirim lamaran kepada Saudara/i {{$lowongan['nama_pic'] }} melalui email <b>{{ $lowongan['email_pic'] }}</b> </center>
                                                        </div>
                                                    </div>
                                                </center>
                                            </div>
                                        </div>
                                    </div> <!-- Panel Default -->
                                    @endif
                                @endforeach
                            </div>
                            <br/>
                    @endforeach
                </div>
            </div>
        </div><!-- Container -->
    </div> <!-- Page Default -->
@endsection