@extends('layouts.app')
    @section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-user font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Data Pribadi</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            {!! Form::open(['route' => 'karir.store', 'id' => 'form_data_diri', 'role' => 'form', 'class' => 'form-horizontal', 'files' => true]) !!}
                            {{method_field('post')}}
                            	<input type="hidden" name="lowongan_id" value="{{ $lowongan_id }}">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Promosikan diri anda</label>
                                        <div class="col-md-9">
                                            {{ Form::textarea('keterangan','',['class' => 'form-control bordered', 'rows' => '5']) }}
                                            {!! $errors->first('keterangan','<span class="help-block alert alert-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cv" class="col-md-3 control-label">Upload CV</label>
                                    <div class="col-md-9">
                                        {{ Form::file('cv',['class' => 'form-control']) }}
                                        {!! $errors->first('cv','<span class="help-block alert alert-danger">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sertifikat" class="col-md-3 control-label">Upload Sertifikat</label>
                                    <div class="col-md-9">
                                       {{ Form::file('sertifikat',['class' => 'form-control'])  }}
                                       {!! $errors->first('sertifikat','<span class="help-block alert alert-danger">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{ url('karir') }}" type="button" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}" type="text/javascript"></script>

    {!! JsValidator::formRequest('App\Http\Requests\PendaftaranRequest', '#form_data_diri'); !!}

@endsection