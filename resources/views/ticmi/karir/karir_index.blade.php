@extends('layouts.app')
    @section('content')
    <link href="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!--css ticmi frontend-->
    <link href="{{ asset('assets/ticmi/css/lib/libs.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/ticmi/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/ticmi/css/default.css') }}" rel="stylesheet">


    <style>
    /*.ticmi {
        background-color: #e13138 !important;
    }*/
    .table-siak td:first-child {
        width: 20%!Important;
    }
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE HEAD-->
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-briefcase font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Daftar Lowongan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    @foreach($list_perusahaan as $perusahaan)
                                        <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                            <h5 class="text-center"><a href="#" target="_blank">{{ $perusahaan['nama'] }}</a> </h5>
                                            @foreach($perusahaan->lowongan as $key => $lowongan)
                                                @if($lowongan['is_publish'] == TRUE && $lowongan['hapus'] == FALSE && in_array($lowongan['lowongan_id'], $arr_not_apply))
                                                <!-- Panel -->
                                                <div class="panel panel-default active">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h5 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $lowongan['lowongan_id'] }}" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                                {{ $lowongan['nama_pekerjaan'] }}
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="{{ $lowongan['lowongan_id'] }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="list-group">
                                                                <span <?php if($lowongan['tgl_close'] < date('Y-m-d')){ echo 'style="color:red" class="expired pull-right"'; } ?>  class="pull-right">Expired : {{ date('d F Y', strtotime($lowongan['tgl_close'])) }}</span>
                                                                <h6 class="uppercase bold">
                                                                    <a href="javascript:;">Kualifikasi</a>
                                                                </h6>
                                                                <p>{!! $lowongan['keterangan'] !!}</p>
                                                                <h6 class="uppercase bold">
                                                                    <a href="javascript:;">Deskripsi Pekerjaan</a>
                                                                </h6>
                                                                <p>{!! $lowongan['deskripsi'] !!}</p>
                                                                <center>Silahkan kirim lamaran kepada Saudara/i {{$lowongan['nama_pic'] }} melalui email <b>{{ $lowongan['email_pic'] }}</b> </center>
                                                            </div>
                                                            <center>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <a href="{{ url('karir/create/'.$lowongan['lowongan_id']) }}" class="btn blue" data-id="{{ $lowongan['lowongan_id'] }}">Apply</a>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div> <!-- Panel Default -->
                                                @endif
                                            @endforeach
                                        </div>
                                        <br/>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/theme/global/plugins/datatables-yajra/datatables.bootstrap.js') }}"></script>
    <script>
        $(document).ready(function(){
            (function blink() { 
              $('.expired').fadeOut(500).fadeIn(500, blink); 
            })();
        })
    </script>
@endsection