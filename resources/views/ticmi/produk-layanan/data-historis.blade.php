@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url( {{ asset('assets/ticmi/images/banner/profile.jpg') }} ) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Data Historis</h3>
                        <h6 class="sub-title">Data historikal pasar modal</h6>
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/blog/data-historis.jpg') }}" class="img-responsive" alt="Event">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class="blog-single-details">
                            <h4><a href="#">Data Historis Pasar Modal</a></h4>
                            <p>TICMI menyediakan data-data seputar informasi Pasar Modal diantaranya sebagai berikut : </p>

                            <ol>
                                <li>Laporan keuangan emiten : Interim Report, Financial Report & Annual Report</li>
                                <li>Prospektus perdana IPO dan Right Issue</li>
                                <li>Tanggal publikasi laporan keuangan <em>Emiten</em></li>
                                <li>Data historis perdagangan saham harian <em>(Daily Trading Stock Summary)</em></li>
                                <li>Data historis indeks saham harian <em>(Daily Trading Index Summary)</em></li>
                                <li>Data historis perdagangan broker summary <em>(Daily Trading Broker Summary)</em></li>
                                <li>Data historis perdagangan obligasi (Daily Trading Bond Summary)</li>
                                <li>Data jumlah saham beredar (Listed Share)</li>
                                <li>Market Capitalization</li>
                                <li>Data Buy-Sell <em>(Foreign Domestic)</em></li>
                                <li>Data Pencatatan <em>Stock Split</em> & <em>Reverse Stock</em></li>
                                <li>Data Penggabungan Perusahaan <em>(Merger & Aquisition)</em></li>
                                <li>Data Pencatatan <em>Right Issue</em> </li>
                                <li>Data Pencatatan <em>Initial Public Offering (IPO)</em> dan <em>Delisted</em> Perusahaan</li>
                                <li><em>List of Corporate Action</em></li>
                                <li>Data Beta Saham </li>
                                <li>Financial Data & Ratio</li>
                                <li>Daftar Efek Indeks Konstituen</li>
                            </ol>

                            <h4>Sample Data</h4>

                            <style type='text/css'>
                                #gallery-1 {
                                    margin: auto;
                                }
                                #gallery-1 .gallery-item {
                                    float: left;
                                    margin-top: 10px;
                                    text-align: center;
                                    width: 25%;
                                }
                                #gallery-1 img {
                                    border: 2px solid #cfcfcf;
                                }
                                #gallery-1 .gallery-caption {
                                    margin-left: 0;
                                }
                                /* see gallery_shortcode() in wp-includes/media.php */
                            </style>
                            <div id='gallery-1' class='gallery galleryid-803 gallery-columns-4 gallery-size-thumbnail'><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/FR-1.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/FR-1-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/FR-2.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/FR-2-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/FR-3.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/FR-3-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/interim-1.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/interim-1-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/interim-2.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/interim-2-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/interim-3.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/interim-3-150x150.jpg" class="attachment-thumbnail" alt="Financial Report" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Financial Report
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/Sample_Daily-Trading-Saham.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/Sample_Daily-Trading-Saham-150x150.jpg" class="attachment-thumbnail" alt="Daily Trading Saham" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Daily Trading Saham
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/Sample_Indeks-Sektoral.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/Sample_Indeks-Sektoral-150x150.jpg" class="attachment-thumbnail" alt="Indeks Sektoral" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Indeks Sektoral
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2014/08/sukuk_summary-outright.jpg' data-rel="prettyPhoto[datahistoris]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2014/08/sukuk_summary-outright-150x150.jpg" class="attachment-thumbnail" alt="Bond summary outright" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Bond summary outright
                                    </dd></dl>
                                <br style='clear: both' />
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="facebook"><a href="http://www.facebook.com/TICMI.ID" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="http://www.twitter.com/TICMI_ID" target="_blank" title="Twitter">Twitter</a></li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection