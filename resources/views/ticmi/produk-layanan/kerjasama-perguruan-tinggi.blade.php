@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{url('assets/images/ticmi/kerjasama-pt.jpg')}}" class="img-responsive" alt="Kerjasama Perguruan Tinggi">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">KERJASAMA PERGURUAN TINGGI</a></h5>
                            <p class="text-justify">
                                Salah satu sasaran kerja Bursa Efek Indonesia (BEI) yang paling krusial adalah peningkatan jumlah investor.
                                BEI melihat bahwa peningkatan jumlah investor ini harus dimulai dengan penguatan posisi broker
                                dimana dalam konteks <em>supply demand</em> terhadap calon investor harus disediakan tenaga professional yang kompeten,
                                agar tingkat kepercayaan dan keyakinan calon investor dapat meningkat. Perguruan Tinggi menjadi salah satu opsi channel terbaik untuk
                                meningkatkan supply tenaga professional dalam industri Pasar Modal Indonesia.
                            </p>
                            <p class="text-justify">
                                Untuk mendukung tujuan Bursa Efek Indonesia itu <strong>The Indonesia Capital Market Intitute (TICMI)</strong> menjalin kerjasama dengan perguruan tinggi yang tersebar diseluruh Indonesia.
                                TICMI memiliki misi meningkatkan pengetahuan pasar modal di Indonesia dan TICMI akan terus meningkatkan jumlah sumber daya manusia berkualitas dan memiliki pengetahuan serta keterampilan dalam bidang pasar modal.
                                Berikut beberapa Perguruan Tinggi yang sudah menjalin kerjasama dengan TICMI : <br>
                            </p>
                            <br>

                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Syiah Kuala.png') }}" alt="Universitas Syiah Kuala" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.unsyiah.ac.id" target="_blank">Universitas Syiah Kuala</a> <br>Banda Aceh
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/IAI Almuslim Aceh.png') }}" alt="IAI Almuslim Aceh" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.almuslimaceh.ac.id" target="_blank">IAI Almuslim Aceh</a> <br>Banda Aceh
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/UIN Sumatera Utara.jpg') }}" alt="UIN Sumatera Utara" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://uinsu.ac.id" target="_blank">UIN Sumatera Utara</a> <br>Medan
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas andalas.png') }}" alt="Universitas Andalas" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://uinsu.ac.id" target="_blank">Universitas Andalas</a> <br>Padang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/IAIN Batusangkar.jpg') }}" alt="IAIN Batusangkar" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://iainbatusangkar.ac.id" target="_blank">IAIN Batusangkar</a> <br>Padang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/politeknik negeri padang.jpg') }}" alt="Politeknik Negeri Padang" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://iainbatusangkar.ac.id" target="_blank">Politeknik Negeri Padang</a> <br>Padang
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Putra Indonesia (YPTK).png') }}" alt="Universitas Putra Indonesia (YPTK)" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://upiyptk.ac.id" target="_blank">Universitas Putra Indonesia (YPTK)</a> <br>Padang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Riau Kepulauan.png') }}" alt="Universitas Riau Kepulauan" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://unrika.ac.id" target="_blank">Universitas Riau Kepulauan</a> <br>Batam
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Politeknik-Negeri-Batam.png') }}" alt="Politeknik Negeri Batam" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.polibatam.ac.id" target="_blank">Politeknik Negeri Batam</a> <br>Batam
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Bangka Belitung.jpg') }}" alt="Universitas Bangka Belitung" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://ubb.ac.id" target="_blank">Universitas Bangka Belitung</a> <br>Pangkal Pinang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIE IBEK Pangkal Pinang.png') }}" alt="STIE IBEK Pangkal Pinang" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://www.stie-ibek.ac.id" target="_blank">STIE IBEK Pangkal Pinang</a> <br>Pangkal Pinang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Muhammadiyah Riau.png') }}" alt="Universitas Muhammadiyah Riau" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://umri.ac.id" target="_blank">Universitas Muhammadiyah Riau</a> <br>Riau
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Riau.png') }}" alt="Universitas Riau" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://unri.ac.id" target="_blank">Universitas Riau</a> <br>Riau
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Islam Negeri Sulthan Thaha Saifuddin Jambi.jpg') }}" alt="UIN Sultan Thaha Saifuddin Jambi" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="https://iainjambi.ac.id" target="_blank">UIN Sultan Thaha Saifuddin Jambi</a> <br>Jambi
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Katolik Musi Charitas.png') }}" alt="Universitas Katolik Musi Charitas" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://ukmc.ac.id" target="_blank">Universitas Katolik Musi Charitas</a> <br>Palembang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Muhammadiyah Palembang.png') }}" alt="Universitas Muhammadiyah Palembang" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.um-palembang.ac.id" target="_blank">Universitas Muhammadiyah Palembang</a> <br>Palembang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/IAIN bengkulu.jpg') }}" alt="IAIN Bengkulu" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://siakad.iainbengkulu.ac.id/" target="_blank">IAIN Bengkulu</a> <br>Bengkulu
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Bengkulu.png') }}" alt="Universitas Bengkulu" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.unib.ac.id" target="_blank">Universitas Bengkulu</a> <br>Bengkulu
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Sekolah Tinggi Ilmu Ekonomi Trisakti.jpg') }}" alt="STIE Trisakti" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.stietrisakti.ac.id" target="_blank">STIE Trisakti</a> <br>Jakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Universitas Mohammad Husni Thamrin .png') }}" alt="Universitas M.H. Thamrin" style="vertical-align:bottom;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <a href="http://thamrin.ac.id" target="_blank">Universitas M.H. Thamrin</a> <br>Jakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Mercubuana.jpg') }}" alt="Universitas Mercubuana" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.mercubuana.ac.id" target="_blank">Universitas Mercubuana</a> <br>Jakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Indonesia Banking School.jpg') }}" alt="Indonesia Banking School" style="height: ;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <a href="http://www.ibs.ac.id" target="_blank">Indonesia Banking School</a> <br>Jakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STMIK ESQ.png') }}" alt="STMIK ESQ" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://esqbs.ac.id" target="_blank">STMIK ESQ</a> <br>Jakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Esa Unggul.png') }}" alt="Universitas Esa Unggul" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.esaunggul.ac.id" target="_blank">Universitas Esa Unggul</a> <br>Jakarta
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Universitas Matana.png') }}" alt="Universitas Matana" style="height: ;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.matanauniversity.ac.id" target="_blank">Universitas Matana</a> <br>Banten
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/STIE Pelita Bangsa.jpg') }}" alt="STIE Pelita Bangsa" style="vertical-align:bottom;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="https://www.pelitabangsa.ac.id" target="_blank">STIE Pelita Bangsa</a> <br>Banten
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIE-kesatuan.jpg') }}" alt="STIE Kesatuan" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://stiekesatuan.ac.id" target="_blank">STIE Kesatuan</a> <br>Bogor
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Universitas Widyatama.png') }}" alt="Universitas Widyatama" style="height: 100px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.widyatama.ac.id" target="_blank">Universitas Widyatama</a> <br>Bandung
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Langlangbuana.jpg') }}" alt="Universitas Langlangbuana" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://www.unla.ac.id" target="_blank">Universitas Langlangbuana</a> <br>Bandung
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Diponegoro .jpg') }}" alt="Universitas Diponegoro" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.undip.ac.id" target="_blank">Universitas Diponegoro</a> <br>Semarang
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/politeknik negeri semarang.png') }}" alt="Politeknik Negeri Semarang" style="height: ;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://www.polines.ac.id" target="_blank">Politeknik Negeri Semarang</a> <br>Semarang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Unika Soegijapranata.jpg') }}" alt="Universitas Unika Soegijapranata" style="vertical-align:bottom;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.unika.ac.id" target="_blank">Universitas Unika Soegijapranata</a> <br>Semarang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Kristen Satya Wacana.jpg') }}" alt="Universitas Kristen Satya Wacana" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.uksw.edu" target="_blank">Universitas Kristen Satya Wacana</a> <br>Semarang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Universitas Batik Surakarta.jpg') }}" alt="Universitas Batik Surakarta" style="height: 100px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://uniba.ac.id" target="_blank">Universitas Batik Surakarta</a> <br>Solo
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/UGM.jpg') }}" alt="Universitas Gadjah Mada" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://www.ugm.ac.id" target="_blank">Universitas Gadjah Mada</a> <br>Yogyakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Islam Indonesia.png') }}" alt="Universitas Islam Indonesia" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://www.uii.ac.id" target="_blank">Universitas Islam Indonesia</a> <br>Yogyakarta
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <br>
                                    <br>
                                    <br>
                                    <img src="{{ asset('assets/images/universitas/Akademi Akuntansi YKPN.jpg') }}" alt="Akademi Akuntansi YKPN" style="height: ;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <br>
                                    <a href="http://www.aaykpn.ac.id" target="_blank">Akademi Akuntansi YKPN</a> <br>Yogyakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Negeri Yogyakarta.png') }}" alt="Universitas Negeri Yogyakarta" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="https://www.uny.ac.id" target="_blank">Universitas Negeri Yogyakarta</a> <br>Yogyakarta
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Airlangga.png') }}" alt="Universitas Airlangga" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://www.unair.ac.id" target="_blank">Universitas Airlangga</a> <br>Surabaya
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIE Perbanas Surabaya.gif') }}" alt="STIE Perbanas Surabaya" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://perbanas.ac.id" target="_blank">STIE Perbanas Surabaya</a> <br>Solo
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Univ. Kanjuruhan Malang.gif') }}" alt="Universitas Kanjuruhan Malang" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://unikama.ac.id" target="_blank">Universitas Kanjuruhan Malang</a> <br>Malang
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Brawijaya.jpg') }}" alt="Universitas Brawijaya" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://ub.ac.id" target="_blank">Universitas Brawijaya</a> <br>Malang
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIE Widya Darma Pontianak.png') }}" alt="STIE Widya Dharma Pontianak" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://widyadharmapnk.ac.id" target="_blank">STIE Widya Dharma Pontianak</a> <br>Pontianak
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Balikpapan.jpg') }}" alt="Universitas Balikpapan" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://uniba-bpn.ac.id" target="_blank">Universitas Balikpapan</a> <br>Balikpapan
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Lambung Mangkurat.png') }}" alt="Universitas Lambung Mangkurat" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://ulm.ac.id" target="_blank">Universitas Lambung Mangkurat</a> <br>Banjarmasin
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/UIN Antasari Banjarmasin.jpg') }}" alt="UIN Antasari" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://iain-antasari.ac.id" target="_blank">UIN Antasari</a> <br>Banjarmasin
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Islam Kalimantan (UNISKA).png') }}" alt="Universitas Islam Kalimantan" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://uniska-bjm.ac.id" target="_blank">Universitas Islam Kalimantan</a> <br>Banjarmasin
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Darwan Ali.png') }}" alt="Universitas Darwan Ali" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://unda.ac.id" target="_blank">Universitas Darwan Ali</a> <br>Palangkaraya
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Palangka Raya.png') }}" alt="Universitas Palangkaraya" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://www.upr.ac.id" target="_blank">Universitas Palangkaraya</a> <br>Palangkaraya
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Udayana.png') }}" alt="Universitas Udayana" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://unud.ac.id" target="_blank">Universitas Udayana</a> <br>Denpasar
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Negeri Makassar.png') }}" alt="Universitas Negeri Makassar" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://unm.ac.id" target="_blank">Universitas Negeri Makassar</a> <br>Makassar
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Hasanuddin Makassar.png') }}" alt="Universitas Hasanuddin" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://unhas.ac.id" target="_blank">Universitas Hasanuddin</a> <br>Makassar
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIE-NOBEL Indonesia Makassar.png') }}" alt="STIE Nobel" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://stienobel-indonesia.ac.id" target="_blank">STIE Nobel </a> <br>Makassar
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/STIEM Bongaya.png') }}" alt="STIE Bongaya" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://stiem-bongaya.ac.id" target="_blank">STIE Bongaya</a> <br>Makassar
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/sekolah tinggi ilmu manajemen nitro makassar.jpg') }}" alt="Sekolah Tinggi Manajemen Nitro Makassar" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://nitromks.ac.id" target="_blank">Sekolah Tinggi Manajemen Nitro Makassar</a> <br>Makassar
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas halu Oleo.png') }}" alt="Universitas Halu Oleo" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://uho.ac.id" target="_blank">Universitas Halu Oleo</a> <br>Kendari
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Pattimura Ambon.png') }}" alt="Universitas Pattimura" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://unpatti.ac.id" target="_blank">Universitas Pattimura</a> <br>Ambon
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/UNIVERSITAS SAM RATULANGI.png') }}" alt="Universitas Sam Ratulangi" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://unsrat.ac.id" target="_blank">Universitas Sam Ratulangi</a> <br>Manado
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/UNIVERSITAS KLABAT.png') }}" alt="Universitas Klabat" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://unklab.ac.id" target="_blank">Universitas Klabat</a> <br>Manado
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Muhammadiyah Gorontalo.jpg') }}" alt="Universitas Muhammadiyah Gorontalo" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="https://umgo.ac.id" target="_blank">Universitas Muhammadiyah Gorontalo</a> <br>Gorontalo
                                </div>
                            </div>
                            <div class="clearfix"><br></div>
                            <div class="row">
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/Universitas Papua.png') }}" alt="Universitas Papua" style="height: 150px;margin: 0 auto;" class="img-responsive">
                                    <br>
                                    <a href="http://unipa.ac.id" target="_blank">Universitas Papua</a> <br>Manokwari
                                </div>
                                <div class="col-sm-2 col-xs-6 text-center">
                                    <img src="{{ asset('assets/images/universitas/universitas yapis papua.jpg') }}" alt="Universitas Yapis Papua" style="height: 150px;margin: 0 auto;" height="100" class="img-responsive">
                                    <br>
                                    <a href="http://uniyap.ac.id" target="_blank">Universitas Yapis Papua</a> <br>Jayapura
                                </div>
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('kerjasama-perguruan-tinggi') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('kerjasama-perguruan-tinggi') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection