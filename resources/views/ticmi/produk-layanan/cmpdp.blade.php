@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{url('/assets/images/ticmi/header-cmpdp-2017-ticmi.jpg')}}" class="img-responsive" alt="Capital Market Professional Development Program 2017">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                                <?php if(date("Y-m-d H:i")>"2018-04-10 00:00") { ?>
                            {{--<div class="alert alert-info">--}}
                                {{--<p class="text-center" style="font-size: 16px;"><strong>Pemberitahuan</strong></p>--}}
                                {{--<p class="text-center">Pendaftaran CMPDP 2018 sudah ditutup.</p>--}}
                            {{--</div>--}}
                                <?php } ?>
                                <?php if(date("Y-m-d H:i")<"2018-04-10 00:00") { ?>
                                <div class="containers">
                                  <h3 id="head">Sisa Waktu Pendaftaran</h3>
                                  <ul>
                                    <li class="li"><span id="days"></span>Hari</li>
                                    <li class="li"><span id="hours"></span>Jam</li>
                                    <li class="li"><span id="minutes"></span>Menit</li>
                                    <li class="li"><span id="seconds"></span>Detik</li>
                                  </ul>
                                </div>
                                <?php } ?>
                            {{--<div class="text-center">--}}
                                {{--<a href="{{ route('cmpdp.pengumuman') }}" class="btn btn-lg">Pengumuman Seleksi Nasional CMPDP 2018</a>--}}
                                {{--<br>--}}
                                {{--<br>--}}
                            {{--</div>--}}
                            <h5><a href="#">I. Tentang Capital Market Professional Development Program</a></h5>
                            <p class="text-justify">
                                <strong>The Indonesia Capital Market Intitute (TICMI)</strong> mengajak talenta-talenta terbaik bangsa bergabung
                                melalui program <strong>Capital Market Professional Development Program (CMPDP)</strong> untuk menjadi
                                professional pasar modal yang kompeten, handal, dan mampu menjawab tantangan di masa depan.
                                Setiap lulusan CMPDP akan ditempatkan bekerja di <em>PT Bursa Efek Indonesia (BEI)</em>,
                                <em>PT Kliring Penjaminan Efek Indonesia (KPEI)</em>, atau <em>PT Kustodian Sentral Efek Indonesia (KSEI)</em> yang berfungsi sebagai <em>Self Regulatory
                                    Organization (SRO)</em>   sebagai calon pemimpin masa depan pasar modal Indonesia.
                            </p>
                            <br>
                            <h5><a href="#">II. Program Pengembangan</a></h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-justify">
                                        Demi mempersiapkan talenta-talenta terpilih untuk menjadi penggerak industri pasar modal, kami
                                        senantiasa mengembangkan berbagai program peningkatan kompetensi agar terbentuk karakter
                                        para pemimpin yang tangguh.
                                        <br/><br/>Program CMPDP akan berlangsung dalam 2 fase dengan total selama 12 bulan, yaitu terdiri dari 6 bulan fase pertama yang merupakan
                                        fase pengembangan dan 6 bulan fase kedua yang merupakan <em>On The Job Training (OTJ)</em> di divisi-divisi SRO. Selama 12 bulan tersebut peserta akan mengikuti:
                                        <br><br>
                                        <strong><em>In Class Training</em></strong> <br>
                                        <em>In Class Training</em> terdiri dari pengenalan Proses Bisnis SRO, <em>Professionalism & Grooming Training</em>, <em>Strive for Excellence & Diciplinary Training dan Entrepreneurship</em>.
                                        <br><br>
                                        <strong><em>Capital Market Certification</em></strong> <br>
                                        Sertifikasi pasar modal meliputi sertifikasi profesi Wakil Perantara Pedagang Efek, Wakil Manager Investasi dan Wakil Penjamin Emisi Efek sebagai bekal di industri pasar modal
                                        <br><br>

                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{ url('/assets/images/ticmi/course-ticmi.jpg') }}" alt="CMPDP TICMI 2018" align="right" class="img-responsive">
                                    <div class="text-justify" style="margin-top: 10px; display: inline-block;">
                                        <strong><em>On The Job Training</em> (OJT)</strong> <br>
                                        <em>On The Job Training</em> pada fase pertama yang akan dilakukan selama 4 bulan dan pada fase kedua selama 6 bulan pada divisi-divisi SRO.
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="{{ url('/assets/images/ticmi/ticmi-cmpdp-class-2017.jpg')}}" alt="CMPDP TICMI 2018" align="left" class="img-responsive" style="margin-top: 10px;">
                                </div>
                                <div class="col-md-6">
                                    <p class="text-justify">
                                        <strong><em>Monthly Presentation (in English) :</em></strong> <br>
                                        <em>Monthly Presentation </em> merupakan sharing session secara rutin yang dilakukan oleh peserta CMPDP kepada peserta lainnya, dimana mereka harus melakukan <em>transfer of knowledge </em> terhadap apa yang telah mereka pelajari selama periode OJT tertentu di divisi terkait.
                                        <br><br>
                                         <strong><em>Individual Project Assignment:  </em></strong> <br>
                                        <em> Individual Project Assignment </em> merupakan tugas  individu terkait pasar modal yang diberikan secara rutin kepada setiap peserta dan harus diselesaikan dalam waktu tertentu.
                                        <br><br>
                                        <strong><em>Job Coaching dan Career Coaching</em></strong>
                                        <br><br>
                                        <strong><em>Organizational Event Involvement</em></strong> <br>
                                        <em>Organizational Event Involvement</em>  merupakan tugas-tugas kelompok  yang diberikan kepada peserta CMPDP berdasarkan kebutuhan <em>ad-hoc</em> dalam waktu tertentu. Peserta CMPDP akan terlibat dalam <em>event-event</em> SRO mulai dari rancangan konsep, persiapan hingga pelaksanaan <em>event</em> seperti IPO, Festival Yuk Nabung Saham, dsb.
                                    </p>
                                </div>
                            </div>
                            <br>
                            <h5><a href="#">III. Kualifikasi dan Persyaratan Umum</a></h5>
                            <p class="text-justify">
                                <ol style="list-style-type: square;">
                                    <li>Minimal lulusan S1 dari semua jurusan baik dari Perguruan Tinggi di dalam maupun luar negeri dengan minimum GPA/IPK 3.0 dari skala 4.0</li>
                                    <li><em>Preferable</em> jika telah memiliki pengalaman kerja minimal 1 (satu) tahun</li>
                                    <li>Memiliki minat yang tinggi untuk mengembangkan industri pasar modal Indonesia</li>
                                    <li>Bersedia ditempatkan di mana saja setelah lulus program</li>
                                    <li>Memiliki kemampuan belajar yang cepat dan memiliki keinginan yang tinggi untuk mempelajari hal baru</li>
                                    <li>Memiliki jiwa <em>entrepreneurship</em> yang tinggi (<em>inovatif, salesmanship skill</em>)</li>
                                    <li>Memiliki perilaku: percaya diri, <em>positive mindset</em>, bertanggung jawab, <em>resilient, strong interpersonal skill</em>, komunikatif (tulisan dan verbal baik Bahasa Indonesia maupun Bahasa Inggris).</li>
                                    
                                </ol>
                            </p>
                            <br>
                            
                            
                            <br> 
                             <h5><a href="#">IV. Pendaftaran CMPDP 2018</a></h5> 
                            <div class="text-justify"> 
                                 Proses pendaftaran untuk mengikuti program CMPDP 2018 dibuka hingga 09 April 2018 Pukul 24:00 WIB. 
                                <br><br> 
                                Berikut adalah jadwal pelaksanaan tahapan seleksi :  <br/> 
                                 <ul style="list-style: disc;margin-left: 40px;"> 
                                     <li>21 & 22 April 2018: Seleksi Tertulis (Bahasa Inggris, Ekonomi, Pengetahuan Pasar Modal, Potensi Akademik dan Etika)<b> bagi kandidat yang lolos dari tahap Seleksi Administrasi</b> </li> 
                                    <li>28 April 2018: Psikotest <b> bagi kandidat yang lolos dari tahap Seleksi Tertulis  </b></li> 
                                     <li>12 Mei 2018: <em>Interview</em> dan <em>Focus Group Discussion (FGD)</em><b> bagi kandidat yang lolos dari tahap Psikotest </b></li> 
                                     <li>14 - 16 Mei 2018: Cek kesehatan<b> bagi kandidat yang terpilih </b></li> 
                                     <li>22 Juni 2018: <em>On Boarding </em><b> bagi kandidat yang terpilih </b></li> 
                                 </ul>  
                                 
                                Catatan : Tanggal-tanggal tersebut masih dapat berubah disesuaikan dengan kondisi yang ada. <br/> <br>
                                <b>Kandidat akan menerima email notifikasi jika memenuhi syarat dan lolos pada setiap tahapan di atas.</b> Untuk itu mohon agar Anda dapat menandai kalender Anda, selalu terhubung dengan email dan mempersiapkan diri pada saat Anda mendapatkan notifikasi tersebut. <br> 
                                 <br>Untuk informasi lebih lanjut mengenai program ini dapat mengirimkan email ke <a href="mailto:cmpdp@ticmi.co.id">cmpdp@ticmi.co.id</a> atau melalui call center 08001009000 
                                </div> 
                                <br>
                                <br>
                                  <h5><a href="#">FAQ</a></h5> 
                                     <ul style="list-style: disc;margin-left: 40px;"> 
                                     <li> <a href="http://ticmi.co.id/FAQ.pdf" target="_blank"> silahkan klik ini untuk detail FAQ</a></li> 
                                     
                                    
                                 </ul>
                                <br>                                 
                                <br>                                 
                                <h5><a href="#">Communication Material</a></h5> 
                                     <ul style="list-style: disc;margin-left: 40px;"> 
                                     
                                     <li><a href="http://ticmi.co.id/communication_material.pdf" target="_blank"> silahkan klik ini untuk detail Communication Material</a></li> 
                                    
                                 </ul>  
                                <br>

                                <?php if(date("Y-m-d H:i")<"2018-04-10 00:00") { ?>
                                    <a href="http://ticmi.co.id/cmpdp/pendaftaran" class="btn btn-default btn-outline btn-lg btn-block ">Daftar</a>
                                <?php } ?>  

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmpdp') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>

                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection