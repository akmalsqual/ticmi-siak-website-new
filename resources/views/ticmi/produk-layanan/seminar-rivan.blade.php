@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ asset('assets/timci/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            {{--<img width="1920" height="700" src="/assets/images/ticmi/seminar/Banner-Investment.jpg" class="img-responsive" alt="Capital Market Professional Development Program">--}}
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            {{--<span style="text-align:center;font-size: 18px;font-weight: bold;display: block;color: #000;">BAGAIMANA CARA ANDA MENSIASATI PASAR MODAL DI SEMESTER KEDUA TAHUN 2017 INI ?</span>--}}
                            {{--<span style="text-align:center;font-size: 18px;font-weight: bold;display: block;color: #000;">ATAU BAGAIMANA ANDA MENGENALI INDIKATOR EKONOMI DI INDONESIA ?</span>--}}
                            <section class="" style="">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            {{--<div class="title-container margin-none">--}}
                                                {{--<div class="title-wrap margin-none" style="padding-bottom: 0;">--}}
                                                    {{--<p style="font-size: 16px;">Temukan caranya dalam seminar</p>--}}
                                                {{--</div>--}}
                                            {{--</div><!-- Name -->--}}
                                            {{--<div class="panel panel-info text-center">--}}
                                                {{--<div class="panel-heading">--}}
                                                    {{--<h3 class="panel-title" style="font-size: 22px;">INVESTMENT GRADE INVESTING STRATEGY</h3>--}}
                                                {{--</div>--}}
                                                {{--<div class="panel-body">--}}
                                                    {{--Yang diadakan oleh <strong>The Indonesia Capital Market Institute (TICMI)</strong> yang telah berpengalaman sebagai Pusat Data, Edukasi dan Sertifikasi Pasar Modal Indonesia.--}}
                                                    {{--Seminar Investment Grade Investing Strategy akan di selenggarakan pada :--}}

                                                    {{--<p class="text-center">--}}
                                                        {{--<br>--}}
                                                        {{--<strong>Sabtu, 22 Juli 2017 - 09:00 s.d 12:00 WIB</strong> <br>--}}
                                                        {{--Lokasi <br>--}}
                                                    {{--<address>--}}
                                                        {{--Ruang Seminar PT Bursa Efek Indonesia <br>--}}
                                                        {{--Gedung Bursa Efek Indonesia Tower 2 Lantai 1 <br>--}}
                                                        {{--Jl. Jend Sudirman Kav 52-53 Jakarta 12190--}}
                                                    {{--</address>--}}

                                                    {{--<strong class="text-red" style="font-size: 18px;">Biaya Investasi Rp350.000,-/peserta</strong><br>--}}
                                                    {{--<span style="background-color: #fbee23; color: #000;">(Special offer Rp300.000,- jika mendaftarkan 2 orang atau lebih)</span>--}}
                                                    {{--<br><br>--}}
                                                    {{--<strong>Topik Seminar : </strong>--}}
                                                    {{--<ul class="">--}}
                                                        {{--<li>Menyiasati pasar modal di semester 2 2017</li>--}}
                                                        {{--<li>Mengenal indikator makro ekonomi sebagai investor saham</li>--}}
                                                        {{--<li>Memahami hubungan antar indikator makro ekonomi dan kaitannya dengan pasar saham</li>--}}
                                                        {{--<li>Memahami cara mengenali apa saja indikator ekonomi yang sehat dalam penerapannya di Indonesia</li>--}}
                                                    {{--</ul>--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title text-center" style="font-size: 22px;">PEMBICARA</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row team-list">
                                                        <div class="col-sm-4">
                                                            <img src="{{ asset('assets/ticmi/images/ticmi/seminar/rivan-pas-foto-3-x-4-1.jpg') }}" class="img-responsive img" alt="Rivan Kurniawan">
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <div class="member-detail-wrap">
                                                                <h4 class="member-name">Rivan Kurniawan</h4>
                                                                <span class="position">Value Investor</span>
                                                                <div class="share">
                                                                    <h5>Connect : </h5>
                                                                    <ul class="social-icons color round">
                                                                        <li class="facebook"><a title="Facebook" target="_blank" href="https://www.facebook.com/valueinvestingindonesia/">Facebook</a></li>
                                                                        <li class="twitter"><a title="Twitter" target="_blank" href="https://twitter.com/rivan.kurniawan">Twitter</a></li>
                                                                        <li class="mail"><a title="mail" target="_blank" href="mailto:rivan.investing@gmail.com">mail</a></li>
                                                                        <li class="instagram"><a title="instagram" target="_blank" href="http://instagram.com/rivan.kurniawan">instagram</a></li>
                                                                    </ul><!-- Blog Social Share -->
                                                                </div>
                                                                <blockquote>
                                                                    <p>Rivan adalah seorang Value Investor. Ia memulai investasi sejak tahun 2008 ketika saya berumur 20 tahun dan sempat mengalami kejatuhan di pasar saham, namun berhasil bangkit dengan menerapkan metode Value Investing. Saat ini, Rivan aktif memberikan jasa konsultasi kepada para profesional dan investor yang ingin memaksimalkan profit serta meminimalisir resiko serta mengadakan berbagai workshop dan kegiatan edukasi mengenai pasar modal lainnya.</p>
                                                                </blockquote>
                                                            </div><!-- Member Detail Wrapper -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <h5>Seminar yang akan berjalan</h5>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped table-hover table-responsive">
                                                            <thead>
                                                            <tr>
                                                                <th>Kelas</th>
                                                                <th>Tanggal</th>
                                                                <th>Lokasi</th>
                                                                <th>&nbsp;</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($workshops && $workshops->count() > 0)
                                                                @foreach($workshops as $workshop)
                                                                    <tr>
                                                                        <td>{{ $workshop->workshop_name }}</td>
                                                                        <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                                                        <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                                                        <td class="text-center">
                                                                            @if($workshop->peserta()->count() < $workshop->minimal_peserta)
                                                                                <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-xs" title="Pendaftaran Workshop">Daftar</a>
                                                                            @else
                                                                                <span class="label label-danger">Kelas sudah penuh</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- Column -->
                                    </div><!-- Row -->
                            </section>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('rivan') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('rivan') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection