@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/profile.jpg')  }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Permintaan Data</h3>
                        <h6 class="sub-title">Permintaan data pasar modal khusus</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/blog/data-historis.jpg') }}" class="img-responsive" alt="Event">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class="blog-single-details">
                            <h4><a href="#">I. Permintaan Data</a></h4>
                            <h5><a href="#">A. Data apa saja yang tersedia di TICMI ?</a></h5>
                            <p class="text-justify">TICMI menyediakan data seputar pasar modal, seperti Laporan Keuangan triwulan, Laporan Keuangan tahunan, Laporan Keuangan akhir tahun, prospektus IPO, prospektus Right Issue, Corporate Action, Data transaksi perdagangan saham maupun obligasi, Data statistik dan data indeks konstituen. Untuk data laporan keuangan, prospektus, Ringkasan kinerja perusahaan, IDX Statistic, IDX LQ45 dan IDX Fact Book sudah bisa di download secara gratis di website ticmi.co.id dan untuk data lainnya merupakan data khusus yang bisa di pesan dengan mengirimkan email ke info@ticmi.co.id. </p>

                            <h5>B. Format data yang digunakan?</h5>
                            <p class="text-justify">
                                Khusus data laporan keuangan, prospektus, Ringkasan kinerja perusahaan, IDX Statistic, IDX LQ45 dan IDX Fact Book saat ini hanya tersedia dalam format PDF, namun untuk data khusus seperti Corporate Action, Perdagangan Saham dan Perdagangan Obligasi hanya tersedia dalam format Excel.
                            </p>

                            <h5>C. Bagaimanakah prosedur pengiriman data yang telah dipesan?</h5>
                            <p class="text-justify">
                                Data akan kami kirimkan melalui email dengan estimasi 3- 7 hari kerja, namun apabila ukuran data yang di request terlalu besar maka data tersebut dapat kami kirimkan dengan menggunakan jasa pengiriman ekspedisi.
                            </p>

                            <h5>D.  Bagaimana proses pemesenan data khusus di TICMI ?</h5>
                            <p class="text-justify">
                                Untuk pemesanan data khusus anda bisa datang langsung ke TICMI atau mengirimkan email ke info@ticmi.co.id dengan mencatumkan secara detail data yang diinginkan, setelah itu kami akan mengecek terlebih dahulu ketersediaan data tsb dan apabila data yang anda butuhkan tersedia akan kami kirimkan sample data beserta rincian biayanya setelah dirasa terpenuhi anda bisa langsung melakukan pembayaran dan konfirmasi.
                            </p>

                            <h4><a href="#">II. Kunjungan Langsung</a></h4>
                            <h5><a href="#">A. Apakah harus ada prosedur khusus apabila ingin berkunjung langsung ke TICMI, seperti membawa surat pengantar dari Universitas?</a> </h5>
                            <p>
                                Tidak ada prosedur khusus apabila anda ingin berkunjung langsung ke TICMI. Apabila anda berkenan untuk mengunjungi TICMI secara langsung, anda hanya perlu mengisi daftar kunjungan yang telah kami sediakan.
                            </p>

                            <h5><a href="#">B. Apakah diperbolehkan untuk membawa Laptop?</a></h5>
                            <p>TICMI memperkenankan setiap pengunjung untuk membawa Laptop.</p>

                            <h5><a href="#">C. Apakah buku-buku yang terdapat di TICMI dapat di pinjam?</a></h5>
                            <p>Buku-buku yang terdapat di Perpustakaan TICMI hanya untuk di baca di tempat saja, namun anda di perkenankan untuk menyalin buku yang diperlukan.</p>

                            <h5><a href="#">D. Layanan dan fasilitas apa saja yang tersedia di TICMI?</a></h5>
                            <p>
                                TICMI menyediakan layanan dan fasilitas Computer Zone yang di dalamnya telah tersedia data-data Laporan Keuangan dan Prospektus, fasilitas buku-buku referensi seputar Pasar Modal, Member , wifi dan TICMI menyediakan stand AB yang memudahkan pengunjung untuk melakukan pembukaan rekening efek
                            </p>

                            <h5><a href="#">E. Apakah saya dapat memilah-milah halaman tertentu saja, apabila saya menggunakan fasilitas dan layanan Computer Zone yang di dalamnya terdapat Prospektus & Laporan Keuangan?</a></h5>
                            <p>
                                Ya, anda dapat memilah-milah halaman yang diperlukan secara mandiri data-data yang di perlukan.
                            </p>

                            <h5><a href="#">F. Apakah diperkenankan untuk menggunakan Flash Disc untuk penggunaan layanan dan fasilitas Computer Zone? Apabila tidak diperkenankan menggunakan Flash Disc, bagaimana solusi untuk dapat menyimpan data dalam bentuk soft file?</a></h5>
                            <p>
                                TICMI tidak memperkenankan bagi pengunjung untuk menggunakan Flash Disc, namun apabila anda berkenan untuk menyimpan data-data tersebut kedalam format soft file, TICMI telah menyediakan CD (Compact Disc).
                            </p>

                            <h5><a href="#">G. Apabila saya berdomisili di luar Jakarta dan tidak dapat mengunjungi TICMI secara langsung, bagaimanakah prosedur untuk mendapatkan data Laporan Keuangan, Prospektus maupun data khusus (Corporate Action, Historical data dll) ?</a></h5>
                            <p>
                                Apabila anda tidak dapat mengunjungi TICMI anda tidak perlu khawatir karena anda dapat mengakses data Laporan Kuartal, Laporan Keuangan, Laporan Tahunan, Ringkasan Kinerja Perusahaan, Fact Book, IDX Statistic melalui webiste icamel. Sedangkan untuk melakukan pemesanan data khusus anda dapat mengirimkan email permintaan data ke alamat info@ticmi.co.id.
                            </p>


                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="facebook"><a href="http://www.facebook.com/TICMI.ID" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="http://www.twitter.com/TICMI_ID" target="_blank" title="Twitter">Twitter</a></li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection