@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ $ogimage }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                             
                            <h5 style="text-align:center">Kelas Moduler </h5>

                           
                            <p style="text-align:justify">Dalam rangka meningkatkan akses terhadap pengetahuan pasar modal, TICMI membuka kelas moduler secara tatap muka maupun online. Kelas moduler ini membahas beberapa materi pengajaran yang juga disampaikan di program sertifikasi WPPE, WMI, maupun WPEE tanpa harus mengikuti ujian.</p>
							 <p style="text-align:justify">Kelas moduler ini sangat dianjurkan untuk diikuti terutama bagi para peserta yang membutuhkan pengajaran ekstra pada beberapa materi tertentu.</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5>Pilihan Materi Pelatihan Moduler :</h5>

                                            <table class="table table-bordered">
											  <thead>
												<tr>
												  <th scope="col">#</th>
												  <th scope="col">Nama Modul</th>
												  
												 
												</tr>
											  </thead>
											  <tbody>
												<tr>
												  <th scope="row">1</th>
												  <td>Penilaian Efek Pendapatan Tetap</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">2</th>
												  <td>Proses Penawaran Umum</td>
												  
												</tr>
												<tr>
												  <th scope="row">3</th>
												  <td>Perilaku dan Etika WPPE</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">4</th>
												  <td>Analisa Kinerja Keuangan Perusahaan</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">5</th>
												  <td>Indeks Efek</td>
												   
												 
												</tr>
												<tr>
												  <th scope="row">6</th>
												  <td>Analisa Ekuitas</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">7</th>
												  <td>Pengelolaan Risiko atas Perubahan Suku Bunga</td>
												 
												 
												</tr>
												<tr>
												  <th scope="row">8</th>
												  <td>Capital Budgeting</td>
												 
												 
												</tr>
												<tr>
												  <th scope="row">9</th>
												  <td>Regulasi terkait Emiten dan Perusahaan Publik & Kejahatan dibidang Pasar Modal</td>
												   
												 
												</tr>
												<tr>
												  <th scope="row">10</th>
												  <td>Aksi Korporasi</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">11</th>
												  <td>Manajemen Portofolio</td>
												 
												 
												</tr>
												<tr>
												  <th scope="row">12</th>
												  <td>Efek yang Diperdagangkan di Pasar Modal</td>
												  
												 
												</tr>
												<tr>
												  <th scope="row">13</th>
												  <td>Regulasi terkait Emiten dan Perusahaan Publik & Kejahatan dibidang Pasar Modal</td>
												 
												 
												</tr>
												
											  </tbody>
											</table>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                   <div class="panel panel-default">
                                            <div class="panel-body">
                                                <h5>Kelas yang tersedia saat ini</h5>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover table-responsive">
                                                        <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>Kelas</th>
                                                            
                                                            <th>Lokasi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($workshops && $workshops->count() > 0)
                                                            @foreach($workshops as $workshop)
                                                                <tr>
                                                                    <td class="text-center">
                                                                          <a href="https://ticmi.co.id/kelas-online/{{$workshop->slugs}}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-xs" title="Pendaftaran Workshop">Daftar</a>
                                                                    </td>
                                                                    <td>{{ $workshop->workshop_name }}</td>
                                                                     
                                                                    <td><a href="http://elearning.ticmi.co.id/">http://elearning.ticmi.co.id/ </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('cmk') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmk') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection