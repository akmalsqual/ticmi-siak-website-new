@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/ticmi/cmk/banner-cmk2.jpg') }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">CERDAS MENGELOLA KEUANGAN</h4>

                            <p style="text-align:center"><strong>Bersama TICMI dan Panin Aset Management</strong></p>
                            <p style="text-align:justify">Pengelolaan keuangan sering menjadi masalah utama di kehidupan sehari-hari. Dengan kebutuhan hidup yang terus meningkat diikuti pola hidup yang semakin konsumtif terutama di kota besar, manajemen keuangan harus benar-benar diperhatikan. Penghasilan sebesar apapun tidak akan cukup untuk memenuhi kebutuhan jika tidak mampu mengendalikannya. Mengatur dan mengelola keuangan secara efektif serta cerdas akan memberikan dampak terhadap kondisi ekonomi seseorang menjadi lebih tertata, terkendali dan selalu stabil. Seminar Cerdas Mengelola Keuangan akan memaparkan serta mendiskusikan strategi menuju kondisi keuangan yang dimimpikan.</p>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5>Materi yang diajarkan</h5>

                                            <table class="table table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Cara Membuat perencanaan keuangan</td>
                                                </tr>
                                                <tr>
                                                    <td>Pengelolaan hutang</td>
                                                </tr>
                                                <tr>
                                                    <td>Cara mengalokasikan gaji anda setiap bulan</td>
                                                </tr>
                                                <tr>
                                                    <td>Membuat anggaran dana cadangan</td>
                                                </tr>
                                                <tr>
                                                    <td>Menyiapkan dana pensiun dan</td>
                                                </tr>
                                                <tr>
                                                    <td>Strategi investasi di reksadana</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5>Biaya pendaftaran</h5>

                                            <p>Rp350.000,-/peserta termasuk penyertaan unik reksadana Rp100.000,-</p>

                                            {{--<div class="alert alert-info">--}}
                                                {{--Dapatkan Cash Back sebesar <strong>Rp250.000</strong> dalam bentuk top up Rekening Reksadana dari Panin Assets Management untuk <strong>25 pendaftar pertama</strong>--}}
                                            {{--</div>--}}

                                            <strong>Lokasi seminar :</strong>
                                            <p>
                                                Ruang Seminar
                                                Gedung Bursa Efek Indonesia Tower 2 Lantai 1
                                                Jl Jend Sudiman kav 52-53 Jakarta Selatan
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5>Kelas yang akan berjalan</h5>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th>Kelas</th>
                                                        <th>Tanggal</th>
                                                        <th>Lokasi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($workshops && $workshops->count() > 0)
                                                        @foreach($workshops as $workshop)
                                                            <tr>
                                                                <td class="text-center">
                                                                    @if($workshop->peserta()->count() < $workshop->minimal_peserta)
                                                                        <a href="https://ticmi.co.id/seminar/{{$workshop->slugs}} {{-- route('pelatihan',['workshopslug'=>$workshop->slugs]) --}}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-xs" title="Pendaftaran Workshop">Daftar</a>
                                                                    @else
                                                                        <span class="label label-danger">Kelas sudah penuh</span>
                                                                    @endif
                                                                </td>
                                                                <td>{{ $workshop->workshop_name }}</td>
                                                                <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                                                <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('cmk') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmk') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection