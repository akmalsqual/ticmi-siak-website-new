@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ url('/assets/ticmi/images/ticmi/pdpms/PDPMS_BANNER.png') }}" class="img-responsive" alt="DSN-MUI">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">PENDIDIKAN DASAR PASAR MODAL SYARIAH (PDPMS)</h4>

                            
                           	 <p style="text-align:justify">Pasar modal syariah di Indonesia terus mengalami pertumbuhan dimana pada bulan Februari 2018 jumlah investor pasar modal syariah terus mengalami peningkatan menjadi 25.864 investor. Dengan demikian industri Pasar Modal Indonesia memerlukan adanya penguatan jumlah dan kualitas Sumber Daya Manusia yang memiliki kompetensi syariah pasar modal.
				<br><br>Sejak tahun 2016, The Indonesia Capital Market Institute (TICMI) telah menjalin kerjasama dengan Dewan Syariah Nasional - Majelis Ulama Indonesia (DSN-MUI) dalam penyelenggaraan pendidikan sertifikasi Keahlian Syariah Pasar Modal (ASPM). Sehubungan dengan hal di atas OJK, TICMI dan DSN-MUI memandang perlunya sebuah program yang dapat mengakomodasi minat masyarakat untuk belajar lebih dalam mengenai Pasar Modal Syariah, tetapi tidak untuk menjadi Ahli Syariah Pasar Modal. </strong> 
				</p>
				<p style="text-align:justify">
					Misalnya :
<ul class="list-group">
  <li class="list-group-item">a. Bagi Komite Investasi Syariah dan/atau Tim Pengelola Investasi Syariah serta Kepala Unit Pengelolaan Investasi Syariah wajib memiliki pengetahuan dan/atau pengalaman di bidang keuangan syariah (sesuai POJK No. 61/POJK.61/2016).</li>
  <li class="list-group-item">b. Dosen/mahasiswa/masyarakat umum yang ingin memperdalam ilmunya mengenai aspek syariah di Pasar Modal.</li>
  <li class="list-group-item">c. Profesi yang terkait dengan Profesi Penunjang Pasar Modal seperti Akuntan, Notaris, Konsultan Hukum, Advokat, Kustodi, Wali Amanat, dan lain-lain.</li>
  
</ul>
					 				
</p>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5>Materi yang diajarkan</h5>

                                            <table class="table table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Prinsip Dasar Ekonomi Syariah</td>
                                                </tr>
                                                <tr>
                                                    <td>Prinsip Muamalah (Tujuan, Metodologi, Prinsip Dasar, dan Pengantar Fatwa)</td>
                                                </tr>
                                                <tr>
                                                    <td>Prinsip Dasar Akad Syariah</td>
                                                </tr>
                                                <tr>
                                                    <td>Pengantar Syariah Pasar Modal</td>
                                                </tr>
                                                <tr>
                                                    <td>Produk Syariah Pasar Modal dan Fatwa Terkait</td>
                                                </tr>
                                                <tr>
                                                    <td>Post Test (Passing Grade 60)</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5>Syarat Peserta (Pilih Salah Satu) </h5>

                                            <table class="table table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Telah mengikuti Sekolah Pasar Modal (SPM) / Sekolah Pasar Modal Syariah (SPMS); atau</td>
                                                </tr>
                                                <tr>
                                                    <td>Memiliki sertifikat profesi pasar modal); atau</td>
                                                </tr>
                                                <tr>
                                                    <td>Memiliki Pengalaman Kerja di Pasar Modal Minimal 6 Bulan; atau</td>
                                                </tr>
                                                <tr>
                                                    <td>Akademisi yang Pernah Mengajar/Mengikuti Mata Kuliah Pasar Modal.</td>
                                                </tr>
                                                
                                                 
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                             

                                            {{--<strong>Investasi  :</strong> Rp. 2.500.000 <br>--}}
											{{--<strong>Benefit  :</strong> Konsumsi, Materi, & Sertifikat <br>--}}
											{{--<strong>Mitra Kami  :</strong> Dewan Syariah Nasional Majelis Ulama Indonesia--}}
                                            {{--<p>--}}
                                                 {{--<img  style="width: 25%;" src="{{ url('/assets/ticmi/images/ticmi/pdpms/dsn-mui.jpg') }}" class="img-responsive" alt="DSN-MUI">--}}
                                            {{--</p>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
							 <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>Kelas yang akan berjalan</h5>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Kelas</th>
                                        <th>Tanggal</th>
                                        <th>Lokasi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($otherbatch && $otherbatch->count() > 0)
                                        @foreach($otherbatch as $item)
                                            <tr>
                                                <td class="text-center">
                                                    <a href="{{ url('daftar-batch/'.$item->batch_id.'/'.$item->nama) }}" class="btn btn-xs" title="{{ $item->nama }}">Daftar</a>
                                                </td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tgl_mulai->formatLocalized('%A, %#d %B %Y') }}</td>
                                                <td>{{ $item->nama_cabang }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Belum ada kelas yang akan berjalan dalam waktu dekat</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                             
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('pdpms') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('pdpms') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('pdpms') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('pdpms') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection