@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
       <section data-background="{{ url('/assets/ticmi/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ $ogimage }}" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">SEKOLAH REKSA DANA (SRD)</h4>

                            {{--<p style="text-align:center"><strong>Bersama TICMI, MAMI dan MNC Aset Management</strong></p>--}}
                            <p style="text-align:justify">Rendahnya minat investasi masyarakat Indonesia di pasar modal disebabkan oleh masih kurangnya edukasi dan anggapan bahwa hal tersebut rumit dan mahal. Investasi di reksa dana merupakan langkah yang tepat bagi masyarakat yang ingin memulai berinvestasi di pasar modal karena sifatnya yang mudah dan murah.</p>
                            
                            <p style="text-align:justify">Reksa dana merupakan sebuah wadah pengelolaan dana bagi Anda untuk berinvestasi dalam instrumen-instrumen investasi yang tersedia di pasar modal dengan cara membeli unit penyertaan. Dana ini kemudian dikelola oleh Manajer Investasi (MI) ke dalam portofolio investasi, baik berupa saham, obligasi, pasar uang ataupun efek lainnya.</p>

                            <p style="text-align:justify">TICMI menjalin kerjasama dengan beberapa Manajer Investasi terkemuka di Indonesia untuk menyelenggarakan Sekolah Reksa Dana (SRD) secara reguler.</p>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5>Materi yang diajarkan</h5>

                                            <table class="table table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Pengetahuan produk reksa dana </td>
                                                </tr>
                                                <tr>
                                                    <td>Menentukan profil risiko calon investor </td>
                                                </tr>
                                                <tr>
                                                    <td>Membaca Fund Fact Sheet</td>
                                                </tr>
                                                <tr>
                                                    <td>Membaca Prospektus </td>
                                                </tr>
                                                <tr>
                                                    <td>Memilih reksa dana yang tepat</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5>Mitra Kami</h5>

                                            <div class="row">
            <!-- Team member -->
            {{--<div class="col-xs-12 col-sm-6 col-md-6">--}}
                {{--<div class="image-flip" ontouchstart="this.classList.toggle('hover');">--}}
                    {{--<div class="mainflip">--}}
                        {{--<div class="frontside">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-body text-center mt-4">--}}
                                    {{--<p><img class="img-fluid" width="200px" src="{{ asset('/assets/images/ticmi/reksadana/mami.png') }}" alt="card image"></p>--}}
                                   {{----}}
                                 {{----}}
                                     {{--<table class="table table-striped">--}}
                                                {{--<tbody>--}}
                                                {{--<tr>--}}
                                                    {{--<td>1.  Biaya pendaftaran sebesar Rp100.000</td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td>2.  Mendapatkan kredit rekening reksa dana senilai Rp50.000</td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td>3.  Konsumsi tersedia</td>--}}
                                                {{--</tr>--}}
                                                {{----}}
                                                {{--</tbody>--}}
                                            {{--</table>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="backside">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-body text-center mt-4">--}}
                                     {{--<p><img style="width: 80%;" class=" img-fluid" src="{{ asset('/assets/images/ticmi/reksadana/mami.png') }}" alt="card image"></p>--}}
                                    {{--<p class="card-text">--}}
{{--Kantor Pusat--}}

{{--Sampoerna Strategic Square South Tower, 31st floor--}}
{{--Jl. Jend. Sudirman Kav.45-46, Jakarta 12930--}}
{{--Telepon: (021) 2555 22 55--}}
{{--</p>--}}
                                    {{----}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- ./Team member -->
            <!-- Team member -->
            {{--<div class="col-xs-12 col-sm-6 col-md-6">--}}
            {{--<div class="image-flip" ontouchstart="this.classList.toggle('hover');">--}}
                    {{--<div class="mainflip">--}}
                        {{--<div class="frontside">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-body text-center">--}}
                                    {{--<p><img class=" img-fluid" width="200px" src="{{ asset('/assets/images/ticmi/reksadana/mncAset.png') }}" alt="card image"></p>--}}
                                    {{----}}
                                   {{--<table class="table table-striped">--}}
                                                {{--<tbody>--}}
                                                {{--<tr>--}}
                                                    {{--<td>1.  Biaya pendaftaran sebesar Rp100.000</td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td>2.  Mendapatkan kredit rekening reksa dana senilai Rp100.000</td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td>3.  Konsumsi tersedia</td>--}}
                                                {{--</tr>--}}
                                                {{----}}
                                                {{--</tbody>--}}
                                            {{--</table>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="backside">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-body text-center mt-4">--}}
                                     {{--<p><img  class=" img-fluid" width="200px" src="{{ asset('/assets/images/ticmi/reksadana/mncAset.png') }}" alt="card image"></p>--}}
                                    {{--<p class="card-text">--}}
{{--MNC Financial Tower, <br>--}}
{{--Lt. 9 & 10, Jl. Kebon Sirih No. 21-27, <br>--}}
{{--Jakarta 10340 - Telp (021) 2970 9600 Fax (021) 3983 6873 <br>--}}
{{--</p>--}}
                                    {{----}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{----}}
            {{--</div>--}}
            <!-- ./Team member -->
            

        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5>Kelas yang akan berjalan</h5>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th>Kelas</th>
                                                        <th>Tanggal</th>
                                                        <th>Lokasi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($workshops && $workshops->count() > 0)
                                                        @foreach($workshops as $workshop)
                                                            <tr>
                                                                <td class="text-center">
                                                                    @if($workshop->peserta()->count() < $workshop->minimal_peserta)
                                                                        <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-xs" title="Pendaftaran Workshop">Daftar</a>
                                                                    @else
                                                                        <span class="label label-danger">Kelas sudah penuh</span>
                                                                    @endif
                                                                </td>
                                                                <td>{{ $workshop->workshop_name }}</td>
                                                                <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                                                <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ url()->current() }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection