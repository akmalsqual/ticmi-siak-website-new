@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <section data-background="{{ asset('assets/ticmi/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <section class="" style="">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title-container margin-none">
                                            <div class="row" style="padding-bottom: 20px;">
                                                <div class="col-sm-6 text-center">
                                                    <img width="1920" height="700" src="{{ asset('assets/images/ticmi/ppl-online-ticmi-flyer.png') }}" class="img-responsive" alt="Program Pendidikan Lanjutan - Online Class">
                                                    {{--<img width="" height="" align="center" src="https://preview.ibb.co/kMKqPQ/PPL_WPPE.jpg" class="img-responsive" alt="Program Pendidikan Lanjutan">--}}
                                                </div>
                                                <div class="col-sm-6 text-center">
                                                    <img width="1920" height="700" src="{{ asset('assets/images/ticmi/ppl-online-wpee-ticmi.png') }}" class="img-responsive" alt="Program Pendidikan Lanjutan - Online Class">
                                                    {{--<img width="" height="" align="center" src="https://preview.ibb.co/kMKqPQ/PPL_WPPE.jpg" class="img-responsive" alt="Program Pendidikan Lanjutan">--}}
                                                </div>

                                            </div>
                                        </div><!-- Name -->

                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <h5>Kelas yang tersedia saat ini</h5>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover table-responsive">
                                                        <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>Kelas</th>
                                                            <th>Tanggal</th>
                                                            <th>Lokasi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($workshops && $workshops->count() > 0)
                                                            @foreach($workshops as $workshop)
                                                                <tr>
                                                                    <td class="text-center">
                                                                        @if($workshop->peserta()->count() < $workshop->minimal_peserta)
                                                                            <a href="{{ route('ppl.index',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-xs" title="Pendaftaran Workshop">Daftar</a>
                                                                        @else
                                                                            <span class="label label-danger">Kelas sudah penuh</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ $workshop->workshop_name }}</td>
                                                                    <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                                                    <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        @if($kelasOnline && $kelasOnline->count() > 0)
                                                            @foreach($kelasOnline as $online)
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <a href="https://ticmi.co.id/kelas-online/{{$online->slugs}}{{-- route('kelasonline',['workshopslug'=>$online->slugs]) --}}" onclick="ga('send', 'event', 'Pendaftaran Online Class', '{{ $online->workshop_name }}', '{{ url()->current() }}' )">{{ $online->workshop_name }}</a>
                                                                    </td>
                                                                    <td>{{ $online->workshop_name }}</td>
                                                                    <td>Online</td>
                                                                    <td><a href="http://elearning.ticmi.co.id">http://elearning.ticmi.co.id</a></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        @if($workshops->count() == 0 && $kelasOnline->count() == 0)
                                                            <tr>
                                                                <td colspan="4">Untuk saat ini belum ada kelas yang akan berjalan.</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Column -->
                                </div><!-- Row -->
                            </section>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('rivan') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('rivan') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection