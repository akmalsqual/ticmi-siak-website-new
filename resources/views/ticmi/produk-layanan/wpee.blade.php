@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">WPEE</h3>
                        <h5 class="sub-title">Wakil Penjamin Emisi Efek</h5>
                        <br/>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-12">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/wppe-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->

            </div><!-- Course Wrapper -->
            <br/>
            <div class="row course-single content-box bg-white shadow">

                <!-- Course Detail -->
                <div class="col-sm-6">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>WPEE REGULER</h4>
                            <span class="cat bg-yellow">WPEE</span><span class="cat bg-green">Sertifikasi</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>IDR 15.000.000,-</h5></li>
                                <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>25+ Orang</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>75 Jam</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                            </ul>
                        </div>

                    </div><!-- Course Detail -->
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>WPEE WAIVER</h4>
                            <span class="cat bg-yellow">WPEE</span><span class="cat bg-green">Sertifikasi</span>
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>IDR 5.000.000,-</h5></li>
                                <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>15+ Orang</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>16 Jam</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                            </ul>
                        </div>

                    </div><!-- Course Detail -->
                </div><!-- Column -->
            </div>

            <div class="row course-full-detail content-box bg-white shadow">
                <div class="col-sm-12">
                    {!! $program->keterangan !!}


                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>Kelas yang akan berjalan</h5>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Kelas</th>
                                        <th>Tanggal</th>
                                        <th>Lokasi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($otherbatch && $otherbatch->count() > 0)
                                        @foreach($otherbatch as $item)
                                            <tr>
                                                <td class="text-center">
                                                    <a href="{{ url('daftar-batch/'.$item->batch_id.'/'.$item->nama) }}" class="btn btn-xs" title="{{ $item->nama }}">Daftar</a>
                                                </td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tgl_mulai->formatLocalized('%A, %#d %B %Y') }}</td>
                                                <td>{{ $item->nama_cabang }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Belum ada kelas yang akan berjalan dalam waktu dekat</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection