@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url( {{ asset('assets/ticmi/images/banner/library.jpg')  }} ) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Perpustakaan Pasar Modal</h3>
                        <h6 class="sub-title">Kumpulan buku-buku tentang pasar modal</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/library-header.jpg') }}" class="img-responsive" alt="Event">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class="blog-single-details">
                            <h4><a href="#">Perpustakaan Pasar Modal</a></h4>
                            <p>
                                Jika Anda berkunjung ke TICMI maka Anda akan bisa menggunakan fasilitas perpustakaan Kami dengan gratis. Anda dapat melihat dan membaca buku-buku yang berkaitan tentang Pasar Modal dan Keuangan secara umum.
                                Di perpustakaan TICMI juga terdapat <em>Computer Zone</em> yang bisa digunakan untuk melihat data-data Pasar Modal seperti Laporan Keuangan Emiten, Prospectus, Laporan Tahunan Emiten, Pengumuman, Peraturan dan lain-lainnya.
                            </p>
                            <p>

                            </p>

                            <h4>Foto Perpustakaan TICMI</h4>

                            <style type='text/css'>
                                #gallery-1 {
                                    margin: auto;
                                }
                                #gallery-1 .gallery-item {
                                    float: left;
                                    margin-top: 10px;
                                    text-align: center;
                                    width: 25%;
                                }
                                #gallery-1 img {
                                    border: 2px solid #cfcfcf;
                                }
                                #gallery-1 .gallery-caption {
                                    margin-left: 0;
                                }
                                /* see gallery_shortcode() in wp-includes/media.php */
                            </style>
                            <div id='gallery-1' class='gallery galleryid-1551 gallery-columns-4 gallery-size-thumbnail'><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/DSC_0074.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/DSC_0074-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0163.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0163-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/DSC_0071.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/DSC_0071-150x150.jpg" class="attachment-thumbnail" alt="Computer Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Computer Zone
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0173.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0173-150x150.jpg" class="attachment-thumbnail" alt="Computer Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Computer Zone
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0168.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0168-150x150.jpg" class="attachment-thumbnail" alt="Computer Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Computer Zone
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/DSC_0268.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/DSC_0268-150x150.jpg" class="attachment-thumbnail" alt="Computer Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Computer Zone
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0174.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0174-150x150.jpg" class="attachment-thumbnail" alt="Computer Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Computer Zone
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/DSC_0308.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/DSC_0308-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0165.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0165-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0154.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0154-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0155.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0155-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0159.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0159-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0161.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0161-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0156.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0156-150x150.jpg" class="attachment-thumbnail" alt="Perpustakaan" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Perpustakaan
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0167.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0167-150x150.jpg" class="attachment-thumbnail" alt="Kids Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Kids Zone
                                    </dd></dl><dl class='gallery-item'>
                                    <dt class='gallery-icon landscape'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0153.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0153-150x150.jpg" class="attachment-thumbnail" alt="Kids Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Kids Zone
                                    </dd></dl><br style="clear: both" /><dl class='gallery-item'>
                                    <dt class='gallery-icon portrait'>
                                        <a href='http://www.icamel.id/wp-content/uploads/2015/02/SAM_0152.jpg' data-rel="prettyPhoto[library]"><img width="150" height="150" src="http://www.icamel.id/wp-content/uploads/2015/02/SAM_0152-150x150.jpg" class="attachment-thumbnail" alt="Kids Zone" /></a>
                                    </dt>
                                    <dd class='wp-caption-text gallery-caption'>
                                        Kids Zone
                                    </dd></dl>
                                <br style='clear: both' />
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="facebook"><a href="http://www.facebook.com/TICMI.ID" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="http://www.twitter.com/TICMI_ID" target="_blank" title="Twitter">Twitter</a></li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection