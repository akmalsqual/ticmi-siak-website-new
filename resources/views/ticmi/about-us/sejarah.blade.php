@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/history.jpg') }}) top right no-repeat; background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Profil Perusahaan</h3>
                        <h6 class="sub-title">Semua tentang TICMI</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row content-box bg-white shadow">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap text-center">
                            <img width="400" height="" align="center" src="{{ asset('assets/ticmi/images/default/logo-ticmi.jpg') }}" class="img-responsive text-center" alt="TICMI" style="margin: 0 auto;">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class="blog-single-details">
                            <h4 class="text-center"><a href="#">The Indonesia Capital Market Institute</a></h4>
                            <p>The Indonesia Capital Market Institute (TICMI) adalah sebuah institusi pendidikan yang menyelenggarakan pendidikan dan pelatihan serta ujian sertifikasi profesi pasar modal.</p>
                            <h4>Dasar Hukum</h4>
                            <p>The Indonesia Capital Market Institute (TICMI) didirikan oleh Perhimpunan Pendidikan Pasar Modal Indonesia (P3MI) pada tanggal 30 Agustus 2010 berdasarkan Akta No. 42 dihadapan Notaris Fathiah Helmi SH yang berkedudukan di Jakarta.</p>
                            <h4>Latar Belakang & Tujuan Pendirian TICMI</h4>
                            <p>P3MI adalah sebuah perhimpunan, yang merupakan gabungan dari Self Regulatory Organizations (SRO) di bidang Pasar Modal Indonesia, yaitu PT. Bursa Efek Indonesia (BEI), PT. Kliring Penjaminan Efek Indonesia (KPEI) dan PT. Kustodian Sentral Efek Indonesia (KSEI). Salah satu komitmen SRO adalah untuk ikut aktif berperan-serta dalam mengembangkan Pasar Modal Indonesia dengan membantu menyiapkan sumber daya manusia yang kompeten dan handal di bidang pasar modal, yang akan diwujudkan melalui Pendidikan Pasar Modal.</p>
                            <p>Dengan adanya target BEI untuk mencapai 2.300.000 investor pada akhir 2012, maka Pasar Modal Indonesia akan memerlukan cukup banyak sumber daya profesional baik sebagai Wakil Perantara Pedagang Efek (WPPE), Wakil Manajer Invetasi serta Wakil Penjamin Emisi Efek (WPEE). Sampai dengan Desember 2010 lalu, SDM yang sudah memiliki sertifikat profesi Pasar Modal baru mencapai jumlah sebanyak 9.721 untuk WPPE, 4.060 untuk WMI dan 2.748 untuk WPEE.</p>
                            <p>TICMI didirikan untuk mendorong dan membantu menciptakan serta mengembangkan Pasar Modal Indonesia yang dinamis melalui kegiatan pendidikan dan pelatihan profesi pasar modal yang dapat memenuhi kebutuhan regulator, perusahaan efek serta investor institusi maupun ritel dalam menyiapkan SDM yang terampil dan memiliki kecakapan profesi di bidang pasar modal serta paham dan mampu bekerja sesuai dengan hukum dan etika yang berlaku.</p>

                            <h4>Penyelenggaraan Kegiatan & Penjaminan Mutu Akademik</h4>
                            <p>Bekerja-sama dengan Fakultas Ekonomi Universitas Indonesia (FEUI), TICMI melaksanakan kegiatan pendidikan dan pelatihan serta ujian sertifikasi profesi WPPE, WMI dan WPEE di bawah pengawasan Komite Standar Pengajaran (KSP) yang dibentuk berdasarkan SK Ketua Bapepam No. 518/BL/2010 tertanggal 22 November 2010.</p>
                            <p>Kurikulum Pendidikan dan Pelatihan serta Ujian Profesi di TICMI dirancang untuk memenuhi kompetensi dasar profesi Pasar Modal dan para pengajarnya berasal dari berbagai kalangan, mulai dari regulator, SRO, profesional Pasar Modal, praktisi serta akademisi.</p>
                            <p>Penyelenggaraan Program WPPE di TICMI telah mendapatkan pengakuan dari Bapepam LK berdasarkan SK Ketua Bapepam No. 349/BL/2011 tertanggal 6 Juli 2011. Sistem Ujian Berbasis Komputer yang diselenggarakan oleh TICMI juga sudah diuji coba kelayakan sistem maupun kualitas soal ujiannya  melalui sebuah Audit Sistem yang diselenggarakan pada tanggal 20 Mei 2011.</p>

                            <h4>Indonesian Capital Market Electronic Library</h4>
                            <p>
                                PT Indonesian Capital Market Electronic Library (ICaMEL) merupakan salah satu anak perusahaan SRO (PT Bursa Efek Indonesia, PT Kliring Penjaminan Efek Indonesia, dan PT Kustodian Sentral Efek Indonesia) dengan pemilikan saham secara berimbang yang didirikan atas dukungan dan arahan dari Bapepam-LK, pada bulan Agustus 2011, dan mulai beroperasi pada bulan April 2012.
                            </p>

                            <h4>TICMI Bergabung dengan ICaMEL</h4>
                            <p>
                                Pada tanggal 10 November 2015 bertepatan dengan acara Investor Summit and Capital Market Expo 2015, TICMI bergabung dengan ICaMEL dan sekaligus melakukan Relaunching brand The Indonesia Capital Market Institute sebagai pusat referensi, edukasi, dan sertifikasi profesi pasar modal Indonesia.

                            </p>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection