@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/bulzeye.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Visi & Misi</h3>
                        <h6 class="sub-title">Goal dan Tujuan</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <section class="bg-grey">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-12">
                    <!-- Content Box -->
                    <div class="content-box shadow icon-box bg-white text-center">
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <p class="text-center"><h3>Menjadi Pusat Referensi, Edukasi dan Sertifikasi Profesi Pasar Modal Terbesar di Indonesia Tahun 2020</h3></p>
                        </div><!-- Content Wraper -->
                        <i class="fa uni-arrow-up color-pink text-center" style="font-size: 190px;"></i>
                    </div><!-- Content Box -->
                </div><!-- Column -->

            </div><!-- Row -->
        </div><!-- Container -->
    </section>

    <!-- Section -->
    <section class="bg-grey">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-4">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-primary icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-eye-visible" style="height: 80px; line-height: 80px;"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Visi</h4>
                            <blockquote>
                                <p>Menjadi pusat referensi, edukasi, dan sertifikasi profesi pasar modal Indonesia <br><br><br><br></p>
                            </blockquote>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->

                <!-- Column -->
                <div class="col-sm-4">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-primary icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-archery-2" style="height: 80px; line-height: 80px;"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Misi</h4>
                            <blockquote>
                                <p>Meningkatkan pengetahuan pasar modal masyarakat Indonesia, sehingga berkontribusi pada peningkatan jumlah profesional dan investor pasar modal</p>
                            </blockquote>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->

                <!-- Column -->
                <div class="col-sm-4">
                    <!-- Content Box -->
                    <div class="content-box shadow bg-primary icon-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="fa uni-speach-bubbledialog" style="height: 80px; line-height: 80px;"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h4 class="heading">Strategi</h4>
                            <blockquote>
                                <p>Menyediakan akses referensi, edukasi, dan sertifikasi profesi pasar modal Indonesia yang mudah dijangkau dan bermutu tinggi<br><br><br></p>
                            </blockquote>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

    <!-- Section -->
    <section class="bg-grey">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-12">
                    <!-- Content Box -->
                    <div class="content-box shadow icon-box bg-white text-center">
                        <i class="fa uni-arrow-up color-pink text-center" style="font-size: 190px;"></i>
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <p class="text-center"><h5>Peningkatan awareness masyarakat tentang pelayanan TICMI</h5></p>
                            <p class="text-center"><h5>Penggunaan teknologi untuk akses referensi, edukasi & sertifikasi profesi</h5></p>
                            <p class="text-center"><h5>Pengembangan produk referensi, program edukasi, dan sertifikasi profesi</h5></p>
                            <p class="text-center"><h5>Peningkatan mutu melalui continuous improvement</h5></p>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->

            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

    <section class="bg-grey"></section>
@endsection