@extends("layouts_ticmi.home")
@include('layouts_ticmi.googlesnippet')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url({{ asset('assets/ticmi/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Profil Perusahaan</h3>
                        <h6 class="sub-title">Semua tentang TICMI</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="{{ asset('assets/ticmi/images/blog/1.jpg') }}" class="img-responsive" alt="Event">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4><a href="#">The Indonesia Capital Market Institute</a></h4>
                            <p class="text-justify"><strong>The Indonesia Capital Market Institute (TICMI)</strong> adalah sebuah lembaga pendidikan dan pelatihan profesi pasar modal yang didirikan oleh <em><strong>Perhimpunan Pendidikan Pasar Modal Indonesia (P3MI)</strong></em>.</p>

                            <p class="text-justify">Pada tanggal <em>10 November 2015</em>, TICMI secara resmi bergabung dengan <em><strong>PT Indonesian Capital Market Electronic Library (ICaMEL)</strong></em> yang sekaligus menjadi anak perusahaan <em>PT Bursa Efek Indonesia</em>, <em>PT Kliring Penjaminan Efek Indonesia</em>, dan <em>PT Kustodian Sentral Efek Indonesia</em> <em>(SRO’s)</em>. TICMI bertekad untuk menjadi penyelenggara pendidikan dan pelatihan profesi pasar modal terkemuka yang diakui secara nasional dan internasional.</p>

                            <p class="text-justify">TICMI merupakan satu-satunya lembaga pendidikan dan pelatihan pasar modal yang sekaligus menyelenggarakan ujian sertifikasi profesi pasar modal di Indonesia. Seluruh lulusan TICMI diharapkan dapat membantu menciptakan dan mengembangkan pasar modal yang dinamis.</p>

                            <p class="text-justify">Pelatihan yang selenggarakan oleh TICMI bertujuan untuk memenuhi kebutuhan regulator, investor ritel, institusional, dan perusahaan efek dalam menyiapkan SDM yang terampil, memiliki kecakapan profesi dibidang pasar modal, serta memahami hukum dan etika yang berlaku. TICMI memberikan program pendidikan dan pelatihan yang unggul, dimana kurikulumnya dirancang untuk dapat memenuhi kompetensi dasar profesi di bidang pasar modal, sehingga pada akhirnya mereka dapat menjadi profesional pasar modal Indonesia yang handal.</p>

                            <p class="text-justify">Di bawah pengawasan Komite Standar Pengajaran yang dibentuk berdasarkan <em><strong>Keputusan Ketua Badan Pengawas Pasar Modal dan Lembaga Keuangan Nomor : KEP-518/BL/2010 tanggal 22 November 2010</strong></em>, TICMI mengembangkan soal-soal ujian, modul pengajaran, serta menyelenggarakan ujian sertifikasi profesi pasar modal sebagai bagian dari persyaratan untuk dapat menjadi <em>Wakil Perantara Pedagang Efek (WPPE)</em>, <em>Wakil Manajer Investasi (WMI)</em>, dan <em>Wakil Penjamin Emisi Efek (WPEE)</em>.</p>

                            <p class="text-justify">Dengan demikian, setelah menyelesaikan pelatihan dan lulus ujian, para peserta secara otomatis memperoleh sertifikat kelulusan TICMI yang dapat digunakan untuk mengajukan permohonan izin profesi ke OJK.</p>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection