
    @if($workshops && $workshops->count() > 0)
        @foreach($workshops as $workshop)
            <div class="col-sm-3 item-batch">
                <div class="event-wrap">
                    <div class="event-img-wrap course-banner-wrap color-white text-center" style="">
                        @if($workshop->program->nama == "Forum")
                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/new/WORKSHOP.png') }}" width="600" height="220">
                        @else
                            @if($workshop->is_online == 1)
                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/new/ONLINE-CLASS.png') }}" width="600" height="220">
                            @else
                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/new/WORKSHOP.png') }}" width="600" height="220">
                            @endif
                        @endif
                        <span class="cat bg-yellow">{{ $workshop->program->nama }} {{ (($workshop->is_online == 1) ? 'ONLINE':'') }}</span>
                    </div>
                    <!-- Event Detail Wrapper -->
                    <div class="event-details" style="height: 450px;">
                        <h4 class="batch-title" style="font-size: 16px;">
                            @if($workshop->is_online == 1)
                                <a href="https://ticmi.co.id/kelas-online/{{ $workshop->slugs }}" onclick="ga('send', 'event', 'Pendaftaran Online Class', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a>
                            @else
                                <a href="https://ticmi.co.id/seminar/{{ $workshop->slugs }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a>
                            @endif
                        </h4>
                        @if($workshop->is_online == 0)
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($workshop->tgl_mulai)) }}</li>
                                <li><i class="fa fa-map-marker"></i>{{ $workshop->program->nama }}</li>
                                <li><i class="fa fa-users"></i> {{ $workshop->waktu }}</li>
                            </ul>
                        @else
                            <ul class="events-meta">
                                <li><i class="fa fa-map-marker"></i>http://elearning.ticmi.co.id</li>
                                <li><i class="fa fa-clock-o"></i>Periode Akses : {{ $workshop->lama_belajar }} Hari</li>
                            </ul>
                        @endif
                        <p>{!! substr(strip_tags($workshop->deskripsi),0,200) !!}</p>
                        @if($workshop->is_online == 1)
                            <a href="https://ticmi.co.id/kelas-online/{{ $workshop->slugs }}" onclick="ga('send', 'event', 'Pendaftaran Online Class', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                        @else
                            <a href="https://ticmi.co.id/seminar/{{ $workshop->slugs }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>

                        @endif
                    </div><!-- Event Meta -->
                </div>
            </div>
        @endforeach
    @endif

    <div class="clearfix"></div>

    @if($programs && $programs->count() > 0)
       
            @foreach($programs as $key => $program)
                <!-- Event Column -->
                    <!-- Event Wrapper -->
                    @if($program->batch_publish->count() > 0)
                        <div id="{{$program->kode}}" >
                            @foreach($program->batch_publish as $key => $batch)
                                <div class="item">
                                    <div class="event-wrap">
                                        <div class="event-img-wrap course-banner-wrap color-white text-center" style="">
                                           <img alt="Event" class="img-responsive" src="{{ $batch_path.$batch->logo }}" width="600" height="220">

                                            <span class="cat bg-yellow">{{ $batch->program->kode }}</span>
                                        </div>
                                        <!-- Event Image Wrapper -->
                                        <!-- Event Detail Wrapper -->
                                        <div class="event-details" style="height: 450px;">
                                            <h4 class="batch-title" style="font-size: 16px;"><a href="{{ url('daftar-batch/'.$batch->batch_id.'/'.$batch->nama) }}" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $batch->nama }}', '{{ url()->current() }}' )">{{ $batch->nama }} </a></h4>
                                            <ul class="events-meta">
                                                <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($batch->tgl_mulai)) }}</li>
                                                {{--<li><i class="fa fa-calendar-o"></i> {{ date('l, d M Y',strtotime($batch->tanggal)) }}</li>--}}
                                                <li><i class="fa fa-map-marker"></i> {{ !empty($batch->lokasi->nama) ? $batch->lokasi->nama:"Gedung Bursa Efek Indonesia" }}</li>
                                                <li><i class="fa fa-users"></i> Minimal Peserta {{ !empty($batch->minimal) ? $batch->minimal:10 }} Orang</li>
                                            </ul>
                                            <p>{!! substr($batch->keterangan,0,150) !!}</p>

                                            <a href="{{ url('daftar-batch/'.$batch->batch_id.'/'.$batch->nama) }}" class="btn"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                                            

                                            {{--@if($value->kd_type != 1)--}}
                                                {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $value->kd_type }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                            {{--@else--}}
                                                {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $value->cabang }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                            {{--@endif--}}
                                        </div><!-- Event Meta -->
                                        <br/>
                                    </div><!-- Event details -->
                                </div><!-- Column -->
                            @endforeach
                        </div>
                    @endif

            @endforeach
    @endif

    @if($workshops->count() == 0 && $programs->count() == 0)
        <div class="col-sm-12">
            <div class="alert alert-warning" role="alert"><h4>Maaf, belum ada jadwal tersedia.</h4></div>
        </div>
    @endif

        {{-- Memasukkan array kode ke dalam select yang isinya akan diambil jquery --}}
    <select id="kode" style="visibility: hidden;">
        @foreach($arr_kode as $kode)
        <option value="{{$kode}}"></option>
        @endforeach
    </select>
    <script type="text/javascript">
        $(document).ready(function(){
            var array = [];
            $('#kode option').each(function() {
                array.push($(this).val());
            });
            
            $.each(array,function(key,val){
                $("#"+val).owlCarousel({
                    items:4,
                    nav:true,
                    margin:20,
                });
            })
        })
    </script>