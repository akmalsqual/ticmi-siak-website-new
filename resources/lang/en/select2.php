<?php
return [
    'select_kat_program'    => '-- Select Kategori Program --',
    'select_program'        => '-- Select Program --',
    'select_kelas'          => '-- Select Kelas --',
    'select_tipe_kelas'     => '-- Select Tipe Kelas --',
    'select_cabang'         => '-- Select Cabang --',
    'select_lokasi'         => '-- Select Lokasi --',
    'select_kat_program'    => '-- Select Kategori Program --',
    'select_kat_pengajar'   => '-- Select Kategori Pengajar --',
    'select_kat_perusahaan' => '-- Select Kategori Perusahaan --',
    'select_gender'         => '-- Select Gender --',
    'select_bank'           => '-- Select Bank --',
    'select_institusi'      => '-- Select Institusi --',
    'select_kat_teacher'    => '-- Select Kategori Teacher --',
    'select_teacher'        => '-- Select Teacher --',
    'select_modul'          => '-- Select Modul --',
    'select_submodul'       => '-- Select Sub Modul --',
    'select_ruang'          => '-- Select Ruang --',
    'select_hari'           => '-- Select Hari --',
    'select_jam'            => '-- Select Jam --',
    'select_jenis_voucher'  => '-- Select Jenis Voucher --',
    'select_item_kat'       => '-- Select Kategori Item --',
    'select_item'           => '-- Select Item --',
    'select_batch'          => '-- Select Batch --',
    'voucher_gratis_bayar'  => '-- Pilih Gratis/Bayar --',
    'select_voucher'        => '-- Select Voucher --',
    'select_perusahaan'     => '-- Select Perusahaan --',
    'select_biaya_kat'      => '-- Select Kategori Biaya --',
    'select_param_ujian'    => '-- Select Parameter Ujian --',
    'select_jenis_ujian'    => '-- Select Jenis Ujian --',
    'select_ruang'          => '-- Select Ruang --',
    'select_pengawas'       => '-- Select Pengawas --',
    'select_sex'            => '-- Select Jenis Kelamin --',
    'select_status_kawin'   => '-- Select Status Kawin --',
    'select_education'      => '-- Select Pendidikan --',
    'select_department'     => '-- Select Departemen --',
    'select_position'       => '-- Select Posisi --',
    'select_identity'       => '-- Select Jenis Identitas --',
    'select_status_employee'=> '-- Select Status Pegawai --',
    'select_durasi_ujian'   => '-- Select Durasi --',
    'select_ujian_jenis'    => '-- Select Jenis Ujian --',
    'select_role'           => '-- Select Role --',
    'select_peserta'        => '-- Select Peserta --',
    'select_rentang'        => '-- Select Rentang --',
    'select_perdana'        => '-- Select Perdana --',
    'select_batch'          => '-- Select Batch --',
    'select_jawaban'        => '-- Select Jawaban --',
    'select_bobot'          => '-- Select Bobot Soal --',
    'select_parent'         => '-- Select Parent Soal --',
    'select_lembaga'        => '-- Select Lembaga --',
    'select_lembg_pdkn'     => '-- Select Kantor Perwakilan --',
    'select_negara'         => '-- Select Warga Negara --',
    'select_profesi'        => '-- Select Profesi --',
];
