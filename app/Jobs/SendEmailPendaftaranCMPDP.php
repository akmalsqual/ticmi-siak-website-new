<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\CMPDP;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailPendaftaranCMPDP extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $cmpdp;
	
	/**
	 * Create a new job instance.
	 *
	 * @param \App\Model\CMPDP $cmpdp
	 */
    public function __construct(CMPDP $cmpdp)
    {
        $this->cmpdp = $cmpdp;
    }
	
	/**
	 * Execute the job.
	 *
	 * @param \Illuminate\Contracts\Mail\Mailer $mailer
	 * @return void
	 */
    public function handle(Mailer $mailer)
    {
        $cmpdp = $this->cmpdp;
        
        $mailer->queue('email-template.email-pendaftaran-cmpdp',['cmpdp'=>$cmpdp], function ($m) use ($cmpdp) {
	        $m->from('noreply@ticmi.co.id', 'TICMI');
	        $m->to($cmpdp->email, $cmpdp->nama_lengkap)->subject('TICMI - Notifikasi Pendaftaran Capital Market Professional Development Program Angkatan 2018');
	        
        });
    }
}
