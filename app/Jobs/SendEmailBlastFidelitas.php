<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\Customer;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailBlastFidelitas extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	
	protected $customer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
	    $customer = $this->customer;
	    
	    $mailer->send('email-template.email-fidelitas',['customer'=>$customer], function ($m) use ($customer){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($customer->email, $customer->name)->subject('Corporate Finance Qualification Preparatory Class');
//		    $m->to('akmal.squal@gmail.com', 'Akmal')->subject('Corporate Finance Qualification Preparatory Class');
	    });
    }
}
