<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\ticmi\PesertaWorkshop;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailPendaftaranOnlineClass extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $pesertaWorkshop;
	
	/**
	 * Create a new job instance.
	 *
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 */
    public function __construct(PesertaWorkshop $pesertaWorkshop)
    {
        $this->pesertaWorkshop = $pesertaWorkshop;
    }
	
	/**
	 * Execute the job.
	 *
	 * @param \Illuminate\Mail\Mailer $mailer
	 * @return void
	 */
    public function handle(Mailer $mailer)
    {
	    $pesertaWorkshop = $this->pesertaWorkshop;
	    $referral = null;
	    if ($pesertaWorkshop->has_referral) {
		    $referral = PesertaWorkshop::where('referral_from',$pesertaWorkshop->id)->get();
	    }
	
	    $mailer->send('email-template.email-pendaftaran-kelas-online',['pesertaWorkshop'=>$pesertaWorkshop,'referral'=>$referral], function ($m) use ($pesertaWorkshop){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($pesertaWorkshop->email, $pesertaWorkshop->nama)->subject('TICMI - Konfirmasi Pendaftaran '.$pesertaWorkshop->workshop->workshop_name.' | No Tagihan : #'.$pesertaWorkshop->invoice_no);
	    });
    }
}
