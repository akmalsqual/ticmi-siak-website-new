<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use Mail;
use Crypt;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;

class SendEmailResetPassword extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $member;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->member = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        // $member = $this->member;
	    // $token = Str::quickRandom(6);
	    // $newPass = bcrypt($token);
	    // if ($member->update(['password'=>$newPass])) {
		   //  $mailer->queue('email-template.email-reset-password',['member'=>$member,'token'=>$token], function ($m) use ($member){
			  //   $m->from('noreply@ticmi.co.id', 'TICMI');
			  //   $m->to($member->email, $member->name)->subject('TICMI - Reset Password');
		   //  });
	    // }
        
        /* UPDATED */
        $member  = $this->member;
        $token   = Str::quickRandom(6);
        $newPass = bcrypt($token);
        if ($member->update(['password'=>$newPass])) {
            $header                  = 'email-template.core.header';
                    $footer          = 'email-template.core.footer';
                    $config          = Config_email::first();
                    $konfirmasi      = Konfirmasi::find(9);
                    Mail::send('email-template.email-reset-password', 
                        [   'header'        => $header, 
                            'footer'        => $footer, 
                            'config'        => $config, 
                            'konfirmasi'    => $konfirmasi, 
                            'peserta'       => $member,
                            'token'         => $token,
                            'link'          => route('reset_password',['useremail'=>Crypt::encrypt($member->email),'user'=>$member->peserta_id])
                        ],
            function ($message) use ($member){
                    $message->subject(' TICMI - Reset Password');
                    $message->from('noreply@ticmi.co.id', 'TICMI');
                    $message->to($member->email);
                }
            );
        }

    }
}
