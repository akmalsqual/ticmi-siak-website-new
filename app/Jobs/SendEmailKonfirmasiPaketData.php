<?php

namespace App\Jobs;

use Mail;
use App\Mail\OrderShipped;
use App\Models\ticmi\PaketUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailKonfirmasiPaketData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $paketUser;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PaketUser $paketUser)
    {
        $this->paketUser = $paketUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $paketUser = $this->paketUser;

        // Mail::queue('email-template.email-konfirmasi-paket-data',['paketUser' => $paketUser], function ($message) use ($paketUser){
        //     $message->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
        //     $message->from('noreply@ticmi.co.id', 'TICMI');
        //     $message->to($paketUser->peserta->email);
        //     $message->bcc('mrezavahlevi66@gmail.com');
        // });
        $message = (new OrderShipped($paketUser))
                    ->from('noreply@ticmi.co.id', 'ADM TICMI')
                    ->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no)
                    ->onQueue('emails');

        Mail::to($paketUser->peserta->email)
            // ->cc('mrezavahlevi66@gmail.com')
            ->bcc('mrezavahlevi66@gmail.com')
            ->queue($message);
    
        // $mailer->send('email-template.email-konfirmasi-paket-data',['paketUser'=>$paketUser], function ($m) use ($paketUser){
        //     $m->from('noreply@ticmi.co.id', 'TICMI');
        //     $m->to($paketUser->peserta->email, $paketUser->peserta->nama)->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
        //     $m->bcc('mrezavahlevi66@gmail.com', 'Datacenter Ticmi')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
            // $m->bcc('tirza.omar@ticmi.co.id', 'Tirza Omar Dana')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
            // $m->bcc('syaipul.amri@ticmi.co.id', 'Syaipul Amri')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
            // $m->bcc('ria.eka@ticmi.co.id', 'Ria Eka')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$paketUser->invoice_no);
        // });
    }
}
