<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\ticmi\SuratRiset;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailPengajuanSuratRiset extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $suratRiset;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SuratRiset $suratRiset)
    {
        $this->suratRiset = $suratRiset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $suratRiset = $this->suratRiset;
        
        $mailer->send('email-template.email-pengajuan-surat-riset-non-sid',['suratRiset'=>$suratRiset], function ($m) use ($suratRiset) {
	        $m->from('noreply@ticmi.co.id', 'TICMI');
	        $m->to($suratRiset->peserta->email, $suratRiset->peserta->nama)->subject('TICMI - Permohonan Surat Riset A/N '.$suratRiset->namaMhs);
        });
    }
}
