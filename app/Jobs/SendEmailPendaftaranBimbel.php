<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\DaftarUjian;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailPendaftaranBimbel extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $ujian;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DaftarUjian $ujian)
    {
        $this->ujian = $ujian;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $ujian = $this->ujian;
	
	    $mailer->queue('email-template.email-pendaftaran-bimbel',['ujian'=>$ujian], function ($m) use ($ujian) {
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    if ($ujian->remark == 'Gratis') {
		    	$subject = 'TICMI - Notifikasi Pendaftaran Ujian Ulang Gratis';
		    } else {
		    	$subject = 'TICMI - Notifikasi Pendaftaran Bimbingan & Ujian Gratis';
		    }
		    $m->to($ujian->email, $ujian->nama)->subject($subject);
            $m->getSwiftMessage();
            $m->getHeaders()->addTextHeader('X-Mailgun-Variables',\GuzzleHttp\json_encode(['variabel_1'=>$ujian->email]));
            $m->getHeaders()->addTextHeader('X-Mailgun-Variables',\GuzzleHttp\json_encode(['variabel_2'=>['email_type'=>'Email-Bimbel']]));
	    });
    }
}
