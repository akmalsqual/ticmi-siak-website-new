<?php

namespace App\Jobs;

use App\Models\ticmi\PaketUser;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailInvoicePembelianPaketData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $paketUser;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PaketUser $paketUser)
    {
        // $paketUser = PaketUser::whereid($paketUserId);
        $this->paketUser = $paketUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $paketUser = $this->paketUser;
    
        $mailer->send('email-template.email-invoice-pembelian-paket-data',['paketUser'=>$paketUser], function ($m) use ($paketUser){
            $m->from('noreply@ticmi.co.id', 'TICMI');
            $m->to($paketUser->peserta->email,$paketUser->peserta->nama)->subject('TICMI - Tagihan Pembelian Paket Data #'.$paketUser->invoice_no);
        });
    }
}
