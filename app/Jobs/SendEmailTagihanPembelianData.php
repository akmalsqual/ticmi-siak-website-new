<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\ticmi\Keranjang;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailTagihanPembelianData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $keranjang;

    /**
     * Create a new job instance.
     *
     * @param Keranjang $keranjang
     */
    public function __construct(Keranjang $keranjang)
    {
        $this->keranjang = $keranjang;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $keranjang = $this->keranjang;
        $mailer->queue('email-template.email-tagihan-pembelian-data',['keranjang'=>$keranjang], function ($m) use ($keranjang){
            $m->from('noreply@ticmi.co.id', 'TICMI');
            $m->to($keranjang->user->email, $keranjang->user->name)->subject('TICMI - Tagihan Pembelian Data #'.$keranjang->invoice_no);
        });
    }
}
