<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\ticmi\Keranjang;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailKonfirmasiPembayaran extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $keranjang;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Keranjang $keranjang)
    {
        $this->keranjang = $keranjang;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $keranjang = $this->keranjang;
        $mailer->queue('email-template.email-konfirmasi-pembayaran',['keranjang'=>$keranjang], function ($m) use ($keranjang){
            $m->from('noreply@ticmi.co.id', 'TICMI');
            $m->to($keranjang->user->email, $keranjang->user->name)->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$keranjang->invoice_no);
            $m->bcc('datacenter@ticmi.co.id', 'Datacenter Ticmi')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$keranjang->invoice_no);
            $m->bcc('tirza.omar@ticmi.co.id', 'Tirza Omar Dana')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$keranjang->invoice_no);
            $m->bcc('syaipul.amri@ticmi.co.id', 'Syaipul Amri')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$keranjang->invoice_no);
            $m->bcc('ria.eka@ticmi.co.id', 'Ria Eka')->subject('TICMI - Konfirmasi pembayaran diterima | Tagihan no #'.$keranjang->invoice_no);
        });
    }
}
