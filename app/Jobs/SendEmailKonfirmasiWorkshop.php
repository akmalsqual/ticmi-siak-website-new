<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\ticmi\PesertaWorkshop;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailKonfirmasiWorkshop extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $pesertaWorkshop;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PesertaWorkshop $pesertaWorkshop)
    {
        $this->pesertaWorkshop = $pesertaWorkshop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $pesertaWorkshop = $this->pesertaWorkshop;
	
	    $mailer->send('email-template.email-konfirmasi-workshop',['pesertaWorkshop'=>$pesertaWorkshop], function ($m) use ($pesertaWorkshop){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($pesertaWorkshop->email, $pesertaWorkshop->nama)->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
		    $m->bcc('tirza.omar@ticmi.co.id', 'Tirza Omar Dana')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
		    $m->bcc('syaipul.amri@ticmi.co.id', 'Syaipul Amri')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
		    $m->bcc('ria.eka@ticmi.co.id', 'Ria Eka')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
	    });
    }
}
