<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelatihan_aktual_dtl extends Model
{
    protected $table = 'pelatihan_aktual_dtl';
    public $primaryKey = 'pelatihan_aktual_dtl_id';
    public $timestamps = false;

    public function ruangan()
    {
    	return $this->belongsTo('App\Models\Ruang', 'ruang_id', 'ruang_id');
    }

    public function pengajar()
    {
    	return $this->belongsTo('App\Models\Pengajar', 'pengajar_id', 'pengajar_id');
    }

    public function pelatihan_aktual()
    {
        return $this->belongsTo('App\Models\Pelatihan_aktual', 'pelatihan_aktual_id', 'pelatihan_aktual_id');
    }
}
