<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Svy extends Model
{
    protected $table = 'svy';
    public $primaryKey = 'svy_id';

    public function svy_manag()
    {
    	return $this->belongsTo('App\Models\Svy_manag','svy_id','svy_id');
    }
}
