<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
    protected $table = 'hari';
    public $primaryKey = 'hari_id';
    public $timestamps = false;
}
