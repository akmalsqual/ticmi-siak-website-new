<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Pendaftaran extends Revisionable
{
    protected $table                    = 'pendaftaran';
    public $primaryKey                  = 'pendaftaran_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
