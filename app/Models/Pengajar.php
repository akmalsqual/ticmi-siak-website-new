<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengajar extends Model
{
    protected $table = 'pengajar';
    public $primaryKey = 'pengajar_id';
    public $timestamps = false;
}
