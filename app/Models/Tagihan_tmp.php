<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Tagihan_tmp extends Revisionable
{
    protected $table                    = 'tagihan_tmp';
    public $primaryKey                  = 'tagihan_tmp_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
