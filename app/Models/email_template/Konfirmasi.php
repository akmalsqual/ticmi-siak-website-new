<?php

namespace App\Models\email_template;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
    public $table       = 'email_konfirmasi';
    public $primaryKey  = 'email_konfirmasi_id';
    public $timestamps  = false;

    public function program()
    {
        return $this->hasMany('App\Models\program_management\Program', 'email_konfirmasi_id', 'email_konfirmasi_id');
    }
}
