<?php

namespace App\models\email_template;

use Illuminate\Database\Eloquent\Model;

class Config_email extends Model
{
    //
    public $table                       = 'email_config';
    public $primaryKey                  = 'email_config_id';
    public $timestamps                  = false;

    public function check_data()
    {
    	// return $this->belongsTo('App\Models\app_management\Role_menu', 'menu_id', 'menu_id');
    }
}
