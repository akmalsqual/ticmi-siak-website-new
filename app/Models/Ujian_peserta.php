<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Ujian_peserta extends Revisionable
{
    protected $table                    = 'ujian_peserta';
    public $primaryKey                  = 'ujian_peserta_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
