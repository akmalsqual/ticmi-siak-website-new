<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Svy_pilihan_jawaban extends Revisionable
{
	protected $table   = 'svy_pilihan_jawaban';
	public $primaryKey = 'svy_pilihan_jawaban_id';
	public $timestamps = false;
    
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
