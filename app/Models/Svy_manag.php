<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Svy_manag extends Model
{
    protected $table = 'svy_manag';
    public $primaryKey = 'svy_manag_id';

    public function svy()
    {
    	return $this->belongsToMany('App\Models\Svy','svy_id','svy_id');
    }
}
