<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
      // protected $connection = 'mysql';
	protected $table = 'ticmi_provinces';
    
    protected $fillable = [
        'nama',
        'program_id',
    ];
	
	/* public function kabupaten()
	{
		return $this->hasMany('\App\Kabupaten','province_id','id');
    } */
}
