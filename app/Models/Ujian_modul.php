<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujian_modul extends Model
{
    protected $table   = 'ujian_modul';
    public $primaryKey = 'ujian_modul_id';
    public $timestamps = false;

    public function batch_modul()
    {
    	return $this->belongsTo('App\Models\Batch_modul_dtl', 'modul_id', 'modul_id');
    }

}
