<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Bayar_manual extends Revisionable
{
    public $table                       = 'bayar_manual';
    public $primaryKey                  = 'bayar_manual_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
