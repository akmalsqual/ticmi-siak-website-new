<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Pelamar extends Revisionable
{
    protected $table 		= 'pelamar';
    protected $primaryKey 	= 'pelamar_id';
    protected $guarded 		= [];
    public $timestamps 		= false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    // public function pelamar_sertifikat(){
    // 	return $this->belongsTo('App\Models\peserta_sertifikat','peserta_sertifikat_id');
    // }

    public function lowongan(){
    	return $this->hasMany('App\Models\lowongan','lowongan_id','lowongan_id');
    }
}
