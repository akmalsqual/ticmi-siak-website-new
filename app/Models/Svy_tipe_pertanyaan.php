<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Svy_tipe_pertanyaan extends Model
{
    protected $table = 'svy_tipe_pertanyaan';
    public $primaryKey = 'svy_tipe_pertanyaan_id';
}
