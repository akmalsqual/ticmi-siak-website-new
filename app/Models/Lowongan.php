<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Lowongan extends Model
{
    protected $table        = 'lowongan';
    protected $primaryKey   = 'lowongan_id';
    protected $guarded      = [];
    public $timestamps      = false;
	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'is_publish' => 'boolean',
		'hapus' => 'boolean',
	];
	
	public static function boot()
	{
		parent::boot();
		
		static::addGlobalScope('lowonganDataDeleted', function (Builder $builder){
			$builder->whereNull('lowongan.date_deleted');
		});
	}

    public function perusahaan(){
    	return $this->belongsTo('App\Models\Perusahaan','perusahaan_id');
    }

    public function user(){
    	return $this->belongsTo('App\Models\User','id');
    }

    public function pelamar(){
        return $this->hasMany('App\Models\Pelamar', 'pelamar_id');
    }
}
