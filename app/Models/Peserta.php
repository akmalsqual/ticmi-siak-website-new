<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Venturecraft\Revisionable\Revisionable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Venturecraft\Revisionable\RevisionableTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Peserta extends Authenticatable
{
//	use SoftDeletes { restore as private restoreB; }
	use RevisionableTrait;
	use Notifiable;
	use EntrustUserTrait;
	
    protected $table    = 'peserta';
    public $primaryKey  = 'peserta_id';
    public $timestamps  = false;
    protected $fillable = [
    	'Actions', 'peserta_id', 'nama', 'email', 'password', 'no_hp', 'tanggal_lahir', 'profesi', 'is_verify', 'status', 
    	'newsletter', 'no_sid', 'sid_card', 'is_valid_sid', 'remember_token', 'created_at', 'updated_at', 'profesi_id', 
    	'kat_peserta_id', 'moodle_user_id'
    ];
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}