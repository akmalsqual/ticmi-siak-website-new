<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujian_batch extends Model
{
    protected $table = 'ujian_batch';
    public $primaryKey = 'ujian_batch_id';
    public $timestamps = false;

    public function batch()
    {
    	return $this->belongsTo('App\Models\Batch', 'batch_id', 'batch_id');
    }

    public function perdana_jadwal()
    {
    	return $this->belongsTo('App\Models\Perdana_jadwal', 'perdana_jadwal_id', 'perdana_jadwal_id');
    }
}
