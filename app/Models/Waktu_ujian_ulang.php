<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Waktu_ujian_ulang extends Revisionable
{
    protected $table                    = 'waktu_ujian_ulang';
    public $primaryKey                  = 'waktu_ujian_ulang_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
