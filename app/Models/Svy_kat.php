<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Svy_kat extends Model
{
    protected $table = 'svy_kat';
    public $primaryKey = 'svy_kat_id';

    public function svy_pertanyaan()
    {
    	return $this->hasMany('App\Models\Svy_pertanyaan','svy_kat_id','svy_kat_id')->whereNull('svy_pertanyaan.deleted_at');
    }
}
