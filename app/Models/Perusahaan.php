<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    public $table                       = 'perusahaan';
    public $primaryKey                  = 'perusahaan_id';
    public $timestamps                  = false;

    public function lowongan(){
        return $this->hasMany('App\Models\Lowongan','perusahaan_id');
    }
}
