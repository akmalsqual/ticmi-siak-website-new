<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class PaketData extends Revisionable
{
    // protected $connection = 'mysql'; 
    protected $table = 'cmeds_paket_data';
    
    protected $fillable = [
        'slug',
        'name',
        'price',
        'jml_download',
        'description',
        'icon',
        'is_active'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function data_type()
	{
		return $this->belongsToMany('App\Models\ticmi\DataType','cmeds_paket_data_detail','cmeds_paket_data_id','cmeds_m_data_type_id')->withTimestamps();
    }
	
	public function paket_user()
	{
		return $this->belongsToMany('App\Models\ticmi\PaketUser');
    }
}
