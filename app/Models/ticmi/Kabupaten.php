<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Kabupaten extends Revisionable
{
    protected $table = 'ticmi_regencies';
    
    protected $fillable = [
        'province_id',
        'name'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function provinsi()
	{
		return $this->belongsTo('\App\Models\ticmi\Provinsi','province_id','id');
    }
	
	public function kecamatan()
	{
		return $this->hasMany('\App\Models\ticmi\Kecamatan','regency_id','id');
    }
}
