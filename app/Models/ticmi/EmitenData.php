<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\Revisionable;

class EmitenData extends Revisionable
{
	protected $table = 'cmeds_emitendata';
	
	protected $softDelete = false;
	
	protected $primaryKey = 'idEmitenData';
	
	protected $dates = [
		'createdDate','updatedDate','deletedDate'
	];
	
	public $timestamps = false;
	
	protected $fillable = [
		'idEmiten',
		'title',
		'type',
		'year',
		'quarter',
		'month',
		'filename',
		'idSector',
		'description',
		'createdDate',
		'createdBy',
		'updatedDate',
		'updatedBy',
		'isActive',
		'isDeleted',
		'docType',
		'deletedDate',
		'deletedBy',
		'prospectus_type',
		'idAnggotaBursa',
		'bondCode',
	];
	
	
	public static function boot()
	{
		parent::boot();
		
		static::addGlobalScope('emitenDataDeleted', function (Builder $builder){
			$builder->where('cmeds_emitendata.isDeleted',0);
		});
	}
	
	public function emiten()
	{
		return $this->belongsTo('App\Models\ticmi\Emiten','idEmiten','idEmiten');
	}
}
