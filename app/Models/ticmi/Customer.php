<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Customer extends Revisionable
{
	protected $table = 'cmeds_customer';
	
	protected $primaryKey = 'idCustomer';
	
	public $timestamps = false;
	
	protected $revisionCreationsEnabled = true;
	
	protected $fillable = [
		'username',
		'password',
		'name',
		'knowFrom',
		'email',
		'institution',
		'phone',
		'avatar',
		'address1',
		'address2',
		'city',
		'postalCode',
		'idRegion',
		'idCountry',
		'registeredDate',
		'deletedBy',
		'updatedBy',
		'isDeleted',
		'updatedDate',
		'province',
		'gender',
		'placeBirth',
		'occupation',
		'cardNumber',
		'notes',
		'country',
		'memberRef',
		'registerNumber',
		'dateOfBirth'
	];
    
	public static function boot()
	{
		parent::boot();
		
		static::addGlobalScope('customerDeleted', function (Builder $builder){
			$builder->where('isDeleted',0);
		});
	}
}
