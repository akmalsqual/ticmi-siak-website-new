<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Provinsi extends Revisionable
{
    protected $table = 'ticmi_provinces';
    
    protected $fillable = [
        'name'
    ];
    
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }

	public function kabupaten()
	{
		return $this->hasMany('\App\Models\ticmi\Kabupaten','province_id','id');
    }
}
