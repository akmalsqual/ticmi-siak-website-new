<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Kelurahan extends Revisionable
{
    protected $table = 'ticmi_districts';
    
    protected $fillable = [
        'district_id',
    	'name'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function kecamatan()
	{
		return $this->belongsTo('\App\Models\ticmi\Kecamatan','district_id','id');
    }
    
}
