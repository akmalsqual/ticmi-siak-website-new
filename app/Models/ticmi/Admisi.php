<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;

class Admisi extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 't_admisi';
    
    public $timestamps = false;
    
    protected $fillable = [
    	'id',
    	'tanggal',
        'nm_peserta',
        'password',
        'workshop_id',
        't_program_id',
        'file_pendukung',
        'foto',
        'tmp_lahir',
        'kelamin',
        'tgl_lahir',
        'hp',
        'telp_rumah',
        'email',
        'alamat',
        'pekerjaan',
        'perusahaan',
        'perusahaan1',
        'perusahaan2',
        'kelembagaan',
        'pendidikan',
        'jurusan',
        'universitas',
        'status_pendaftar',
        'status_pendaftar2',
        'noregwmi',
        'PS',
        'PE',
        'PA',
        'PHB',
        'MK',
        'SEB',
        'MIE',
        'MAE',
        'MIAS',
        'MP',
        'foto2',
        'sid',
        'Sertifikat1_PM',
        'Sertifikat1_PN',
        'Sertifikat1_NO',
        'Sertifikat2_LN',
        'Sertifikat2_NO',
        'psp',
        'wppe',
        'wmi',
        'wpee',
        'tgl_daftar',
        'kd_program',
        'kd_kelas',
        'kd_ruang',
        'batch',
        'batch2',
        'sesi',
        'retail',
        'cabang',
        'nilai',
        'nilai2',
        'jml',
        'nip',
        'publish',
        'share_telp',
        'share_fb',
        'share_twitter',
        'share_linkedin',
        'share_email',
        'tgl_akhir',
        'virtual_account',
        'status',
        'tansfer',
        'tansfer2',
        'aktif',
        'parent',
        'ranting',
        'captcha',
        'karir',
        'is_career',
	    'jadwal_kelas_online_id'
    ];
	
	public function program()
	{
		return $this->belongsTo('App\Model\Program','kd_program','id');
    }
	
	public function kelas() {
		return $this->belongsTo('App\Model\Kelas','kd_kelas','id');
	}
}
