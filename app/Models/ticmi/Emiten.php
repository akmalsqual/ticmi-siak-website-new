<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Emiten extends Revisionable
{
	protected $table = 'cmeds_emiten';
	
	protected $primaryKey = 'idEmiten';
	
	public $timestamps = false;
	
	protected $dates = ['createdDate','updatedDate','deletedDate'];
	
	protected $fillable = [
		'name',
		'code',
		'description',
		'idSector',
		'idSubsector',
		'createdDate',
		'createdBy',
		'updatedDate',
		'updatedBy',
		'isActive',
		'isDeleted',
		'deletedDate',
		'deletedBy'
	];
	protected $revisionCreationsEnabled = true;

	public static function boot()
	{
		parent::boot();
		static::addGlobalScope('emitenDeleted', function (Builder $builder){
			$builder->where('isDeleted',0);
		});
	}
	
	public function emitenData()
	{
		return $this->hasOne('App\Models\ticmi\EmitenData','idEmiten','idEmiten');
	}
}
