<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
 
class FormReksadana extends Model
{
	use SoftDeletes;
	
    protected $table = 'ticmi_peserta_form_reksadana';
    
    protected $primaryKey = 'id_form';
    
    protected $fillable = [
	    'am_id',
	    'alamat_sesuai_ktp',
	    'workshop_id',
	    't_program_id',
	    'kelamin',
	    'nama',
	    'nama_ibu',
	    'nasional',
	    'tipe_id',
	    'no_ktp',
	    'tempat_lahir',
	    'file_ktp',
	    'tgl_lahir',
	    'marital',
	    'pendidikan',
	    'agama',
	    'alamat_ktp',
	    'provinsi_ktp',
	    'kabupaten_ktp',
	    'kecamatan_ktp',
	    'alamat_skrg',
	    'provinsi_skrg',
	    'kabupaten_skrg',
	    'kecamatan_skrg',
	    'bank',
	    'cbg_bank',
	    'rekening',
	    'nama_rekening',
	    'nilai',
	    'email',
	    'no_hp',
	    'pekerjaan',
	    'pendapatan',
	    'sumber_dana',
	    'goal'
    ];
	
    protected $dates = ['confirm_at','deleted_at'];
    
	public function workshop()
	{
		return $this->belongsTo('App\Model\Workshop','workshop_id','id');
    }
	
	public function konfirmasi()
	{
		return $this->hasOne('App\Model\Konfirmasi','invoice_no','invoice_no');
    }
	
	public function referral()
	{
		return $this->belongsTo('App\Model\PesertaReksadana','referral_from','id');
	}
	
	public function child_referral()
	{
		return $this->hasMany('App\Model\PesertaReksadana','referral_from','id');
	}
}
