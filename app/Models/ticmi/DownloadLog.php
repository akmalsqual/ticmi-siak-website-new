<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class DownloadLog extends Revisionable
{
    // protected $connection = 'mysql';
    protected $table = 'cmeds_download_log';
    
    protected $fillabe = [
        'user_id',
        'idEmitenData',
        'emiten_code',
        'file_title',
        'file_type',
        'file_doctype',
        'year',
        'quarter',
        'date_file',
        'institusi',
        'download_date',
        'counter',
        'from_web',
        'from_email',
        'paket_user_id'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function user()
	{
		return $this->belongsTo('App\User');
    }
	
	public function paket_user()
	{
		return $this->belongsTo('App\Models\ticmi\PaketUser','paket_user_id','id');
    }
}
