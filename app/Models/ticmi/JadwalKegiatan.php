<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;

class JadwalKegiatan extends Model
{
    protected $table = 'ticmi_jadwal_kegiatan';
    
    protected $fillable = [
        't_program_id',
        't_type_id',
        'ppl_type',
        'nama_kegiatan',
        'tgl_dari',
        'tgl_type',
        'tgl_sampai',
        'cabang_id',
        'keterangan',
        'link',
        'is_publish'
    ];
    
//    protected $dates = ['tgl_dari', 'tgl_sampai'];
	
	public function program()
	{
		return $this->belongsTo('App\Models\Program','t_program_id','program_id');
	}
	
	public function tipe_kelas()
	{
		return $this->belongsTo('App\Models\Tipe','t_type_id','id');
	}
	
	public function cabang()
	{
		return $this->belongsTo('App\Models\Cabang','cabang_id','cabang_id');
	}
	
//	public function setTglDariAttribute($tanggal)
//	{
//		$this->attributes['tgl_sampai'] = date('Y-m-d', strtotime($tanggal));
//	}
//
//	public function setTglSampaiAttribute($tanggal)
//	{
//		$this->attributes['tgl_dari'] =  date('Y-m-d', strtotime($tanggal));
//	}
	
//	public function getTglDariAttribute($tanggal)
//	{
//		$this->attributes['tgl_sampai'] = date('Y-m-d', strtotime($tanggal));
//	}
//
//	public function getTglSampaiAttribute($tanggal)
//	{
//		$this->attributes['tgl_dari'] =  date('Y-m-d', strtotime($tanggal));
//	}
}
