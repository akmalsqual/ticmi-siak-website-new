<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class KeranjangDetail extends Revisionable
{
    protected $table = 'cmeds_keranjang_detail';

    protected $fillable = [
        'cmeds_keranjang_id',
        'user_id',
        'data_emiten_id',
        'item_name',
        'item_doctype',
        'item_type',
        'item_year',
        'item_filename',
        'price',
        'qty',
        'diskon',
        'total_price',
        'downloaded',
    ];
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function keranjang() {
        return $this->belongsTo('App\Models\ticmi\Keranjang','cmeds_keranjang_id','id');
    }
}
