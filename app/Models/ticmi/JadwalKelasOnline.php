<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JadwalKelasOnline extends Model
{
	use SoftDeletes;
	
    protected $table = 'ticmi_jadwal_kelas_online';
    
    protected $fillable = [
    	'program_id',
	    'tgl_start',
	    'tgl_end',
	    'remark',
	    'is_publish',
	    'nama'
    ];
}
