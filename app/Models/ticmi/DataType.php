<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class DataType extends Revisionable
{
    // protected $connection = 'mysql'; 
    protected $table = 'cmeds_m_data_type';
    
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    protected $fillable = [
        'name',
        'parent_id',
        'doctype',
        'type',
        'is_active'
    ];
	
	public function paket_data()
	{
		return $this->belongsToMany('App\Models\ticmi\PaketData')->using('App\PaketDataDetail');
    }
}
