<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\Revisionable;

class Keranjang extends Revisionable
{
    use SoftDeletes;

    protected $table = 'cmeds_keranjang';

    protected $fillable = [
        'user_id',
        'invoice_no',
        'subtotal',
        'biaya_admin',
        'diskon',
        'grandtotal',
        'virtual_account',
        'is_cancel',
        'cancel_date',
        'is_confirm',
        'confirm_date',
        'is_payment_approve',
        'payment_approve_by',
        'payment_approve_date',
        'valid_until',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','confirm_date','cancel_date','payment_approve_date'];
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'peserta_id');
    }

    public function keranjang_detail() {
        return $this->hasMany('App\Models\ticmi\KeranjangDetail','cmeds_keranjang_id','id');
    }

    public function konfirmasi()
    {
        return $this->hasOne('App\Models\ticmi\Konfirmasi');
    }
}
