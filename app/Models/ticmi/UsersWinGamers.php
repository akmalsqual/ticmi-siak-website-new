<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class UsersWinGamers extends Revisionable
{
    protected $table = 'users_wingamers';
    
    protected $fillable = [
        'user_id',
        'loginid',
        'cif',
        'url_conf',
        'accountcode',
        'date_expired',
    ];
	
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }

	public function user()
    {
        return $this->belongsTo('App\User','id','peserta_id');
    }
}
