<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\Revisionable;

class PesertaWorkshop extends Revisionable
{
	use SoftDeletes;
	
    protected $table = 'ticmi_peserta_workshop';
    
    protected $fillable = [
    	'invoice_no',
    	'workshop_id',
	    'urutan',
	    'nama',
	    'email',
	    'email_corp',
	    'company',
	    'jabatan',
	    'telp',
	    'no_hp',
	    'no_sk_ojk',
	    'tgl_sk_ojk',
	    'surat_izin_ojk',
	    'provinsi_id',
	    'kabupaten_id',
	    'biaya',
	    'diskon',
	    'total_bayar',
	    'has_referral',
	    'referral_from',
	    'virtual_account',
	    'is_enrolled',
	    'course_id',
	    'course_start_date',
	    'course_end_date',
	    'is_course_completed',
	    'course_completed_at',
        'is_confirm',
        'confirm_at',
        'is_payment_approved',
        'payment_approved_at',
        'payment_approved_by',
	    'is_payment_cancel',
	    'payment_cancel_at',
	    'payment_cancel_by',
	    'cancel_remark'
    ];
	
    protected $dates = ['course_start_date', 'course_end_date', 'confirm_at'];
    
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }

	public function workshop()
	{
		return $this->belongsTo('App\Models\ticmi\Workshop','workshop_id','id');
    }
	
	public function konfirmasi()
	{
		return $this->hasOne('App\Models\ticmi\Konfirmasi','invoice_no','invoice_no');
    }
	
	public function referral()
	{
		return $this->belongsTo('App\Models\ticmi\PesertaWorkshop','referral_from','id');
    }
	
	public function child_referral()
	{
		return $this->hasMany('App\Models\ticmi\PesertaWorkshop','referral_from','id');
    }
}
