<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Peraturan extends Model
{
    protected $table = 'cmeds_peraturan';

    protected $primaryKey = 'idPeraturan';

    public $timestamps = false;

    protected $fillable = [
        'date',
        'filename',
        'institution',
        'title',
        'description',
        'keyword',
        'createdDate',
        'createdBy',
        'updatedDate',
        'updatedBy',
        'folder',
        'isActive',
        'isDeleted',
        'docType',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('peraturanDeleted', function (Builder $builder){
            $builder->where('isDeleted',0);
        });
    }
}
