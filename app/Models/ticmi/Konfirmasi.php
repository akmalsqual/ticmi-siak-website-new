<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Konfirmasi extends Revisionable
{
    protected $table = 'cmeds_konfirmasi';

    protected $fillable = [
        'user_id',
        'keranjang_id',
        'invoice_no',
        'virtual_account',
        'nominal',
        'no_rekening',
        'nama_rekening',
        'tgl_transfer',
        'jam_transfer',
        'bukti_transfer',
        'jns_konfirmasi'
    ];
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','peserta_id');
    }

    public function keranjang()
    {
        return $this->belongsTo('App\Models\ticmi\Keranjang');
    }
	
	public function pesertaworkshop()
	{
		return $this->belongsTo('App\Models\ticmi\PesertaWorkshop','peserta_id','id');
    }
}
