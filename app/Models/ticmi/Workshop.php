<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\Revisionable;

class Workshop extends Model
{
	use SoftDeletes;
    protected $table = 'ticmi_workshop';
	
	protected $fillable = [
		't_program_id',
		'workshop_name',
		'sub_title',
	    'slugs',
	    'tgl_mulai',
	    'tgl_selesai',
	    'waktu',
	    'lokasi',
	    'ruangan',
	    'biaya',
	    'minimal_peserta',
	    'deskripsi',
	    'is_publish',
	    'is_hide',
	    'jml_peserta_diskon',
	    'jml_group_referral',
	    'persen_diskon',
	    'nominal_diskon',
	    'pesan_email_umum',
	    'pesan_email_khusus'
	];
	
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function peserta()
	{
		return $this->hasMany('App\Models\ticmi\PesertaWorkshop','workshop_id','id');
	}
	
	public function program()
	{
		return $this->belongsTo('App\Models\Program','t_program_id','program_id');
	}
	
	public function materi()
	{
		return $this->hasMany('\App\Models\ticmi\FileMateri','workshop_id','id');
	}
}
