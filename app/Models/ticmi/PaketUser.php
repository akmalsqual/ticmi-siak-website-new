<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\Revisionable;

class PaketUser extends Revisionable
{
	use SoftDeletes;
	// protected $connection = 'mysql';
    protected $table = 'cmeds_paket_user';
    
    protected $fillable = [
        'user_id',
        'cmeds_paket_data_id',
        'price',
        'biaya_admin',
        'diskon',
        'total',
        'virtual_account',
        'invoice_no',
        'due_date',
        'confirm_bank',
        'confirm_nominal',
        'confirm_rekening',
        'confirm_rekening_nama',
        'confirm_date',
        'confirm_time',
        'confirm_file',
        'payment_approve',
        'payment_approve_by',
        'valid_until',
        'is_active'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
//	public function user()
//	{
//		return $this->belongsTo('App\User');
//    }
	
    public function peserta()
    {
        return $this->belongsTo('App\Models\Peserta','user_id','peserta_id');
    }

	public function paket_data()
	{
		return $this->belongsTo('App\Models\ticmi\PaketData','cmeds_paket_data_id','id');
    }
	
	public function download_log()
	{
		return $this->hasMany('App\Models\ticmi\DownloadLog', 'paket_user_id','id');
    }
}
