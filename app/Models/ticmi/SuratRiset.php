<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Venturecraft\Revisionable\Revisionable;

class SuratRiset extends Revisionable
{
    protected $table = 'cmeds_researchapplication';

    protected $primaryKey = 'idResearchApplication';

    protected $fillable = [
        'paket_user_id',
        'invoiceNumber',
        'idCustomer',
        'namaMhs',
        'noIndukMhs',
        'judulTugasAkhir',
        'isApproved',
        'createdDate',
        'suratPengantar',
        'approvedBy',
        'approvedDate',
        'noSuratPengantar',
        'tglSuratPengantar',
        'universitas',
        'jenisTugasAkhir',
        'programStudi',
        'skripsi',
        'thesis',
        'countEdited',
        'noSurat',
        'remarks',
        'price',
        'approveSuperAdmin',
        'approveSuperBy',
        'approveSuperDate',
        'sendEmailDate',
        'isDeleted',
        'isConfirm',
        'confirmDate',
        'virtual_account',
        'confirmBank',
        'confirmNominal',
        'confirmRekening',
        'confirmRekeningNama',
        'confirmTime',
        'confirmFile',
        'paymentDueDate',
        'paymentApproved',
        'paymentApprovedBy',
        'paymentApprovedDate',
    ];
    
    protected $dates = ['createdDate','confirmDate'];

    public $timestamps = false;

    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('suratRisetDeleted', function (Builder $builder){
            $builder->where('cmeds_researchapplication.isDeleted',0);
        });
    }
	
	public function user()
	{
		return $this->belongsTo('App\Models\Peserta','idCustomer','peserta_id');
    }
	
	public function peserta()
	{
		return $this->belongsTo('App\Models\Peserta','idCustomer','peserta_id');
    }
	
	public function gettglSuratPengantarAttribute($value)
	{
		return date('Y-m-d', strtotime($value));
    }
}
