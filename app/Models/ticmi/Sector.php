<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Sector extends Revisionable
{
	protected $table = 'cmeds_sector';
	
	protected $primaryKey = 'idSector';
	
	public $timestamps = false;
	
	protected $fillable = [
		'idParentSector',
		'name',
		'description',
		'idSector',
		'idSubsector',
		'createdDate',
		'createdBy',
		'updatedDate',
		'updatedBy',
		'isActive',
		'isDeleted'
	];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function emitensector()
	{
		return $this->hasMany('App\Models\ticmi\EmitenToSector');
	}
	
	public function emiten()
	{
		return $this->belongsTo('App\Models\ticmi\Emiten','idSector','idSector');
	}
	
	public function subemiten()
	{
		return $this->belongsTo('App\Models\ticmi\Emiten','idSector','idSubsector');
	}
	
	public function emitendata()
	{
		return $this->belongsTo('App\Models\ticmi\EmitenData','idSector','idSector');
	}
}
