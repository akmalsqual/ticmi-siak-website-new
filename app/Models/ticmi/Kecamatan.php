<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Kecamatan extends Revisionable
{
    protected $table = 'ticmi_districts';
    
    protected $fillable = [
        'regency_id',
        'name'
    ];
	protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
	public function kabupaten()
	{
		return $this->belongsTo('\App\Models\ticmi\Kabupaten','regency_id','id');
    }
	
	public function kelurahan()
	{
		return $this->hasMany('\App\Models\ticmi\Kelurahan','district_id','id');
    }
}
