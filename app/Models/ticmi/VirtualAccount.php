<?php

namespace App\Models\ticmi;

use Illuminate\Database\Eloquent\Model;

class VirtualAccount extends Model
{
	protected $table      = 'virtual_account';
	public    $primaryKey = 'va_id';
	public    $timestamps = false;
	protected $fillable   = [
		'account_type',
		'account_code',
		'account_no',
		'account_status',
		'account_balance'
	];
}
