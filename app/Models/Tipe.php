<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipe extends Model
{
    protected $table 		= 'tipe_kelas';

	protected $fillable 	= [
		'nama',
	    'aktif'
	];

	public function kelas() {
		return $this->hasMany('App\Models\Kelas','tipe_kelas_id','tipe_kelas_id');
	}
}
