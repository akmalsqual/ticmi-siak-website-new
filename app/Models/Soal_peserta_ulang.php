<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Soal_peserta_ulang extends Revisionable
{
    protected $table   = 'soal_peserta_ulang';
    public $primaryKey = 'soal_peserta_ulang_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function ujian_modul()
    {
    	return $this->belongsTo('App\Models\Ujian_modul', 'ujian_modul_id', 'ujian_modul_id');
    }
}	
