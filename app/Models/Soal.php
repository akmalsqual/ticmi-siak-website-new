<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table   = 'soal';
    public $primaryKey = 'soal_id';
    public $timestamps = false;
}
