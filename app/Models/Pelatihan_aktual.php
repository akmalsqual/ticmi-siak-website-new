<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelatihan_aktual extends Model
{
    protected $table = 'pelatihan_aktual';
    public $primaryKey = 'pelatihan_aktual_id';
    public $timestamps = false;

    public function batch()
    {
    	return $this->belongsTo('App\Models\Batch', 'batch_id', 'batch_id');
    }
    
    // public function absensi_peserta()
    // {
    // 	return $this->hasMany('App\Models\Absensi_peserta','pelatihan_aktual_id','pelatihan_aktual_id');
    // }
}
