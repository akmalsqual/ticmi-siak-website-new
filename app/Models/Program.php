<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	// protected $connection = 'pgsql';
    protected $table   = 'program';
    public $primaryKey = 'program_id';
    public $timestamps = false;

	public function batch() {
		return $this->hasMany('App\Models\Batch','program_id','program_id');
	}

	public function batch_publish() {
		return $this->hasMany('App\Models\Batch','program_id','program_id')
					->where('aktif', TRUE)
            		->where('hapus', FALSE)
            		->where('is_publish', TRUE)
            		->where('is_hidden', FALSE)
            		->where('is_pelatihan', FALSE);
	}	
}
