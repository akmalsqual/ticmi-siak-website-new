<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PesertaWisuda extends Model
{
	protected $connection = 'mysql';
    protected $table = 'ticmi_peserta_wisuda';
    
    protected $fillable = [
        'nama',
        'email',
        'no_hp',
        'is_confirm',
        'is_hadir',
        'program',
        'kota_domisili',
        'no_kursi',
    ];
}
