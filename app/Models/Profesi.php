<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profesi extends Model
{
    //
    protected $table   = 'profesi';
    public $primaryKey = 'profesi_id';
    public $timestamps = false;
}
