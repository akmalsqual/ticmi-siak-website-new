<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Vcr_manag_prshn extends Revisionable
{
    protected $table                    = 'vcr_manag_prshn';
    public $primaryKey                  = 'vcr_manag_prshn_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
