<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Waktu_sisa_ulang extends Revisionable
{
    protected $table                    = 'waktu_sisa_ulang';
    public $primaryKey                  = 'waktu_sisa_ulang_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
