<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lembg_pdkn extends Model
{
    protected $table = 'lembg_pdkn';
    public $primaryKey = 'lembg_pdkn_id';
    public $timestamps = false;

    public function batch() {
		return $this->hasMany('App\Models\Batch','lembg_pdkn_id','lembg_pdkn_id');
	}
}
