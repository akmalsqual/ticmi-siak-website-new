<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rab_batch extends Model
{
    protected $table = 'rab_batch';
    public $primaryKey = 'rab_batch_id';
    public $timestamps = false;
}
