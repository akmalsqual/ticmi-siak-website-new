<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Peserta_jawab_ulang extends Revisionable
{
    protected $table   = 'peserta_jawab_ulang';
    public $primaryKey = 'peserta_jawab_ulang_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }

    public function soalUjian()
    {
    	return $this->belongsTo('App\Models\Soal', 'soal_id', 'soal_id');
    }
    
}
