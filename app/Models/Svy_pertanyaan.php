<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Svy_pertanyaan extends Model
{
    protected $table = 'svy_pertanyaan';
    public $primaryKey = 'svy_pertanyaan_id';

    public function svy_pilihan()
    {
    	return $this->hasMany('App\Models\Svy_pilihan','svy_pertanyaan_id','svy_pertanyaan_id');
    }
}
