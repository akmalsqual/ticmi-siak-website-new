<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Pendaftaran_trx extends Revisionable
{
    protected $table = 'pendaftaran_trx';
    public $primaryKey = 'pendaftaran_trx_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
