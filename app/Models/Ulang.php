<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ulang extends Model
{
    protected $table   = 'ulang';
    public $primaryKey = 'ulang_id';
    public $timestamps = false;
}
