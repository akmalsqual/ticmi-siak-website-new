<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Konfirmasi extends Revisionable
{
    protected $table                    = 'konfirmasi';
    public $primaryKey                  = 'konfirmasi_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
