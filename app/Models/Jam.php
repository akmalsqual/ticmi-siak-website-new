<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jam extends Model
{
    protected $table = 'jam';
    public $primaryKey = 'jam_id';
    public $timestamps = false;
}
