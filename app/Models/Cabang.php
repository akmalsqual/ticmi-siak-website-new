<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 'cabang';
    public $primaryKey = 'cabang_id';
    public $timestamps = false;

    public function lembg_pdkn()
    {
    	return $this->hasMany('App\Models\Lembg_pdkn', 'cabang_id', 'cabang_id');
    }
}
