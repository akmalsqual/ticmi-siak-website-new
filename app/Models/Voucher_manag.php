<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Voucher_manag extends Revisionable
{
    protected $table                    = 'voucher_manag';
    public $primaryKey                  = 'voucher_manag_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
