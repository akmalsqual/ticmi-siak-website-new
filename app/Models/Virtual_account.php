<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Virtual_Account extends Revisionable
{
    protected $table   = 'virtual_account';
    public $primaryKey = 'va_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
	
	protected $fillable = [
		'account_type',
		'account_code',
		'account_no',
		'account_status',
		'account_balance'
	];

//    public static function boot()
//    {
//        parent::boot();
//    }
}
