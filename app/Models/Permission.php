<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	protected $fillable = [
		'name',
		'display_name',
		'description'
	];
	
	protected $table = 'frontend_permissisons';

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
		return 'name';
	}
	
	/**
	 * @param $roleName
	 *
	 * @return bool
	 */
	public function hasRole($roleName)
	{
		foreach($this->roles as $role)
		{
			if($role->name == $roleName)
			{
				return true;
			}
		}
		return false;
	}
}
