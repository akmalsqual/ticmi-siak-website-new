<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Batch extends Revisionable
{
    protected $table   = 'batch';
    public $primaryKey = 'batch_id';
    public $timestamps = false;
    
    protected $dates   = ['tgl_mulai'];
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function waktuUjian()
    {
    	return $this->belongsTo('App\Models\Ujian_parameter', 'ujian_parameter_id', 'ujian_parameter_id');
    }

    public function program() {
		return $this->belongsTo('App\Models\Program','program_id','program_id');
	}

    public function kelas() {
		return $this->belongsTo('App\Models\Kelas','kelas_id','kelas_id');
	}

	public function lembaga() {
		return $this->belongsTo('App\Models\Lembg_pdkn','lembg_pdkn_id','lembg_pdkn_id');
	}

    public function rab_batch() {
        return $this->hasOne('App\Models\Rab_batch','batch_id','batch_id');
    }

    public function lokasi(){
        return $this->hasOne('App\Models\Lokasi','lokasi_id','lokasi_id');
    }

    public function cabang(){
        return $this->hasOne('App\Models\Cabang','cabang_id','cabang_id');
    }
}
