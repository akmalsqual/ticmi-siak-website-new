<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Ulang_trx extends Revisionable
{
    protected $table    = 'ulang_trx';
    public $primaryKey  = 'ulang_trx_id';
    public $timestamps  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}