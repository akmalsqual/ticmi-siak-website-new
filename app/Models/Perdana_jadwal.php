<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Perdana_jadwal extends Revisionable
{
    protected $table = 'perdana_jadwal';
    public $primaryKey = 'perdana_jadwal_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function perdana()
    {
    	return $this->belongsTo('App\Models\Perdana', 'perdana_id', 'perdana_id');
    }

    public function jam()
    {
    	return $this->belongsTo('App\Models\Jam', 'jam_id', 'jam_id');
    }
}
