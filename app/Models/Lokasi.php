<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'lokasi';
    public $primaryKey = 'lokasi_id';
    public $timestamps = false;

    public function cabang()
    {
    	return $this->hasMany('App\Models\Cabang', 'lokasi_id', 'lokasi_id');
    }
}
