<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lembaga extends Model
{
    protected $table = 'lembaga';
    public $primaryKey = 'lembaga_id';
    public $timestamps = false;
}
