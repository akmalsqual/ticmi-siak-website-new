<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jenis_kelamin extends Model
{
    protected $table = 'sex';
    public $primaryKey = 'sex_id';
    public $timestamps = false;
}
