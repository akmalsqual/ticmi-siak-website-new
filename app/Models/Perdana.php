<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Perdana extends Revisionable
{
    protected $table = 'perdana';
    public $primaryKey = 'perdana_id';
    public $timestamps = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function ruang()
    {
    	return $this->belongsTo('App\Models\Ruang', 'ruang_id', 'ruang_id');
    }
}
