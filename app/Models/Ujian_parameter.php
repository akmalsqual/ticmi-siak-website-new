<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujian_parameter extends Model
{
    protected $table = 'ujian_parameter';
    public $primaryKey = 'ujian_parameter_id';
    public $timestamps = false;
}
