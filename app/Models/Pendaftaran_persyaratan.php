<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Pendaftaran_persyaratan extends Revisionable
{
    protected $table                    = 'pendaftaran_persyaratan';
    public $primaryKey                  = 'pendaftaran_persyaratan_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
