<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Perdana_trx extends Revisionable
{
    protected $table    = 'perdana_trx';
    public $primaryKey  = 'perdana_trx_id';
    public $timestamps  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}