<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ulang_jadwal extends Model
{
    protected $table = 'ulang_jadwal';
    public $primaryKey = 'ulang_jadwal_id';
    public $timestamps = false;

    public function hari()
    {
    	return $this->belongsTo('App\Models\Hari', 'hari_id', 'hari_id');
    }

    public function jam()
    {
    	return $this->belongsTo('App\Models\Jam', 'jam_id', 'jam_id');
    }

    public function ulang()
    {
        return $this->belongsTo('App\Models\Ulang', 'ulang_id', 'ulang_id');
    }
}
