<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    protected $table = 'negara';
    public $primaryKey = 'negara_id';
    public $timestamps = false;
}
