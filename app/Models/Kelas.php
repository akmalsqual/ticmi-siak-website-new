<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table 		= 'kelas';

	protected $fillable 	= [
		'kelas_id',
	    'kode',
	    'nama',
	    'nick',
	    'tipe_kelas_id',
	    'keterangan',
	    'aktif',
	    // 'cabang'
	];

	public function type() {
		return $this->belongsTo('App\Models\Tipe','tipe_kelas_id','tipe_kelas_id');
	}

	public function batch() {
		return $this->hasMany('App\Models\Batch','kelas_id','kelas_id');
	}
}
