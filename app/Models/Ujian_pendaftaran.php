<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Ujian_pendaftaran extends Revisionable
{
    protected $table                    = 'ujian_pendaftaran';
    public $primaryKey                  = 'ujian_pendaftaran_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
