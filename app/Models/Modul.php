<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    protected $table = 'modul';
    public $primaryKey = 'modul_id';
    public $timestamps = false;
}
