<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\Revisionable;

class Voucher_dump extends Revisionable
{
    protected $table                    = 'voucher_dump';
    public $primaryKey                  = 'voucher_dump_id';
    public $timestamps                  = false;
    protected $revisionCreationsEnabled = true;
    
    public static function boot()
    {
        parent::boot();
    }
}
