<?php
/**
 * Created by PhpStorm.
 * User: akmalbashan
 * Date: 22-Jul-16
 * Time: 3:27 PM
 */

namespace App\Http\Helpers;

use App\Model\CMPDP;
use App\Model\Kategori;

/**
 * Class GeneralHelper
 * @package App\Http\Helper
 */
class GeneralHelper
{
	
	public static function getKategori($id)
	{
		$katId    = explode(",", $id);
		$kategori = Kategori::whereIn('id', $katId)->where('ajar', 'y')->get();
		
		return $kategori;
	}
	
	public static function getJenisData($type, $doctype)
	{
		$jenis = "";
		if ($doctype == "ED") {
			if ($type == "ir") {
				$jenis = "Interim Report";
			} elseif ($type == 'fr') {
				$jenis = "Financial Report";
			} elseif ($type == 'pr') {
				$jenis = "Prospektus";
			} elseif ($type == 'ar') {
				$jenis = "Annual Report";
			} else {
				$jenis = "Unknown Data Type";
			}
		} elseif ($doctype == 'FH') {
			$jenis = "Financial Highlights";
		} elseif ($doctype == 'CA') {
			if ($type == "dv") {
				$jenis = "Corporate Action Dividen";
			} elseif ($type == 'ss') {
				$jenis = "Corporate Action Stock Split";
			} elseif ($type == 'ri') {
				$jenis = "Corporate Action Right Issue";
			} elseif ($type == 'ma') {
				$jenis = "Corporate Action Merger & Aquisition";
			} elseif ($type == 'li') {
				$jenis = "Corporate Action List of IPO";
			} elseif ($type == 'ld') {
				$jenis = "Corporate Action Lis of Delisting";
			}
		} elseif ($doctype == "TP") {
			if ($type == 'ps') {
				$jenis = "Transaksi Perdagangan Saham";
			} elseif ($type == 'pi') {
				$jenis = "Transaksi Perdagangan Indeks";
			} elseif ($type == 'ab') {
				$jenis = "Transaksi Perdagangan Anggota Bursa";
			} elseif ($type == 'ob') {
				$jenis = "Transaksi Perdagangan Obligasi";
			} elseif ($type == 'is') {
				$jenis = "Transaksi Perdagangan Indeks Sektoral";
			} elseif ($type == 'pl') {
				$jenis = "Transaksi Perdagangan Indeks LQ45";
			}
		} elseif ($doctype == 'AN') {
			$jenis = "Pengumuman";
		} elseif ($doctype == 'RR') {
			$jenis = "Peraturan";
		}
		
		return $jenis;
	}
	
	public static function getPriceList($doctype, $type)
	{
		$price = 0;
		if ($doctype == "ED") {
			if ($type == "ir") {
				$price = 40000;
			} elseif ($type == 'fr') {
				$price = 40000;
			} elseif ($type == 'pr') {
				$price = 130000;
			} elseif ($type == 'ar') {
				$price = 65000;
			}
		} elseif ($doctype == 'FH') {
			$price = 40000;
		} elseif ($doctype == 'CA') {
			if ($type == "dv") {
				$price = 40000;
			} elseif ($type == 'ss') {
				$price = 40000;
			} elseif ($type == 'ri') {
				$price = 40000;
			} elseif ($type == 'ma') {
				$price = 40000;
			} elseif ($type == 'li') {
				$price = 40000;
			} elseif ($type == 'ld') {
				$price = 40000;
			}
		} elseif ($doctype == "TP") {
			if ($type == 'ps') {
				$price = 5000;
			} elseif ($type == 'pi') {
				$price = 5000;
			} elseif ($type == 'ab') {
				$price = 40000;
			} elseif ($type == 'ob') {
				$price = 5000;
			} elseif ($type == 'is') {
				$price = 40000;
			} elseif ($type == 'pl') {
				$price = 40000;
			}
		} elseif ($doctype == 'AN' || $doctype == 'RR') {
			$price = 8000;
		}
		
		return $price;
	}
	
	public static function base_url($path = "")
	{
		if ($path) {
			return config('app.url') . "/" . $path;
		}
		return config('app.url');
	}
	
	public static function getFileName($docType, $type, $kodeEmiten, $title, $year, $quarter, $month, $institution)
	{
		$filename = '';
		if ($docType == 'ED') {
			$filename = strtolower($kodeEmiten . '-' . $year . '-' . $type . '-' . $quarter . '-' . $month . '.pdf');
			$filename = str_replace(' ', '-', $filename);
		} else if ($docType == 'FH') {
			$filename = strtolower($kodeEmiten . '-' . $year . '-' . $type . '-' . $quarter . '-' . $month . '.pdf');
			$filename = str_replace(' ', '-', $filename);
		} else if ($docType == 'RR') {
			$filename = strtolower($institution . '-' . $title . '-' . $year . '.pdf');
			$filename = str_replace(' ', '-', $filename);
		} else if ($docType == 'AN') {
			$filename = strtolower($title . '-' . $year . '.pdf');
			$filename = str_replace(' ', '-', $filename);
		} elseif ($docType == "CA") {
			$filename = strtolower($title . '-' . $year);
			$filename = str_replace(' ', '-', $filename);
		} elseif ($docType == "TP") {
			if ($kodeEmiten) {
				$filename = strtoupper($kodeEmiten) . strtolower("-" . $year . '-' . $title);
			} else {
				$filename = strtolower("-" . $year . '-' . $title);
			}
			$filename = str_replace(' ', '-', $filename);
		}
		
		$filename = str_replace(':', '-', $filename);
		$filename = str_replace('%', '-', $filename);
		$filename = str_replace('(', '', $filename);
		$filename = str_replace(')', '', $filename);
		return $filename;
	}
	
	public static function getPdfPath($emitendata)
	{
//		$emitendata = EmitenData::find($idEmitenData);
		if ($emitendata->docType == 'AN' || $emitendata->docType == 'RR') {
			$year = date('Y', strtotime($emitendata->date));
		} else {
			$year = $emitendata->year;
		}
		$type = $emitendata->type;
		$path = "";
		if ($emitendata->docType == "ED") {
			$path = "/uploads/NEW-CMEDS/pdf/emiten-data/" . $year . '/' . $type . '/';
		} elseif ($emitendata->docType == "FH") {
			$path = "/uploads/NEW-CMEDS/pdf/financial-highlights/$year/";
		} elseif ($emitendata->docType == "RR") {
			$path = "/uploads/NEW-CMEDS/pdf/rules-regulations/$year/";
		} elseif ($emitendata->docType == "AN") {
			$year = date('Y', strtotime($emitendata->date));
			$path = "/uploads/NEW-CMEDS/pdf/announcements/$year/";
		} elseif ($emitendata->docType == "CA") {
			$path = "/uploads/NEW-CMEDS/pdf/corporate-action/$year/";
		} elseif ($emitendata->docType == "TP") {
			$path = "/uploads/NEW-CMEDS/pdf/transaksi-perdagangan/$year/$type/";
		}
		
		return $path . $emitendata->filename;
	}
	
	public static function getUploadPath($doctype, $year, $type)
	{
		$path = null;
		if ($doctype == 'ED') {
			$path = public_path('uploads/NEW-CMEDS/pdf/emiten-data/' . $year . '/' . $type);
		} elseif ($doctype == 'FH') {
			$path = public_path('uploads/NEW-CMEDS/pdf/financial-highlights/' . $year);
		} elseif ($doctype == 'RR') {
			$path = public_path('uploads/NEW-CMEDS/pdf/rules-regulations/' . $year);
		} elseif ($doctype == 'AN') {
			$path = public_path('uploads/NEW-CMEDS/pdf/announcements/' . $year);
		} elseif ($doctype == 'CA') {
			$path = public_path('uploads/NEW-CMEDS/pdf/corporate-action/' . $year);
		} elseif ($doctype == 'TP') {
			$path = public_path('uploads/NEW-CMEDS/pdf/transaksi-perdagangan/' . $year . '/' . $type);
		}
		return $path;
	}
	
	public static function getRemotePath($doctype, $year, $type)
	{
		$path = null;
		if ($doctype == 'ED') {
			$path = "uploads/NEW-CMEDS/pdf/emiten-data/" . $year . '/' . $type . '/';
		} elseif ($doctype == 'FH') {
			$path = "uploads/NEW-CMEDS/pdf/financial-highlights/$year/";
		} elseif ($doctype == 'RR') {
			$path = "uploads/NEW-CMEDS/pdf/rules-regulations/$year/";
		} elseif ($doctype == 'AN') {
			$path = "uploads/NEW-CMEDS/pdf/announcements/$year/";
		} elseif ($doctype == 'CA') {
			$path = "uploads/NEW-CMEDS/pdf/corporate-action/$year/";
		} elseif ($doctype == 'TP') {
			$path = "uploads/NEW-CMEDS/pdf/transaksi-perdagangan/$year/$type/";
		}
		return $path;
	}
	
	public static function getPdfUrl($doctype, $type, $year)
	{
//		$path = 'http://p.icamel.id/';
		$path = '/data/pdf/';
		if ($doctype == 'ED') {
			$path .= "emiten-data/" . $year . '/' . $type . '/';
		} elseif ($doctype == 'FH') {
			$path .= "financial-highlights/$year/";
		} elseif ($doctype == 'RR') {
			$path .= "rules-regulations/$year/";
		} elseif ($doctype == 'AN') {
			$path .= "announcements/$year/";
		} elseif ($doctype == 'CA') {
			$path .= "corporate-action/$year/";
		} elseif ($doctype == 'TP') {
			$path .= "transaksi-perdagangan/$year/$type/";
		}
		return $path;
	}
	
	public static function getThumbPath($doctype, $type, $year, $filename)
	{
		$extractedFilename = explode('.', $filename);
		$fileExt           = end($extractedFilename);
		$key               = array_search($fileExt, $extractedFilename);
		unset($extractedFilename[$key]);
		$filename = implode('', $extractedFilename);
		$path     = '/data/pdf/';
		if ($doctype == 'ED') {
			$path .= "emiten-data/" . $year . '/' . $type . '/';
		} elseif ($doctype == 'FH') {
			$path .= "financial-highlights/$year/";
		} elseif ($doctype == 'RR') {
			$path .= "rules-regulations/$year/";
		} elseif ($doctype == 'AN') {
			$path .= "announcements/$year/";
		} elseif ($doctype == 'CA') {
			$path .= "corporate-action/$year/";
		} elseif ($doctype == 'TP') {
			$path .= "transaksi-perdagangan/$year/$type/";
		}
		
		if (file_exists(public_path($path . 'thumb/thumb_' . $filename . '.jpg'))) {
			return $path . 'thumb/thumb_' . $filename . '.jpg';
		} else {
			return "/assets/images/default/pdf-not-found.jpg";
		}
	}
	
	public static function countTotalPage($page)
	{
		$page  = str_replace(' ', '', $page);
		$num   = 0;
		$aPage = explode(',', $page);
		if (count($aPage) > 0) {
			foreach ($aPage as $key => $item) {
				$num++;
				if (strpos($item, '-') !== false) {
					$num--;
					$sPage = explode('-', $item);
					$num   += (abs(($sPage[0] - $sPage[1])) + 1);
				}
			}
		}
		return $num;
	}
	
	public static function waktuPelaksanaan($dateStart, $dateEnd)
	{
		setlocale(LC_TIME, 'id_ID.UTF-8');
		$dateReturn = "";
		if ($dateStart == $dateEnd) {
			$dateReturn = strftime('%A, %#d %B %Y', strtotime($dateStart));
		} elseif (date('mY', strtotime($dateStart)) == date('mY', strtotime($dateEnd))) {
			$dateReturn = date('d', strtotime($dateStart)) . ' - ' . date('d F Y', strtotime($dateEnd));
		} else {
			$dateReturn = date('d F Y', strtotime($dateStart)) . ' - ' . date('d F Y', strtotime($dateEnd));
		}
		return $dateReturn;
	}
	
	public static function checkJadwalKegiatan($jadwalKegiatan, $program = null, $type = null, $bulan = null, $tahun = null, $ppl_type = null)
	{
		if ($jadwalKegiatan) {
			if (is_array($program)) {
				$filteredKegiatan = $jadwalKegiatan->whereIn('t_program_id', $program);
			} else {
				$filteredKegiatan = $jadwalKegiatan->where('t_program_id', $program);
			}
			
			if ($type) {
				$filteredKegiatan = $filteredKegiatan->where('t_type_id', $type);
			}
			if ($ppl_type) {
				$filteredKegiatan = $filteredKegiatan->where('ppl_type',$ppl_type);
			}
			
			if ($filteredKegiatan->count() > 0) {
				$cek = false; $tgl = ""; $no = 1;
				foreach ($filteredKegiatan as $item) {
					if (date('Y-m', strtotime($item->tgl_dari)) == $tahun.'-'.$bulan ) {
						$cek = true; $date_connector = '-'; $break = '';
						if ($item->tgl_type == 'dan') $date_connector = '&';
						if ($no > 1) $tgl .= '<br/>';
						if (date('Y-m-d', strtotime($item->tgl_dari)) < date('Y-m-d')) {
							if ($item->tgl_dari == $item->tgl_sampai) {
								$tgl .= "<span class='label label-danger showtooltip' title='".$item->nama_kegiatan."'>".date('d',strtotime($item->tgl_dari))."</span>";
							} else {
								$tgl .= "<span class='label label-danger showtooltip' title='".$item->nama_kegiatan."'>".date('d',strtotime($item->tgl_dari)).$date_connector.date('d',strtotime($item->tgl_sampai))."</span>";
							}
						} else {
							if ($item->tgl_dari == $item->tgl_sampai) {
								$tgl .= "<a href='".route('jadwal-kegiatan-view',['jadwalkegiatan'=>$item])."' title='".$item->nama_kegiatan."' class='modalIframe showtooltip' data-frame-height='300'><span class='label label-info'>".date('d',strtotime($item->tgl_dari))."</span></a>";
							} else {
								$tgl .= "<a href='".route('jadwal-kegiatan-view',['jadwalkegiatan'=>$item])."' title='".$item->nama_kegiatan."' class='modalIframe showtooltip' data-frame-height='300'><span class='label label-info'>".date('d',strtotime($item->tgl_dari)).$date_connector.date('d',strtotime($item->tgl_sampai))."</span></a>";
							}
						}
						$no++;
					}
				}
				
				if ($cek) {
					return $tgl;
				} else {
					return "<span class='badge'>x</span>";
				}
				//->where('tgl_dari','LIKE','"'.$tahun.'-'.$bulan.'%"')
			} else {
				return "<span class='badge'>x</span>";
			}
		} else {
			return "<span class='badge'>x</span>";
		}
	}
	
	/**
	 * Get either a Gravatar URL or complete image tag for a specified email address.
	 *
	 * @param string $email The email address
	 * @param string $s     Size in pixels, defaults to 80px [ 1 - 2048 ]
	 * @param string $d     Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
	 * @param string $r     Maximum rating (inclusive) [ g | pg | r | x ]
	 * @param boole $img    True to return a complete IMG tag False for just the URL
	 * @param array $atts   Optional, additional key/value attributes to include in the IMG tag
	 * @return String containing either just a URL or a complete image tag
	 * @source https://gravatar.com/site/implement/images/php/
	 */
	public static function getGravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = [])
	{
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5(strtolower(trim($email)));
		$url .= "?s=$s&d=$d&r=$r";
		if ($img) {
			$url = '<img src="' . $url . '"';
			foreach ($atts as $key => $val)
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}
	
	/**
	 * Cmpdp
	 */
	public static function getNoPendaftaranCMPDP()
	{
		$cmpdp  = CMPDP::max('id');
		$result = "2017";
		
		if ($cmpdp) {
			$nextId = $cmpdp + 1;
			$newId = str_pad($nextId, 5, "0", STR_PAD_LEFT);
			$result .= $newId;
		} else {
			$result .= "00001";
		}
		return $result;
	}
}