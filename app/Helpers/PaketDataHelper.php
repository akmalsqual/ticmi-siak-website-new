<?php

namespace App\Helpers;


use App\Models\ticmi\PaketUser;
use App\Models\Virtual_account;

class PaketDataHelper
{
    public static function getInvoiceNo()
    {
        $maxId = PaketUser::whereYear('created_at', [date('Y')])->max('id');
        if ($maxId) {
            $nextId = $maxId + 1;
            $newId  = str_pad($nextId, 4, "0", STR_PAD_LEFT);
            $newId  = date("ymd") . $newId;
            return "PD".$newId;
        } else {
            return "PD".date('ymd') . "0001";
        }
    }
    
    public static function getVirtualAccount($code='001')
    {
        $vc = Virtual_account::where('account_code',strval($code))->where('account_status',0)->orderBy('va_id')->first();
        if ($vc && $vc->count() > 0) {
            $no_vc = $vc->account_no;
            $vc->update(['account_status'=>1]);
        } else {
            $no_vc = '4581349992';
        }
        return $no_vc;
    }
}