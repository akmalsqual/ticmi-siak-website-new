<?php
/**
 * User: akmalbashan
 * Date: 29-Nov-17
 * Time: 9:55 AM
 */

namespace App\Http\Helpers;


use App\Model\MoodleLog;
use App\Model\PesertaWorkshop;
use App\Model\Workshop;
use App\User;
use GuzzleHttp\Client;

class MoodleHelper
{
	public static function connect($method = 'POST', $function, $params)
	{
		$url = 'https://elearning.ticmi.co.id/webservice/rest/server.php';
		$token = '92d9ef302e08ea55bdb8f73c6d56d53c';


		$client = new Client();
		$result = $client->request($method,$url,[
			'query' => [
				'wstoken' => $token,
				'wsfunction' => $function,
				'moodlewsrestformat' => 'json'
			],
			'headers' => [
				'Content-Type' => 'application/x-www-form-urlencoded',
			],
			'form_params' => $params
		]);

		return self::checkResult($result->getBody()->getContents());
	}

	public static function getCourse($id=0)
	{
		if (!empty($id)) {
			$res = self::connect('POST', 'core_course_get_courses', ['options'=>['ids'=>[$id]]]);
		} else {
			$res = self::connect('POST', 'core_course_get_courses', []);
		}
		return $res;
	}

	public static function checkResult($result)
	{
//		$returnVal = $result->getBody()->getContents();
		$response = json_decode($result,true);
		if (isset($response['exception']) && !empty($response['exception'])) {

			$moodleLog = new MoodleLog();
			$moodleLog->user_id   = auth()->user()->id;
			$moodleLog->exception = $response['exception'];
			$moodleLog->errorcode = $response['errorcode'];
			$moodleLog->message   = $response['message'];
			$moodleLog->debuginfo = $response['debuginfo'];
			$moodleLog->save();

			return false;
		} else {
			return $response;
		}
	}

	public static function sendUserToMoodle(User $user)
	{
		if ($user->moodle_user_id != 0) {
			/** Jika user sudah pernah diinput ke moodle maka update passwordnya saja */
			$res = \DB::connection('mysql_elearning')->table('mdl_user')->where('id',$user->moodle_user_id)->update(['password'=>$user->password]);
		} else {
			/**
			 * Cek apakah user sudah ada di moodle
			 */
			$searchParam = ['field'=>'email','values'=>[strtolower(trim($user>email))]];
			$cekUser = self::connect('POST', 'core_user_get_users_by_field', $searchParam);
			if ($cekUser !== false) {
				/** User sudah ada di database moodle maka ambil id dan update table user */
				$user->update([
					'moodle_user_id' => $cekUser[0]->id
				]);
				/** Update password di moodle user */
				$res = \DB::connection('mysql_elearning')->table('mdl_user')->where('id',$user->moodle_user_id)->update(['password'=>$user->password]);
			} else {
				/**
				 * Create user moodle baru
				 */
				$userMoodle = new \stdClass();

				$arrName = explode(' ',$user->name);
				$firstName = $user->name;
				$lastName = $user->name;
				if (count($arrName)  == 1) {
					$firstName = $user->name;
					$lastName = $user->name;
				} elseif (count($arrName) == 2) {
					$firstName = $arrName[0];
					$lastName = $arrName[1];
				} elseif (count($arrName) == 3) {
					$firstName = $arrName[0].' '.$arrName[1];
					$lastName = $arrName[2];
				} elseif (count($arrName) >= 4) {
					$firstName = $arrName[0].' '.$arrName[1];
					unset($arrName[0]); unset($arrName[1]);
					$name = implode(' ',$arrName);
					$lastName = $name;
				}


				$userMoodle->username = strtolower(trim($user->email));
				$userMoodle->password = '123456';
				$userMoodle->firstname = $firstName;
				$userMoodle->lastname = $lastName;
				$userMoodle->email = strtolower(trim($user->email));
				$userMoodle->auth = 'manual';
				$userMoodle->idnumber = '';
				$userMoodle->lang = 'en';
				$userMoodle->theme = '';
				$userMoodle->timezone = '99';
				$userMoodle->mailformat = 1;
				$userMoodle->description = $user->name;
				$userMoodle->city = 'Jakarta';
				$userMoodle->country = 'ID';
				$users = array((array) $userMoodle);
				$params = array('users' => $users);

				$response = self::connect('POST', 'core_user_create_users', $params);

				if ($response) {
					foreach ($response as $item) {
						$moodle_user_id = $item['id'];
						$moodle_username =	$item['username'];
						/** Update password di tabel user di moodle database */
						if ($user->update(['moodle_user_id'=>$moodle_user_id])) {
							$res = \DB::connection('mysql_elearning')->table('mdl_user')->where('id',$moodle_user_id)->update(['password'=>$user->password]);
						}
					}
				}
			}
		}

	}

	public static function enrollUserToMoodle(User $user, PesertaWorkshop $pesertaWorkshop)
	{
		if ($user->moodle_user_id) {
			$workshop  = $pesertaWorkshop->workshop;
			$enrolment = [];

			$day = $workshop->lama_belajar;

			$course_start_date = strtotime('+10 minute');
			$course_end_date   = strtotime("+{$day} day +10 minute");

			$enrolment['roleid']    = 5;
			$enrolment['userid']    = $user->moodle_user_id;
			$enrolment['courseid']  = $workshop->course_id;
			$enrolment['timestart'] = strtotime(date('Y-m-d'));
			$enrolment['timeend']   = strtotime('2017-12-12 23:59:59');
			$enrolment['suspend']   = 0;

			$enrolments = [$enrolment];
			$params     = ['enrolments' => $enrolments];

			$response = self::connect('POST','enrol_manual_enrol_users', $params);
			if ($response !== false) {
				$update = [
					'is_enrolled'       => 1,
					'course_id'         => $workshop->course_id,
					'course_start_date' => date('Y-m-d H:i:s', $course_start_date),
					'course_end_date'   => date('Y-m-d H:i:s', $course_end_date)
				];
				$pesertaWorkshop->update($update);
			}
		}
	}
}