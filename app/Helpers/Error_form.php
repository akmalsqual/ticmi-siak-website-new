<?php
namespace App\Helpers;
use Session;
class Error_form {
	
	public static function errorColumn($has){
		$class_name = 'form-control';
		if($has != false){
			$class_name = 'form-control error-column'; 
		}
		return $class_name;
	}


	public static function errorLabel($has, $name){
		$class_name = '';
		if($has != false){
			$class_name = '<div class="error">'. $name .'</div>'; 
		}
		return $class_name;
	}

	public static function setAlert($messageName, $messageValue, $className, $classValue){
		Session::flash($messageName, $messageValue); 
		Session::flash($className, $classValue); 
	}
}

?>
