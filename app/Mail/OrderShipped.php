<?php

namespace App\Mail;

use App\Models\ticmi\PaketUser;
// use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;
    public $paketUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PaketUser $paketUser)
    {
        $this->paketUser = $paketUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $paketUser = $this->$paketUser;
        return $this->view('email-template.email-konfirmasi-paket-data');
    }
}
