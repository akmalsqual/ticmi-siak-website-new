<?php

namespace App\Providers;

use App\Models\ticmi\EmitenData;
use App\Models\ticmi\Keranjang;
use App\Models\ticmi\PaketData;
use App\Models\ticmi\PaketUser;
use App\Models\ticmi\PesertaWorkshop;
use App\Models\ticmi\SuratRiset;
use App\Models\ticmi\Workshop;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
	
	    Route::model('emiten', 'App\Models\ticmi\Emiten', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('emitendata', 'App\Models\ticmi\EmitenData', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('keranjang', 'App\Models\ticmi\Keranjang', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('invoice', 'App\Models\ticmi\Keranjang', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::bind('invoice', function ($invoice){
		    $invoice = Keranjang::where('invoice_no',$invoice)->first();
		    if ($invoice && $invoice->count() > 0) {
			    return $invoice;
		    } else abort(404);
	    });
	
	    Route::model('pengumuman', 'App\Models\ticmi\Peraturan', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('peraturan', 'App\Models\ticmi\Peraturan', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('suratriset', 'App\Models\ticmi\SuratRiset', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::bind('emitendataencrypt', function ($emitendataencrypt){
		    $idemitendata = decrypt($emitendataencrypt);
		    $emitendata = EmitenData::find($idemitendata);
		    if ($emitendata && $emitendata->count() > 0) {
			    return $emitendata;
		    } else abort(404);
	    });
	
	    Route::bind('pengumumanencrypt', function ($pengumumanencrypt){
		    $idpengumuman = decrypt($pengumumanencrypt);
		    $pengumuman = EmitenData::find($idpengumuman);
		    if ($pengumuman && $pengumuman->count() > 0) {
			    return $pengumuman;
		    } else abort(404);
	    });
	
	    Route::bind('peraturanencrypt', function ($peraturanencrypt){
		    $idperaturan = decrypt($peraturanencrypt);
		    $peraturan = EmitenData::find($idperaturan);
		    if ($peraturan && $peraturan->count() > 0) {
			    return $peraturan;
		    } else abort(404);
	    });
	
	    Route::bind('invoicesuratriset', function ($invoicesuratriset){
		    $suratRiset = SuratRiset::where('invoiceNumber',strval($invoicesuratriset))->first();
		    if ($suratRiset && $suratRiset->count() > 0) {
			    return $suratRiset;
		    } else abort(404);
	    });
	
	    Route::model('pesertaworkshop', 'App\Models\ticmi\PesertaWorkshop', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::bind('workshopslug', function ($workshopslug){
		    $workshop = Workshop::whereSlugs($workshopslug)->first();
		    if ($workshop && $workshop->count() > 0) {
			    return $workshop;
		    } else abort(404);
	    });
	
	    Route::bind('paketdatacode', function ($paketdatacode) {
		    $paketData = PaketData::where('slug',$paketdatacode)->first();
		    if ($paketData && $paketData->count() > 0) {
			    return $paketData;
		    } else abort(404);
	    });
	
	    Route::model('paketuser','App\Models\ticmi\PaketUser', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::model('jadwalkegiatan','App\Models\ticmi\JadwalKegiatan', function () {
		    throw new NotFoundHttpException;
	    });
	
	    Route::bind('paketdatainvoice', function ($paketdatainvoice) {
		    $paketdatainvoice = PaketUser::where('invoice_no',$paketdatainvoice)->first();
		    if ($paketdatainvoice && $paketdatainvoice->count() > 0) {
			    return $paketdatainvoice;
		    } else abort(404);
	    });
	
	    Route::bind('pesertaworkshopinvoice', function ($pesertaworkshopinvoice){
		    $pesertaWorkshop = PesertaWorkshop::where('invoice_no',$pesertaworkshopinvoice)->first();
		    if ($pesertaWorkshop && $pesertaWorkshop->count() > 0) {
			    return $pesertaWorkshop;
		    } else abort(404);
	    });
	
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
