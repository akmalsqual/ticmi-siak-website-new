<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    
    public $table                       = 'peserta';
    public $primaryKey                  = 'peserta_id';
    public $timestamps                  = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'moodle_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getProfesi()
    {
        return $this->belongsTo('App\Models\Profesi', 'profesi_id', 'profesi_id');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile_peserta', 'peserta_id', 'peserta_id');
    }
    
    /* function dari ticmi */
    public function keranjang()
    {
        return $this->hasMany('App\Models\ticmi\Keranjang');
    }
    
    public function keranjang_detail()
    {
        return $this->hasMany('App\Models\ticmi\KeranjangDetail');
    }
    
    public function konfirmasi()
    {
        return $this->hasMany('App\Models\ticmi\Konfirmasi');
    }
    
    public function suratriset()
    {
        return $this->hasMany('App\Models\ticmi\SuratRiset');
    }
    
    public function user_wingamers()
    {
        return $this->hasOne('App\Models\ticmi\UsersWinGamers');
    }
    
    public function paket_data()
    {
        return $this->hasMany('App\Models\ticmi\PaketUser','user_id','peserta_id');
    }
    
    public function provinsi()
    {
        return $this->belongsTo('\App\Models\ticmi\Provinsi', 'provinsi_id', 'id');
    }
}

