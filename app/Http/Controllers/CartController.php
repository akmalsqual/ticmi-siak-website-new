<?php
/**
 * Created by PhpStorm.
 * User: akmalbashan
 * Date: 22-Jul-16
 * Time: 3:27 PM
 */

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Http\Helpers\GeneralHelper;
use App\Http\Requests\KonfirmasiRequest;
use App\Jobs\SendEmailKonfirmasiPembayaran;
use App\Jobs\SendEmailTagihanPembelianData;
use App\Models\ticmi\EmitenData;
use App\Models\ticmi\Keranjang;
use App\Models\ticmi\KeranjangDetail;
use App\Models\ticmi\Konfirmasi;
use App\Models\ticmi\Peraturan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CartController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function viewcart()
	{
		return view('ticmi.cart.view');
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function addItem(Request $request)
	{
		$doctype      = $request->get('doctype');
		$type         = $request->get('type');
		$idEmitenData = $request->get('idEmitenData');
		$title        = $request->get('title');
		$year         = $request->get('year');
		$price        = GeneralHelper::getPriceList($doctype, $type);
		$idCart       = $doctype . '-' . $idEmitenData;
		
		$filename = '';
		if ($doctype == 'ED' || $doctype == 'FH') {
			$emitenData = EmitenData::where('idEmitenData', $idEmitenData)->first();
			$filename   = $emitenData->filename;
		} elseif ($doctype == 'AN' || $doctype == 'RR') {
			$emitenData = Peraturan::where('idPeraturan', $idEmitenData)->first();
			$filename   = $emitenData->filename;
		}
		
		$dataCart = [];
		
		if ($request->session()->has('cart')) {
			$dataCart = $request->session()->get('cart');
			if (array_key_exists($idCart, $dataCart)) {
				return response()->json(['doctype' => [$title . ' sudah ada didalam keranjang belanja']], 422);
			} else {
				$dataCart[$idCart] = [
					'doctype'      => $doctype,
					'type'         => $type,
					'idEmitenData' => $idEmitenData,
					'title'        => $title,
					'year'         => $year,
					'price'        => $price,
					'filename'     => $filename
				];
				$request->session()->put('cart', $dataCart);
			}
		} else {
			$dataCart[$idCart] = [
				'doctype'      => $doctype,
				'type'         => $type,
				'idEmitenData' => $idEmitenData,
				'title'        => $title,
				'year'         => $year,
				'price'        => $price,
				'filename'     => $filename
			];
			$request->session()->put('cart', $dataCart);
		}
		
		return view('ticmi.cart.additem', compact('dataCart', 'idCart'));
	}
	
	/**
	 * @param $idCart
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function removeItem($idCart)
	{
		if (session()->has('cart')) {
			$cart = session()->forget('cart.' . $idCart);
//			return $cart;
			flash()->success('Item berhasil dihapus dari keranjang');
		} else {
			flash()->error('Keranjang Anda masih kosong');
		}
		return redirect()->route('cart.view');
	}
	
	/**
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function removeAllItem()
	{
		if (session()->has('cart')) {
			session()->forget('cart');
			flash()->success('Semua Item berhasil dihapus dari keranjang');
		} else {
			flash()->error('Keranjang Anda masih kosong');
		}
		return redirect()->route('cart.view');
	}
	
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function checkout()
	{
		if (session()->has('cart') && count(session()->get('cart')) > 0) {
			return view('ticmi.cart.checkout');
		} else {
			flash()->error('Maaf keranjang masih kosong');
			return redirect()->route('cart.view');
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request)
	{
		if ($request->session()->has('cart') && count($request->session()->get('cart')) > 0) {
			if (\Auth::check()) {
				/**
				 * Save Keranjang
				 */
				$cart = new Keranjang();
				
				$cart->user_id         = \Auth::user()->peserta_id;
				$cart->invoice_no      = CartHelper::getInvoiceNo();
				$cart->subtotal        = $request->get('subtotal');
				$cart->biaya_admin     = $request->get('biaya_admin');
				$cart->diskon          = 0;
				$cart->grandtotal      = $request->get('grandtotal');
				$cart->virtual_account = CartHelper::getVirtualAccount();
				
				$cart->save();
				
				/**
				 * Save keranjang_detail
				 */
				foreach ($request->session()->get('cart') as $key => $value) {
					$cart_detail = new KeranjangDetail();
					
					$cart_detail->cmeds_keranjang_id = $cart->id;
					$cart_detail->user_id            = \Auth::user()->peserta_id;
					$cart_detail->data_emiten_id     = $value['idEmitenData'];
					$cart_detail->item_name          = $value['title'];
					$cart_detail->item_doctype       = $value['doctype'];
					$cart_detail->item_type          = $value['type'];
					$cart_detail->item_year          = $value['year'];
					$cart_detail->item_filename      = $value['filename'];
					$cart_detail->price              = $value['price'];
					$cart_detail->qty                = 1;
					$cart_detail->diskon             = 0;
					$cart_detail->total_price        = $value['price'];
					$cart_detail->downloaded         = 0;
					
					$cart_detail->save();
				}
				
				/**
				 * Kirim Email Tagihan
				 */
				$job = (new SendEmailTagihanPembelianData($cart))->onQueue('ticmi')->delay(10);
				$this->dispatch($job);
				/**
				 * Delete Session
				 */
				session()->forget('cart');
				
				flash()->success('Transaksi Pembelian Data berhasil');
				return redirect()->route('transaksi.pembelian', ['invoice' => $cart->invoice_no]);
			}
		} else {
			flash()->error('Keranjang Anda kosong, tidak bisa proses checkout');
			return redirect()->route("cart.checkout");
		}
	}
	
	/**
	 * @param \App\Model\Keranjang $keranjang
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function viewKeranjang(Keranjang $keranjang)
	{
		if (Auth::check()) {
			if (Auth::user()->peserta_id == $keranjang->user_id) {
				return view('ticmi.cart.viewpembelian', compact('keranjang'));
			} else {
				abort(404, 'Maaf halaman yang Anda cari tidak ditemukan');
			}
		} else {
			abort(404, 'Maaf halaman yang Anda cari tidak ditemukan');
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Keranjang $keranjang
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function konfirmasi(Request $request, Keranjang $keranjang)
	{
		$dashboardTitle = 'Konfirmasi Tagihan &#' . $keranjang->invoice_no;
		if ($keranjang->user_id == \Auth::user()->peserta_id) {
			return view('ticmi.cart.konfirmasi', compact('keranjang'));
		} else {
			return redirect()->route('dashboard');
		}
	}
	
	/**
	 * @param \App\Http\Requests\KonfirmasiRequest $request
	 * @param \App\Model\Keranjang $keranjang
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function storeKonfirmasi(KonfirmasiRequest $request, Keranjang $keranjang)
	{
		$konfirmasi = new Konfirmasi($request->all());
		
		/**
		 * Cek jika ada upload bukti transfer
		 */
		if ($request->hasFile('bukti_transfer')) {
			if ($request->file('bukti_transfer')->isValid()) {
				$image    = $request->file('bukti_transfer');
				$filename = $keranjang->invoice_no . '_' . $request->get('virtual_account') . '.' . $image->getClientOriginalExtension();

//                $uploadPath = public_path('upload/bukti_konfirmasi/' . $filename);
//                Image::make($image->getRealPath())->resize(600,null, function ($constraint){
//                    $constraint->aspectRatio();
//                })->save($uploadPath);
				
				
				$uploadPath = public_path('upload/bukti_konfirmasi/');
				$request->file('bukti_transfer')->move($uploadPath, $filename);
				/** set bukti transfer */
				$konfirmasi->bukti_transfer = $filename;
			}
		}
		
		$konfirmasi->user_id      = Auth::user()->peserta_id;
		$konfirmasi->keranjang_id = $keranjang->id;
		/** save konfirmasi */
		if ($konfirmasi->save()) {
			/** Jika saving sukses maka set is_confirm = 1 ditabel keranjang */
			$keranjang->update(['is_confirm' => 1, 'confirm_date' => date('Y-m-d H:i:s')]);
			
			/** Kirim Email Notifikasi Konfirmasi sukses */
			$job = (new SendEmailKonfirmasiPembayaran($keranjang))->onQueue('ticmi')->delay(10);
			$this->dispatch($job);
			
			flash()->success('Konfirmasi berhasil');
			return redirect()->route('transaksi.tagihan.view', ['keranjang' => $keranjang]);
		} else {
			flash()->error('Konfirmasi gagal tersimpan, silahkan lakukan konfirmasi kembali');
			return redirect()->route('transaksi.konfirmasi', ['keranjang' => $keranjang]);
		}
	}
	
	/**
	 * @param \App\Model\Keranjang $keranjang
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function viewTagihanMember(Keranjang $keranjang)
	{
		$dashboardTitle = 'Tagihan #' . $keranjang->invoice_no;
		return view('ticmi.member.tagihan-view', compact('keranjang', 'dashboardTitle'));
	}
	
	
	
	public function riwataPembelianData()
	{
		$keranjang = Keranjang::where('user_id', Auth::user()->peserta_id)->orderBy('created_at', 'DESC')->get();
		return view('ticmi.member.riwayat-pembelian-data', compact('keranjang'));
	}
}
