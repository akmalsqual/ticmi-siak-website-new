<?php

namespace App\Http\Controllers\ujian_perdana;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Encryption_decryp_soal;

use Route;
use Url;
use Auth;
use Lang;
use EncryptionSoal;
use Cookie;
use DB;
use Mail;
use Crypt;
use App\Models\Peserta_jawab;
use App\Models\Perdana_peserta;
use App\Models\Ujian_batch;
use App\Models\Ujian_modul;
use App\Models\Soal;
use App\Models\Soal_peserta;
use App\Models\Batch;
use App\Models\Users;
use App\Models\Perdana_trx;
use App\Models\Peserta_lulus;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;

class Ujian_perdanaController_old extends Controller
{

    public function index(Request $request, $batch_id)
    {
        $peserta_id             = Auth::user()->peserta_id;
        $emp_id                 = $request->emp_id;
        $peserta                = Auth::user()->nama;
        $tanggal                = date("d-m-Y");
        $nama_batch             = Batch::where('batch_id', $batch_id)->value('nama');
        $waktu                  = Batch::where('batch_id', $batch_id)->first();
        $waktu                  = ($waktu->waktuUjian->durasi_default_ujian)*60;
        $ujian_batch_id         = Ujian_batch::where('batch_id', $batch_id)->pluck('ujian_batch_id');

        $check                  = Perdana_peserta::where('peserta_id', $peserta_id)
                                                 ->whereIn('ujian_batch_id', $ujian_batch_id)
                                                 ->where('aktivasi', false);
        if($check->count() > 0) {
            return abort(404);
        } else {
            $perdana_peserta_id     = Perdana_peserta::where('peserta_id', $peserta_id)->whereIn('ujian_batch_id', $ujian_batch_id)->pluck('perdana_peserta_id');
            $soal_peserta_id        = Soal_peserta::whereIn('perdana_peserta_id', $perdana_peserta_id)->pluck('soal_peserta_id');
            $peserta_jawab          = Peserta_jawab::whereIn('soal_peserta_id', $soal_peserta_id)->inRandomOrder()->get();
            $string_soal_peserta_id = Soal_peserta::whereIn('perdana_peserta_id', $perdana_peserta_id)->value('soal_peserta_id');

            // Set cookie pertama(default) untuk soal
            foreach ($peserta_jawab as $uji) {
                $toCookie = array('soal_id'=>0, 'kunci_id'=>0);
                $json = json_encode($toCookie);
                setcookie($uji->soalUjian->soal_id, $json, 0, '/');
            }

            // Set cookie untuk waktu ujian
            setcookie("waktu", $waktu, 0, '/');

            $string_perdana_peserta_id = Perdana_peserta::where('peserta_id', $peserta_id)->whereIn('ujian_batch_id', $ujian_batch_id)->value('perdana_peserta_id');

            // Menonaktifkan peserta
            Perdana_peserta::where('peserta_id', $peserta_id)->whereIn('ujian_batch_id', $ujian_batch_id)->update(['aktivasi'=>false]);

            $keyboard_lock = $request->keyboard_lock;
            return view('sertifikasi.ujian_perdana.start_ujian', compact('peserta_jawab', 'peserta', 'tanggal', 'waktu', 'nama_batch', 'soal_peserta_id', 'cek_boleh_ujian', 'string_perdana_peserta_id', 'peserta_id', 'emp_id', 'keyboard_lock'));
        }
    }

    public function ajax_password_ulang(Request $request, $emp_id)
    {
        $emp_id            = $emp_id;
        $password_ulang    = $request->password_ulang;
        
        $password_pengawas = Users::where('emp_id', $emp_id)->value('password');

            if(\Hash::check($password_ulang, $password_pengawas))
            {
                return response()->json([
                    'pesan'=>'berhasil'
                ]);
            }
            else
            {
                return response()->json([
                    'pesan'=>'gagal'
                ]);
            }
    }

    public function ajax_save_peserta_jawab(Request $request)
    {
        $soal_id              = $request->soal_id;
        $kunci_id             = $request->kunci_id;
        $soal_peserta_id      = $request->soal_peserta_id;
        $kunci_id_master      = Soal::where('soal_id', $soal_id)->value('kunci_id');
        
        $check_peserta_jawab  = Peserta_jawab::whereIn('soal_peserta_id', $soal_peserta_id)->where('soal_id', $soal_id)->get();
        $jumlah_peserta_jawab = count($check_peserta_jawab);

        DB::beginTransaction();
            $success_trans = false;
            try {

            if($jumlah_peserta_jawab > 0){
                foreach ($check_peserta_jawab as $cpj) {
                    $new_cpj = Peserta_jawab::where('soal_peserta_id', $cpj->soal_peserta_id)->where('soal_id', $cpj->soal_id)->first();
                    $new_cpj->kunci_id = $kunci_id;

                    if($kunci_id == $kunci_id_master){
                        $new_cpj->is_bener = true;
                    }else{
                        $new_cpj->is_bener = false;
                    }

                    $new_cpj->save();
                }
            }else{
                foreach ($soal_peserta_id as $spi) {
                    $peserta_jawab                  = new Peserta_jawab;
                    $peserta_jawab->soal_peserta_id = $spi;
                    $peserta_jawab->soal_id         = $soal_id;
                    $peserta_jawab->kunci_id        = $kunci_id;

                    if($kunci_id == $kunci_id_master){
                        $new_cpj->is_bener = true;
                    }else{
                        $new_cpj->is_bener = false;
                    }

                    $peserta_jawab->save();
                }
            }

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }

        if($success_trans == true){
            return response()->json([
                'check_peserta_jawab'=>$check_peserta_jawab,
                'soal_peserta_id'=>$soal_peserta_id,
                'soal_id'=>$soal_id
            ]);
        } else if($success_trans == false){
            return response()->json([
                'check_peserta_jawab'=>'',
                'soal_peserta_id'=>'',
                'soal_id'=>''
            ]);
        }

    }

    public function ajax_persentase_kelulusan(Request $request)
    {
        $soal_peserta_id    = array_unique($request->soal_peserta_id);
        $temp = 0;
        //$semua_soal_id      = $request->semua_soal_id;
        //print json_encode($semua_soal_id);
        $data               = [];
         DB::beginTransaction();
            $success_trans = false;
            try {
                $all_spi = collect();
                // memberikan persentase kelulusan pada table soal_peserta
                foreach ($soal_peserta_id as $spi) {
                    $all_spi->push($spi);
                    $soal_peserta = Soal_peserta::where('soal_peserta_id', $spi)->first();
                    $soal_peserta->persentase_kelulusan = $soal_peserta->ujian_modul->batch_modul->persentase_kelulusan_modul;
                    // get soal_peserta_id with same modul_id
                    $ujian_modul_id        = $soal_peserta->ujian_modul_id;
                    $modul_id              = Ujian_modul::find($ujian_modul_id)->modul_id;
                    $ujian_batch_id        = Ujian_modul::find($ujian_modul_id)->ujian_batch_id;
                    $per_modul             = Ujian_modul::where('ujian_batch_id', $ujian_batch_id)->where('modul_id', $modul_id)->pluck('ujian_modul_id');
                    $soal_pesertas         = Soal_peserta::whereIn('soal_peserta_id', $soal_peserta_id)
                                                         ->whereIn('ujian_modul_id', $per_modul)
                                                         ->pluck('soal_peserta_id');
                    $total_soal_modul      = Peserta_jawab::whereIn('soal_peserta_id', $soal_pesertas)->count();
                    $total_benar_modul     = Peserta_jawab::whereIn('soal_peserta_id', $soal_pesertas)->where('is_bener', true)->count();
                    $persentase_kelulusan  = $soal_peserta->ujian_modul->batch_modul->persentase_kelulusan_modul;
                    (float)$nilai_modul    = @((float)$total_benar_modul / (float)$total_soal_modul) * 100;

                    $fix_persentase         = floor($nilai_modul);
                    if($fix_persentase < $persentase_kelulusan)
                    {
                        $is_lulus = false;
                    }else{
                        $is_lulus = true;
                    }

                        // update tbl:soal_peserta
                        $soal_peserta->nilai_ujian          = $fix_persentase;
                        $soal_peserta->persentase_kelulusan = $persentase_kelulusan;
                        $soal_peserta->is_lulus             = $is_lulus;
                        $soal_peserta->nilai                = $total_benar_modul;
                        $soal_peserta->save();

                /*
                    data untuk print hasil ujian
                */
                $peserta_id   = Auth::user()->peserta_id;
                $nama_peserta = Auth::user()->nama; 
                $tgl_lahir    = \Helper::date_formats(Auth::user()->tanggal_lahir, 'view'); 
                $program      = DB::table('program')
                                  ->where('program_id', function($x) use($ujian_batch_id){
                                        $x->select('program_id')
                                          ->from('batch')
                                          ->where('batch_id', function($q) use($ujian_batch_id){
                                            $q->select('batch_id')
                                              ->from('ujian_batch')
                                              ->where('ujian_batch_id', function($y) use($ujian_batch_id){
                                                    $y->select('ujian_batch_id')
                                                      ->from('perdana_peserta')
                                                      ->where('ujian_batch_id', $ujian_batch_id)
                                                      ->first();
                                              })
                                              ->first();
                                          })
                                          ->first();
                                  })->first();
                $batch       = DB::table('batch')
                              ->where('batch_id', function($q) use($ujian_batch_id){
                                  $q->select('batch_id')
                                    ->from('ujian_batch')
                                    ->where('ujian_batch_id', function($y) use($ujian_batch_id){
                                            $y->select('ujian_batch_id')
                                              ->from('perdana_peserta')
                                              ->where('ujian_batch_id', $ujian_batch_id)
                                              ->first();
                                      })
                                    ->first();
                                  })->first();
                $reg_wppe   = DB::table('pendaftaran')->where('peserta_id', $peserta_id)->where('batch_id', $batch->batch_id)->whereNotNull('no_reg_wppe')->value('no_reg_wppe');                                  
                /*
                    end data
                */
                
                /*
                    data untuk insert tbl:perdana_trx
                */
                $perdana_jadwal = DB::table('perdana_jadwal')
                                                   ->where('perdana_jadwal_id', function($q) use($ujian_batch_id){
                                                        $q->select('perdana_jadwal_id')
                                                          ->from('ujian_batch')
                                                          ->where('ujian_batch_id', $ujian_batch_id);
                                                   })->first();
                $modul = Ujian_modul::select(['ujian_modul.modul_id'])->where('ujian_batch_id', $ujian_batch_id)->distinct()->get();

                foreach ($modul as $value) {
                    if($modul_id != $temp)
                    {
                        // insert to tbl:perdana_trx
                        $perdana_trx               = new Perdana_trx;
                        $perdana_trx->nama_perdana = $perdana_jadwal->nama;
                        $perdana_trx->tgl_perdana  = $perdana_jadwal->tgl_perdana;
                        $perdana_trx->nama_peserta = $nama_peserta;
                        $perdana_trx->nama_batch   = $batch->nama;
                        $perdana_trx->nama_modul   = DB::table('modul')->where('modul_id', $modul_id)->value('nama');
                        $perdana_trx->nilai        = $fix_persentase;
                        $perdana_trx->is_pengawas  = $perdana_jadwal->is_pengawas;

                        
                        if($perdana_jadwal->is_pengawas == true){
                            $pengawas = DB::table('employee')->where('emp_id', $perdana_jadwal->emp_id)->first();
                            $perdana_trx->nama_pengawas = $pengawas->first_name.' '.$pengawas->middle_name.' '.$pengawas->last_name;
                        }

                        $perdana_trx->persentase_kelulusan = $persentase_kelulusan;
                        $perdana_trx->status_ujian         = $is_lulus == true ? 'Lulus' : 'Gagal';
                        $perdana_trx->peserta_id           = Auth::user()->peserta_id;
                        $perdana_trx->batch_id             = $batch->batch_id;
                        $perdana_trx->modul_id             = $modul_id;
                        $perdana_trx->save();
                    }
                    $temp = $modul_id;
                    // end insert

                    // push data hasil ujian
                   $arr['nama_modul']    = DB::table('modul')->where('modul_id', $modul_id)->value('nama');
                   $arr['nama_program']  = $program->nama;
                   $arr['singkatan_ind'] = $program->singkatan_ind;
                   $arr['singkatan_eng'] = $program->singkatan_eng;
                   $arr['nama_peserta']  = $nama_peserta;
                   $arr['tgl_lahir']     = $tgl_lahir;
                   $arr['reg_wppe']      = $reg_wppe;
                   $arr['total_soal']    = $total_soal_modul;
                   $arr['total_benar']   = $total_benar_modul;
                   $arr['nilai']         = $fix_persentase;
                   $arr['lulus']         = $is_lulus;
                   array_push($data, $arr);
                }

            } // end foreach soal_peserta_id
            
            $gagal = Soal_peserta::whereIn('soal_peserta_id', $all_spi)->where('is_lulus', false)->count();
            // jika tidak ada modul yang gagal / semua modul lulus
            if($gagal < 1) {
                // cari no sertifikat terbesar berdasarkan nama program
                $no            = Peserta_lulus::where('no_sertifikat', 'like', $program->nama.'%')->max('no_sertifikat');
                $char          =  '-';
                $strpos        = strpos($no, $char); 
                $highest       = substr($no, $strpos + strlen($char)); 
                $no_sertifikat = str_pad($highest + 1, 6, 0, STR_PAD_LEFT);
                $profile       = DB::table('profile_peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
                // insert to tbl:peserta_lulus
                $pl                  = new Peserta_lulus;
                $pl->peserta_id      = Auth::user()->peserta_id;
                $pl->batch_id        = $batch->batch_id;
                $pl->nama_batch      = $batch->nama;
                $pl->no_sertifikat   = $program->kode.'-'.$no_sertifikat;
                $pl->nama_peserta    = Auth::user()->nama;
                $pl->nama_program    = $program->nama;
                $pl->singkatan_ind   = $program->singkatan_ind;
                $pl->singkatan_eng   = $program->singkatan_eng;
                $pl->email           = Auth::user()->email;
                $pl->tempat_lahir    = $profile->tempat_lahir;
                $pl->photo           = $profile->photo;
                $pl->tgl_lahir       = Auth::user()->tanggal_lahir;
                $pl->tgl_lulus       = date('Y-m-d');
                $pl->date_created    = date('Y-m-d H:i:s');
                $pl->nama_lokasi     = DB::table('lokasi')->where('lokasi_id', $profile->lokasi_id)->value('nama');
                $pl->nama_lembg_pdkn = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $profile->lembg_pdkn_id)->value('nama');
                $pl->lembg_pdkn_id   = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $profile->lembg_pdkn_id)->value('lembg_pdkn_id');
                $pl->nama_cabang     = DB::table('cabang')->where('cabang_id', function($q) use($profile){
                                            $q->select('cabang_id')
                                              ->from('lembg_pdkn')
                                              ->where('lembg_pdkn_id', $profile->lembg_pdkn_id);
                                        })->value('nama');
                $pl->save();

                /** Send email verifikasi data sertifikat kelulusan **/
                $header          = 'email-template.core.header';
                $footer          = 'email-template.core.footer';
                $config          = Config_email::first();
                $konfirmasi      = Konfirmasi::find(14);
                $peserta         = DB::table('peserta')->where('peserta_id', $peserta_id)->first();
                 Mail::send('email-template.email-konfirmasi-sertifikat', 
                    [   'header'        => $header, 
                        'footer'        => $footer, 
                        'config'        => $config, 
                        'konfirmasi'    => $konfirmasi, 
                        'peserta'       => $peserta,
                        'link'          => url('profile/konfirmasi_sertifikat/' . Crypt::encrypt($peserta->peserta_id)). '/' . Crypt::encrypt($batch->batch_id)
                    ],
                    function ($message) use ($peserta){
                        $message->subject(' TICMI - Verifikasi Data Sertifikat');
                        $message->from('mg@ticmi.co.id', 'TICMI');
                        $message->to($peserta->email);
                    }
                );
            }
            
            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }
        //
        if($success_trans == true){
            $json = array_unique($data, SORT_REGULAR);
            // clear cookies jawaban dan hasil ujian
            if (isset($_SERVER['HTTP_COOKIE'])) {
                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                foreach($cookies as $cookie) {
                    $parts = explode('=', $cookie);
                    $name = trim($parts[0]);
                    setcookie($name, '', time()-1000);
                    setcookie($name, '', time()-1000, '/');
                }
            }
            // set cookies untuk print hasil ujian
            setcookie("hasil_ujian", json_encode($json), 0, '/');
            echo json_encode($json);
        }else if($success_trans == false){
            $json = 'failed';
            echo json_encode($json);
        }

    }

    public function akhiri_ujian(Request $request)
    {
        $cookie_name = 'nilai';
        if(!isset($_COOKIE[$cookie_name])) {
          $nilai_ujian = '100';
        } else {
          $nilai_ujian = $_COOKIE[$cookie_name];
        }
    
        return view('sertifikasi.ujian_perdana.akhiri_ujian', compact('nilai_ujian'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ujian           = Peserta_jawab::where('soal_peserta_id', $id)->get();
        $soal_peserta_id = $id;
        $total_soal      = count($ujian);

        return view('sertifikasi.ujian_perdana.ujian_show', compact('ujian', 'soal_peserta_id', 'total_soal'));
    }

    public function start_ujian($id)
    {
        $ujian = Peserta_jawab::where('soal_peserta_id', $id)->paginate(1);

        return view('sertifikasi.ujian_perdana.start_ujian', compact('ujian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
