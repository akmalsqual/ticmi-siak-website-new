<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Lang;
use Auth;
use DB;
use URL;
use JsValidator;
use App\Helpers\Error_form;
use App\User;
use App\Models\Profile_peserta;
use App\Models\Peserta_lulus;
use App\Models\Tagihan;
use App\Models\Tagihan_tmp;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\VerifySertifikatRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peserta                   = User::find(Auth::user()->peserta_id);

        $profiles                  = Profile_peserta::where('peserta_id', Auth::user()->peserta_id)->first();

        if(!empty($profiles))
        {
            $cabang_id                 = DB::table('cabang')
                                        ->where('lokasi_id', $profiles->lokasi_id)
                                        ->where('hapus', false)
                                        ->pluck('cabang_id');

            $kp                        = DB::table('lembg_pdkn')
                                        ->where('hapus', false)
                                        ->where('aktif', true)
                                        ->whereIn('cabang_id', $cabang_id)
                                        ->pluck('nama', 'lembg_pdkn_id')
                                        ->prepend('','');
        } else {
            $kp = [];
        }

        $perusahaan                = DB::table('perusahaan')
                                    ->where('hapus', false)
                                    ->where('aktif', true)  
                                    ->pluck('nama', 'perusahaan_id')
                                    ->prepend('','');
                                    // ->union(['0' => 'Lainnya']);
                                    
        $lokasi                    = DB::table('lokasi')
                                    ->where('hapus', false)
                                    ->where('aktif', true)
                                    ->orderBy('nama')
                                    ->pluck('nama', 'lokasi_id')
                                    ->prepend('','');

        $lembaga                   = DB::table('lembaga')
                                    ->where('aktif', true)
                                    ->pluck('nama', 'lembaga_id')
                                    ->prepend('','');

        $profesi                   = DB::table('profesi')
                                    ->where('aktif', true)
                                    ->where('hapus', false)
                                    ->orderBy('profesi_id', 'desc')
                                    ->pluck('nama', 'profesi_id')
                                    ->prepend('','');

        $sex                       = DB::table('sex')->pluck('name', 'sex_id')->prepend('','');
        $negara                    = DB::table('negara')->orderBy('nama')->pluck('nama', 'negara_id')->prepend('','');
        $tgl_lahir                 = \Helper::date_formats($peserta->tanggal_lahir, 'view');

        return view('profile.profile_index',compact('peserta', 'perusahaan', 'lembaga', 'profesi', 'lokasi', 'kp', 'sex', 'negara', 'profiles','tgl_lahir'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, $peserta_id)
    {
        $profiles = Profile_peserta::where('peserta_id', $peserta_id)->first();
        $peserta  = User::find($peserta_id);

        if(!empty($request->no_sid)){
          $verify_tgl_sid     = date('dm', strtotime($peserta->tanggal_lahir));
          $tgl_lahir_from_sid = substr($request->no_sid, 3, 4);
          if ($tgl_lahir_from_sid == $verify_tgl_sid) {
              $peserta->is_valid_sid = 1;
              $peserta->no_sid       = $request->no_sid;
          } else {
              Error_Form::setAlert('message', 'SID yang Anda masukkan tidak valid, silahkan masukkan SID yang valid sesuai dengan yang ada di kartu Akses KSEI Anda', 'alert-class', 'alert-danger');
              return redirect()->back()->withInput();
          }
        }

        DB::beginTransaction();
        $success_trans = false;
        // @update data tbl:peserta

        try {

            if ($request->hasFile('foto')) {
                $image             = $request->file('foto');
                $imagename         = 'PHOTO_'.$peserta_id. '_' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath   = public_path('assets/upload_files/peserta/photos');

                if($profiles) {
                  $old_foto   = $profiles->photo;
                } else {
                  $old_foto   = null;
                }

            } else {
                $imagename         = !empty($profiles->photo) ? $profiles->photo : ''; 
            }

            if ($request->hasFile('ktp')) {
                $ktp             = $request->file('ktp');
                $ktpname         = 'KTP_'.$peserta_id. '_' . time() . '.' . $ktp->getClientOriginalExtension();
                $destinationKtp  = public_path('assets/upload_files/peserta/ktp');

                if($profiles) {
                  $old_ktp   = $profiles->file_ktp;
                } else {
                  $old_ktp   = null;
                }
            } else {
                $ktpname         = !empty($profiles->file_ktp) ? $profiles->file_ktp : ''; 
            }

            if ($request->hasFile('sid_card')) {
                $sid_card          = $request->file('sid_card');
                $sid_cardname      = $request->no_sid . '-' . time() . '.' . $sid_card->getClientOriginalExtension();
                $destinationSid   = public_path('assets/upload_files/peserta/sid_card');

                if($peserta) {
                  $old_sid   = $peserta->sid_card;
                } else {
                  $old_sid   = null;
                }
            } else {
                $sid_cardname      = !empty($peserta->sid_card) ? $peserta->sid_card : NULL; 
            }

            $peserta->nama          = $request->nama;
            $peserta->sid_card      = $sid_cardname;
            $peserta->no_hp         = $request->no_telp;
            $peserta->tanggal_lahir = \Helper::date_formats($request->tgl_lahir, 'db');
            $peserta->profesi_id    = $request->profesi;
            $peserta->profesi       = $request->profesi == 1 ? $request->profesi_lainnya : NULL;
            $peserta->save();

            /* update nama tagihan */
            if(Tagihan::where('peserta_id', $peserta_id)->first()){
              Tagihan::where('peserta_id', $peserta_id)->update(['nama_peserta' => $request->nama]);
              Tagihan_tmp::where('peserta_id', $peserta_id)->update(['nama_peserta' => $request->nama]);
            }
            
            if($request->perusahaan == '1') // perusahaan lainnya
            {
                $per_lainnya = $request->perusahaan_lainnya;
            } else {
                $per_lainnya = null;
            }

            // if peserta has profile do update else insert new data tbl:profile_peserta
            if($profiles) {
              Profile_peserta::where('peserta_id', $peserta_id)
                          ->update(
                          ['sex_id'            => $request->sex,
                          'tempat_lahir'       => $request->tempat_lahir,
                          'alamat_ktp'         => $request->alamat_ktp,
                          'alamat_tinggal_skr' => $request->alamat_tinggal_skr,
                          'pendidikan'         => $request->pendidikan,
                          'jurusan'            => $request->jurusan,
                          'universitas'        => $request->universitas,
                          'pekerjaan'          => $request->pekerjaan,
                          'perusahaan_id'      => $request->perusahaan,
                          'perusahaan_lainnya' => $per_lainnya,
                          'lembaga_id'         => $request->lembaga,
                          'lokasi_id'          => $request->lokasi,
                          'negara_id'          => $request->negara,
                          'lembg_pdkn_id'      => $request->kp,
                          'photo'              => $imagename,
                          'file_ktp'           => $ktpname
                        ]
                    );                
            } else {
              $profile_peserta                     = new Profile_peserta;
              $profile_peserta->peserta_id         = $peserta_id;
              $profile_peserta->sex_id             = $request->sex;
              $profile_peserta->tempat_lahir       = $request->tempat_lahir;
              $profile_peserta->alamat_ktp         = $request->alamat_ktp;
              $profile_peserta->alamat_tinggal_skr = $request->alamat_tinggal_skr;
              $profile_peserta->pendidikan         = $request->pendidikan;
              $profile_peserta->jurusan            = $request->jurusan;
              $profile_peserta->universitas        = $request->universitas;
              $profile_peserta->pekerjaan          = $request->pekerjaan;
              $profile_peserta->perusahaan_id      = $request->perusahaan;
              $profile_peserta->perusahaan_lainnya = $per_lainnya;
              $profile_peserta->lembaga_id         = $request->lembaga;
              $profile_peserta->lokasi_id          = $request->lokasi;
              $profile_peserta->negara_id          = $request->negara;
              $profile_peserta->lembg_pdkn_id      = $request->kp;
              $profile_peserta->photo              = $imagename;
              $profile_peserta->file_ktp           = $ktpname;
              $profile_peserta->save();
            }
            

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            // abort(403, $e->getMessage());
            Error_Form::setAlert('message', Lang::get('db.failed_updated'), 'alert-class', 'alert-danger');
            return redirect()->route('profile.index');
        }

        if ($success_trans == true) {

          if(isset($destinationPath)) {
            if($old_foto !== null) {
                File::delete($destinationPath.'/'.$old_foto);  // delete old file
            }
            $image->move($destinationPath, $imagename);
          }

          if(isset($destinationKtp)) {
            if($old_ktp !== null) {
                File::delete($destinationKtp.'/'.$old_ktp);  // delete old file
            }
            $ktp->move($destinationKtp, $ktpname);
          }

          if(isset($destinationSid)) {
            if($old_sid !== null) {
                File::delete($destinationSid.'/'.$old_sid);  // delete old file
            }
            $sid_card->move($destinationSid, $sid_cardname);
          }

            Error_Form::setAlert('message', Lang::get('db.updated'), 'alert-class', 'alert-success');
            return redirect()->route('profile.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function ajax_get_kp(Request $request){
        if($request->ajax()){
            $lokasi_id = $request->lokasi;

            $kp = DB::table('lokasi')->select('lembg_pdkn.lembg_pdkn_id AS _id', 'lembg_pdkn.nama')
                                    ->join('cabang', 'cabang.lokasi_id', '=', 'lokasi.lokasi_id')
                                    ->join('lembg_pdkn', 'lembg_pdkn.cabang_id', '=', 'cabang.cabang_id')
                                    ->where('lokasi.lokasi_id', '=', $lokasi_id)
                                    ->where('cabang.hapus', '=', false)
                                    ->where('lembg_pdkn.hapus', '=', false)
                                    ->get()->toArray();
            if(!empty($kp)){
                echo json_encode($kp);
            }else{
                echo json_encode(array());
            }
        }
    }

    public function verifySertifikat($peserta_id, $batch_id)
    {
        $peserta_id = \Crypt::decrypt($peserta_id);
        $batch_id   = \Crypt::decrypt($batch_id);
        $check     = DB::table('peserta_lulus')->where('peserta_id', $peserta_id)->where('batch_id', $batch_id)->first();
        if($check)
        {
          $peserta   = DB::table('peserta')->where('peserta_id', $peserta_id)->first();
          $profiles  = DB::table('profile_peserta')->where('peserta_id', $peserta_id)->first();
          $lokasi    = DB::table('lokasi')->where('hapus', false)->where('aktif', true)->pluck('nama', 'lokasi_id')->prepend('','');
          $cabang    = DB::table('cabang')->where('hapus', false)->where('aktif', true)->pluck('nama', 'cabang_id')->prepend('','');
          $kp        = DB::table('lembg_pdkn')->where('hapus', false)->where('aktif', true)->pluck('nama', 'lembg_pdkn_id')->prepend('','');

          $row_cabang    = DB::table('cabang')
                         ->where('cabang_id', function($q) use($profiles){
                            $q->select('cabang_id')
                              ->from('lembg_pdkn')
                              ->where('lembg_pdkn_id', $profiles->lembg_pdkn_id);
                         })
                         ->first();

          return view('profile.konfirmasi_sertifikat', compact('peserta','profiles', 'batch_id','kp','cabang','lokasi', 'row_cabang'));
        } else {
          return abort(404);
        }
  }

  public function insertDataSertifikat(VerifySertifikatRequest $request)
  {
    $peserta_id = Auth::user()->peserta_id;
    $batch_id   = $request->batch_id;
    $profiles = Profile_peserta::where('peserta_id', $peserta_id)->first();
        DB::beginTransaction();
        $success_trans = false;
        // @update data tbl:peserta

        try {
            if ($request->hasFile('foto')) {
                $image             = $request->file('foto');
                $imagename         = 'PHOTO_'.$peserta_id. '_' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath   = public_path('assets/upload_files/peserta/photos');

                if($profiles) {
                  $old_foto   = $profiles->photo;
                } else {
                  $old_foto   = null;
                }

            } else {
                $imagename         = !empty($profiles->photo) ? $profiles->photo : ''; 
            }

            DB::table('peserta')->where('peserta_id', $peserta_id)
                                ->update(['nama'          => $request->nama,
                                          'tanggal_lahir' => \Helper::date_formats($request->tgl_lahir, 'db')
                                         ]);
            // if peserta has profile do update else insert new data tbl:profile_peserta
            if($profiles) {
              Profile_peserta::where('peserta_id', $peserta_id)
                          ->update(['photo' => $imagename]);                
            } else {
              $profile_peserta                     = new Profile_peserta;
              $profile_peserta->peserta_id         = $peserta_id;
              $profile_peserta->photo              = $imagename;
              $profile_peserta->save();
            }
            /* update tbl:peserta_lulus */
            $pl                  = Peserta_lulus::where('peserta_id', $peserta_id)->where('batch_id', $batch_id)->first();
            $pl->nama_peserta    = $request->nama;
            $pl->tgl_lahir       = \Helper::date_formats($request->tgl_lahir, 'db');
            $pl->photo           = $imagename;
            $pl->nama_lokasi     = DB::table('lokasi')->where('lokasi_id', $request->lokasi)->value('nama');
            $pl->nama_cabang     = DB::table('cabang')->where('cabang_id', $request->cabang)->value('nama');
            $pl->nama_lembg_pdkn = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $request->kp)->value('nama');
            $pl->lembg_pdkn_id   = $request->kp;
            $pl->is_confirm      = TRUE;
            $pl->tgl_confirm     = date('Y-m-d');;
            $pl->save();

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            // abort(403, $e->getMessage());
            Error_Form::setAlert('message', Lang::get('db.failed_updated'), 'alert-class', 'alert-danger');
            return redirect()->route('profile.index');
        }

        if ($success_trans == true) {

          if(isset($destinationPath)) {
            if($old_foto !== null) {
                File::delete($destinationPath.'/'.$old_foto);  // delete old file
            }
            $image->move($destinationPath, $imagename);
          }

            Error_Form::setAlert('message', Lang::get('db.updated'), 'alert-class', 'alert-success');
            return redirect()->route('profile.index');
        }
  }
}
