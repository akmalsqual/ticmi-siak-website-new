<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Error_form;
use Session;

//use Mail;
//use Crypt;
use DB;
use App\Models\Profesi;
use App\Models\Peserta;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;


class RegisterController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if (Session::has('log_in')) {
			return redirect()->route('login');
		} else {
			$profesi  = Profesi::where('aktif', 't')->orderBy('profesi_id', 'desc')->pluck('nama', 'profesi_id')->prepend('', '');
			$provinsi = DB::table('ticmi_provinces')->orderBy('name')->pluck('name', 'id')->prepend('', '');
			
			return view('register', compact('profesi', 'provinsi'));
		}
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$header     = 'email-template.core.header';
		$footer     = 'email-template.core.footer';
		$config     = Config_email::first();
		$konfirmasi = Konfirmasi::find(5);
		
		//
		$validator = Validator::make($request->all(), [
			'name'            => 'required|max:100',
			'no_hp'           => 'required|max:13',
			'tanggal_lahir'   => 'required',
			'profesi'         => 'required',
			'profesi_lainnya' => 'required_if:profesi,1', // required jika profesi lainnya
			'email'           => 'required|email|unique:peserta,email|max:100',
			'email_repeat'    => 'required|email|unique:peserta,email|max:100',
			'password'        => 'required|max:255',
			'password_repeat' => 'required|max:255',
			'no_sid'          => 'required|max:255',
			'file_sid'        => 'required|mimes:jpg,jpeg,png', //Remove space from 'picture'
		]);
		
		$validator->after(function () use ($request, $validator) {
			// validation if email and repeat email is not same
			if ($request->input('email') != $request->input('email_repeat')) {
				$validator->errors()->add('email_repeat', 'Email yang anda masukkan tidak sama!');
			}
			// validation if password and repeat password is not same
			if ($request->input('password') != $request->input('password_repeat')) {
				$validator->errors()->add('password_repeat', 'Password yang anda masukkan tidak sama!');
			}
		});
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		}
		
		$peserta                = new Peserta;
		$peserta->nama          = $request->name;
		$peserta->no_hp         = $request->no_hp;
		$peserta->tanggal_lahir = $request->tanggal_lahir;
		$peserta->kabupaten_id  = $request->kabupaten;
		$peserta->provinsi_id   = $request->provinsi;
		$peserta->instansi      = $request->instansi;
		// $peserta->profesi           = Profesi::where('profesi_id', $request->profesi)->take(1)->get()[0]['nama'];
		$peserta->email          = $request->email;
		$peserta->password       = \Hash::make($request->password);
		$peserta->no_sid         = $request->no_sid;
		$peserta->is_verify      = 'f';
		$peserta->status         = 'f';
		$peserta->status         = 'f';
		$peserta->profesi_id     = $request->profesi;
		$peserta->profesi        = ($request->profesi == 1) ? $request->profesi_lainnya : 'NULL';
		$peserta->kat_peserta_id = 1;
		$peserta->newsletter     = 'f';
		$peserta->sid_card       = '';
		$peserta->created_at     = date('Y-m-d H:i:s');
		if ($request->has('newsletter')) {
			$peserta->newsletter = 't';
		}
		
		$verify_tgl_sid     = date('dm', strtotime($request->tanggal_lahir));
		$tgl_lahir_from_sid = substr($request->get('no_sid'), 3, 4);
		if ($tgl_lahir_from_sid == $verify_tgl_sid) {
			$peserta->is_valid_sid = 1;
		} else {
			Error_Form::setAlert('message', 'SID yang Anda masukkan tidak valid, silahkan masukkan SID yang valid sesuai dengan yang ada di kartu Akses KSEI Anda', 'alert-class', 'alert-danger');
			return redirect()->back()->withErrors($validator)->withInput();
		}
		
		// Upload File
		if ($file = $request->file('file_sid')) {
			$name = $request->get('no_sid') . '-' . time() . '.' . $request->file_sid->extension();
			$file->move(public_path('assets/upload_files/peserta/sid_card/'), $name);
			$peserta->sid_card = $name;
		}
		
//		$peserta->save();
		
		if ($peserta->save()) {
			$peserta = Peserta::where('email', $peserta->email)->first();
			
			/**
			 * Tambahkan Role
			 */
			if ($peserta->is_valid_sid == 1) {
				$roleUser = Role::where('name', 'member')->first();
				$peserta->attachRole($roleUser);
			} else {
				$roleUser = Role::where('name', 'register')->first();
				$peserta->attachRole($roleUser);
				
			}
			// Send Email
			Mail::send('email-template.email-register-template',
				[
					'header'     => $header,
					'footer'     => $footer,
					'config'     => $config,
					'konfirmasi' => $konfirmasi,
					'peserta'    => $peserta,
					'link'       => url('/verify/' . Crypt::encrypt($peserta->email)) . '/' . $peserta->peserta_id
				],
				function ($message) use ($peserta) {
					$message->subject(' TICMI - Verifikasi Email');
					$message->from('mg@ticmi.co.id', 'TICMI');
					$message->to($peserta->email);
				}
			);
			Error_Form::setAlert('message', 'Pendaftaran berhasil!', 'alert-class', 'alert-info');
		} else {
			Error_Form::setAlert('message', 'Pendaftaran gagal!', 'alert-class', 'alert-danger');
		}
		
		return redirect()->route('register-success');
	}
	
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeNonSID(Request $request)
	{
		$header     = 'email-template.core.header';
		$footer     = 'email-template.core.footer';
		$config     = Config_email::first();
		$konfirmasi = Konfirmasi::find(5);
		
		//
		$validator = Validator::make($request->all(), [
			'name_non'            => 'required|max:100',
			'no_hp_non'           => 'required|max:13',
			'tanggal_lahir_non'   => 'required',
			'profesi_non'         => 'required',
			'profesi_non_lainnya' => 'required_if:profesi_non,1', // required jika profesi lainnya
			'email_non'           => 'required|email|unique:peserta,email|max:100',
			'email_repeat_non'    => 'required|email|unique:peserta,email|max:100',
			'password_non'        => 'required|max:255',
			'password_repeat_non' => 'required|max:255'
		]);
		
		$validator->after(function () use ($request, $validator) {
			// validation if email and repeat email is not same
			if ($request->input('email_non') != $request->input('email_repeat_non')) {
				$validator->errors()->add('email_repeat_non', 'Email yang anda masukkan tidak sama!');
			}
			// validation if password and repeat password is not same
			if ($request->input('password_non') != $request->input('password_repeat_non')) {
				$validator->errors()->add('password_repeat_non', 'Password yang anda masukkan tidak sama!');
			}
		});
		if ($validator->fails()) {
			return redirect()->route('register.index')->withErrors($validator)->withInput();
		}
		
		$peserta                = new Peserta;
		$peserta->nama          = $request->name_non;
		$peserta->no_hp         = $request->no_hp_non;
		$peserta->tanggal_lahir = $request->tanggal_lahir_non;
		$peserta->kabupaten_id  = $request->kabupaten_non;
		$peserta->provinsi_id   = $request->provinsi_non;
		$peserta->instansi      = $request->instansi_non;
		// $peserta->profesi           = Profesi::where('profesi_id', $request->profesi_non)->take(1)->get()[0]['nama'];
		$peserta->email          = $request->email_non;
		$peserta->password       = \Hash::make($request->password_non);
		$peserta->no_sid         = NULL;
		$peserta->is_verify      = 'f';
		$peserta->status         = 'f';
		$peserta->profesi_id     = $request->profesi_non;
		$peserta->profesi        = $request->profesi_non == 1 ? $request->profesi_non_lainnya : NULL;
		$peserta->kat_peserta_id = 2;
		$peserta->sid_card       = NULL;
		$peserta->created_at     = date('Y-m-d H:i:s');
		if ($request->has('newsletter')) {
			$peserta->newsletter = 't';
		} else {
			$peserta->newsletter = 'f';
		}
		$roleUser = Role::where('name', 'register')->first();
		
//		return $roleUser;
//		$peserta->attachRole($roleUser);
//		return $peserta->roles;
		
//		$peserta->save();
		
		if ($peserta->save()) {
			$peserta = Peserta::where('email', $peserta->email)->first();
			
			/**
			 * Tambahkan Role
			 */
			if ($peserta->is_valid_sid == 1) {
				$roleUser = Role::where('name', 'member')->first();
				$peserta->attachRole($roleUser);
			} else {
				$roleUser = Role::where('name', 'register')->first();
				$peserta->attachRole($roleUser);
			}
			
			// Send Email
			Mail::send('email-template.email-register-template',
				[
					'header'     => $header,
					'footer'     => $footer,
					'config'     => $config,
					'konfirmasi' => $konfirmasi,
					'peserta'    => $peserta,
					'link'       => url('/verify/' . Crypt::encrypt($peserta->email)) . '/' . $peserta->peserta_id
				],
				function ($message) use ($peserta) {
					$message->subject(' TICMI - Verifikasi Email');
					$message->from('mg@ticmi.co.id', 'TICMI');
					$message->to($peserta->email);
				}
			);
			Error_Form::setAlert('message', 'Pendaftaran berhasil!', 'alert-class', 'alert-info');
		} else {
			Error_Form::setAlert('message', 'Pendaftaran gagal!', 'alert-class', 'alert-danger');
		}
		
		return redirect()->route('register-success');
	}
	
	
	public function verifyRegistration($useremail, $userid)
	{
//		return $useremail;
		$email   = \Crypt::decrypt($useremail);
		$peserta = Peserta::where('email', $email)->where('peserta_id', $userid)->first();
//		return $peserta;
		if ($peserta && count($peserta) > 0) {
			if ($peserta->is_verify == false) {
				$update = [
					'is_verify'  => true,
					'status'     => true,
					'updated_at' => date('Y-m-d')
				];
				if ($peserta->update($update)) {
					Error_Form::setAlert('message', 'Verifikasi email berhasil', 'alert-class', 'alert-info');
				} else {
					Error_Form::setAlert('message', 'Verifikasi email gagal', 'alert-class', 'alert-danger');
				}
			} else {
				Error_Form::setAlert('message', 'Anda sudah melakukan verifikasi sebelumnya', 'alert-class', 'alert-danger');
			}
		} else {
			Error_Form::setAlert('message', 'Maaf akun dengan email ' . $email . ' tidak ada di data kami. Silahkan lakukan pendaftaran.', 'alert-class', 'alert-danger');
		}
		return redirect('/login');
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function registrationSuccess()
	{
		return view('register-success');
	}
	
	public function ajax_get_kabupaten(Request $request)
	{
		if ($request->ajax()) {
			$provinsi_id = $request->provinsi;
			
			$kabupaten = DB::table('ticmi_regencies')
				->where('province_id', '=', $provinsi_id)
				->get()
				->toArray();
			if (!empty($kabupaten)) {
				echo json_encode($kabupaten);
			} else {
				echo json_encode([]);
			}
		}
	}
	
	public function resendEmailVerification($useremail)
	{
		$user = User::where('email', trim($useremail))->first();
		if ($user && $user->count() > 0) {
			return view('reset_password.not_verify', compact('user'));
		} else {
			return redirect('/login');
		}
	}
	
	public function resendEmailVerificationProses(Request $request)
	{
		$email  = $request->get('email');
		$userid = $request->get('userid');
		
		$header     = 'email-template.core.header';
		$footer     = 'email-template.core.footer';
		$config     = Config_email::first();
		$konfirmasi = Konfirmasi::find(5);
		
		$user = User::where('email', trim($email))->where('peserta_id', $userid)->first();
		if ($user && $user->count() > 0) {
			// Send Email
			Mail::send('email-template.email-register-template',
				[
					'header'     => $header,
					'footer'     => $footer,
					'config'     => $config,
					'konfirmasi' => $konfirmasi,
					'peserta'    => $user,
					'link'       => url('/verify/' . Crypt::encrypt($user->email)) . '/' . $user->peserta_id
				],
				function ($message) use ($user) {
					$message->subject(' TICMI - Verifikasi Email');
					$message->from('mg@ticmi.co.id', 'TICMI');
					$message->to($user->email, $user->nama);
				}
			);
			Error_form::setAlert('message', 'Email verifikasi sudah dikirim kembali, silahkan cek email Anda.', 'alert-class', 'alert-success');
		} else {
			Error_form::setAlert('message', 'Kami tidak menemukan user dengan email yang Anda masukkan, harap masukkan email Anda dengan benar.', 'alert-class', 'alert-success');
		}
		
		return redirect()->route('register.resend-email-verification', ['useremail'=>$email]);
	}
}
