<?php

namespace App\Http\Controllers;

// use App\Model\Admisi;
use App\Models\PesertaWisuda;
use App\Models\Program;
use App\Models\Provinsi;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;

use App\Http\Requests;

class WisudaController extends Controller
{
	public function index()
	{
		 $pageTitle = 'Wisuda Akbar TICMI';
		  // $program = Program::whereIn('kode',['WPPE','WPPP','WPPT','ASPM','WMI','WPEE'])->get();
		  
		  // $program = array_pluck($program,'nama','nama');
		  
		     // $provinces = Provinsi::all();
		
		  // $provinces = array_pluck($provinces,'name','name');
		 return view('wisuda', compact('pageTitle'));
		 // dd($provinces);
    }
	
	public function store(Requests\DaftarWisudaRequest $request)
	{
		$program = Program::where('nm_program',$request->get('program'))->first();
		$cek = Admisi::where('email',$request->get('email'))->where('kd_program',$program->id)->whereRaw("tanggal > UNIX_TIMESTAMP('2017-11-01') AND tanggal < UNIX_TIMESTAMP('2018-03-31 23:59:59')")->first();
		
		if ($cek && $cek->count() > 0) {
			
			if ($cek->nip != 0) {
				
				$pesertaWisuda = new PesertaWisuda($request->all());
				
				if ($pesertaWisuda->save()) {
					
					if ($pesertaWisuda->is_confirm == 1) {
						flash()->success('Konfirmasi kehadiran berhasil. Sampai jumpa di acara Wisuda Akbar TICMI.');
					} else {
						flash()->success('Konfirmasi kehadiran berhasil. Anda memilih untuk tidak dapat menghadiri acara Wisuda Akbar TICMI.');
					}
					return redirect()->route('wisuda.message',['status'=>'success']);
				} else {
					flash()->error('Maaf Konfirmasi kehadiran gagal tersimpan, mohon untuk mengulangi konfirmasi sekali lagi');
				}
			} else {
				flash()->error('Maaf Anda belum masuk kedalam daftar lulusan Kami.');
			}
		} else {
			flash()->error('Maaf, Kami tidak menemukan data dengan email '.$request->get('email').' pada data lulusan Kami yang lulus di periode November 2017 s/d Maret 2018');
		}
		return redirect()->route('wisuda');
    }
	
	public function message($status='')
	{
		return view('frontend.wisuda.message', compact('status'));
    }
	
	public function absensi($email)
	{
		$cekemail =  base64_decode($email);
		$pesertaWisuda = PesertaWisuda::where('email',$cekemail)->first();
		
		return view('frontend.wisuda.absensi', compact('pesertaWisuda'));
    }
	
	public function absensiStore($email, $from="")
	{
		$cekemail =  base64_decode($email);
		$pesertaWisuda = PesertaWisuda::where('email',$cekemail)->first();
		if ($pesertaWisuda && $pesertaWisuda->count() > 0) {
			$pesertaWisuda->update(['is_hadir'=>1]);
			flash()->success('Absensi Berhasil');
		} else {
			flash()->error('Absensi Gagal');
		}
		
		if ($from == "report") {
			return redirect()->route('wisuda.report');
		} else {
			return redirect()->route('wisuda.absensi',['emailwisuda'=>$email]);
		}
    }
	
	public function report()
	{
		$pageTitle = 'Absensi Kehadiran Akbar TICMI';
		$pesertaWisuda = PesertaWisuda::where('is_confirm',1)->orderBy('program')->orderBy('nama')->get();
		return view('frontend.wisuda.report', compact('pesertaWisuda', 'pageTitle'));
    }
	
	public function hadir()
	{
		$pageTitle = 'Data Peserta Hadir Wisuda Akbar TICMI';
		$pesertaWisuda = PesertaWisuda::where('is_hadir',1)->orderBy('program')->orderBy('nama')->get();
		return view('frontend.wisuda.report', compact('pesertaWisuda', 'pageTitle'));
    }
	
	public function blastUndanganWisuda()
	{
		$pesertaWisuda = PesertaWisuda::where('is_confirm',1)->orderBy('program')->orderBy('nama')->get();
		
//		$pesertaWisuda = PesertaWisuda::where('id',1)->get();
		foreach ($pesertaWisuda as $item) {
			\Mail::send('email-template.email-kehadiran-wisuda',['pesertaWisuda'=>$item], function ($m) use ($item){
				$m->from('noreply@ticmi.co.id', 'TICMI');
				$m->to($item->email,$item->nama);
//				$m->to('octaviantika.kumala@ticmi.co.id','Octa');
				$m->subject('TICMI - Undangan Wisuda Akbar TICMI 2018');
			});
		}
    }
	
	public function tesemail()
	{
		return view('email-template.email-wisuda-confirm-hadir');
    }
}
