<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    //
    public function profilPerusahaan()
	{
		$pageTitle = "Profil The Indonesia Capital Market Institute";
		return view('ticmi.about-us.profil-perusahaan', compact('pageTitle'));
	}
	
	public function visiMisi()
	{
		$pageTitle = "Visi dan Misi";
		return view('ticmi.about-us.visi-misi', compact('pageTitle'));
	}
	
	public function sejarah()
	{
		$pageTitle = "Sejarah TICMI";
		return view('ticmi.about-us.sejarah', compact('pageTitle'));
	}
	
	public function struktur()
	{
		$pageTitle = "Struktur Organisasi";
		return view('ticmi.about-us.struktur', compact('pageTitle'));
	}

}
