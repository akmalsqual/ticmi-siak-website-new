<?php

namespace App\Http\Controllers;

use App\Models\ticmi\PesertaWorkshop;
use App\Models\ticmi\Workshop;
use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use Yajra\Datatables\Facades\Datatables;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url_ajax_datatable = route('tagihan.ajax_datatable');
        
//        return auth()->user()->roles;

        return view('dashboard.dashboard_index', compact('url_ajax_datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	public function seminarWorkshop()
	{
		$pageTitle = "Seminar dan Workshop";
		
		$pesertaWorkshop = PesertaWorkshop::where('email', auth()->user()->email)->get();
		$workshops       = Workshop::whereHas('peserta', function ($query) {
			$query->where('email', auth()->user()->email);
		})->orderBy('tgl_mulai', 'DESC')->get();
		return view('ticmi.member.seminar-workshop', compact('pageTitle', 'workshops'));
	}
	
	public function seminarWorkshopView(Workshop $workshop)
	{
		$peserta = $workshop->peserta()->where('email', auth()->user()->email)->first();
		if ($peserta && $peserta->is_payment_approved) {
			$pageTitle = "Seminar dan Workshop";
			return view('ticmi.member.seminar-workshop-view', compact('pageTitle', 'workshop', 'peserta'));
		} else {
			return redirect()->back();
		}
	}
}
