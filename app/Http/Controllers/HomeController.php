<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Str;
use Mail;
use Crypt;
use Auth;
use DB;
use URL;
use App\Helpers\Error_form;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Program;
use App\Models\Batch;
use App\Models\Cabang;
use App\Models\ticmi\Workshop;
use App\Models\Peserta;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;
use App\User;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batch_path     = \Config::get('app.url_siak').'/upload_files/images/logo_batch/';
        $cabang         = Cabang::where('aktif', TRUE)->where('hapus', FALSE)->orderBy('nama')->get();
        $select_program = Program::where('aktif', TRUE)->where('hapus', FALSE)->get();
        $programs       = Program::whereHas('batch', function($q){
            $q->where('aktif', TRUE);
            $q->where('hapus', FALSE);
            $q->where('disetujui', TRUE);
            $q->where('is_publish', TRUE);
            $q->where('is_hidden', FALSE);
            $q->where('is_pelatihan', FALSE);
        })->get();

        $workshops    = Workshop::where('is_publish', 1)->where('is_hide', '0')->where('is_online', '=', '0')->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai','ASC')->get();
        $kelas_online = Workshop::where('is_publish', 1)->where('is_online', '=', 1)->get();

        foreach ($kelas_online as $item) {
            $workshops = $workshops->push($item);
        }
        $url_ajax_search = URL::to('/search');

        //memasukan kode program ke sebuah array
        $arr_kode =[];
        foreach ($programs as $key => $value) {
            $kode_program = $value->kode;
            array_push($arr_kode, $kode_program);
        }

        return view('home', compact('arr_kode','programs', 'batch_path', 'cabang', 'select_program', 'workshops','url_ajax_search'));
    }

    public function forgot_pasword()
    {
        return view('reset_password.forgot_password');
    }

    public function verify_email()
    {
        return view('verify-email');
    }

    public function maintenance()
    {
        return view('maintenance');
    }

    public function verify_email_proses(Request $request)
    {
        $email = $request->get('email');
        
        $peserta = Peserta::where('email', $email)->first();
            if ($peserta && count($peserta) > 0) {
                if ($peserta->is_verify === FALSE) {
                    $header          = 'email-template.core.header';
                    $footer          = 'email-template.core.footer';
                    $config          = Config_email::first();
                    $konfirmasi      = Konfirmasi::find(18);

                    Mail::send('email-template.email-verifikasi', 
                        [ 
                            'header'        => $header, 
                            'footer'        => $footer, 
                            'config'        => $config, 
                            'konfirmasi'    => $konfirmasi, 
                            'peserta'       => $peserta,
                            'link'          => url('/verify/' . Crypt::encrypt($peserta->email) . '/' . $peserta->peserta_id)
                        ],
                        function ($message) use ($peserta){
                            $message->subject(' TICMI - Verifikasi Email');
                            $message->from('mg@ticmi.co.id', 'TICMI');
                            $message->to($peserta->email);
                        }
                    );

                    Error_Form::setAlert('verified', 'Silahkan cek email Anda untuk melanjutkan proses verifikasi email.', 'alert-class', 'alert-success');

                } else {
                    Error_Form::setAlert('message', 'Email Anda telah terverifikasi sebelumnya silahkan login kembali', 'alert-class', 'alert-success');
                }
            } else {
                Error_Form::setAlert('message', 'Maaf Kami tidak menemukan akun dengan email <strong>' . $email . '</strong>', 'alert-class', 'alert-danger');
            }

//        return redirect()->route('verify-email');
        return redirect()->route('register.verify');
    }
    
    public function forgot_pasword_admisi()
    {
        Error_Form::setAlert('forgot', 'Untuk menjaga keamanan sistem dan akun Anda, silahkan reset password terlebih dahulu', 'alert-class', 'alert-info');
        
        return view('reset_password.forgot_password');
    }
    
    public function forgot_pasword_proses(Request $request)
    {
        $email = $request->get('email');
        
        $member = User::where('email','ILIKE', "%$email%")->first();
            if ($member && count($member) > 0) {
                $token   = Str::random(6);
                $newPass = bcrypt($token);

                if ($member->update(['password'=>$newPass])) {
                	
                    $header          = 'email-template.core.header';
                    $footer          = 'email-template.core.footer';
                    $config          = Config_email::first();
                    $konfirmasi      = Konfirmasi::find(9);
                    Mail::send('email-template.email-reset-password', 
                        [   'header'        => $header, 
                            'footer'        => $footer, 
                            'config'        => $config, 
                            'konfirmasi'    => $konfirmasi, 
                            'peserta'       => $member,
                            'token'         => $token,
                            'link'          => route('reset_password',['useremail'=>Crypt::encrypt($member->email),'user'=>$member->peserta_id])
                        ],
                        function ($message) use ($member){
                            $message->subject(' TICMI - Reset Password');
                            $message->from('noreply@ticmi.co.id', 'TICMI');
                            $message->to($member->email);
                        }
                    );
            }
            Error_Form::setAlert('message', 'Password Anda berhasil direset, silahkan cek email Anda untuk cek password baru Anda.', 'alert-class', 'alert-success');
            return redirect()->route('reset_password', ['useremail' => Crypt::encrypt($member->email), 'user' => $member->peserta_id]);
        } else {
            Error_Form::setAlert('message', 'Maaf Kami tidak menemukan akun dengan email <strong>' . $email . '</strong>', 'alert-class', 'alert-danger');
            return redirect()->route('forgot_password');
        }
    }

    public function reset_password($useremail, $userid)
    {
        return view('reset_password.reset_password', compact('useremail','userid'));
    }

    public function reset_password_proses(ResetPasswordRequest $request, $useremail, $userid)
    {
        
		$token    = $request->get('token');
        $password = $request->get('password');
        $email    = Crypt::decrypt($useremail);
        $user     = User::where('email','ILIKE',"%$email%")->where('peserta_id',$userid)->first();
        if ($user && $user->count() > 0) {
            if (\Hash::check(trim($token), $user->password)) {
                $new_password = bcrypt($password);
                if ($user->update(['password'=>$new_password])) {
	                
	                \DB::connection('mysql_elearning')->table('mdl_user')->where('id', $user->moodle_user_id)->update(['password' => $new_password]);
	                
                    Error_Form::setAlert('message', 'Password berhasil di reset, silahkan login', 'alert-class', 'alert-success');
                    return redirect()->route('login');
                } else {
                    Error_Form::setAlert('message', 'Maaf perubahan password gagal silahkan isi form reset password kembali', 'alert-class', 'alert-danger');
                }
            } else {
                Error_Form::setAlert('message', 'Maaf token yang Anda masukkan tidak sesuai, silahkan cek kembali token pada email Anda', 'alert-class', 'alert-danger');
            }
        } else {
            Error_Form::setAlert('message', 'Maaf Kami tidak menemukan akun dengan email <strong>' . $email . '</strong>', 'alert-class', 'alert-danger');
        }
        return redirect()->route('reset_password',['useremail'=>$useremail,'userid'=>$userid]);
    }

    public function pendaftaranDetail(Request $request, $batchid, $batchname)
    {
        $zona_dt        = 'Asia/Jakarta';
        $user           = Auth::user();
        $data           = [];
        $pageTitle      = $batchname;
        $batch          = Batch::findOrFail($batchid);
        $batch_path     = \Config::get('app.url_siak').'/upload_files/images/logo_batch/';
        if($user != null){
            // Cek apakah data user di microsite sudah lengkap
            $peserta            = Peserta::leftJoin('profile_peserta', function($join) {
                                    $join->on('peserta.peserta_id', '=', 'profile_peserta.peserta_id');
                                })->where('peserta.email', $user->email)->first();
            
            // $data['peserta']    = ( empty($peserta->profile_peserta_id) || empty($peserta->alamat_tinggal_skr) || empty($peserta->no_hp) || 
            //                         empty($peserta->tempat_lahir) || empty($peserta->alamat_ktp) || empty($peserta->alamat_tinggal_skr) || 
            //                         empty($peserta->photo) || empty($peserta->lokasi_id) || empty($peserta->lembg_pdkn_id) || 
            //                         empty($peserta->negara_id) || empty($peserta->pendidikan) || empty($peserta->jurusan) || 
            //                         empty($peserta->universitas) || empty($peserta->lembaga_id) || empty($peserta->profesi_id) || 
            //                         (empty($peserta->perusahaan_id) && empty($peserta->perusahaan_lainnya)) || empty($peserta->pekerjaan)) 
            //                         ? FALSE:TRUE;
            // $data['peserta']    =  empty($peserta->profile_peserta_id) ? FALSE:TRUE;
            
            $data['is_loggin']  =  TRUE;

            $terdaftar          = DB::table('pendaftaran')
                                    ->where('batch_id', $batchid)
                                    ->where('peserta_id', $peserta->peserta_id)
                                    ->count();
            if($terdaftar > 0)
            {
                $data['terdaftar']  = TRUE;
            } else {
                $data['terdaftar']  = FALSE;
            }
        }
        else{
            $data['peserta']    =   FALSE;
            $data['is_loggin']  =   FALSE;
            $data['terdaftar']  =   FALSE;
        }

        $time       = strtotime("-7 days");
        $otherbatch = Batch::where('batch_id', '!=', $batchid)
                             ->where('aktif', TRUE)
                             ->where('hapus', FALSE)
                             ->where('is_publish', TRUE)
                             ->where('is_hidden', FALSE)
                             ->where('is_pelatihan', FALSE)
                             ->get();
        return view('pendaftaran-detail', compact('batch', 'batch_path', 'otherbatch', 'pageTitle', 'data'));
    }

    public function search(Request $request){
        $cabang         = $request->input("cabang");
        $bulan          = $request->input("bulan");
        $program_id     = $request->input("kd_program");

        $programs       = Program::whereHas('batch', function($q){
            $q->where('aktif', TRUE);
            $q->where('hapus', FALSE);
            $q->where('disetujui', TRUE);
            $q->where('is_publish', TRUE);
            $q->where('is_hidden', FALSE);
            $q->where('is_pelatihan', FALSE);
        });

        // $batch     = Batch::where('is_hidden', FALSE)->where('aktif', TRUE)->where('hapus', FALSE)->where('is_publish', TRUE)->where('disetujui', TRUE);

        if ($cabang){
            $programs = $programs->whereHas('batch', function ($batch) use ($cabang) {
                $batch->whereHas('cabang', function($query) use ($cabang){
                    $query->where('nama', $cabang);
                });
            });
        } 
        if ($bulan){
            $programs = $programs->whereHas('batch', function($query) use ($bulan){
                $query->whereMonth('tgl_mulai', $bulan);
            });
        }
        if ($program_id){
            $programs = $programs->where('program_id', $program_id);
        } 

        $programs = $programs->get();

        //memasukan kode program ke sebuah array
        $arr_kode =[];
        foreach ($programs as $key => $value) {
            $kode_program = $value->kode;
            array_push($arr_kode, $kode_program);
        }
        
        $workshops    = Workshop::where('is_publish', 1)->where('is_hide', 0)->where('is_online','0')->where('tgl_mulai', '>', date('Y-m-d'));

        $workshops->when($program_id, function ($query) use ($program_id) {
            return $query->where('t_program_id', $program_id);
        })->when($bulan, function ($query) use ($bulan) {
            return $query->whereMonth('tgl_mulai', $bulan);
        });
        $workshops = $workshops->get();
        
        $batch_path     = \Config::get('app.url_siak').'/upload_files/images/logo_batch/';

        return view('batch', compact('arr_kode','programs','workshops', 'batch_path'));
    }

}
