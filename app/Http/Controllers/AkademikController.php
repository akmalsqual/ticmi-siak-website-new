<?php

namespace App\Http\Controllers;

use App\Models\ticmi\JadwalKegiatan;
use Illuminate\Http\Request;

class AkademikController extends Controller
{
    //
    public function gratis()
	{
		$pageTitle = "Standar Kelulusan Ujian Sertifikasi TICMI";
		return view('ticmi.akademik.gratis', compact('pageTitle'));
	}

	public function jadwalKegiatan(Request $request)
	{
		$pageTitle = "Jadwal Kegiatan Pelatihan dan Seminar TICMI";
		
		$year = $request->get('year');
		$year = empty($year) ? date('Y'):$year;
		
		$jadwalSertifikasi = JadwalKegiatan::whereHas('program', function ($query) {
			$query->where('kat_program_id',1);
		})->where('is_publish',1)->get();
		
		$jadwalNonSertifikasi = JadwalKegiatan::whereHas('program', function ($query) {
			$query->where('kat_program_id',2);
		})->where('is_publish',1)->get();
		
		$jenisProgramNonSertifikasi =
			[
				1=>'WPPE',19=>'WPPE Pemasaran',18=>'WPPE Pemasaran Terbatas',2=>'WMI',24=>'WPEE',22=>'ASPM',
				500=>'Cerdas Mengelola Keuangan',501=>'Risk Management',502=>'Workshop GCG/Corsec',503=>'AFNA / FR Analysis',
				504=>'Financial Planner',505=>'Sekolah Reksa Dana',506=>'Technical Investing',507=>'Value Investing',
			];
		
		return view('ticmi.akademik.jadwal-kegiatan', compact('pageTitle', 'jadwalSertifikasi', 'jadwalNonSertifikasi', 'year'));
	}
	
	public function jadwalKegiatanView(JadwalKegiatan $jadwalKegiatan)
	{
		return view('ticmi.akademik.jadwal-kegiatan-view', compact('jadwalKegiatan'));
	}

	public function standarKelulusan()
	{
		$pageTitle = "Standar Kelulusan Ujian Sertifikasi TICMI";
		return view('ticmi.akademik.standar-kelulusan', compact('pageTitle'));
	}

	public function hubungiKami()
	{
		$pageTitle = "Hubungi Kami";
		return view('ticmi.akademik.hubungi-kami', compact('pageTitle'));
	}

	public function faq()
	{
		$pageTitle = "Frequently Asked Question";
		return view('ticmi.akademik.faq', compact('pageTitle'));
	}
}
