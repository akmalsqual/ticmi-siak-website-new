<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Pbkdf2Helper;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Jobs\SendEmailResetPassword;
use Session;
use Auth;
use Lang;
use DB;
use Crypt;
use App\User;
use App\Models\Role;
use App\Models\ticmi\Customer;
use App\Http\Requests\LoginRequest;
use App\Helpers\Error_form;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;
    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('login');
    }

    public function authenticate(LoginRequest $request)
    {
        /** Get Requested Data */
        $email    = $request->get('email');
        $password = $request->get('password');

        /** Cek jika email ada di tabel users */

        $cek = User::with('roles')->where('email', $email)->first();
        
        if ($cek && $cek->count() > 0) {
            /**
             * Jika email sudah ada ditabel users maka lakukan login attempt
             */
            if($cek->is_verify == TRUE && $cek->status == TRUE) {
                if (Auth::attempt(['email' => $request->email, 'password' => \Request::input('password'), 'is_verify' => 1, 'status' => 1])) {
                    // Authentication Passed
                    return redirect()->intended('/dashboard');
                } elseif ($cek->password == null ) {
                    // logged in as admisi redirect reset password
                    return redirect()->route('forgot_password_admisi');
                } elseif ( ! User::where('email', $request->email)->first() ) {
                    return redirect()->route('login')
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors([
                            $this->username() => Lang::get('auth.email'),
                        ]);
                } elseif ( ! User::where('email', $request->email)->where('password', \Request::input('password'))->first() ) {
                    return redirect()->route('login')
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors([
                            'password' => Lang::get('auth.password'),
                        ]);
                } 
            } elseif ($cek->is_verify == FALSE || $cek->status == FALSE) {
//                return redirect()->route('login')->with('alert-error', 'Email not verified');
	            /**
	             * Proses pengiriman kembali Email Verifikasi
	             */
	            $user = $cek;
	            return view('reset_password.not_verify', compact('user'));
            }

        } else {
	        /**
	         * User tidak punya role, cek jika user sudah ada didatabase atau tidak
	         */
	        $cek2 = User::where('email', $email)->first();
	        if ($cek2 && $cek2->count() > 0) {
	        	/** Cek apakah user punya SID atau tidak *
		         *  Tambahkan Role untuk user tersebut
		         */
		        if ($cek2->is_valid_sid == 1) {
			        $roleUser = Role::where('name', 'member')->first();
			        $cek2->attachRole($roleUser);
		        } else {
			        $roleUser = Role::where('name', 'register')->first();
			        $cek2->attachRole($roleUser);
			
		        }
	        }
        	
            return redirect()->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors([
                            $this->username() => Lang::get('auth.email'),
                        ]);
        } 
        
    }

    /**
    * Logout and then redirect
    */
    public function logout(Request $request)
    {
        // delete cookies
        foreach($_COOKIE AS $key => $value) {
            SETCOOKIE($key,$value,TIME()- 3600,"/",\Config::get('APP_URL'));
        }

        $this->performLogout($request);
        return redirect()->route('login');
    }
}
