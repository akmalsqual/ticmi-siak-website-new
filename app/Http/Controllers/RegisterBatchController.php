<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;
use DB;
use Auth;
use Crypt;
use Mail;
use File;
use App\Profesi;
use App\Models\Batch;
use App\Models\Perusahaan;
use App\Models\Lembaga;
use App\Models\Lokasi;
use App\Models\Jenis_kelamin;
use App\Models\Negara;
use App\Models\Profile_peserta;
use App\Models\Peserta;
use App\Models\Voucher_manag;
use App\Models\Vcr_manag_prshn;
use App\Models\Tagihan;
use App\Models\Tagihan_tmp;
use App\Models\Pendaftaran;
use App\Models\Virtual_Account;
use App\Models\Pendaftaran_persyaratan;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;
use App\Models\Bayar_manual;
use App\Http\Requests\CompleteProfileRequest;
use App\Http\Requests\Daftar_batchRequest;

class RegisterBatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($batch_id)
    {
        $batch_id           = Crypt::decrypt($batch_id);
        $peserta_id         = Auth::user()->peserta_id;
        
        $batch              = Batch::FindOrFail($batch_id);
        $peserta            = Peserta::leftJoin('profile_peserta', function($join) {
                                $join->on('peserta.peserta_id', '=', 'profile_peserta.peserta_id');
                            })->leftJoin('sex', function($join) {
                                $join->on('profile_peserta.sex_id', '=', 'sex.sex_id');
                            })->where('peserta.peserta_id', $peserta_id)->first();
        $tgl_lahir          = \Helper::date_formats($peserta->tanggal_lahir, 'view');
        
        $check_peserta      = ( empty($peserta->profile_peserta_id) || empty($peserta->alamat_tinggal_skr) || empty($peserta->no_hp) || 
                                empty($peserta->tempat_lahir) || empty($peserta->alamat_ktp) || empty($peserta->alamat_tinggal_skr) || 
                                empty($peserta->photo) || empty($peserta->lokasi_id) || empty($peserta->lembg_pdkn_id) || 
                                empty($peserta->negara_id) || empty($peserta->pendidikan) || empty($peserta->jurusan) || 
                                empty($peserta->universitas) || empty($peserta->lembaga_id) || empty($peserta->profesi_id) || 
                                (empty($peserta->perusahaan_id) && empty($peserta->perusahaan_lainnya)) || empty($peserta->pekerjaan)) 
                                ? FALSE:TRUE;
        if($check_peserta === FALSE){
            
        }
        
        return view('register_batch.register-before', compact('batch_id', 'batch', 'peserta', 'tgl_lahir'));
    }

    public function daftar($batch_id)
    {
        $batch_id           = Crypt::decrypt($batch_id);
        $peserta_id         = Auth::user()->peserta_id;

        $cek_pendaftaran    = DB::table('pendaftaran')->where('batch_id', $batch_id)->where('peserta_id', $peserta_id)->first();
        if($cek_pendaftaran){
          return redirect('home')->with('alert', 'Anda telah terdaftar dalam batch tersebut!');
        }

        $batch              = Batch::FindOrFail($batch_id);
        $peserta            = Peserta::leftJoin('profile_peserta', function($join) {
                                $join->on('peserta.peserta_id', '=', 'profile_peserta.peserta_id');
                            })->leftJoin('sex', function($join) {
                                $join->on('profile_peserta.sex_id', '=', 'sex.sex_id');
                            })->where('peserta.peserta_id', $peserta_id)->first();
        $tgl_lahir          = \Helper::date_formats($peserta->tanggal_lahir, 'view');
        
        // $check_peserta      = ( empty($peserta->profile_peserta_id) || empty($peserta->alamat_tinggal_skr) || empty($peserta->no_hp) || 
        //                         empty($peserta->tempat_lahir) || empty($peserta->alamat_ktp) || empty($peserta->alamat_tinggal_skr) || 
        //                         empty($peserta->photo) || empty($peserta->lokasi_id) || empty($peserta->lembg_pdkn_id) || 
        //                         empty($peserta->negara_id) || empty($peserta->pendidikan) || empty($peserta->jurusan) || 
        //                         empty($peserta->universitas) || empty($peserta->lembaga_id) || empty($peserta->profesi_id) || 
        //                         (empty($peserta->perusahaan_id) && empty($peserta->perusahaan_lainnya)) || empty($peserta->pekerjaan)) 
        //                         ? FALSE:TRUE;

        //$check_peserta      = empty($peserta->profile_peserta_id) ? FALSE:TRUE;

        $status             = DB::table('pendaftaran_status')
                                ->where('hapus', FALSE)
                                ->where('aktif', TRUE)
                                ->pluck('nama', 'pendaftaran_status_id')
                                ->prepend('','');

        $persyaratan        = DB::table('persyaratan')
                                ->whereIn('persyaratan_id', function($q) use($batch){
                                    $q->select('persyaratan_id')
                                      ->from('tipekelas_persyaratan')
                                      ->where('program_id', $batch->program_id);
                                })
                                ->where('hapus', FALSE)
                                ->where('is_aktif', TRUE)
                                ->get();

        $jawaban            = DB::table('jawaban')
                                ->where('hapus', false)
                                ->where('is_aktif', TRUE)
                                ->pluck('nama', 'jawaban_id');
        $program            = DB::table('program')->where('program_id', $batch->program_id)->value('nama');
        $lembg_pdkn         = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $batch->lembg_pdkn_id)->value('nama');
        $tgl_mulai          = \Helper::date_formats($batch->tgl_mulai, 'view');

        // profile peserta
        $profiles           = DB::table('profile_peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
        $sex                = DB::table('sex')->pluck('name', 'sex_id')->prepend('','');
        $lokasi             = DB::table('lokasi')
                                ->where('hapus', false)
                                ->where('aktif', true)
                                ->orderBy('nama')
                                ->pluck('nama', 'lokasi_id')
                                ->prepend('','');

        if(!empty($profiles))
        {
            $cabang_id                 = DB::table('cabang')
                                        ->where('lokasi_id', $profiles->lokasi_id)
                                        ->where('hapus', false)
                                        ->pluck('cabang_id');

            $kp                        = DB::table('lembg_pdkn')
                                        ->where('hapus', false)
                                        ->where('aktif', true)
                                        ->whereIn('cabang_id', $cabang_id)
                                        ->pluck('nama', 'lembg_pdkn_id')
                                        ->prepend('','');
        } else {
            $kp = [];
        }

        // if($check_peserta == true) {
            return view('register_batch.register', compact('batch_id', 'batch', 'peserta', 'tgl_lahir', 'status', 'persyaratan', 'jawaban', 'program', 'lembg_pdkn', 'tgl_mulai', 'profiles', 'sex', 'lokasi', 'kp'));
        // }
        // else {
        //     return redirect(url('profile'));
        // }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cek_voucher(Request $request)
    {
      $data     = $request->data;
      $batch_id = $request->batch_id;
      $today    = date('Y-m-d');
      $voucher  = DB::table('voucher')
                  ->where('tgl_awal', '<=', $today)
                  ->where('tgl_akhir', '>=', $today)
                  ->where('kode_voucher', $data)
                  ->where('aktif', TRUE)
                  ->where('hapus', false)
                  ->first();
      if($voucher){
        $vcr_dtl  = DB::table('voucher_detail')->where('voucher_id', $voucher->voucher_id)->first();
        // DB::beginTransaction();
        // $success_trans = false;

        // try
        // {
        if($vcr_dtl->batch_id == $batch_id)
        {
          /** jika voucher perusahaan **/
          $vcr_perusahaan = DB::table('voucher_perusahaan')->where('voucher_id', $voucher->voucher_id)->where('hapus', false)->first();
          
          if($vcr_perusahaan) { // if voucher perusahaan
            $perusahaan_peserta = DB::table('profile_peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
            /** jika voucher perusahaan sama dengan perusahaan di profile peserta **/
            if($perusahaan_peserta->perusahaan_id == $vcr_perusahaan->perusahaan_id){
                /** cek apakah voucher sudah digunakan **/
              $used_vcr = Vcr_manag_prshn::where('voucher_perusahaan_id', $vcr_perusahaan->voucher_perusahaan_id)
                                          ->where('peserta_id', Auth::user()->peserta_id)
                                          ->count();

              /** cek apakah jumlah voucher sudah digunakan melebihi batas**/
              $limit_vcr = Vcr_manag_prshn::where('voucher_perusahaan_id', $vcr_perusahaan->voucher_perusahaan_id)
                                           ->count();

              if($limit_vcr >= $voucher->qty){
                return json_encode(['error'=> 'Batas Pemakaian Voucher Telah Habis']);
              }

              if($used_vcr > 0) {
                return json_encode(['error'=> 'Anda Telah Menggunakan Voucher Ini Sebelumnya']);

              } else {
                if($voucher->total == 0){
                  /** insert to tbl:vcr_manag_prshn **/
                  $ins_vcr                        = new Vcr_manag_prshn;
                  $ins_vcr->voucher_perusahaan_id = $vcr_perusahaan->voucher_perusahaan_id;
                  $ins_vcr->peserta_id            = Auth::user()->peserta_id;
                  $ins_vcr->save();

                  return json_encode(['success'=> $voucher, 'status_bayar' => 'gratis', 'tipe' => 'perusahaan', 'idInserted' => $ins_vcr->vcr_manag_prshn_id ]); // return data voucher dan status bayar gratis
                } else {
                  /** insert to tbl:vcr_manag_prshn **/
                  $ins_vcr                        = new Vcr_manag_prshn;
                  $ins_vcr->voucher_perusahaan_id = $vcr_perusahaan->voucher_perusahaan_id;
                  $ins_vcr->peserta_id            = Auth::user()->peserta_id;
                  $ins_vcr->save();

                  return json_encode(['success'=> $voucher, 'tipe' => 'perusahaan', 'idInserted' => $ins_vcr->vcr_manag_prshn_id ]); // return data voucher dan status bayar gratis
                }
              }
              
            } else {
              return json_encode(['error'=>'Anda menggunakan voucher yang tidak sesuai dengan perusahaan pada profile Anda']);
            } // end if profile perusahaan peserta == perusahaan voucher

          /** jika voucher perorangan **/
          } else {
            /** cek apakah voucher sudah digunakan **/
            $used_vcr = Voucher_manag::where('voucher_detail_id', $vcr_dtl->voucher_detail_id)
                                         ->where('peserta_id', Auth::user()->peserta_id)
                                         ->count();

            $limit_vcr = Voucher_manag::where('voucher_detail_id', $vcr_dtl->voucher_detail_id)
                                       ->count();

            if($limit_vcr >= $voucher->qty){
              return json_encode(['error'=> 'Batas Pemakaian Voucher Telah Habis']);
            
            }

            if($used_vcr > 0) {
              return json_encode(['error'=>'Anda Telah Menggunakan Voucher Ini Sebelumnya']);
            } else {
              if($voucher->total == 0){
                /** insert to tbl:voucher_manag **/
                $ins_vcr                    = new Voucher_manag;
                $ins_vcr->voucher_detail_id = $vcr_dtl->voucher_detail_id;
                $ins_vcr->peserta_id        = Auth::user()->peserta_id;
                $ins_vcr->save();

                return json_encode(['success'=> $voucher, 'status_bayar' => 'gratis', 'tipe' => 'personal', 'idInserted' => $ins_vcr->voucher_manag_id]); // return data voucher tanpa va
              } else {
                /** insert to tbl:voucher_manag **/
                $ins_vcr                    = new Voucher_manag;
                $ins_vcr->voucher_detail_id = $vcr_dtl->voucher_detail_id;
                $ins_vcr->peserta_id        = Auth::user()->peserta_id;
                $ins_vcr->save();

                return json_encode(['success'=> $voucher, 'tipe' => 'personal', 'idInserted' => $ins_vcr->voucher_manag_id]); // return data voucher
              }
            } // end if voucher used  
          } // end if voucher perorangan
        } else {
          return json_encode(['error'=>'Voucher ini tidak dapat digunakan pada batch ini']);
        } // end if voucher batch_id != batch yang sedang didaftarkan
      } else {
        return json_encode(['error'=>'Voucher Tidak Terdaftar']); 
      }

    }


    public function save_pendaftaran_batch(Daftar_batchRequest $request, $batch_id)
    {
      DB::beginTransaction();
      $success_trans = false;

      try{
          /** UPDATE PROFILE PESERTA **/
          $peserta_id = Auth::user()->peserta_id;
          $profiles = Profile_peserta::where('peserta_id', $peserta_id)->first();
          if ($request->hasFile('foto')) {
                $image             = $request->file('foto');
                $imagename         = 'PHOTO_'.$peserta_id. '_' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath   = public_path('assets/upload_files/peserta/photos');

                if($profiles) {
                  $old_foto   = $profiles->photo;
                } else {
                  $old_foto   = null;
                }
                
            } else {
                $imagename         = !empty($profiles->photo) ? $profiles->photo : ''; 
            }

            if ($request->hasFile('ktp')) {
                $ktp             = $request->file('ktp');
                $ktpname         = 'KTP_'.$peserta_id. '_' . time() . '.' . $ktp->getClientOriginalExtension();
                $destinationKtp  = public_path('assets/upload_files/peserta/ktp');
                if($profiles) {
                  $old_ktp   = $profiles->file_ktp;
                } else {
                  $old_ktp   = null;
                }
            } else {
                $ktpname         = !empty($profiles->file_ktp) ? $profiles->file_ktp : ''; 
            }
            // update tbl:peserta
            DB::table('peserta')->where('peserta_id', $peserta_id)
                                ->update(['nama'          => $request->nama,
                                          'no_hp'         => $request->no_hp,
                                          'tanggal_lahir' => \Helper::date_formats($request->tgl_lahir, 'db')
                                         ]);
            // if peserta has profile do update else insert new data tbl:profile_peserta
            if($profiles) {
              Profile_peserta::where('peserta_id', $peserta_id)
                          ->update(
                          ['sex_id'            => $request->sex,
                          'tempat_lahir'       => $request->tempat_lahir,
                          'alamat_ktp'         => $request->alamat_ktp,
                          'lokasi_id'          => $request->lokasi,
                          'lembg_pdkn_id'      => $request->kp,
                          'photo'              => $imagename,
                          'file_ktp'           => $ktpname
                        ]
                    );                
            } else {
              $profile_peserta                     = new Profile_peserta;
              $profile_peserta->peserta_id         = $peserta_id;
              $profile_peserta->sex_id             = $request->sex;
              $profile_peserta->tempat_lahir       = $request->tempat_lahir;
              $profile_peserta->alamat_ktp         = $request->alamat_ktp;
              $profile_peserta->lokasi_id          = $request->lokasi;
              $profile_peserta->lembg_pdkn_id      = $request->kp;
              $profile_peserta->photo              = $imagename;
              $profile_peserta->file_ktp           = $ktpname;
              $profile_peserta->save();
            }

            //* END UPDATE PROFILE *//

          /* create data */
          $batch           = DB::table('batch')->where('batch_id', $batch_id)->first();
          $peserta         = DB::table('peserta')->where('peserta_id', $peserta_id)->first();
          $profile_peserta = DB::table('profile_peserta')->where('peserta_id', $peserta->peserta_id)->first();

          if($request->status_bayar == 'gratis')
          {
            $va_id = DB::table('voucher_perusahaan')->where('voucher_id', $request->voucher_id)->value('va_id');
            if(!empty($va_id)){ // jika menggunakan voucher perusahaan
              $va    = DB::table('virtual_account')->where('va_id', $va_id)->first();
            } else {
              // menggunakan voucher personal dengan potongan hingga menjadi gratis
              $va    = null;
            }
          } else {
            $va    = \Helper::get_available_va($batch->program_id, $batch->tipe_kelas_id);
          }
          if(!empty($request->status)){
            $status_id   = $request->status;
            $status_nama = DB::table('pendaftaran_status')->where('pendaftaran_status_id', $status_id)->value('nama');
          } else {
            $status_id = null;
            $status_nama = null;
          }
          /* data voucher */
          if(!empty($request->voucher_id)){
            $voucher_id = $request->voucher_id;
            $vcr        = DB::table('voucher')->where('voucher_id', $voucher_id)->first();
            $vcr_dtl    = DB::table('voucher_detail')->where('voucher_id', $voucher_id)->first();
            $vcr_perusahaan = DB::table('voucher_perusahaan')->where('voucher_id', $voucher_id)->first();
            /* cek apakah voucher perusahaan */
            // if($vcr_perusahaan){
            //   /* insert to tbl:vcr_manag_prshn */
            //   $ins_vcr                        = new Vcr_manag_prshn;
            //   $ins_vcr->voucher_perusahaan_id = $vcr_perusahaan->voucher_perusahaan_id;
            //   $ins_vcr->peserta_id            = $peserta->peserta_id;
            //   $ins_vcr->save();
            // } else {
            //   /* insert to tbl:voucher_manag */
            //   $ins_vcr                    = new Voucher_manag;
            //   $ins_vcr->voucher_detail_id = $vcr_dtl->voucher_detail_id;
            //   $ins_vcr->peserta_id        = $peserta->peserta_id;
            //   $ins_vcr->save();
            // }
          }

          /** create invoice **/  
          $year              = substr(date('Y'), 2);
          $month             = date('m');
          $date              = date('d');
          $now               = $year.$month;
          $last_invoice      = Pendaftaran::latest('date_created')->value('no_invoice');
          $last_date_invoice = substr($last_invoice, 2, -6);
          $last_counter      = substr($last_invoice, -4);
          $new_counter       = str_pad($last_counter + 1, 4, 0, STR_PAD_LEFT);

          if($now != $last_date_invoice){
            $no_invoice = 'ST'.$now.$date.'0000';
          } else {
            $no_invoice = 'ST'.$now.$date.$new_counter;
          }

          /** get data syarat kelas **/ 
          $status = $request->status;
          if($status == 2 || $status == 3){ // jika status pendaftar mahasiswa atau dosen
            $persyaratan = DB::table('tipekelas_persyaratan')->where('program_id', $batch->program_id)->get();
            foreach($persyaratan as $syarat){
              $jawaban_id  = $request->input($syarat->persyaratan_id);
              $jawaban     = DB::table('jawaban')->where('jawaban_id', $jawaban_id)->first();
              $data_syarat = DB::table('persyaratan')->where('persyaratan_id', $syarat->persyaratan_id)->first();

              // insert jawaban persyaratan to tbl:pendaftaran_persyaratan
              $ins_syarat                           = new Pendaftaran_persyaratan;
              $ins_syarat->profile_peserta_id       = $profile_peserta->profile_peserta_id;
              $ins_syarat->tipekelas_persyaratan_id = $syarat->tipekelas_persyaratan_id;
              $ins_syarat->persyaratan_nama         = $data_syarat->nama;
              $ins_syarat->jawaban_nama             = $jawaban->nama;
              $ins_syarat->save();
            }
          } 

          /** create data for sending email **/
          $harga_pendaftaran = \Helper::number_formats($request->total, 'db', 0);
          $program           = DB::table('program')->where('program_id', $batch->program_id)->value('nama');
          $kp                = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $batch->lembg_pdkn_id)->value('nama');
          if(!empty($request->voucher_id)){
            $voucher       = DB::table('voucher')->where('voucher_id', $request->voucher_id)->first();
            $nama_voucher  = $voucher->nama;
            $nilai_voucher = \Helper::number_formats($voucher->total, 'view', 0);
          } else {
            $nama_voucher  = ' - ';
            $nilai_voucher = ' - ';
          }
          if(!empty($status)){
            $status       = DB::table('pendaftaran_status')->where('pendaftaran_status_id', $request->status)->first();
            $nama_status  = $status->nama;
          } else {
            $nama_status  = ' - ';
          }

          /** insert to tbl:pendaftaran **/
           $pendaftaran                          = new Pendaftaran;
           $pendaftaran->batch_id                = $batch_id;
           $pendaftaran->peserta_id              = $peserta->peserta_id;
           $pendaftaran->va_id                   = $va !== null ? $va->va_id : null;
           $pendaftaran->account_no              = $va !== null ? $va->account_no : null;
           $pendaftaran->voucher_id              = !empty($request->voucher_id) ? $request->voucher_id : null;
           $pendaftaran->tgl_daftar              = date('Y-m-d H:i:s');
           $pendaftaran->harga_pendaftaran       = $harga_pendaftaran;
           $pendaftaran->no_invoice              = $no_invoice;
           $pendaftaran->pendaftaran_status_id   = $status_id;
           $pendaftaran->pendaftaran_status_nama = $status_nama;

           if ($request->hasFile('ijazah')) {
              $ijazah                       = $request->file('ijazah');
              $ijazahname                   = "IJAZAH_".$peserta_id."_".time().'.'.$ijazah->getClientOriginalExtension();
              $pendaftaran->file_ijazah_ori = $ijazah->getClientOriginalName();
              $pendaftaran->file_ijazah     = $ijazahname;
              $destinationijazah            = public_path('/assets/upload_files/peserta/ijazah');
           }

           $pendaftaran->no_reg_wppe  = !empty($request->no_reg_wppe) ? $request->no_reg_wppe : null;
           $pendaftaran->date_created = date('Y-m-d H:i:s');
           $pendaftaran->user_created = $peserta->peserta_id;
           $pendaftaran->save();
           $harga_pendaftaran = $pendaftaran->harga_pendaftaran;

           // if($va !== null){
           //    // update status tbl virtual account
           //    $va                 = Virtual_Account::find($va->va_id);
           //    $va->account_status = 1;
           //    $va->save();      
           // }     

           $pendaftaran_id = $pendaftaran->pendaftaran_id;
           /** insert to tbl:tagihan **/
           $tagihan                    = new Tagihan;
           $tagihan->pendaftaran_id    = $pendaftaran_id;
           $tagihan->nama_batch        = $batch->nama;
           $tagihan->no_batch          = $batch->no_batch;
           $tagihan->batch_id          = $batch->batch_id;
           $tagihan->account_no        = $va !== null ? $va->account_no : null;
           $tagihan->no_invoice        = $no_invoice;
           $tagihan->peserta_id        = $peserta->peserta_id;
           $tagihan->nama_peserta      = $peserta->nama;
           $tagihan->harga_pendaftaran = $batch->harga;
           $tagihan->nilai_voucher     = !empty($request->voucher_id) ? \Helper::number_formats($nilai_voucher, 'db', 0) : null;
           $tagihan->voucher_id        = !empty($request->voucher_id) ? $request->voucher_id : null;
           $tagihan->total_tagihan     = $harga_pendaftaran;
           $tagihan->tgl_daftar        = date('Y-m-d H:i:s');
           $tagihan->date_created      = date('Y-m-d H:i:s');
           $tagihan->user_created      = Auth::user()->peserta_id;
           $tagihan->save();
           $tagihan_id = $tagihan->tagihan_id;

           /** insert to tbl:tagihan_tmp **/
           $tagihan_tmp                    = new Tagihan_tmp;
           $tagihan_tmp->tagihan_id        = $tagihan_id;
           $tagihan_tmp->pendaftaran_id    = $pendaftaran_id;
           $tagihan_tmp->nama_batch        = $batch->nama;
           $tagihan_tmp->no_batch          = $batch->no_batch;
           $tagihan_tmp->batch_id          = $batch->batch_id;
           $tagihan_tmp->account_no        = $va !== null ? $va->account_no : null;
           $tagihan_tmp->no_invoice        = $no_invoice;
           $tagihan_tmp->peserta_id        = $peserta->peserta_id;
           $tagihan_tmp->nama_peserta      = $peserta->nama;
           $tagihan_tmp->harga_pendaftaran = $batch->harga;
           $tagihan_tmp->voucher_id        = !empty($request->voucher_id) ? $request->voucher_id : null;
           $tagihan_tmp->nilai_voucher     = !empty($request->voucher_id) ? \Helper::number_formats($nilai_voucher, 'db', 0) : null;
           $tagihan_tmp->total_tagihan     = $harga_pendaftaran;
           $tagihan_tmp->saldo             = $harga_pendaftaran;
           $tagihan_tmp->tgl_daftar        = date('Y-m-d H:i:s');
           $tagihan_tmp->date_created      = date('Y-m-d H:i:s');
           $tagihan_tmp->user_created      = Auth::user()->peserta_id;
           $tagihan_tmp->save();

          if($request->status_bayar == 'gratis')
          {
            $tagihan_tmp                  = Tagihan_tmp::findOrFail($tagihan_tmp->tagihan_tmp_id);
            $tagihan_tmp->saldo           = 0;
            $tagihan_tmp->status_bayar = 2; // SUKSES (uang yang ditransfer sesuai dengan tagihan)
            $tagihan_tmp->save();

            /** insert tbl:bayar_manual **/
            $bayar_manual                 = new Bayar_manual;
            $bayar_manual->tagihan_tmp_id = $tagihan_tmp->tagihan_tmp_id;
            $bayar_manual->nominal        = NULL;
            $bayar_manual->no_rekening    = NULL;
            $bayar_manual->nama_rekening  = NULL;
            $bayar_manual->tgl_transfer   = NULL;
            $bayar_manual->jam_transfer   = NULL;
            $bayar_manual->bukti_transfer = NULL;
            $bayar_manual->is_comp        = TRUE;
            $bayar_manual->date_created   = date('Y-m-d H:i:s');
            $bayar_manual->user_created   = \Auth::user()->id;
            $bayar_manual->hapus          = false;
            $bayar_manual->save();
          }


          DB::commit();
          $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }
        /** jika berhasil save ke database **/
        if ($success_trans == true) {
          /* store file foto */
          if(isset($destinationPath)) {
            if($old_foto !== null) {
                File::delete($destinationPath.'/'.$old_foto);  // delete old file
            }
            $image->move($destinationPath, $imagename);
          }
          /* store file ktp */
          if(isset($destinationKtp)) {
            if($old_ktp !== null) {
                File::delete($destinationKtp.'/'.$old_ktp);  // delete old file
            }
            $ktp->move($destinationKtp, $ktpname);
          }
          /* store file ijazah */
          if ($request->hasFile('ijazah')) {
              $ijazah->move($destinationijazah, $ijazahname);
           }

           /** sending email **/
          $header          = 'email-template.core.header';
          $footer          = 'email-template.core.footer';
          $config          = Config_email::first();
          $konfirmasi      = Konfirmasi::find(12); // Konfirmasi Pendaftaran Program Sertifikasi
          /** send email **/
          Mail::send('email-template.email-register-batch', 
              [   'header'        => $header, 
                  'footer'        => $footer, 
                  'config'        => $config, 
                  'konfirmasi'    => $konfirmasi, 
                  'batch'         => $batch->nama,
                  'program'       => $program,
                  'nama_peserta'  => $peserta->nama,
                  'email_peserta' => $peserta->email,
                  'kp'            => $kp,
                  'tgl_daftar'    => date('d-m-Y'),
                  'no_invoice'    => $no_invoice,
                  'voucher'       => $nama_voucher,
                  'nilai_voucher' => $nilai_voucher,
                  'va'            => $request->status_bayar == 'gratis' ? '-' : $va->account_no,
                  'status'        => $nama_status,
                  'harga'         => 'Rp '.$request->total,
                  'pembayaran'    => $request->status_bayar == 'gratis' ? '' :
                  '<p style="text-align: justify">Silahkan lakukan pembayaran maksimal 1x24 jam setelah Anda menerima email ini. Jika Anda telah selesai melakukan pembayaran silahkan konfirmasi pembayaran pada link dibawah ini.</p></td></tr><tr> <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner"> <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;"> <tbody> <tr> <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 16px; padding: 15px;"> <a class="mcnButton " title="Hadir" href='.route("konfirmasi.index").' target="_blank" style="font-weight: bold;letter-spacing: 2px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Konfirmasi Pembayaran</a> </td></tr></tbody> </table> </td></tr></tbody> </table><!--[if mso]> </td><![endif]--><!--[if mso]> </tr></table> <hr> <p style="text-align: left;font-family: " trebuchet="" ms",="" "lucida="" grande",="" sans="" unicode",="" sans",="" tahoma,="" sans-serif;margin:="" 10px="" 0;padding:="" 0;mso-line-height-rule:="" exactly;-ms-text-size-adjust:="" 80%;-webkit-text-size-adjust:="" 80%;color:="" #202020;font-size:="" 12px;line-height:="" 150%;"=""> Jika Anda mempunyai pertanyaan silahkan hubungi : <br><strong style="color: red;">The Indonesia Capital Market Institute (TICMI)</strong> <br>Gedung Bursa Efek Indonesia Tower 2 Lantai 1 <br>Jl. Jend Sudirman Kav 52-53 Jakarta Selatan <br>Call Center : 0800 100 900 (bebas pulsa) <br>Telepon : 021 515 2318 <br>Email : info@ticmi.co.id </p></td></tr></tbody></table><!--[if mso]> </td><![endif]--><!--[if mso]> </tr></table><![endif]--> </td></tr></tbody> </table></td></tr></tbody></table></td></tr>'
              ],
              function ($message) use ($peserta){
                  $message->subject('TICMI - Pendaftaran Pelatihan');
                  $message->from('mg@ticmi.co.id', 'TICMI');
                  $message->to($peserta->email);
              }
          );

          return redirect()->route('registration-success');
        }
    }

    public function registrationSuccess()
    {
      return view('register_batch.registration-success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompleteProfileRequest $request)
    {
        DB::beginTransaction();
        $success_trans = false;

        $peserta_id = Auth::user()->peserta_id;

        try{
            if($request->hasFile('photo'))
            {
                $file = $request->file('photo');

                $extension = $file->getClientOriginalExtension();
                $destination = public_path('assets/upload_files/peserta/photos');
                $file_name = 'PHOTO'.'_'.$peserta_id.'_'.time().'.'.$extension;

                $file->move($destination, $file_name);
            }

            $profile = Profile_peserta::create([
                'peserta_id' => Auth::user()->peserta_id,
                'perusahaan_id' => $request->perusahaan_id,
                'lembaga_id' => $request->lembaga_id,
                'lokasi_id' => $request->lokasi_id,
                'sex_id' => $request->sex_id,
                'negara_id' => $request->negara_id,
                'alamat_ktp' => $request->alamat_ktp,
                'pekerjaan' => $request->pekerjaan,
                'jurusan' => $request->jurusan,
                'universitas' => $request->universitas,
                'tempat_lahir' => $request->tempat_lahir,
                'tlp_rumah' => $request->tlp_rumah,
                'pendidikan' => $request->pendidikan,
                'keterangan' => $request->keterangan,
                'perusahaan_lainnya' => $request->perusahaan_lainnya,
                'lembg_pdkn_id' => $request->lembg_pdkn_id,
                'alamat_tinggal_skr' => $request->alamat_tinggal_skr,
                'photo' => $file_name
            ]);

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }

        if ($success_trans == true) {
            echo 'konfirmasi pembayaran';
        }
    }

    public function ajax_lembg_pdkn(Request $request)
    {
        $lokasi = $request->lokasi;

        $lembg_pdkn = DB::table('lokasi')
                      ->join('cabang', 'lokasi.lokasi_id', '=', 'cabang.lokasi_id')
                      ->join('lembg_pdkn', 'lembg_pdkn.cabang_id', 'cabang.cabang_id')
                      ->select('lembg_pdkn.lembg_pdkn_id', 'lembg_pdkn.nama')
                      ->where('lokasi.lokasi_id', $lokasi)
                      ->get();

        return response()->json([
            'lembaga'=>$lembg_pdkn
        ]);
    }

    public function reset_voucher(Request $request)
    {
      if ($request->ajax()) {
        $type        = $request->type;
        $id_inserted = $request->idInserted;

        if($type == 'personal'){
          $trans = Voucher_manag::destroy($id_inserted);
        } elseif($type == 'perusahaan'){
          $trans = Vcr_manag_prshn::destroy($id_inserted);
        }

        if($trans){
          return response()->json('success');
        } else {
          return response()->json('failed');
        }
        
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
