<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use JsValidator;
use Lang;
use App\Helpers\Error_form;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Peserta;
use App\Http\Requests\PasswordRequest;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('password.password_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordRequest $request, $peserta_id)
    {
        DB::beginTransaction();
        $success_trans = false;
        // @update data tbl:peserta

        try {
            $peserta = Peserta::findOrFail($peserta_id);
            // password not empty, then update password
            if(!empty($request->password)){
                $peserta->password = Hash::make($request->password);
                $peserta->save();
            }

            DB::commit();
	        $res = \DB::connection('mysql_elearning')->table('mdl_user')->where('id',$peserta->moodle_user_id)->update(['password'=>$peserta->password]);
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            //abort(403, $e->getMessage());
            Error_Form::setAlert('message', Lang::get('db.failed_updated'), 'alert-class', 'alert-danger');
            return redirect()->route('password.index');
        }

        if ($success_trans == true) {
            Error_Form::setAlert('message', Lang::get('db.updated'), 'alert-class', 'alert-success');
            return redirect()->route('password.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
