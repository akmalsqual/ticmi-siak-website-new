<?php

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Jobs\SendEmailPendaftaranOnlineClass;
use App\Models\Peserta;
use App\Models\ticmi\Admisi;
use App\Models\ticmi\JadwalKelasOnline;
use Log;
use App\Models\Batch;
use App\Models\Perusahaan;
use App\Models\ticmi\Kabupaten;
use App\Models\ticmi\PesertaWorkshop;
use App\Models\ticmi\Provinsi;
use App\Models\ticmi\VirtualAccount;
use App\Models\ticmi\Workshop;
use Illuminate\Http\Request;
use App\Http\Requests;

class KelasOnlineController extends Controller
{
	
	
	public function index(Request $request, Workshop $workshop)
	{
		if ($workshop->is_publish == 1) {
			$pageTitle  = $workshop->workshop_name;
			$pelatihan  = $workshop;
			$id         = $workshop->t_program_id;
			$batch_path = \Config::get('app.url_siak') . '/public/upload_files/images/logo_batch/';
			$otherbatch = Batch::where('aktif', TRUE)
				->where('hapus', FALSE)
				->where('is_publish', TRUE)
				->where('is_hidden', FALSE)
				->where('is_pelatihan', FALSE)
				->where('tgl_pendaftaran_close', '>', date('Y-m-d H:i:s'))->get();
			
			
			return view('kelasonline.index', compact('otherbatch', 'pelatihan', 'pageTitle', 'batch', 'batch_path'));
		} else {
			abort(404);
		}
	}
	
	public function lokasiUjian()
	{
		
		$pageTitle = "Kelas Online | Lokasi Ujian";
		return view('kelasonline.lokasi-ujian', compact('pageTitle'));
	}
	
	public function FormUjianstore1(Request $request, Workshop $workshop)
	{
		
		$this->validate($request, [
			
			'jadwal_kelas_online_id' => 'required',
			'foto'                   => 'required | mimes:jpeg,jpg,png | max:1000',
			'tempat_lahir'           => 'required',
			'jurusan'                => 'required',
			'alamat'                 => 'required',
			'pendidikan'             => 'required',
			'status_pendaftar'       => 'required',
			'kelembagaan'            => 'required',
			'file_pendukung'         => 'required|mimes:pdf|max:10000'
		
		]);
		
		$FormUjianRequest = new Admisi($request->all());
		if ($workshop->t_kelas_id == 2) {
			$FormUjianRequest->is_payment = 0;
		}
		$FormUjianRequest->tgl_daftar = date('Y-m-d H:i:s');
		$FormUjianRequest->aktif      = 'n';
		// dd($FormUjianRequest);
		Peserta::where('email', $FormUjianRequest->email)->update(['tanggal_lahir' => $FormUjianRequest->tgl_lahir]);
		if ($request->hasFile('foto')) {
			
			
			if ($request->file('foto')->isValid()) {
				
				$image    = $request->file('foto');
				$filename = $FormUjianRequest->foto . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/sertifikasi/foto/');
				$request->file('foto')->move($uploadPath, $filename);
				
				$FormUjianRequest->foto = $filename;
				
			}
		}
		
		if ($request->hasFile('file_pendukung')) {
			
			if ($request->file('file_pendukung')->isValid()) {
				
				$image    = $request->file('file_pendukung');
				$filename = $FormUjianRequest->file_pendukung . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/sertifikasi/file_pendukung/');
				$request->file('file_pendukung')->move($uploadPath, $filename);
				
				$FormUjianRequest->file_pendukung = $filename;
				
			}
		}
		
		
		$admisi = Admisi::where('email', auth()->user()->email)->where('workshop_id', $workshop->id)
			->where('kd_program', $workshop->t_program_id)->where('nip', '!=', '0')->first();
		
		if ($admisi && $admisi->count() > 0) {
			flash()->error('Email yang anda gunakan sudah terdaftar di kelas ini' . auth()->user()->email);
			return redirect()->back();
		} else {
			$FormUjianRequest->save();
			$workshopslug = $request->input('workshopslug');
			
			$pesertaWorkshop = new PesertaWorkshop($request->all());
			
			
			$slug = ['workshopslug' => $workshopslug, 'pesertaWorkshop' => $pesertaWorkshop->workshop_id];
			if ($request->t_kelas_id == 2) {
				return redirect()->route('kelasonline.form.validasi', $slug);
			} else {
				return redirect()->route('kelasonline.checkout', ['workshopslug' => $workshopslug]);
			}
		}
		
		
	}
	
	public function FormUjianstore(Request $request, Workshop $workshop)
	{
		if ($workshop->t_kelas_id == 2) {
			$this->validate($request, [
				
				'foto'             => 'required | mimes:jpeg,jpg,png | max:1000',
				'tempat_lahir'     => 'required',
				'jurusan'          => 'required',
				'alamat'           => 'required',
				'pendidikan'       => 'required',
				'status_pendaftar' => 'required',
				'kelembagaan'      => 'required',
				'file_pendukung'   => 'required|mimes:pdf|max:10000'
			
			]);
		} else {
		
		
		}
		$FormUjianRequest = new Admisi($request->all());
		
		
		Peserta::where('peserta_id', $FormUjianRequest->id)->update(['tanggal_lahir' => $FormUjianRequest->tgl_lahir]);
		// dd($FormUjianRequest);
		if ($request->hasFile('foto')) {
			
			
			if ($request->file('foto')->isValid()) {
				
				$image    = $request->file('foto');
				$filename = $FormUjianRequest->foto . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/sertifikasi/foto/');
				$request->file('foto')->move($uploadPath, $filename);
				
				$FormUjianRequest->foto = $filename;
				
			}
		}
		
		if ($request->hasFile('file_pendukung')) {
			
			if ($request->file('file_pendukung')->isValid()) {
				
				$image    = $request->file('file_pendukung');
				$filename = $FormUjianRequest->file_pendukung . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/sertifikasi/file_pendukung/');
				$request->file('file_pendukung')->move($uploadPath, $filename);
				
				$FormUjianRequest->file_pendukung = $filename;
				
			}
		}
		
		
		$FormUjianRequest->save();
		$workshopslug = $request->input('workshopslug');
		
		
		return redirect()->route('kelasonline.checkout', ['workshopslug' => $workshopslug]);
		
	}
	
	public function form(Request $request, Workshop $workshop)
	{
		// dd($workshop);
		$agama   = [
			'Islam'   => 'Islam',
			'Katolik' => 'Katolik',
			'Kristen' => 'Kristen',
			'Hindu'   => 'Hindu',
			'Budha'   => 'Budha',
			'Lainnya' => 'Lainnya'
		];
		$kelas   = $workshop->workshop_name;
		$t_kelas = $workshop->t_kelas_id;
		
		switch ($t_kelas) {
			case 1;
				$tipe = "Reguler";
				break;
			default;
				$tipe = "Waiver";
			
		}
		
		
		if ($t_kelas == 1) {
			if (strpos($kelas, "WPPE") !== false) {
				
				$pendidikan = [
					
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			} elseif (strpos($kelas, "WPEE") !== false) {
				
				$pendidikan = [
					
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			} elseif (strpos($kelas, "WMI") !== false) {
				
				$pendidikan = [
					
					
					'D3' => 'D3',
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} elseif (strpos($kelas, "ASPM") !== false) {
				
				$pendidikan = [
					
					
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} else {
				$pendidikan = [
					
					'SD'  => 'SD',
					'SMP' => 'SMP',
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
				
				
			}
		} else {
			$pendidikan = [
				
				
				'S1' => 'S1',
				'S2' => 'S2',
				'S3' => 'S3'
			];
			
		}
		
		
		$nm_ab    = PerusahaanAnggotaBursa::all();
		$nm_ab    = array_pluck($nm_ab, 'name', 'name');
		$nm_nonab = PerusahaanNonAnggotaBursa::all();
		$nm_nonab = array_pluck($nm_nonab, 'name', 'name');
		
		$perusahaan = [
			
			'1' => 'Anggota Bursa',
			'2' => 'Bukan Anggota Bursa',
			'3' => 'Lainnya'
		];
		
		$status_pendaftar = [
			
			'Mahasiswa'   => 'Mahasiswa',
			'Alumni'      => 'Alumni',
			'Dosen'       => 'Dosen',
			'Karyawan PE' => 'Karyawan PE',
			'Umum'        => 'Umum'
		];
		
		$nm_perusahaan = [];
		$kelembagaan   = [
			'Asset Manajemen' => 'Asset Manajemen',
			'Asuransi'        => 'Asuransi',
			'Bank'            => 'Bank',
			'Universitas'     => 'Universitas',
			'Pemerintah'      => 'Pemerintah',
			'Lainnya'         => 'Lainnya'
		];
		
		$pekerjaan = [
			'Pegawai Swasta'   => 'Pegawai Swasta',
			'Pegawai Negeri'   => 'Pegawai Negeri',
			'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
			'Wiraswasta'       => 'Wiraswasta',
			'Pelajar'          => 'Pelajar',
			'TNI /Polisi'      => 'TNI / Polisi',
			'Pensiunan'        => 'Pensiunan',
			'Guru'             => 'Guru',
			'Lainnya'          => 'Lainnya'
		];
		$pageTitle = 'Form Pendaftaran Sertifikasi Online';
		$peserta   = Peserta::find(auth()->user()->peserta_id);
		return view('kelasonline.form', compact('pageTitle', 'workshop', 'agama', 'pendidikan', 'pekerjaan', 'perusahaan', 'nm_ab', 'nm_nonab', 'kelembagaan', 'nm_perusahaan', 'status_pendaftar', 'peserta'));
	}
	
	
	public function form1(Request $request, Workshop $workshop)
	{
		
		$agama   = [
			'Islam'   => 'Islam',
			'Katolik' => 'Katolik',
			'Kristen' => 'Kristen',
			'Hindu'   => 'Hindu',
			'Budha'   => 'Budha',
			'Lainnya' => 'Lainnya'
		];
		$kelas   = $workshop->workshop_name;
		$t_kelas = $workshop->t_kelas_id;
		
		switch ($t_kelas) {
			case 1;
				$tipe = "Reguler";
				break;
			default;
				$tipe = "Waiver";
			
		}
		
		
		if ($t_kelas == 2) {
			if (strpos($kelas, "WPPE") !== false) {
				$pendidikan = [
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			} elseif (strpos($kelas, "WPEE") !== false) {
				$pendidikan = [
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			} elseif (strpos($kelas, "WMI") !== false) {
				$pendidikan = [
					'D3' => 'D3',
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} elseif (strpos($kelas, "ASPM") !== false) {
				$pendidikan = [
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} else {
				$pendidikan = [
					'SD'  => 'SD',
					'SMP' => 'SMP',
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			}
		} else {
			if (strpos($kelas, "WMI") !== false) {
				$pendidikan = [
					'D3' => 'D3',
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} elseif (strpos($kelas, "ASPM") !== false) {
				$pendidikan = [
					'S1' => 'S1',
					'S2' => 'S2',
					'S3' => 'S3'
				];
			} else {
				$pendidikan = [
					'SMA' => 'SMA',
					'D3'  => 'D3',
					'S1'  => 'S1',
					'S2'  => 'S2',
					'S3'  => 'S3'
				];
			}
		}
		
		
		$nm_ab    = Perusahaan::where('kat_perusahaan_id', 1)->get();
		$nm_ab    = array_pluck($nm_ab, 'name', 'name');
		$nm_nonab = Perusahaan::where('kat_perusahaan_id', 2)->get();
		$nm_nonab = array_pluck($nm_nonab, 'name', 'name');
		
		$perusahaan = [
			
			'1' => 'Anggota Bursa',
			'2' => 'Bukan Anggota Bursa',
			'3' => 'Lainnya'
		];
		
		$status_pendaftar = [
			
			'Mahasiswa'   => 'Mahasiswa',
			'Alumni'      => 'Alumni',
			'Dosen'       => 'Dosen',
			'Karyawan PE' => 'Karyawan PE',
			'Umum'        => 'Umum'
		];
		
		$nm_perusahaan = [];
		$kelembagaan   = [
			'Asset Manajemen' => 'Asset Manajemen',
			'Asuransi'        => 'Asuransi',
			'Bank'            => 'Bank',
			'Universitas'     => 'Universitas',
			'Pemerintah'      => 'Pemerintah',
			'Lainnya'         => 'Lainnya'
		];
		
		$pekerjaan = [
			'Pegawai Swasta'   => 'Pegawai Swasta',
			'Pegawai Negeri'   => 'Pegawai Negeri',
			'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
			'Wiraswasta'       => 'Wiraswasta',
			'Pelajar'          => 'Pelajar',
			'TNI /Polisi'      => 'TNI / Polisi',
			'Pensiunan'        => 'Pensiunan',
			'Guru'             => 'Guru',
			'Lainnya'          => 'Lainnya'
		];
		$pageTitle = 'Form Pendaftaran Sertifikasi Online';
		
		$jadwalKelas    = JadwalKelasOnline::where('is_publish', 1)->whereRaw("tgl_start > '" . date('Y-m-d H:i:s') . "'")->get();
		$arrJadwalKelas = [];
		if ($jadwalKelas && $jadwalKelas->count() > 0) {
			foreach ($jadwalKelas as $item) {
				$arrJadwalKelas[$item->id] = strftime("%d %B %Y", strtotime($item->tgl_start)) . " s/d " . strftime('%d %B %Y', strtotime($item->tgl_end));
			}
		}
		
		$peserta = Peserta::find(auth()->user()->peserta_id);
//		return $peserta;
		return view('kelasonline.form1', compact('tipe', 'pageTitle', 'workshop', 'agama', 'pendidikan', 'pekerjaan', 'perusahaan', 'nm_ab', 'nm_nonab', 'kelembagaan', 'nm_perusahaan', 'status_pendaftar', 'peserta', 'arrJadwalKelas'));
	}
	
	
	public function checkout(Request $request, Workshop $workshop)
	{
		
		if ($workshop && $workshop->count() > 0) {
			if ($workshop->is_publish == 1) {
				$provinsi  = Provinsi::all();
				$provinsi  = array_pluck($provinsi, 'name', 'id');
				$kabupaten = Kabupaten::all();
				$kabupaten = array_pluck($kabupaten, 'name', 'id');
				return view('kelasonline.checkout', compact('workshop', 'provinsi', 'kabupaten'));
			} else {
				return redirect('/');
			}
		} else {
			abort(404);
		}
	}
	
	public function store(Request $request, Workshop $workshop)
	{
		setlocale(LC_TIME, 'id_ID.UTF-8');
		// dd($request->all());
		if ($workshop->t_program_id == 34) {
			$this->validate($request, [
				'nama'              => 'required|max:255',
				//			'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
				'company'           => 'required|max:255',
				'jabatan'           => 'required|max:255',
				'provinsi_id'       => 'required',
				'kabupaten_id'      => 'required',
				'no_sk_ojk'         => 'required|max:64',
				'tgl_sk_ojk'        => 'required|max:64',
				'upload_surat_izin' => 'required|mimes:jpeg,jpg,png,pdf',
				'agree_ol'          => 'required'
			]);
		}
		$pesertaWorkshop = new PesertaWorkshop($request->all());
		
		Log::error($pesertaWorkshop);
		
		$pesertaWorkshop->invoice_no      = CartHelper::getWorkshopInvoice('OL');
		$pesertaWorkshop->virtual_account = CartHelper::getVirtualAccount('041');
		$pesertaWorkshop->urutan          = CartHelper::getUrutanPeserta($workshop);
		$pesertaWorkshop->workshop_id     = $workshop->id;
		$pesertaWorkshop->biaya           = $workshop->biaya;
		$pesertaWorkshop->nama            = ucwords(strtolower($request->get('nama')));
		$pesertaWorkshop->no_sk_ojk       = strtoupper($request->get('no_sk_ojk'));
		
		
		/**
		 * Cek jika ada upload surat izin OJK
		 */
		if ($request->hasFile('upload_surat_izin')) {
			if ($request->file('upload_surat_izin')->isValid()) {
				$image    = $request->file('upload_surat_izin');
				$filename = $pesertaWorkshop->invoice_no . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/surat_izin_ojk/');
				$request->file('upload_surat_izin')->move($uploadPath, $filename);
				/** set bukti transfer */
				$pesertaWorkshop->surat_izin_ojk = $filename;
			}
		}
		
		/**
		 * Check if there's Discount or Promo
		 */
		if (!empty($workshop->jml_peserta_diskon) && empty($workshop->jml_group_referral)) {
			if ($pesertaWorkshop->urutan <= $workshop->jml_peserta_diskon) {
				$pesertaWorkshop->diskon      = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar = $workshop->biaya - $workshop->nominal_diskon;
			} else {
				$pesertaWorkshop->diskon      = 0;
				$pesertaWorkshop->total_bayar = $workshop->biaya;
			}
		} elseif (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
			if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
				$pesertaWorkshop->diskon       = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar  = $workshop->biaya - $workshop->nominal_diskon;
				$pesertaWorkshop->has_referral = 1;
			} else {
				$pesertaWorkshop->total_bayar = $workshop->biaya;
				$pesertaWorkshop->diskon      = 0;
			}
		} else {
			$pesertaWorkshop->diskon      = 0;
			$pesertaWorkshop->total_bayar = $workshop->biaya;
		}
		
		
		if ($pesertaWorkshop->save()) {
			
			/**
			 * Saving Referral if there any
			 */
			if (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
				if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
					foreach (session()->get('referral_promo') as $key => $value) {
						$pesertaWorkshopReferral                  = new PesertaWorkshop();
						$pesertaWorkshopReferral->nama            = $value['nama'];
						$pesertaWorkshopReferral->email           = $value['email'];
						$pesertaWorkshopReferral->no_hp           = $value['no_hp'];
						$pesertaWorkshopReferral->invoice_no      = $pesertaWorkshop->invoice_no;
						$pesertaWorkshopReferral->virtual_account = $pesertaWorkshop->virtual_account;
						$pesertaWorkshopReferral->urutan          = CartHelper::getUrutanPeserta($workshop);
						$pesertaWorkshopReferral->workshop_id     = $workshop->id;
						$pesertaWorkshopReferral->biaya           = $workshop->biaya;
						$pesertaWorkshopReferral->diskon          = $workshop->nominal_diskon;
						$pesertaWorkshopReferral->total_bayar     = $workshop->biaya - $workshop->nominal_diskon;
						$pesertaWorkshopReferral->referral_from   = $pesertaWorkshop->id;
						
						$pesertaWorkshopReferral->save();
					}
				}
			}
			
			/** Send email Notification */
			$job = (new SendEmailPendaftaranOnlineClass($pesertaWorkshop))->delay(3)->onQueue('ticmi');
			$this->dispatch($job);
			/** Removing Session */
			session()->forget('referral_promo');
			flash()->success('Pendaftaran berhasil');
			return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
		} else {
			flash()->error('Pendaftaran gagal, mohon ulangi pendaftaran kembali');
			return redirect()->route('kelasonline.checkout', ['workshopslug' => $workshop->slugs]);
		}
	}
	
	public function informasi(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$pesertaWorkshop;
		$referral = null;
		if ($pesertaWorkshop->has_referral) {
			$referral = PesertaWorkshop::where('referral_from', $pesertaWorkshop->id)->get();
		}
		return view('kelasonline.message', compact('pesertaWorkshop', 'workshop', 'referral'));
	}
	
	public function validasi(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		return view('kelasonline.info', compact('pesertaWorkshop', 'workshop'));
	}
	
	
	public function Showcase(Workshop $workshop)
	{
		return Workshop::where('is_publish', 1)->where('is_online', 1)->get();
		// return $workshop->workshop_name;
		
	}
	
	
}
