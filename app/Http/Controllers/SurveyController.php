<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use DB;
use Auth;
use App\Models\Svy;
use App\Models\Svy_manag;
use App\Models\Svy_pertanyaan;
use App\Models\Svy_tipe_pertanyaan;
use App\Models\Svy_pilihan_jawaban;

class SurveyController extends Controller
{
    public function ajax_survey(Request $request, $id, $pelatihan_aktual_dtl_id)
    {
        // $survey = Svy::where('svy.svy_id',$id)
        //             ->join('svy_kat','svy.svy_id','=','svy.svy_id')
        //             ->join('svy_pertanyaan','svy_pertanyaan.svy_kat_id','=','svy_kat.svy_kat_id')
        //             ->join('svy_tipe_pertanyaan','svy_tipe_pertanyaan.svy_tipe_pertanyaan_id','=','svy_pertanyaan.svy_tipe_pertanyaan_id')
                    //  ->leftJoin('svy_pilihan','svy_pilihan.svy_pertanyaan_id','svy_pertanyaan.svy_pertanyaan_id')
                    // ->join('svy_pilihan_jawaban','svy_pilihan_jawaban.svy_pertanyaan_id','=','svy_pertanyaan.svy_pertanyaan_id')
                    // ->get();
        $survey = Svy::select('nama_survey as nama')->where('svy_id',$id)->first();
        $svy_pertanyaan = Svy_pertanyaan::select('svy_pertanyaan.*')
                                ->join('svy_kat','svy_kat.svy_kat_id','=','svy_pertanyaan.svy_kat_id')
                                ->join('svy','svy.svy_id','=','svy_kat.svy_id')
                                ->where('svy.svy_id',$id)
                                ->whereNull('svy_pertanyaan.deleted_at')
                                ->get();

        $single   = Svy_tipe_pertanyaan::where('nama', 'single')->first();
        $multiple = Svy_tipe_pertanyaan::where('nama', 'multiple')->first();
        $essay    = Svy_tipe_pertanyaan::where('nama', 'essay')->first();
        return view('survey/pertanyaan_survey', compact('survey','svy_pertanyaan','single','multiple','essay', 'pelatihan_aktual_dtl_id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $pelatihan_aktual_dtl_id = $request->pelatihan_aktual_dtl_id;
        $nama_survey = $request->nama_survey;
        $user_id = Auth::user()->peserta_id;
        $pertanyaan_id[] = $request->arr_pertanyaan;
        // $single   = Svy_tipe_pertanyaan::where('nama', 'single')->first();
        // $multiple = Svy_tipe_pertanyaan::where('nama', 'multiple')->first();
        // $essay    = Svy_tipe_pertanyaan::where('nama', 'essay')->first();
        DB::beginTransaction();
        $success_trans = false;

        try {
            if($request->single){
                foreach($request->single as $pertanyaan => $jawaban){
                    $svy_jawaban                          = new Svy_pilihan_jawaban;
                    $svy_jawaban->svy_pertanyaan_id       = $pertanyaan;
                    $svy_jawaban->svy_pilihan_id          = $jawaban;
                    $svy_jawaban->peserta_id              = $user_id;
                    $svy_jawaban->pelatihan_aktual_dtl_id = $pelatihan_aktual_dtl_id;
                    $svy_jawaban->save();
                }
            }
            if($request->multiple){
                foreach($request->multiple as $pertanyaan => $jawaban){
                    foreach($jawaban as $jawab){
                        $svy_jawaban                          = new Svy_pilihan_jawaban;
                        $svy_jawaban->svy_pertanyaan_id       = $pertanyaan;
                        $svy_jawaban->svy_pilihan_id          = $jawab;
                        $svy_jawaban->peserta_id              = $user_id;
                        $svy_jawaban->pelatihan_aktual_dtl_id = $pelatihan_aktual_dtl_id;
                        $svy_jawaban->save();
                    }
                }
            }
            if($request->essay){
                foreach($request->essay as $pertanyaan => $jawaban){
                    $svy_jawaban                          = new Svy_pilihan_jawaban;
                    $svy_jawaban->svy_pertanyaan_id       = $pertanyaan;
                    $svy_jawaban->jawaban                 = $jawaban;
                    $svy_jawaban->peserta_id              = $user_id;
                    $svy_jawaban->pelatihan_aktual_dtl_id = $pelatihan_aktual_dtl_id;
                    $svy_jawaban->save();
                }
            }

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }

        if ($success_trans == true) {
            return redirect()->route('dashboard')->with('success-survey','Anda telah melakukan '.$nama_survey);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
