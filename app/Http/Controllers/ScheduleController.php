<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use JsValidator;
use Lang;
use App\Helpers\Error_form;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Peserta;
use App\Http\Requests\PasswordRequest;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $url                    = url('schedule/Calendarview/');
        $batch                  = DB::table('pendaftaran_trx')
                                    ->where('peserta_id',Auth::User()->peserta_id)
                                    ->pluck('batch_id');

        /** add jadwal pelatihan to calendar **/
        $pelatihanakutail       =  DB::table("pelatihan_aktual")
                                    ->whereIn("batch_id",$batch)
                                    ->pluck('pelatihan_aktual_id');
        $pelatihanakutaildetail =  DB::table("pelatihan_aktual_dtl")
                                    ->whereIn("pelatihan_aktual_id",$pelatihanakutail)
                                    ->get();

        /** add jadwal ujian perdana to calendar **/
        $perdanas               =  DB::table("perdana_jadwal")
                                    ->whereIn("perdana_jadwal_id",function($x) use($batch){
                                        $x->select('perdana_jadwal_id')
                                          ->from('ujian_batch')
                                          ->whereIn("batch_id",$batch)
                                          ->whereIn('ujian_batch_id', function($y){
                                                $y->select('ujian_batch_id')
                                                  ->from('perdana_peserta')
                                                  ->where('peserta_id', Auth::User()->peserta_id);
                                              });
                                      })
                                    ->get();

        /** add jadwal ujian ulang to calendar **/
        $ulangs                 =  DB::table("ulang_jadwal")
                                    ->whereIn("ulang_jadwal_id",function($x) use($batch){
                                        $x->select('ulang_jadwal_id')
                                          ->from('ujian_pendaftaran')
                                          ->where('peserta_id', Auth::User()->peserta_id)
                                          ->whereIn("batch_id",$batch)
                                          ->whereIn('ujian_pendaftaran_id', function($y){
                                                $y->select('ujian_pendaftaran_id')
                                                  ->from('ujian_peserta');
                                              });
                                      })
                                    ->get();

        $events   = array();
        foreach ($pelatihanakutaildetail as $detail) {
            $events[] = \Calendar::event(

                $detail->nama_pelatihan, //event title
                true, //full day event?
                $detail->tgl_pelatihan, //start time
                $detail->tgl_pelatihan, //end time
                $url.'/pelatihan/'.$detail->pelatihan_aktual_dtl_id,
                [
                    'color' => '#f05050',
                ]

            );
        }

        foreach ($perdanas as $perdana) {
            $events[] = \Calendar::event(

                'Ujian Perdana '.$perdana->nama,
                true,
                $perdana->tgl_perdana,
                $perdana->tgl_perdana,
                $url.'/perdana/'.$perdana->perdana_jadwal_id,
                [
                    'color' => '#a9dded',
                ]

            );
        }

        foreach ($ulangs as $ulang) {
            $events[] = \Calendar::event(

                'Ujian Ulang '.$ulang->nama,
                true,
                $ulang->tgl_ulang,
                $ulang->tgl_ulang,
                $url.'/ulang/'.$ulang->ulang_jadwal_id,
                [
                    'color' => '#24292e',
                ]

            );
        }

        $calendar = \Calendar::addEvents($events)
                    ->setOptions([
                        // 'firstDay=>1',
                        'themeSystem' => 'Bootstrap3',
                        'themeName' => 'Cyborg'
                    ])->setCallbacks([
                        'eventClick' => 'function(event) {
                          window.open(event.id); 
                    }'
        ]);
        return view('schedule.schedule_index',array('calendar'=>$calendar));
    }
    
    /**
    //  $("#modalTitle").html(event.title);
                            $(".titleevent").html(event.title);

                            $("#eventUrl").attr("href",event.id);
                            $("#calendarModal").modal();
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Calendarview($event,$id)
    {
       if($event == 'pelatihan'){
            $check   = DB::table('pendaftaran_trx')
                         ->where('peserta_id', Auth::User()->peserta_id)
                         ->whereIn('batch_id', function($x) use($id){
                            $x->select('batch_id')
                              ->from('pelatihan_aktual')
                              ->whereIn('pelatihan_aktual_id', function($y) use($id){
                                $y->select('pelatihan_aktual_id')
                                  ->from('pelatihan_aktual_dtl')
                                  ->where('pelatihan_aktual_dtl_id',$id);
                              });
                         })
                         ->first();

            if($check){
                $getdata     = DB::table('pelatihan_aktual_dtl')->where('pelatihan_aktual_dtl_id',$id)->first();
                $getpengajar = DB::table('pengajar')->where('pengajar_id',$getdata->pengajar_id)->first();
                $ruang       = DB::table('ruang')->where('ruang_id',$getdata->ruang_id)->first();

                return view('schedule.schedule_pelatihan',compact('getdata','getpengajar','ruang'));
            } else {
                return abort(404);
            }
            
       } elseif($event == 'perdana'){
            $check  =  DB::table('perdana_jadwal')
                               ->where('perdana_jadwal_id', $id)
                               ->whereIn('perdana_jadwal_id', function($x){
                                    $x->select('perdana_jadwal_id')
                                      ->from('ujian_batch')
                                      ->whereIn('ujian_batch_id', function($y){
                                            $y->select('ujian_batch_id')
                                              ->from('perdana_peserta')
                                              ->where('peserta_id', Auth::User()->peserta_id);
                                      });
                               })
                               ->first();
            if($check){
                $detailJadwal = DB::table('perdana_jadwal')
                              ->select(['perdana_jadwal.perdana_jadwal_id', 'perdana_jadwal.nama AS nama_perdana', 'perdana_jadwal.emp_id', 'employee.first_name', 'employee.middle_name', 'employee.last_name', 'perdana_jadwal.keterangan', 'perdana_jadwal.tgl_perdana', 'ruang.nama AS nama_ruang', 'ruang.keterangan AS ket_ruang', 'jam.nama AS nama_jam'])
                              ->join('hari', 'hari.hari_id', '=', 'perdana_jadwal.hari_id')
                              ->join('perdana', 'perdana.perdana_id', '=', 'perdana_jadwal.perdana_id')
                              ->join('ruang', 'ruang.ruang_id', '=', 'perdana.ruang_id')
                              ->join('jam', 'jam.jam_id', '=', 'perdana_jadwal.jam_id')
                              ->leftJoin('employee', 'employee.emp_id', '=', 'perdana_jadwal.emp_id')
                              ->where('perdana_jadwal_id', $id)
                              ->first();
                return view('schedule.schedule_perdana',compact('detailJadwal'));
            } else {
                return abort(404);
            }
            
       } elseif($event == 'ulang'){
            $check  =  DB::table('ulang_jadwal')
                               ->where('ulang_jadwal_id', $id)
                               ->whereIn('ulang_jadwal_id', function($x){
                                    $x->select('ulang_jadwal_id')
                                      ->from('ujian_pendaftaran')
                                      ->where('peserta_id', Auth::User()->peserta_id)
                                      ->whereIn('ujian_pendaftaran_id', function($y){
                                            $y->select('ujian_pendaftaran_id')
                                              ->from('ujian_peserta');
                                       });
                               })
                               ->first();
            if($check){
                $detailJadwal = DB::table('ulang_jadwal')
                              ->select(['ulang_jadwal.perdana_jadwal_id', 'ulang_jadwal.nama AS nama_ulang', 'ulang_jadwal.emp_id', 'employee.first_name', 'employee.middle_name', 'employee.last_name', 'ulang_jadwal.keterangan', 'ulang_jadwal.tgl_ulang', 'ruang.nama AS nama_ruang', 'ruang.keterangan AS ket_ruang', 'jam.nama AS nama_jam'])
                              ->join('hari', 'hari.hari_id', '=', 'ulang_jadwal.hari_id')
                              ->join('ulang', 'ulang.perdana_id', '=', 'ulang_jadwal.perdana_id')
                              ->join('ruang', 'ruang.ruang_id', '=', 'ulang.ruang_id')
                              ->join('jam', 'jam.jam_id', '=', 'ulang_jadwal.jam_id')
                              ->leftJoin('employee', 'employee.emp_id', '=', 'ulang_jadwal.emp_id')
                              ->where('ulang_jadwal', $id)
                              ->first();
                return view('schedule.schedule_ulang',compact('detailJadwal'));
            } else {
                return abort(404);
            }
       } else {
            return abort(404);
       }
       
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordRequest $request, $peserta_id)
    {
        // DB::beginTransaction();
        // $success_trans = false;
        // // @update data tbl:peserta

        // try {
        //     $peserta = Peserta::findOrFail($peserta_id);
        //     // password not empty, then update password
        //     if(!empty($request->password)){
        //         $peserta->password = Hash::make($request->password);
        //         $peserta->save();
        //     }

        //     DB::commit();
        //     $success_trans = true;
        // } catch (\Exception $e) {
        //     DB::rollback();

        //     // error page
        //     //abort(403, $e->getMessage());
        //     Error_Form::setAlert('message', Lang::get('db.failed_updated'), 'alert-class', 'alert-danger');
        //     return redirect()->route('password.index');
        // }

        // if ($success_trans == true) {
        //     Error_Form::setAlert('message', Lang::get('db.updated'), 'alert-class', 'alert-success');
        //     return redirect()->route('password.index');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
