<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use Lang;
use Mail;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Konfirmasi_pembayaranRequest;
use App\Models\Konfirmasi;
use App\Models\email_template\Config_email;

class KonfirmasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url_ajax_datatable = route('konfirmasi.ajax_datatable');
        return view('konfirmasi.konfirmasi_index', compact('url_ajax_datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Konfirmasi_pembayaranRequest $request)
    {
        DB::beginTransaction();
        $success_trans = false;

        try {
            /** insert tbl:konfirmasi **/
            $konfirmasi                 = new Konfirmasi;
            $konfirmasi->tagihan_tmp_id = $request->tagihan_tmp_id;
            $konfirmasi->nominal        = \Helper::number_formats($request->nominal_transfer, 'db', 0);
            $konfirmasi->no_rekening    = $request->transfer_dari;
            $konfirmasi->nama_rekening  = $request->nama_rekening;
            $konfirmasi->tgl_transfer   = \Helper::date_formats($request->tanggal_transfer, 'db');
            $konfirmasi->jam_transfer   = $request->jam_transfer;
            //  Upload bukti_transfer
            if ($request->hasFile('bukti_transfer')) {
                $image                      = $request->file('bukti_transfer');
                $imagename                  = 'BUKTI_TRANSFER_' . $request->tagihan_tmp_id . '_'. time() . '.' . $image->getClientOriginalExtension();
                $konfirmasi->bukti_transfer = $imagename;
                $destinationPath            = public_path('assets/upload_files/peserta/bukti_transfer');
            }
            $konfirmasi->date_created = date('Y-m-d H:i:s');
            $konfirmasi->user_created = \Auth::user()->peserta_id;
            $konfirmasi->hapus        = false;
            $konfirmasi->save();

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }

        if ($success_trans == true) {
            //store image
            if(isset($destinationPath)) {
                $image->move($destinationPath, $imagename);
            }

            /** send email to peserta **/
            $tagihan_tmp     = DB::table('tagihan_tmp')->where('tagihan_tmp_id', $request->tagihan_tmp_id)->first();
            $email_peserta   = DB::table('peserta')->where('peserta_id', $tagihan_tmp->peserta_id)->value('email');
            $header          = 'email-template.core.header';
            $footer          = 'email-template.core.footer';
            $config          = Config_email::first();
            $konfirmasi      = DB::table('email_konfirmasi')->where('email_konfirmasi_id', 17)->first(); // Email Pemberitahuan Konfirmasi Pembayaran 
              /** send email **/
              Mail::send('email-template.email-pemberitahuan-konfirmasi-pembayaran', 
                      ['header'          => $header, 
                      'footer'           => $footer, 
                      'config'           => $config, 
                      'konfirmasi'       => $konfirmasi, 
                      'nama_peserta'     => $tagihan_tmp->nama_peserta,
                      'invoice_no'       => $tagihan_tmp->no_invoice,
                      'nama_batch'       => $tagihan_tmp->nama_batch,
                      'tgl_transfer'     => \Helper::date_formats($request->tanggal_transfer, 'view'),
                      'jam_transfer'     => $request->jam_transfer,
                      'nominal_transfer' => $request->nominal_transfer,
                      'nama_rekening'    => $request->nama_rekening,
                      'no_rekening'      => $request->transfer_dari,
                  ],
                  function ($message) use ($email_peserta){
                      $message->subject('TICMI - Pemberitahuan Konfirmasi Pembayaran');
                      $message->from('mg@ticmi.co.id', 'TICMI');
                      $message->to($email_peserta);
                  }
              );
            /** end email **/

            return redirect()->route('konfirmasi.index')->with('alert-success', Lang::get('db.updated'));
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tagihan_tmp_id)
    {
        $tagihan                  = DB::table('tagihan_tmp')->where('tagihan_tmp_id', $tagihan_tmp_id)->first();
        $peserta                  = DB::table('peserta')->where('peserta_id', $tagihan->peserta_id)->first();

        return view('konfirmasi.konfirmasi_edit', compact('tagihan', 'peserta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajax_datatable(Request $request)
    {
        if ($request->ajax()) {
        $sql_no_urut     = \Yajra_datatable::get_no_urut('tagihan_tmp.tagihan_tmp_id'/*primary_key*/, $request);
        $tagihan         = DB::table('tagihan_tmp')
                            ->select([
                                    DB::raw($sql_no_urut), // nomor urut
                                    'tagihan_tmp.tagihan_tmp_id AS tagihan_tmp_id',
                                    'tagihan_tmp.total_tagihan AS total_tagihan',
                                    'tagihan_tmp.nama_batch AS nama_batch',
                                    'tagihan_tmp.tgl_daftar AS tgl_daftar',
                                    'tagihan_tmp.no_invoice AS no_invoice',
                                    'tagihan_tmp.account_no AS account_no',
                                    ])
                            // ->join('tagihan_tmp', 'tagihan_tmp.tagihan_tmp_id', '=', 'bayar.tagihan_tmp_id')
                            ->where('tagihan_tmp.peserta_id', Auth::user()->peserta_id)
                            ->whereNotIn('tagihan_tmp.tagihan_tmp_id', function($q){
                                $q->select('tagihan_tmp_id')
                                  ->from('konfirmasi');
                            })
                            ->whereNotIn('tagihan_tmp.tagihan_tmp_id', function($q){
                                $q->select('tagihan_tmp_id')
                                  ->from('validasi');
                            });

        return Datatables::of($tagihan)
                            ->addIndexColumn()
                            ->addColumn('jenis', function ($tagihan) {
                                $char = substr($tagihan->no_invoice, 0,2);
                                if($char == 'ST'){
                                    $jenis = 'Pendaftaran Sertifikasi';
                                } elseif($char == 'UU'){
                                    $jenis = 'Pendaftaran Ujian Ulang';
                                }
                                return $jenis;
                            })
                            ->addColumn('total_tagihan', function($tagihan){
                                return \Helper::number_formats($tagihan->total_tagihan, 'view', 0);
                            })
                            ->addColumn('tgl_daftar', function($tagihan){
                                return \Helper::date_formats($tagihan->tgl_daftar, 'view');
                            })
                            ->addColumn('action', function ($tagihan){
                                $btn_action = '<a href="'. route('konfirmasi.edit', $tagihan->tagihan_tmp_id) .'" class="btn red btn-outline">Konfirmasi</a>&nbsp;';

                                return $btn_action;
                            })
                            ->rawColumns(['total_tagihan', 'tgl_daftar','jenis', 'action']) // to html
                            ->make(true);
        }
    }
}
