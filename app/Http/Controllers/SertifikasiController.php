<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use Yajra\Datatables\Facades\Datatables;
use Cookie;

use App\Models\Batch;
use App\Models\Perdana_peserta;
use App\Models\Perdana_jadwal;
use App\Models\Ujian_batch;
use App\Models\Pendaftaran_trx;
use App\Models\Pelatihan_aktual;
use App\Models\Pelatihan_aktual_dtl;
use App\Models\Soal_peserta;
use App\Models\Ulang_jadwal;
use App\Models\Svy_manag;

class SertifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $peserta_id   = Auth::user()->peserta_id;
        $perdana      = Perdana_peserta::where('peserta_id', $peserta_id)
                        ->whereNotIn('perdana_peserta_id', function($query){
                            $query->select(DB::raw('perdana_peserta_id'))->from('soal_peserta')->whereNotNull('nilai_ujian');
                        })
                        ->get();

        // ======================================== Ujian Perdana ========================================
        // ======================================== Ujian Perdana ========================================
        // ======================================== Ujian Perdana ========================================
        // ======================================== Ujian Perdana ========================================
        // ======================================== Ujian Perdana ========================================
        
        $jumlah_batch = count($perdana);

        $tanggal_sekarang = date("Y-m-d");
        $tanggal_perdana = '';

        $nama_batch = array();

        if($jumlah_batch > 0)
        {
            foreach ($perdana as $pd) {
                $pdArray      = array();
                $ujian_param  = DB::table('ujian_parameter')
                      ->where('ujian_parameter_id', function($q) use($pd){
                          $q->select('ujian_parameter_id')
                            ->from('batch')
                            ->where('batch_id', $pd->ujian_batch->batch->batch_id);
                      })->first();

                $pdArray['id']            = $pd->ujian_batch->batch->batch_id;
                $pdArray['nama']          = $pd->ujian_batch->batch->nama;
                $pdArray['tanggal']       = $pd->ujian_batch->perdana_jadwal->tgl_perdana;
                $pdArray['lama_ujian']    = $ujian_param->durasi_default_ujian;
                $tanggal_perdana          = $pd->ujian_batch->perdana_jadwal->tgl_perdana;
                
                $pdArray['ruang']         = $pd->ujian_batch->perdana_jadwal->perdana->ruang->nama;
                $pdArray['jam']           = $pd->ujian_batch->perdana_jadwal->jam->nama;
                $pdArray['emp_id']        = $pd->ujian_batch->perdana_jadwal->emp_id;
                $pdArray['keyboard_lock'] = $pd->ujian_batch->perdana_jadwal->keyboard_lock;
                $pdArray['aktivasi']      = $pd->aktivasi;
                if($tanggal_perdana == $tanggal_sekarang)
                {
                    array_push($nama_batch, $pdArray);
                }
            }
        }

        // Daftar pelatihan batch yang di ikutin
        $peserta         = Auth::user()->peserta_id;
        $pendaftaran_trx = Pendaftaran_trx::where('peserta_id', $peserta)
                                          ->whereNotIn('peserta_aktivasi_id', function($q){
                                                $q->select('peserta_aktivasi_id')
                                                  ->from('peserta_cancel');
                                            })
                                          ->whereNotIn('batch_id', function($q) use($peserta_id){
                                                $q->select('batch_id')
                                                  ->from('peserta_lulus')
                                                  ->where('peserta_id', $peserta_id);
                                            })
                                          ->pluck('batch_id');
        $batch_peserta   = Pendaftaran_trx::where('peserta_id', $peserta)
                                          ->whereNotIn('peserta_aktivasi_id', function($q){
                                                $q->select('peserta_aktivasi_id')
                                                  ->from('peserta_cancel');
                                            })
                                          ->pluck('batch_id');
        $batch_nilai     = DB::table('batch')
                             ->whereIn('batch_id', $batch_peserta)
                             ->whereIn('batch_id', function($q) use($peserta){
                                $q->select('batch_id')
                                  ->from('ujian')
                                  ->where('peserta_id', $peserta);
                             })
                             ->get();
        $batch_aktif     = DB::table('batch')
                             ->whereIn('batch_id', $pendaftaran_trx)
                             ->where('aktif', TRUE)
                             ->where('hapus', FALSE)
                             ->get();
        $batch_id        = array();

        foreach ($pendaftaran_trx as $pdt) {
            array_push($batch_id, $pdt);
        }

        $pelatihan_aktual_id = array();

        foreach ($batch_id as $bci) {
            //pelatihan_aktual_id
            $pai = Pelatihan_aktual::where('batch_id', $bci)->value('pelatihan_aktual_id');
            array_push($pelatihan_aktual_id, $pai);
        }

        $pelatihan_aktual_dtl = Pelatihan_aktual_dtl::whereIn('pelatihan_aktual_id', $pelatihan_aktual_id)->get();

        $pelatihan_detail = array();

        foreach ($pelatihan_aktual_dtl as $pad) {
            $padArray = array();
            $padArray['batch']          = $pad->pelatihan_aktual->batch->nama;
            $padArray['nama_pelatihan'] = $pad->nama_pelatihan;
            $padArray['keterangan']     = $pad->keterangan;
            $padArray['jam_pelatihan']  = $pad->jam_pelatihan;
            $padArray['durasi_jam']     = $pad->durasi_jam;
            $padArray['ruang']          = $pad->ruang_id != null ? $pad->ruangan->nama : 'Belum diset';
            $padArray['pelatihan_ke']   = $pad->pelatihan_ke;
            $padArray['pengajar']       = $pad->pengajar->nama;
            array_push($pelatihan_detail, $padArray);
        }

        // Daftar ujian batch yang di ikutin
        $perdana_jadwal_id = array();
        $ujian_batch = Ujian_batch::whereIn('batch_id', $batch_id)->get();

        foreach ($ujian_batch as $u_b) {
            array_push($perdana_jadwal_id, $u_b->perdana_jadwal_id);
        }

        $perdana_jadwal = Perdana_jadwal::whereIn('perdana_jadwal_id', $perdana_jadwal_id)->get();

        $batch = Batch::whereIn('batch_id', $batch_id)->get();

        // check apakah sudah boleh ujian
        $ujian_batch_id = Ujian_batch::whereIn('batch_id', $batch_id)->pluck('ujian_batch_id');
        $perdana_peserta_id = Perdana_peserta::where('peserta_id', $peserta_id)
                                             ->whereIn('ujian_batch_id', $ujian_batch_id)
                                             ->pluck('perdana_peserta_id');
        $soal_peserta_id = Soal_peserta::whereIn('perdana_peserta_id', $perdana_peserta_id)->whereNull('nilai_ujian')->pluck('soal_peserta_id');
        $tanggal_perdana_array = array();
        $status_ujian = false;

        if($perdana_peserta_id)
        {
            foreach ($perdana_jadwal as $pj) {
                array_push($tanggal_perdana_array, $pj->tgl_perdana);
            }

            foreach ($tanggal_perdana_array as $value) {
                $tanggal_sekarang = date("Y-m-d");

                if($tanggal_sekarang == $value)
                {

                    $cek_boleh_ujian = Soal_peserta::whereIn('soal_peserta_id', $soal_peserta_id)->whereIn('perdana_peserta_id', $perdana_peserta_id)->whereNotNull('nilai_ujian')->get();

                     $cek_boleh_ujian = count($cek_boleh_ujian);

                     if($cek_boleh_ujian == 0)
                     {
                        $status_ujian = true;
                     }
                }
            }
        }

        // ======================================== Ujian Ulang ========================================
        // ======================================== Ujian Ulang ========================================
        // ======================================== Ujian Ulang ========================================
        // ======================================== Ujian Ulang ========================================
        // ======================================== Ujian Ulang ========================================

        $ulang_jadwal = Ulang_jadwal::where('tgl_ulang', '>=', $tanggal_sekarang)->get();
        // cek apakah peserta terdaftar di batch
        $batch_id_trx = Pendaftaran_trx::where('peserta_id', $peserta_id)->pluck('batch_id');
        $alert = '';
        if($batch_id_trx->count() > 0) {
            // cek apakah peserta boleh atau dapat melakukan ujian ulang
            $ujian_batch_ids = Perdana_peserta::where('peserta_id', $peserta_id)->pluck('ujian_batch_id');
            // cek apakah peserta sudah melakukan ujian perdana
            if($ujian_batch_ids->count() > 0) {
                $batch_ids       = DB::table('ujian_batch')
                                     ->whereIn('ujian_batch_id', $ujian_batch_ids)
                                     ->pluck('batch_id');
                $batch_ulang = Batch::whereIn('batch_id', $batch_ids)
                                ->whereNotIn('batch_id', function($q) use($peserta_id){
                                  $q->select('batch_id')
                                    ->from('peserta_lulus')
                                    ->where('peserta_id', $peserta_id);
                                })
                                ->where('tgl_ujian_close', '>=', date('Y-m-d'))
                                ->where('aktif', true)
                                ->where('is_ujian', true)
                                ->get();
            } else {  // end if $soal_peserta_ids
                $alert .= 'Anda belum mengikuti ujian perdana';
            }
        } else {  // end if $batch_id_trx
            $alert .= 'Anda belum mengikuti pelatihan';
        }

        $jadwal_uji_ulang = DB::table('ujian_pendaftaran')
                              ->select(['ujian_pendaftaran.ujian_pendaftaran_id AS _id', 'ujian_pendaftaran.nama_batch AS nama_batch', 'ujian_pendaftaran.nama_lembg_pdkn AS nama_lembg_pdkn', 'ujian_pendaftaran.tgl_daftar AS tgl_daftar', 'ruang.nama AS ruang', 'ulang_jadwal.tgl_ulang AS tgl_ulang', 'hari.nama AS hari', 'jam.nama AS jam', 'ujian_pendaftaran.ulang_ke AS ulang_ke','ujian_peserta.is_aktivasi AS is_aktivasi', 'ujian_pendaftaran.batch_id AS batch_id'])
                              ->where('peserta_id', Auth::user()->peserta_id)
                              ->whereIn('ujian_pendaftaran.ujian_pendaftaran_id', function($query) {
                                $query->select('ujian_pendaftaran_id')
                                      ->from('ujian_peserta')
                                      ->where('selesai', false)
                                      ->whereIn('ujian_peserta_id', function($q){
                                          $q->select('ujian_peserta_id')
                                            ->from('soal_ujian_ulang');
                                    });
                              })
                              ->whereDate('ulang_jadwal.tgl_ulang', '>=' , date('Y-m-d'))
                              ->join('ujian_peserta', 'ujian_peserta.ujian_pendaftaran_id', '=', 'ujian_pendaftaran.ujian_pendaftaran_id')
                              ->join('ulang_jadwal', 'ulang_jadwal.ulang_jadwal_id', '=', 'ujian_pendaftaran.ulang_jadwal_id')
                              ->join('ulang', 'ulang.ulang_id', '=', 'ulang_jadwal.ulang_id')
                              ->leftJoin('ruang', 'ruang.ruang_id', '=', 'ulang.ruang_id')
                              ->join('hari', 'hari.hari_id', '=', 'ulang_jadwal.hari_id')
                              ->join('jam', 'jam.jam_id', '=', 'ulang_jadwal.jam_id')
                              ->get();

        return view('sertifikasi.sertifikasi_index', compact('jumlah_batch', 'nama_batch', 'pelatihan_detail', 'perdana_jadwal', 'batch', 'status_ujian', 'ulang_jadwal', 'batch_ulang', 'alert','batch_aktif','jadwal_uji_ulang', 'batch_nilai'));
    }

    public function jadwal_pelatihan(Request $request, $batch_id)
    {
        $peserta_id          = Auth::user()->peserta_id;
        $url_ajax_get_jadwal = route('sertifikasi.ajax_get_jadwal');
        $pendaftaran         = DB::table('pendaftaran_trx')
                                 ->where('peserta_id', $peserta_id)
                                 ->where('batch_id', $batch_id)
                                 ->whereNotIn('peserta_aktivasi_id', function($q){
                                        $q->select('peserta_aktivasi_id')
                                          ->from('peserta_cancel');
                                   });
        $url_ajax_survey     = URL::to('survey/ajax_survey');
        if($pendaftaran->count() > 0) {
            return view('sertifikasi.jadwal_pelatihan', compact('peserta_id', 'url_ajax_get_jadwal', 'batch_id','url_ajax_survey'));
        } else {
            return abort(404);
        }
    }

    public function materi_pelatihan(Request $request, $program_id)
    {
        $peserta_id          = Auth::user()->peserta_id;
        $url_ajax_get_materi = route('sertifikasi.ajax_get_materi');
        
        return view('sertifikasi.materi_pelatihan', compact('peserta_id', 'url_ajax_get_materi', 'program_id'));
    }

    public function ajax_get_jadwal(Request $request)
    {
        if ($request->ajax()) {
            //get row number datatable
            $sql_no_urut    = \Yajra_datatable::get_no_urut('pelatihan_aktual_dtl.pelatihan_aktual_dtl_id' /*primary_key*/, $request);
            $batch_id       = $request->get('batch');
            $jadwal         = DB::table('pelatihan_aktual_dtl')
                                ->select([
                                        DB::raw($sql_no_urut), // nomor urut
                                        'pelatihan_aktual_dtl.pelatihan_aktual_dtl_id AS _id',
                                        'pelatihan_aktual_dtl.nama_pelatihan AS nama',
                                        'modul.nama AS modul',
                                        'ruang.nama AS ruang',
                                        'pelatihan_aktual_dtl.tgl_pelatihan AS tanggal',
                                        'pelatihan_aktual_dtl.jam_pelatihan AS jam',
                                        'svy.nama_survey AS survey'
                                        ])
                                ->join('pelatihan_aktual', 'pelatihan_aktual.pelatihan_aktual_id', '=', 'pelatihan_aktual_dtl.pelatihan_aktual_id')
                                ->join('modul', 'modul.modul_id', '=', 'pelatihan_aktual.modul_id')
                                ->join('submodul', 'submodul.submodul_id', '=', 'pelatihan_aktual_dtl.submodul_id')
                                ->join('pengajar', 'pengajar.pengajar_id', '=', 'pelatihan_aktual_dtl.pengajar_id')
                                ->leftJoin('ruang', 'ruang.ruang_id', '=', 'pelatihan_aktual_dtl.ruang_id')
                                ->leftJoin('svy_manag', 'svy_manag.pelatihan_aktual_dtl_id', '=', 'pelatihan_aktual_dtl.pelatihan_aktual_dtl_id')
                                ->leftJoin('svy', 'svy.svy_id', '=', 'svy_manag.svy_id')
                                ->where('pelatihan_aktual.batch_id', $batch_id);

            return Datatables::of($jadwal)
                ->addIndexColumn()
                ->addColumn('tanggal', function($jadwal){
                    return \Helper::date_formats($jadwal->tanggal, 'view');
                })
                ->addColumn('ruangan', function($jadwal){
                    $ruangan = !empty($jadwal->ruang) ? $jadwal->ruang : "<span class='label label-sm label-success'> Belum diset </span>";
                    return $ruangan;
                })
                ->addColumn('survey', function($jadwal) use ($batch_id){
                    $button = '';
                    $survey = Svy_manag::where('pelatihan_aktual_dtl_id',$jadwal->_id)->join('svy','svy.svy_id','=','svy_manag.svy_id')->get();
                    if($survey){
                        foreach($survey as $svy){
                            $pilihan_jawaban = 
                            DB::table('svy_pilihan_jawaban')
                            ->join('svy_pertanyaan','svy_pertanyaan.svy_pertanyaan_id','svy_pilihan_jawaban.svy_pertanyaan_id')
                            ->join('svy_kat','svy_kat.svy_kat_id','svy_pertanyaan.svy_kat_id')
                            ->where('svy_pilihan_jawaban.peserta_id', Auth::user()->peserta_id)
                            ->where('svy_pilihan_jawaban.pelatihan_aktual_dtl_id', $jadwal->_id)
                            ->where( 'svy_kat.svy_id', $svy->svy_id)->first();
                            if($pilihan_jawaban){
                               $button .= '<a class="btn btn-sm btn-default tooltips" data-toogle="tooltip" data-placement="top" title="Anda telah menjawab survey ini" disabled>'. $svy->nama_survey.' </a>';
                            }else{
                                $button .= " <button type='button' class='btn btn-sm green btn-outline' onclick='return survey(".$svy->svy_id.",".$jadwal->_id.")' style='margin:5px;'> ".$svy->nama_survey."</button>";
                            }
                        }
                        return $button;
                    }else{
                        return "<div class='alert alert-info' role='alert'>Belum diset</div>";
                    }
                })
                ->rawColumns(['tanggal', 'ruangan', 'survey']) // to html
                ->make(true);
        }
    }

    public function ajax_get_materi(Request $request)
    {
        if ($request->ajax()) {
            //get row number datatable
            $sql_no_urut    = \Yajra_datatable::get_no_urut('materi.materi_id' /*primary_key*/, $request);
            $program_id     = $request->get('program');
            $materi         = DB::table('materi')
                                ->select([
                                        DB::raw($sql_no_urut), // nomor urut
                                        'materi.materi_id AS materi_id',
                                        'materi.nama AS nama',
                                        'materi.keterangan AS keterangan',
                                        'materi.file_name AS file_name',
                                        'program.nama AS program'
                                        ])
                                ->join('program', 'program.program_id', '=', 'materi.program_id')
                                ->whereNull('materi.date_deleted')
                                ->where('materi.program_id', $program_id);

            return Datatables::of($materi)
                ->addIndexColumn()
                ->addColumn('action', function($materi) use ($program_id){
                    $link       = \Config::get('app.url_siak').'/upload_files/materi/'. $materi->file_name;
                    $btn_action = '<a href="'. $link .'" title="Lihat Materi" target="_blank" class="glyphicon glyphicon-search"></a>&nbsp;';
                    return $btn_action;
                })
                ->rawColumns(['action']) // to html
                ->make(true);
        }
    }

    public function nilai_perdana(Request $request, $batch_id)
    {
        $peserta_id          = Auth::user()->peserta_id;
        $ujian_detail        = DB::table('ujian_detail')
                                 ->whereIn('ujian_id', function($q) use($peserta_id, $batch_id){
                                    $q->select('ujian_id')
                                      ->from('ujian')
                                      ->where('peserta_id', $peserta_id)
                                      ->where('batch_id', $batch_id);
                                 })
                                 ->whereNotNull('perdana_jadwal_id')
                                 ->get();

        return view('sertifikasi.ujian_perdana.nilai_ujian', compact('ujian_detail'));
        
    }

    public function nilai_ulang(Request $request, $batch_id)
    {
        $peserta_id          = Auth::user()->peserta_id;
        $ulang_jadwal_ids    = DB::table('ujian_detail')
                                 ->whereIn('ujian_id', function($q) use($peserta_id, $batch_id){
                                    $q->select('ujian_id')
                                      ->from('ujian')
                                      ->where('peserta_id', $peserta_id)
                                      ->where('batch_id', $batch_id);
                                 })
                                 ->whereNotNull('ulang_jadwal_id')
                                 ->pluck('ulang_jadwal_id')
                                 ->unique();
        $ujian_detail           = [];
        foreach ($ulang_jadwal_ids as $ulang_jadwal_id) {
            $arra                    = [];
            $arra['ulang_jadwal_id'] = $ulang_jadwal_id;
            $arra['tgl_ujian']       = DB::table('ujian_detail')->where('ulang_jadwal_id', $ulang_jadwal_id)->first()->tgl_ujian;
            $arra['moduls']          = [];
            $ujians                  = DB::table('ujian_detail')
                                         ->whereIn('ujian_id', function($q) use($peserta_id, $batch_id){
                                            $q->select('ujian_id')
                                              ->from('ujian')
                                              ->where('peserta_id', $peserta_id)
                                              ->where('batch_id', $batch_id);
                                         })
                                         ->whereNotNull('ulang_jadwal_id')
                                         ->where('ulang_jadwal_id', $ulang_jadwal_id)
                                         ->orderBy('ujian_detail_id')
                                         ->get();
            foreach ($ujians as $ujian) {
                $arrb['nama_modul'] = $ujian->nama_modul;
                $arrb['nilai']      = $ujian->nilai;
                array_push($arra['moduls'], $arrb);
            }
            array_push($ujian_detail,$arra);
        }
        //dd($ujian_detail);
        return view('sertifikasi.ujian_ulang.nilai_ujian', compact('ujian_detail'));
    }

    public function print_ujian()
    {
        $data['hasil_ujian'] = json_decode($_COOKIE['hasil_ujian'], true);
        $pdf = \PDF::loadView('sertifikasi.print_hasil_ujian', $data);
        return $pdf->stream('sertifikasi.pdf', array('Attachment' => false));
    }
}
