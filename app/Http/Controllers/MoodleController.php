<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
use Url;
use Form;
use Lang;
use DB;
use App\Helpers\Encryption_moodle;

class MoodleController extends Controller
{
	
	public function create_update_delete_user_moodle($moodle_url, $request, $action)
    {
        // curl is disabled, then die
        if(function_exists('curl_version') == 0){
            echo "curl extension is disabled.";
            die();
        }

        stream_context_set_default( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);
        // check url exist or not
        $file_headers = get_headers($moodle_url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            echo \Lang::get('check_data.failed_url_moodle') .' : '. $moodle_url . '; <a href="javascript:location.reload();">'. \Lang::get('check_data.try_again') .'</a>';
            die();
        }


        $role_moodle = 'auth_user'; // default

        // role admin (in moodle)
        if($request->role_id == 1){
            $role_moodle = 'admin';
        }
        // role coursecreator (in moodle)
        else if($request->role_id == 2){
            $role_moodle = 'coursecreator';
        }
        // role authentication user (in moodle)
        // role authentication user dianggap sebagai role student saja :).
        else if($request->role_id == 3){
            $role_moodle = 'auth_user';   
        }

        // class encrypt password
        $en_mod = new Encryption_moodle();


        // curl to Moodle
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $moodle_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // max trying to connect to url
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); // max curl connection
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // set post fields
        $post = [
            'username'  => $request->username,
            'password'  => $en_mod->safe_b64encode($request->password),
            'email'     => $request->email,
            'fullname'  => $request->name,
            'rolemoodle'=> $role_moodle,
        ];
        if($action == 'update' || $action == 'delete'){
            $post['id'] = $request->users_moodle_id;
        }

        if($action == 'update'){
            $role_moodle_old = '';

            // role admin
            if($request->role_old_id == 1){
                $role_moodle_old = 'admin';
            }
            // role coursecreator
            else if($request->role_old_id == 2){
                $role_moodle_old = 'coursecreator';
            }
            // role authentication user
            else if($request->role_old_id == 3){
                $role_moodle_old = 'auth_user';   
            }

            $post['rolemoodleold'] = $role_moodle_old;
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute url
        $data = curl_exec($ch); // return user id

        // no response with curl connection
        if(curl_errno($ch)){
            echo 'gagal konek moodle ' ." <a href='javascript:location.reload();'>". 'gagal save ' ."</a>";
            die();
        }
        if(empty($data)){
            echo 'gagal save moodle ' . " <a href='javascript:location.reload();'>". 'gagal save moodle ' ."</a>";
            die();
        }

        return $data;
    }


}
