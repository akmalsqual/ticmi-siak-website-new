<?php

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Http\Requests\KonfirmasiRequest;
use App\Http\Requests\SuratRisetRequest;
use App\Jobs\SendEmailKonfirmasiSuratRiset;
use App\Jobs\SendEmailPengajuanSuratRiset;
use App\Models\ticmi\Keranjang;
use App\Models\ticmi\PaketUser;
use App\Models\ticmi\SuratRiset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuratRisetController extends Controller
{
	
	public function suratRiset()
	{
		/**
		 * Cek apakah user punya Paket Data Aktif dan jika sudah punya Paket Aktif apakah sudah pernah buat permohonan atau belum
		 * jika sudah pernah maka untuk melakukan perubahan atau buat permohonan baru akan dikenakan biaya Rp50.000,-
		 */
		$paket_aktif   = auth()->user()->paket_data()->where('is_active', 1)->where(\DB::raw("DATE(valid_until)"), '>=', date('Y-m-d'))->first();
		$jmlSuratRiset = null;
		if ($paket_aktif && $paket_aktif->count() > 0) {
			/**
			 * Cek Surat Riset apakah sudah pernah buat surat dengan paket data yang aktif saat ini, jika sudah pernah buat maka akan
			 * dikenakan biaya Rp50.000,-
			 */
			$jmlSuratRiset = SuratRiset::where('paket_user_id', $paket_aktif->id)->get();
		}
		
		$sumAmountPembelian = Keranjang::where('user_id', auth()->user()->peserta_id)->where('is_payment_approve', 1)->sum('grandtotal');
		$suratRiset         = SuratRiset::where('idCustomer', Auth::user()->peserta_id)->get();
		return view('ticmi.suratriset.surat-riset', compact('suratRiset', 'sumAmountPembelian', 'jmlSuratRiset'));
	}
	
	public function createSuratRiset()
	{
		$jnsTugasAkhir = [
			'Tugas Akhir' => 'Tugas Akhir',
			'Skripsi'     => 'Skripsi',
			'Thesis'      => 'Thesis',
			'Disertasi'   => 'Disertasi'
		];
		
		
		return view('ticmi.suratriset.surat-riset-create', compact('jnsTugasAkhir'));
	}
	
	public function storeSuratRiset(SuratRisetRequest $request)
	{
		$paket_aktif = auth()->user()->paket_data()->where('is_active', 1)->where(\DB::raw("DATE(valid_until)"), '>=', date('Y-m-d'))->first();
		if ($paket_aktif && $paket_aktif->count() > 0) {
			$jmlSuratRiset = null;
			if ($paket_aktif && $paket_aktif->count() > 0) {
				/**
				 * Cek Surat Riset apakah sudah pernah buat surat dengan paket data yang aktif saat ini, jika sudah pernah buat maka akan
				 * dikenakan biaya Rp50.000,-
				 */
				$jmlSuratRiset = SuratRiset::where('paket_user_id', $paket_aktif->id)->get();
			}
			
			$suratRiset = new SuratRiset($request->all());
			$user       = Auth::user();
			
			/**
			 * Uploading surat pengantar
			 */
			if ($request->hasFile('surat_pengantar')) {
				if ($request->file('surat_pengantar')->isValid()) {
					/** @var  $image  Get The Image */
					$image                      = $request->file('surat_pengantar');
					$filename                   = 'surat-pengantar' . '-' . $user->peserta_id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
					$suratRiset->suratPengantar = $filename;
					$uploadPath                 = public_path('upload/surat-pengantar/');
					$request->file('surat_pengantar')->move($uploadPath, $filename);
				}
			}
			
			if ($request->hasFile('dokumenskripsi')) {
				if ($request->file('dokumenskripsi')->isValid()) {
					/** @var  $image  Get The Image */
					$image               = $request->file('dokumenskripsi');
					$filename            = 'skripsi' . '-' . $user->peserta_id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
					$suratRiset->skripsi = $filename;
					$uploadPath          = public_path('upload/skripsi/');
					$request->file('dokumenskripsi')->move($uploadPath, $filename);
				}
			}
			
			/**
			 * Lengkapi data isian
			 */
			if ($jmlSuratRiset && $jmlSuratRiset->count() >= 1) {
				$suratRiset->price           = 100000;
				$suratRiset->paymentDueDate  = date('Y-m-d H:i:s', strtotime('+2 day'));
				$suratRiset->virtual_account = CartHelper::getVirtualAccount('031');
				$suratRiset->invoiceNumber   = CartHelper::getSuratRisetInvoice();
			} else {
				$suratRiset->price               = 0;
				$suratRiset->paymentApproved     = 1;
				$suratRiset->paymentApprovedDate = date('Y-m-d H:i:s');
				$suratRiset->paymentApprovedBy   = 1;
			}
			
			$suratRiset->paket_user_id = $paket_aktif->id;
			$suratRiset->namaMhs       = $user->nama;
			$suratRiset->idCustomer    = $user->peserta_id;
			$suratRiset->isApproved    = 1;
			$suratRiset->createdDate   = date('Y-m-d H:i:s');
			$suratRiset->approvedBy    = 1;
			$suratRiset->approvedDate  = date('Y-m-d H:i:s');
			$suratRiset->countEdited   = 0;
			$suratRiset->isDeleted     = false;
			
			if ($suratRiset->save()) {
				/**
				 * Kirim Email notifikasi
				 */
				$job = (new SendEmailPengajuanSuratRiset($suratRiset))->delay(3)->onQueue('ticmi');
				$this->dispatch($job);
				
				flash()->success('Pengajuan Surat Riset berhasil disimpan');
				return redirect()->route('member.suratriset');
			} else {
				flash()->error('Pengajuan Surat Riset gagal disimpan');
				return redirect()->withInput()->route('member.suratriset.create');
			}
		} else {
			flash()->error('Pengajuan Surat Riset gagal, Anda tidak mempunyai Paket Data Aktif saat ini. Silahkan lakukan pembelian Paket Data terlebih dahulu.');
			return redirect()->route('member.suratriset');
		}
	}
	
	public function editSuratRiset(SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa diubah');
			return redirect()->route('member.suratriset');
		}
		$jnsTugasAkhir = [
			'Tugas Akhir' => 'Tugas Akhir',
			'Skripsi'     => 'Skripsi',
			'Thesis'      => 'Thesis',
			'Disertasi'   => 'Disertasi'
		];
		
		return view('ticmi.suratriset.surat-riset-edit', compact('jnsTugasAkhir', 'suratRiset'));
	}
	
	public function updateSuratRiset(SuratRisetRequest $request, SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa diubah');
			return redirect()->route('member.suratriset');
		}
		$update = [];
		$user   = Auth::user();
		
		/**
		 * Uploading surat pengantar
		 */
		if ($request->hasFile('surat_pengantar')) {
			if ($request->file('surat_pengantar')->isValid()) {
				/** @var  $image  Get The Image */
				$image                    = $request->file('surat_pengantar');
				$filename                 = 'surat-pengantar' . '-' . $user->peserta_id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$update['suratPengantar'] = $filename;
				$uploadPath               = public_path('upload/surat-pengantar/');
				$request->file('surat_pengantar')->move($uploadPath, $filename);
			}
		}
		
		if ($request->hasFile('dokumenskripsi')) {
			if ($request->file('dokumenskripsi')->isValid()) {
				/** @var  $image  Get The Image */
				$image             = $request->file('dokumenskripsi');
				$filename          = 'skripsi' . '-' . $user->peserta_id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$update['skripsi'] = $filename;
				$uploadPath        = public_path('upload/skripsi/');
				$request->file('dokumenskripsi')->move($uploadPath, $filename);
			}
		}
		
		/**
		 * Lengkapi data isian
		 */
		$update['countEdited'] = $suratRiset->countEdited + 1;
		$update                = array_merge($update, $request->all());
		
		if ($suratRiset->update($update)) {
			flash()->success('Pengajuan surat riset berhasil diubah');
			return redirect()->route('member.suratriset');
		} else {
			flash()->error('Pengajuan surat riset gagal diubah');
			return redirect()->route('member.suratriset.edit', ['suratriset' => $suratRiset]);
		}
	}
	
	public function destroySuratRiset(SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa dihapus');
			return redirect()->route('member.suratriset');
		}
		if ($suratRiset->update(['isDeleted' => 1])) {
			flash()->success('Pengajuan surat riset berhasil dihapus');
		} else {
			flash()->error('Pengajuan surat riset gagal dihapus');
		}
		return redirect()->route('member.suratriset');
	}
	
	public function infoSuratRiset(SuratRiset $suratRiset)
	{
		if ($suratRiset->idCustomer == auth()->user()->peserta_id) {
			return view('ticmi.suratriset.surat-riset-info', compact('suratRiset'));
		} else {
			abort(404, 'Halaman yang anda cari tidak ditemukan');
		}
	}
	
	public function konfirmasiSuratRiset(SuratRiset $suratRiset)
	{
		return view('ticmi.suratriset.surat-riset-konfirmasi', compact('suratRiset'));
	}
	
	public function storeKonfirmasiSuratRiset(KonfirmasiRequest $request, SuratRiset $suratRiset)
	{
		if ($suratRiset->user->peserta_id == auth()->user()->peserta_id) {
			/**
			 * Cek jika ada upload bukti transfer
			 */
			if ($request->hasFile('bukti_transfer')) {
				if ($request->file('bukti_transfer')->isValid()) {
					$image    = $request->file('bukti_transfer');
					$filename = $suratRiset->invoiceNumber . '_' . $request->get('virtual_account') . '.' . $image->getClientOriginalExtension();
					
					$uploadPath = public_path('upload/bukti_konfirmasi/');
					$request->file('bukti_transfer')->move($uploadPath, $filename);
					/** set bukti transfer */
					$suratRiset->confirmFile = $filename;
				}
			}
			
			$suratRiset->confirmBank         = "";
			$suratRiset->isConfirm           = 1;
			$suratRiset->confirmRekening     = $request->get('no_rekening');
			$suratRiset->confirmRekeningNama = $request->get('nama_rekening');
			$suratRiset->confirmDate         = $request->get('tgl_transfer');
			$suratRiset->confirmTime         = $request->get('jam_transfer');
			$suratRiset->confirmNominal      = $request->get('nominal');
			
			
			/** save konfirmasi */
			if ($suratRiset->save()) {
				/** Kirim Email Notifikasi Konfirmasi sukses */
				
				$job = (new SendEmailKonfirmasiSuratRiset($suratRiset))->onQueue('ticmi')->delay(3);
				$this->dispatch($job);
				
				flash()->success('Konfirmasi berhasil, kami akan verifikasi pembayaran Anda.');
				return redirect()->route('member.suratriset.info', ['invoicesuratriset' => $suratRiset->invoiceNumber]);
			} else {
				flash()->error('Konfirmasi gagal tersimpan, silahkan lakukan konfirmasi kembali');
				return redirect()->route('member.suratriset.konfirmasi', ['paketdatainvoice' => $suratRiset->invoiceNumber]);
			}
		} else {
			abort(404);
		}
	}
}
