<?php

namespace App\Http\Controllers;

use App\Http\Helpers\GeneralHelper;
use App\Models\ticmi\DataType;
use App\Models\ticmi\DownloadLog;
use App\Models\ticmi\Emiten;
use App\Models\ticmi\EmitenData;
use App\Models\ticmi\PaketData;
use App\Models\ticmi\PaketUser;
use App\Models\ticmi\Peraturan;
use App\Models\ticmi\Sector;
use FPDI;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use JavaScript;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverter;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverterCommand;
use Xthiago\PDFVersionConverter\Guesser\RegexGuesser;

class DataEmitenController extends Controller
{
	public function index(Request $request, $doctype, $type)
	{
		$jenisData = GeneralHelper::getJenisData($type, $doctype);
		$pageTitle = "TICMI Data Pasar Modal - " . $jenisData;
		$title     = $request->get('title');
//		$quartal         = $request->get('quartal');
		$year = $request->get('year');
		$code = $request->get('code');
//		$prospectus_type = $request->get('prospectus_type');
//		$idSector        = $request->get('idSector');

//		$emiten = [''=>''] + Emiten::lists('name','code')->all();
		$emiten = Emiten::select(DB::raw("CONCAT(code,' - ',name) as name,code"))->orderBy('code')->get()->pluck('name', 'code');
		
		if ($doctype && $type) {
			$data = EmitenData::where('docType', $doctype)->where('type', $type)->orderBy('year', 'DESC');
			
			if ($title) $data->where('title', 'ILIKE', "%$title%");
//			if ($quartal) $data->where('quarter',$quartal);
			if ($year) $data->where('year', $year);
//			if ($prospectus_type) $data->where('prospectus_type',$prospectus_type);
//			if ($idSector) $data->where('idSector',$idSector);
			
			if ($doctype != 'CA' && $type != 'pi') {
				$data->leftJoin('cmeds_emiten', function ($join) use ($code) {
					$join->on('cmeds_emitendata.idEmiten', '=', 'cmeds_emiten.idEmiten');
//					$join->where('cmeds_emiten.isDeleted','=','0');
				});
				if ($code) $data->where('code', 'ILIKE', "%$code%");
			}
//			if ($type == 'pi') {
//				$data->leftJoin('cmeds_sector', function ($join) {
//					$join->on('cmeds_emitendata.idSector','=','cmeds_sector.idSector');
//				});
//			}
			$arrDataType = null;
			if (auth()->check()) {
				/** cek punya paket aktif atau tidak */
				$paket_aktif = auth()->user()->paket_data()->where('is_active', 1)->where(DB::raw("DATE(valid_until)"), '>=', date('Y-m-d'))->orderBy('valid_until','DESC')->first();
				if ($paket_aktif && $paket_aktif->count() > 0) {
//					$arrDataType = $paket_aktif->paket_data->data_type()->where('is_active',1)->get()->pluck('type')->toArray();
					$arrDataType = $paket_aktif->paket_data->data_type()->where('cmeds_m_data_type.is_active',1)->get()->pluck('type')->toArray();
//		            return in_array($type,$arrDataType) ? 'true':'false';
				}
			}

//			return $data->toSql();
			$emitenData = $data->paginate(12);
//			return $emitenData;
//			return \DB::getQueryLog();
			$parentsektor = Sector::where('idParentSector', 0)->get()->pluck('name', 'idSector');
			$jenisData    = GeneralHelper::getJenisData($type, $doctype);
			return view('ticmi.emitendata.index', compact('emitenData', 'emiten', 'doctype', 'type', 'title', 'quartal', 'year', 'code', 'prospectus_type', 'parentsektor', 'jenisData', 'arrDataType', 'pageTitle'));
		} else {
			abort(404);
		}
	}
	
	public function overview()
	{
		$pageTitle = 'Overview Data Services TICMI';
		$paketDataAll = PaketData::where('is_active', 1)->get();
		$dataTypes    = DataType::where('is_active', 1)->get();
		return view('ticmi.emitendata.overview', compact('pageTitle', 'paketDataAll', 'dataTypes'));
	}
	
	public function dataLainnya()
	{
		$pageTitle = "Data Pasar Modal Lainnya";
		return view('ticmi.emitendata.data-lainnya', compact('pageTitle'));
	}
	
	public function viewpdf($doctype, $type, EmitenData $emitendata)
	{
		$path = GeneralHelper::getPdfUrl($emitendata->docType, $emitendata->type, $emitendata->year);
		
		$limitYear = date('Y') - 3;
		$numpages  = 0;
		if (auth()->check()) {
			if ($emitendata->year < $limitYear && auth()->user()->roles()->first() == 'member') {
				$numpages = 10;
			}
		} else {
			if ($emitendata->year < $limitYear) {
				$numpages = 10;
			}
		}
		
//		JavaScript::put([
//			'base_url'     => url('/'),
//			//            'pdf_url' => route('dataemiten.getpdf', ['doctype' => $doctype, 'type' => $type, 'emitendata' => $emitendata]),
//			'pdf_url'      => url($path . $emitendata->filename),
//			//			'pdf_url' => url('ptro-1990-ar-q0-04.pdf'),
//			'num_pages'    => $numpages,
//			'idEmitenData' => $emitendata->idEmitenData
//		]);
		
		return view('ticmi.emitendata.viewpdf', compact('emitendata'));
	}
	
	public function viewSingle($doctype, $type, EmitenData $emitenData, $title = '')
	{
		$jenisData = GeneralHelper::getJenisData($type, $doctype);
		$pageTitle = $emitenData->title;
		
		$arrDataType = null;
		if (auth()->check()) {
			/** cek punya paket aktif atau tidak */
			$paket_aktif = auth()->user()->paket_data()->where('is_active', 1)->where(DB::raw("DATE(valid_until)"), '>=', date('Y-m-d'))->first();
			if ($paket_aktif && $paket_aktif->count() > 0) {
				$arrDataType = $paket_aktif->paket_data->data_type()->where('cmeds_m_data_type.is_active',1)->get()->pluck('type')->toArray();
//		            return in_array($type,$arrDataType) ? 'true':'false';
			}
		}
		return view('ticmi.emitendata.view-single', compact('emitenData', 'pageTitle', 'jenisData', 'doctype', 'type', 'arrDataType'));
	}
	
	public function getpdf($doctype, $type, EmitenData $emitendata)
	{
		$path = GeneralHelper::getPdfUrl($emitendata->docType, $emitendata->type, $emitendata->year);
		$path .= $emitendata->filename;
		class_exists('TCPDF', true);
		$pdf = new FPDI();
		
		$pageCount = $pdf->setSourceFile($path);
		
		/**
		 * Cek data tahun dan member role
		 */
		$limitYear = date('Y') - 3;
		if (auth()->check()) {
			if ($emitendata->year < $limitYear && auth()->user()->roles()->first() == 'member') {
				$pageCount = 10;
			}
		} else {
			if ($emitendata->year < $limitYear) {
				$pageCount = 10;
			}
		}
		
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
			// import a page
			$templateId = $pdf->importPage($pageNo);
			// get the size of the imported page
			$size = $pdf->getTemplateSize($templateId);
			
			// create a page (landscape or portrait depending on the imported page size)
			if ($size['w'] > $size['h']) {
				$pdf->AddPage('L', [$size['w'], $size['h']]);
			} else {
				$pdf->AddPage('P', [$size['w'], $size['h']]);
			}
			
			// use the imported page
//			$pdf->AddPage();
			$pdf->useTemplate($templateId, 0, 0, null);
//
//			$pdf->SetFont('Helvetica');
//			$pdf->SetXY(5, 5);
//			$pdf->Write(8, 'A complete document imported with FPDI');
		}
		
		$pdf->Output($emitendata->filename);
	}
	
	public function getthumbnail($doctype, $type, EmitenData $emitenData)
	{
		$base_pdf_path = '/data/pdf/';
		$path          = GeneralHelper::getPdfUrl($doctype, $type, $emitenData->year);
		$path          = $path . $emitenData->filename;
		if (file_exists($path)) {
			echo 'OK';
			$thumb = new \Imagick($path . '[0]');
//			$thumb->setImageResolution(100,150);
//			$thumb->setImageFormat('png');
//			$thumb->resizeImage(350,250,1,0);
//			//		$thumb->enhanceImage();
//			$thumb->setBackgroundColor(new \ImagickPixel('#ffffff'));
//
//			header("Content-Type: image/jpeg");
//			$thumbnail = $thumb->getImageBlob();
//			echo $thumbnail;
		} else {
			echo 'false';
//			header("Content-Type: image/jpeg");
//			$image = file_get_contents(public_path('assets/images/default/logo-ticmi.jpg'));
//			echo $image;
		}
	}
	
	public function getFileRetail(Request $request, $doctype, $type, $emitendataencrypt, $keranjang = null)
	{
		/** Cek dulu apakah sudah ada paket */
		$paketUserActive = PaketUser::where('user_id', auth()->user()->peserta_id)->where('is_active', 1)->where('valid_until', '>', date('Y-m-d H:i:s'))->first();
		if ($paketUserActive && $paketUserActive->count() > 0) {
			
			/** Cek apakah masih dalam periode download */
			if ($keranjang && $keranjang->valid_until >= date('Y-m-d H:I:s')) {
				$path = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);
				
				$file    = explode('.', $emitendataencrypt->filename);
				$extfile = array_pop($file);
				
				if ($extfile == 'xls') {
					$extfile = 'xls';
				} elseif ($extfile == 'xlsx') {
					$extfile = 'xlsx';
				} elseif ($extfile == 'txt') {
					$extfile = 'txt';
				} else {
					$extfile = 'pdf';
				}
				$filename   = implode('.', $file) . '.' . $extfile;
				$path       .= $filename;
				$idEncrypt  = encrypt($emitendataencrypt->idEmitenData);
				$from_email = $request->get('from_email');
				/**
				 * Cek Download Log for current date
				 */
				$cekLog = DownloadLog::where('user_id', \Auth::user()->peserta_id)
					->where('idEmitenData', $emitendataencrypt->idEmitenData)
					->where(DB::raw('EXTRACT(YEAR FROM download_date)'), date('Y'))
					->where(DB::raw('EXTRACT(MONTH FROM download_date)'), date('m'))
					->where('paket_user_id', $paketUserActive->id)->first();
				if ($cekLog && $cekLog->count() > 0) {
					$cekLog->increment('counter');
					if ($from_email == 1) {
						$cekLog->increment('from_email');
					} else {
						$cekLog->increment('from_web');
					}
				} else {
					
					$arrDataType = $paketUserActive->paket_data->data_type()->where('cmeds_m_data_type.is_active',1)->get()->pluck('type')->toArray();
					
					$download_log                = new DownloadLog();
					$download_log->user_id       = \Auth::user()->peserta_id;
					$download_log->idEmitenData  = $emitendataencrypt->idEmitenData;
					$download_log->emiten_code   = $emitendataencrypt->emiten->code;
					$download_log->file_title    = $emitendataencrypt->title;
					$download_log->file_type     = $emitendataencrypt->type;
					$download_log->file_doctype  = $emitendataencrypt->docType;
					$download_log->year          = $emitendataencrypt->year;
					$download_log->quarter       = $emitendataencrypt->quarter;
					$download_log->download_date = date('Y-m-d H:i:s');
					$download_log->counter       = 1;
					$download_log->save();
				}
				
				return view('frontend.emitendata.downloadcounter', compact('idEncrypt', 'doctype', 'type', 'path'));
			} else {
				flash()->error('Periode unduhan sudah berakhir');
				return view('frontend.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
			}
		} else {
			flash()->error('Anda tidak bisa unduh data jika belum berlangganan Paket Data');
			return view('ticmi.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
		}
	}
	
	public function getFile(Request $request, $doctype, $type, $emitendataencrypt)
	{
		/** Cek dulu apakah sudah ada paket */
//		return $emitendataencrypt;
		$paketUserActive = PaketUser::where('user_id', auth()->user()->peserta_id)->where('is_active', 1)->where('valid_until', '>', date('Y-m-d H:i:s'))->first();
		if ($paketUserActive && $paketUserActive->count() > 0) {
			
			$path = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);
			
			$file    = explode('.', $emitendataencrypt->filename);
			$extfile = array_pop($file);
			
			if ($extfile == 'xls') {
				$extfile = 'xls';
			} elseif ($extfile == 'xlsx') {
				$extfile = 'xlsx';
			} elseif ($extfile == 'txt') {
				$extfile = 'txt';
			} else {
				$extfile = 'pdf';
			}
			$filename   = implode('.', $file) . '.' . $extfile;
			$path       .= $filename;
			$idEncrypt  = encrypt($emitendataencrypt->idEmitenData);
			$from_email = $request->get('from_email');
			
			/**
			 * Cek Download Log for current date
			 */
			$cekLog = DownloadLog::where('user_id', \Auth::user()->peserta_id)
				->where('idEmitenData', $emitendataencrypt->idEmitenData)
				->where(DB::raw('EXTRACT(YEAR FROM download_date)'), date('Y'))
				->where(DB::raw('EXTRACT(MONTH FROM download_date)'), date('m'))
				->where('paket_user_id', $paketUserActive->id)->first();
			if ($cekLog && $cekLog->count() > 0) {
				$cekLog->increment('counter');
				if ($from_email == 1) {
					$cekLog->increment('from_email');
				} else {
					$cekLog->increment('from_web');
				}
			} else {
				
				$arrDataType = $paketUserActive->paket_data->data_type()->where('cmeds_m_data_type.is_active',1)->get()->pluck('type')->toArray();
				
				$download_log                = new DownloadLog();
				$download_log->user_id       = \Auth::user()->peserta_id;
				$download_log->idEmitenData  = $emitendataencrypt->idEmitenData;
				$download_log->emiten_code   = $emitendataencrypt->emiten->code;
				$download_log->file_title    = $emitendataencrypt->title;
				$download_log->file_type     = $emitendataencrypt->type;
				$download_log->file_doctype  = $emitendataencrypt->docType;
				$download_log->year          = $emitendataencrypt->year;
				$download_log->quarter       = $emitendataencrypt->quarter;
				$download_log->download_date = date('Y-m-d H:i:s');
				$download_log->counter       = 1;
				$download_log->paket_user_id = $paketUserActive->id;
				$download_log->save();
			}
			
			return view('ticmi.emitendata.downloadcounter', compact('idEncrypt', 'doctype', 'type', 'path'));
		} else {
			flash()->error('Anda tidak bisa unduh data jika belum berlangganan Paket Data');
			return view('ticmi.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
		}
	}
	
	public function getFileDownload($doctype, $type, $emitendataencrypt, $keranjang = null)
	{
		$limitYear       = date('Y') - 3;
		$paketUserActive = PaketUser::where('user_id', auth()->user()->peserta_id)->where('is_active', 1)->where('valid_until', '>', date('Y-m-d H:i:s'))->first();
		
		/** Cek Apakah user Punya SID dan dalam Tahun masih bisa View */
		if ($emitendataencrypt->year > $limitYear && auth()->user()->roles()->first() == 'member') {
			/** Jika masuk dalam tahun yang dapat di unduh dan Member SID langsung unduh file */
			$path    = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);
			$file    = explode('.', $emitendataencrypt->filename);
			$extfile = array_pop($file);
			if ($extfile == 'xls') {
				$extfile = 'xls';
				$mime = 'application/vnd.ms-excel';
			} elseif ($extfile == 'xlsx') {
				$extfile = 'xlsx';
				$mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
			} elseif ($extfile == 'txt') {
				$extfile = 'txt';
				$mime = 'text/plain';
			} else {
				$extfile = 'pdf';
				$mime = 'application/pdf';
			}
			$filename = implode('.', $file) . '.' . $extfile;
			$path     .= $filename;
			
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header('Content-Type: '.$mime);
			header("Content-Length: " . (string)(filesize($path)));
			header('Content-Disposition: attachment; filename="' . $filename . '"');
			header("Content-Transfer-Encoding: binary\n");
			
			readfile($path); // outputs the content of the file
			
			exit();
		} else {
			/** Cek apakah punya Paket Data Aktif atau tidak */
			if ($paketUserActive && $paketUserActive->count() > 0) {
				/** Cek apakah retail atau bukan */
				if ($keranjang && $keranjang->valid_until < date('Y-m-d H:I:s')) {
					flash()->error('Periode unduhan sudah berakhir');
					return view('frontend.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
				} else {
					$arrDataType = $paketUserActive->paket_data->data_type()->get()->pluck('type')->toArray();
					if (!empty($arrDataType) && in_array($type, $arrDataType)) {
						$path    = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);
						$file    = explode('.', $emitendataencrypt->filename);
						$extfile = array_pop($file);
						
						if ($extfile == 'xls') {
							$extfile = 'xls';
							$mime = 'application/vnd.ms-excel';
						} elseif ($extfile == 'xlsx') {
							$extfile = 'xlsx';
							$mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
						} elseif ($extfile == 'txt') {
							$extfile = 'txt';
							$mime = 'text/plain';
						} else {
							$extfile = 'pdf';
							$mime = 'application/pdf';
						}
						$filename = implode('.', $file) . '.' . $extfile;
						$path     .= $filename;
						
						header("Pragma: public");
						header("Expires: 0");
						header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
						header("Cache-Control: public");
						header("Content-Description: File Transfer");
						header('Content-Type: '.$mime);
						header("Content-Length: " . (string)(filesize($path)));
						header('Content-Disposition: attachment; filename="' . $filename . '"');
						header("Content-Transfer-Encoding: binary\n");
						
						readfile($path); // outputs the content of the file
						
						exit();
					} else {
						flash()->error('Anda tidak bisa unduh data jika belum berlangganan Paket Data');
						return view('frontend.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
					}
				}
			} else {
				flash()->error('Anda tidak bisa unduh data jika belum berlangganan Paket Data');
				return view('frontend.emitendata.no-download', compact('idEncrypt', 'doctype', 'type', 'path'));
				
			}
		}
	}
	
	public function pengumuman(Request $request, $doctype, $type)
	{
		$title = $request->get('title');
		$year  = $request->get('year');
		
		$data = Peraturan::where('doctype', $doctype)->orderBy('date', 'DESC');
		
		if ($title) {
			$data->where('title', 'LIKE', "%$title%");
		}
		if ($year) {
			$data->where('date', 'LIKE', "$year%");
		}
		$pengumuman = $data->paginate(12);
		
		$jenisData = GeneralHelper::getJenisData($type, $doctype);
		return view('ticmi.emitendata.pengumuman', compact('pengumuman', 'doctype', 'type', 'year', 'title', 'jenisData'));
	}
	
	public function viewPengumumanPdf($doctype, $type, Peraturan $pengumuman)
	{
		$path    = GeneralHelper::getPdfUrl($doctype, $type, date('Y', strtotime($pengumuman->date)));
		$file    = explode('.', $pengumuman->filename);
		$extfile = array_pop($file);
		
		if ($extfile !== 'pdf') {
			$extfile = 'pdf';
		}
		$filename = implode('.', $file) . '.' . $extfile;
		JavaScript::put([
			'base_url'  => url('/'),
			'pdf_url'   => 'http://dev.ticmi.co.id' . $path . $filename,
			'num_pages' => 1
		]);
		
		return view('ticmi.emitendata.viewpdf', compact('pengumuman'));
	}
	
	public function getPengumumanFile(Request $request, $doctype, $type, Peraturan $pengumumanencrypt)
	{
		$path       = GeneralHelper::getPdfUrl($pengumumanencrypt->docType, $type, date('Y', strtotime($pengumumanencrypt->date)));
		$path       .= $pengumumanencrypt->filename;
		$from_email = $request->get('from_email');
		
		/**
		 * Cek Download Log for current date
		 */
		$cekLog = DownloadLog::where('user_id', \Auth::user()->peserta_id)->where('idEmitenData', $pengumumanencrypt->idPeraturan)->where(DB::raw('YEAR(download_date)'), date('Y'))->WHERE(DB::raw('MONTH(download_date)'), date('m'))->first();
		if ($cekLog && $cekLog->count() > 0) {
			$cekLog->increment('counter');
			if ($from_email == 1) {
				$cekLog->increment('from_email');
			} else {
				$cekLog->increment('from_web');
			}
		} else {
			$download_log                = new DownloadLog();
			$download_log->user_id       = \Auth::user()->peserta_id;
			$download_log->idEmitenData  = $pengumumanencrypt->idPeraturan;
			$download_log->institusi     = $pengumumanencrypt->institusi;
			$download_log->file_title    = $pengumumanencrypt->title;
			$download_log->file_type     = $pengumumanencrypt->docType;
			$download_log->file_doctype  = $pengumumanencrypt->docType;
			$download_log->date_file     = $pengumumanencrypt->date;
			$download_log->download_date = date('Y-m-d H:i:s');
			$download_log->counter       = 1;
			$download_log->save();
		}
		
		$idEncrypt = encrypt($pengumumanencrypt->idPeraturan);
		
		return view('ticmi.emitendata.downloadcounter', compact('idEncrypt', 'doctype', 'type', 'path'));
	}
	
	public function getPengumumanDownload($doctype, $type, Peraturan $pengumumanencrypt)
	{
		$path = GeneralHelper::getPdfUrl($pengumumanencrypt->docType, $type, date('Y', strtotime($pengumumanencrypt->date)));
		$path .= $pengumumanencrypt->filename;
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header('Content-Type: application/pdf');
		header("Content-Length: " . (string)(filesize($path)));
		header('Content-Disposition: attachment; filename="' . basename($pengumumanencrypt->filename) . '"');
		header("Content-Transfer-Encoding: binary\n");
		
		readfile($path); // outputs the content of the file
		
		exit();
	}
	
	public function peraturan(Request $request, $doctype, $type)
	{
		$title = $request->get('title');
		$year  = $request->get('year');
		
		$data = Peraturan::where('doctype', $doctype)->orderBy('date', 'DESC');
		
		if ($title) {
			$data->where('title', 'LIKE', "%$title%");
		}
		if ($year) {
			$data->where('date', 'LIKE', "$year%");
		}
		$pengumuman = $data->paginate(12);
		
		$jenisData = GeneralHelper::getJenisData($type, $doctype);
		return view('ticmi.emitendata.pengumuman', compact('pengumuman', 'doctype', 'type', 'year', 'title', 'jenisData'));
	}
	
	public function corporateAction(Request $request, $doctype, $type)
	{
		if ($doctype && $type) {
			$year            = $request->get('year');
			$title           = $request->get('title');
			$data            = EmitenData::where('docType', $doctype)->where('type', $type)->orderBy('idEmitenData', 'DESC');
			$corporateAction = $data->paginate(12);
			$jenisData       = GeneralHelper::getJenisData($type, $doctype);
			return view('frontend.emitendata.corporateaction.index', compact('doctype', 'type', 'corporateAction', 'year', 'title', 'jenisData'));
		} else {
			abort(404);
		}
	}
}
