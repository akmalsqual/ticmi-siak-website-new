<?php

namespace App\Http\Controllers\ujian_ulang;

use Illuminate\Http\Request;
use App\Http\Requests\Upload_pembayaranRequest;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Route;
use Url;
use Auth;
use Lang;
use DB;
use File;
use Mail;
use Crypt;
use App\Models\Ujian_pendaftaran;
use App\Models\Ujian_peserta;
use App\Models\Soal_ujian_ulang;
use App\Models\Soal_peserta_ulang;
use App\Models\Peserta_jawab_ulang;
use App\Models\Ujian_modul;
use App\Models\Soal;
use App\Models\Peserta_lulus;
use App\Models\Ujian;
use App\Models\Ujian_detail;
use App\Models\Waktu_persoal;
use App\Models\Waktu_sisa_perdana;
use App\Models\Waktu_ujian_ulang;
use App\Models\Waktu_sisa_ulang;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;

class Ujian_ulangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_batch)
    {
        $batch_id           = $id_batch;
        $url_ajax_datatable = route('ujian_ulang.ajax_datatable');
        $profile            = DB::table('profile_peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
        $lokasi             = DB::table('lokasi')->where('hapus', false)->where('aktif', true)->pluck('nama', 'lokasi_id')->prepend('','');
        $cabang             = DB::table('cabang')
                                 ->where('hapus', false)
                                 ->where('aktif', true)
                                 ->where('lokasi_id', $profile->lokasi_id)
                                 ->pluck('nama', 'cabang_id')->prepend('','');
        $cabang_id          = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $profile->lembg_pdkn_id)->value('cabang_id');
        $kp                 = DB::table('lembg_pdkn')
                                ->where('hapus', false)
                                ->where('aktif', true)
                                ->where('cabang_id', $cabang_id)
                                ->pluck('nama', 'lembg_pdkn_id')->prepend('','');
        $ujian_batch_ids    = DB::table('ujian_batch')->where('batch_id', $id_batch)->pluck('ujian_batch_id');
        $cek_peserta        = DB::table('perdana_peserta')
                                ->whereIn('ujian_batch_id', $ujian_batch_ids)
                                ->where('peserta_id', Auth::user()->peserta_id);

        if($cek_peserta->count() > 0) {
            return view('sertifikasi.ujian_ulang.jadwal_ujian_ulang', compact('url_ajax_datatable','lokasi', 'profile', 'cabang','cabang_id', 'kp', 'batch_id'));
        } else {
            return abort(404);
        }
    }

    public function info_ulang($id_batch)
    {
        $pendaftaran        = DB::table('ujian_pendaftaran')
                                ->where('peserta_id', Auth::user()->peserta_id)
                                ->where('batch_id', $id_batch)
                                ->whereIn('ulang_jadwal_id', function($query){
                                    $query->select(DB::raw('ulang_jadwal_id'))
                                          ->from('ulang_jadwal')
                                          ->whereDate('tgl_ulang', '>=', date('Y-m-d'));
                                  })
                                ->get();
                                
        $ujian_batch_ids    = DB::table('ujian_batch')->where('batch_id', $id_batch)->pluck('ujian_batch_id');
        $cek_peserta        = DB::table('perdana_peserta')
                                ->whereIn('ujian_batch_id', $ujian_batch_ids)
                                ->where('peserta_id', Auth::user()->peserta_id);
        $url_ajax_info_ulang = route('sertifikasi.ajax_info_ulang');
        $url_ajax_get_detail_pendaftaran = route('sertifikasi.ajax_get_detail_pendaftaran');

        if($cek_peserta->count() > 0) {
            return view('sertifikasi.ujian_ulang.info_ujian_ulang', compact('pendaftaran', 'url_ajax_info_ulang', 'id_batch','url_ajax_get_detail_pendaftaran'));
        } else {
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_pendaftaran_ulang(Request $request)
    {
        if ($request->ajax()) {
            $batch_id        = $request->batch_id;
            $peserta_id      = $request->peserta_id;
            $ulang_jadwal_id = $request->sesi_id; 
            

            $pendaftaran_id     = DB::table('pendaftaran_trx')
                                    ->where('peserta_id', $peserta_id)
                                    ->where('batch_id', $batch_id)
                                    ->whereNotIn('peserta_aktivasi_id', function($q){
                                        $q->select('peserta_aktivasi_id')
                                          ->from('peserta_cancel');
                                    })
                                    ->value('pendaftaran_id');

            $pendaftaran_trx_id = DB::table('pendaftaran_trx')
                                    ->where('peserta_id', $peserta_id)
                                    ->where('batch_id', $batch_id)
                                    ->whereNotIn('peserta_aktivasi_id', function($q){
                                        $q->select('peserta_aktivasi_id')
                                          ->from('peserta_cancel');
                                    })
                                    ->value('pendaftaran_trx_id');
            $ulang_jadwal       = DB::table('ulang_jadwal')->where('ulang_jadwal_id', $ulang_jadwal_id)->first();
            $ulang              = DB::table('ulang')->where('ulang_id', $ulang_jadwal->ulang_id)->first();
            $nama_peserta       = DB::table('peserta')->where('peserta_id', $peserta_id)->value('nama');
            $email_peserta      = DB::table('peserta')->where('peserta_id', $peserta_id)->value('email');
            $nama_batch         = DB::table('batch')->where('batch_id', $batch_id)->value('nama');
            $nama_kp            = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $ulang->lembg_pdkn_id)->value('nama');
            $tgl_daftar         = date('Y-m-d');
            
            // ujian ulang ke berapa ?
            $cek_uji_ulang     = DB::table('ujian_pendaftaran')
                                   ->where('pendaftaran_id', $pendaftaran_id)
                                   ->where('pendaftaran_trx_id', $pendaftaran_trx_id)
                                   ->where('batch_id', $batch_id)
                                   ->whereIn('ujian_pendaftaran_id', function($query){
                                              $query->select('ujian_pendaftaran_id')
                                                    ->from('ujian_peserta')
                                                    ->whereIn('ujian_peserta_id', function($q){
                                                              $q->select('ujian_peserta_id')
                                                                ->from('soal_ujian_ulang');
                                                    });
                                     });

            if($cek_uji_ulang->count() > 0) {
                $last_ulang           = $cek_uji_ulang->max('ujian_pendaftaran_id');
                $ujian_peserta_id     = DB::table('ujian_peserta')->where('ujian_pendaftaran_id', $last_ulang)->value('ujian_peserta_id');
                $soal_peserta_ids = DB::table('soal_ujian_ulang')
                                          ->where('ujian_peserta_id', $ujian_peserta_id)
                                          ->whereIn('soal_ujian_ulang_id', function($query){
                                            $query->select('soal_peserta_ulang.soal_ujian_ulang_id')
                                                  ->from('soal_peserta_ulang')
                                                  ->where('soal_peserta_ulang.is_lulus', false);
                                            })
                                          ->pluck('soal_peserta_id');
            } else {
                $perdana_peserta_id  = DB::table('perdana_peserta')
                                          ->where('peserta_id', $peserta_id)
                                          ->max('perdana_peserta_id');

                $soal_peserta_ids    = DB::table('soal_peserta')
                                          ->where('perdana_peserta_id', $perdana_peserta_id)
                                          ->where('is_lulus', false)
                                          ->pluck('soal_peserta_id');
            }

            DB::beginTransaction();
            $success_trans = false;
            try {
                // insert to tbl:ujian_pendaftaran
               $ujian_pendaftaran                     = new Ujian_pendaftaran;
               $ujian_pendaftaran->pendaftaran_id     = $pendaftaran_id;
               $ujian_pendaftaran->ulang_jadwal_id    = $ulang_jadwal_id;
               $ujian_pendaftaran->pendaftaran_trx_id = $pendaftaran_trx_id;
               $ujian_pendaftaran->batch_id           = $batch_id;
               $ujian_pendaftaran->peserta_id         = $peserta_id;
               $ujian_pendaftaran->nama_peserta       = $nama_peserta;
               $ujian_pendaftaran->email              = $email_peserta;
               $ujian_pendaftaran->nama_batch         = $nama_batch;
               $ujian_pendaftaran->nama_lembg_pdkn    = $nama_kp;
               $ujian_pendaftaran->tgl_daftar         = $tgl_daftar;
               $ujian_pendaftaran->ulang_ke           = null; //$ulang_ke;
               $ujian_pendaftaran->is_bayar           = null;
               $ujian_pendaftaran->harga_pendaftaran  = null;
               $ujian_pendaftaran->no_invoice         = null;
               $ujian_pendaftaran->referral_id        = null;
               $ujian_pendaftaran->no_va              = null;
               $ujian_pendaftaran->date_created       = date('Y-m-d H:i:s');
               $ujian_pendaftaran->user_created       = \Auth::user()->peserta_id;
               $ujian_pendaftaran->save();
               
            DB::commit();
            $success_trans = true;
            } catch (\Exception $e) {
                DB::rollback();

                // error page
                abort(403, $e->getMessage());
            }

            if($success_trans == true){
                return response()->json([
                    'status' => Lang::get('db.saved')
                ]);
            } else if($success_trans == false){
                return response()->json([
                    'status' => Lang::get('db.failed_upadated')
                ]);
            }

        }
    }

    public function ajax_datatable(Request $request)
    {
        if ($request->ajax()) {
             // get row number datatable
            $sql_no_urut    = \Yajra_datatable::get_no_urut('ulang_jadwal.ulang_jadwal_id' /*primary_key*/, $request);
            $batch_id       = $request->get('batch');
            if (!empty($request->get('kp'))) {
                $kp = $request->get('kp');
            } else {
                $kp = 0;
            }

            $jadwal         = DB::table('ulang_jadwal')
                                ->select([
                                        DB::raw($sql_no_urut), // nomor urut
                                        'ulang_jadwal.ulang_jadwal_id AS _id',
                                        'ulang_jadwal.nama AS nama',
                                        'ulang_jadwal.tgl_ulang AS tgl_ulang',
                                        'lembg_pdkn.nama AS nama_kp',
                                        'ruang.nama AS nama_ruang',
                                        'ruang.kapasitas AS kapasitas',
                                        'hari.nama AS nama_hari',
                                        'jam.nama AS nama_jam'
                                        ])
                                ->join('ulang', 'ulang.ulang_id', '=', 'ulang_jadwal.ulang_id')
                                ->join('lembg_pdkn', 'lembg_pdkn.lembg_pdkn_id', '=', 'ulang.lembg_pdkn_id')
                                ->join('ruang', 'ruang.ruang_id', '=', 'ulang.ruang_id')
                                ->join('jam', 'jam.jam_id', '=', 'ulang_jadwal.jam_id')
                                ->join('hari', 'hari.hari_id', '=', 'ulang_jadwal.hari_id')
                                ->where('ulang.hapus', false)
                                ->where('lembg_pdkn.lembg_pdkn_id', $kp)
                                ->whereDate('ulang_jadwal.tgl_ulang', '>=', date('Y-m-d'))
                                ->where('ulang_jadwal.is_aktif', TRUE);

            return Datatables::of($jadwal)
                ->addIndexColumn()
                ->addColumn('tgl_ulang', function($jadwal){
                    return \Helper::date_formats($jadwal->tgl_ulang, 'view');
                })
                ->addColumn('kuota', function($jadwal){
                    $peserta_terdaftar = DB::table('ujian_pendaftaran')->where('ulang_jadwal_id', $jadwal->_id)->count();
                    $kuota   = $jadwal->kapasitas - $peserta_terdaftar;
                    return '<span class="label label-sm label-success">'.$kuota.' Peserta</span>';
                })
                ->addColumn('action', function ($jadwal) use($batch_id){
                    $check_daftar = Ujian_pendaftaran::where('ulang_jadwal_id', $jadwal->_id)
                                                     ->where('peserta_id', Auth::user()->peserta_id)
                                                     ->where('batch_id', $batch_id);
                    $peserta_terdaftar = DB::table('ujian_pendaftaran')->where('ulang_jadwal_id', $jadwal->_id)->count();
                    $kuota   = $jadwal->kapasitas - $peserta_terdaftar;

                    if($check_daftar->count() > 0) {
                        $btn_action = '<a class="btn btn-sm btn-default tooltips" data-toogle="tooltip" data-placement="top" title="Anda telah terdaftar disesi ini" disabled>Daftar</a>';
                    } elseif ($kuota == 0) {
                        $btn_action = '<a class="btn btn-sm btn-default tooltips" data-toogle="tooltip" data-placement="top" title="Kuota pendaftaran ujian telah habis" disabled>Daftar</a>';
                    } else {
                        $btn_action = '&nbsp;<a data-jadwal="'.$jadwal->_id.'" class="btn green btn-outline daftar_ulang">Daftar</a>&nbsp;';
                    }

                    return $btn_action;
                })
                ->rawColumns(['kuota','action']) // to html
                ->make(true);
        }
    }

    public function ajax_info_ulang(Request $request)
    {
        if ($request->ajax()) {
             // get row number datatable
            $sql_no_urut    = \Yajra_datatable::get_no_urut('ujian_pendaftaran.ujian_pendaftaran_id' /*primary_key*/, $request);
            $batch_id       = $request->get('batch');
            $peserta_id     = $request->get('peserta');
            $jadwal         = DB::table('ujian_pendaftaran')
                                ->select([
                                        DB::raw($sql_no_urut), // nomor urut
                                        'ujian_pendaftaran.ujian_pendaftaran_id AS _id',
                                        'ulang_jadwal.nama AS nama',
                                        'ulang_jadwal.tgl_ulang AS tgl_ulang',
                                        'lembg_pdkn.nama AS nama_kp',
                                        'ruang.nama AS nama_ruang',
                                        'ruang.kapasitas AS kapasitas',
                                        'hari.nama AS nama_hari',
                                        'jam.nama AS nama_jam',
                                        'ujian_pendaftaran.is_bayar AS is_bayar',
                                        'ujian_pendaftaran.file_upload AS file_upload'
                                        ])
                                ->join('ulang_jadwal', 'ulang_jadwal.ulang_jadwal_id', '=', 'ujian_pendaftaran.ulang_jadwal_id')
                                ->join('ulang', 'ulang.ulang_id', '=', 'ulang_jadwal.ulang_id')
                                ->join('lembg_pdkn', 'lembg_pdkn.lembg_pdkn_id', '=', 'ulang.lembg_pdkn_id')
                                ->join('ruang', 'ruang.ruang_id', '=', 'ulang.ruang_id')
                                ->join('jam', 'jam.jam_id', '=', 'ulang_jadwal.jam_id')
                                ->join('hari', 'hari.hari_id', '=', 'ulang_jadwal.hari_id')
                                //->where('ulang.hapus', false)
                                //->whereDate('ulang_jadwal.tgl_ulang', '>=', date('Y-m-d'))
                                ->where('ujian_pendaftaran.batch_id', $batch_id)
                                ->where('ujian_pendaftaran.peserta_id', $peserta_id);
                                //->where('ulang_jadwal.is_aktif', TRUE);

            return Datatables::of($jadwal)
                ->addIndexColumn()
                ->addColumn('tgl_ulang', function($jadwal){
                    return \Helper::date_formats($jadwal->tgl_ulang, 'view');
                })
                ->addColumn('pembayaran', function($jadwal){
                    if($jadwal->is_bayar === true){
                        $pembayaran = 'Berbayar';
                        $class      = 'info';
                    } elseif($jadwal->is_bayar === false){
                        $pembayaran = 'Gratis';
                        $class      = 'success';
                    } else {
                        $pembayaran = 'Menunggu Konfirmasi';
                        $class      = 'warning';
                    }
                    return '<span class="label label-'. $class .'">'.$pembayaran.'</span>';
                })
                ->addColumn('status', function ($jadwal) use($batch_id){
                  $check_selesai   = DB::table('ujian_peserta')->where('ujian_pendaftaran_id', $jadwal->_id)->where('selesai', true)->first();
                  if(!empty($check_selesai))
                  {
                    $status = '<span class="label label-success">Telah selesai</span>';
                  } else {
                    $valid  = DB::table('tagihan_tmp')
                                ->where('ujian_pendaftaran_id', $jadwal->_id)
                                ->first();

                    if($jadwal->is_bayar === TRUE){
                      $bayar  = DB::table('tagihan_tmp')
                                ->where('ujian_pendaftaran_id', $jadwal->_id)
                                ->where(function ($query) {
                                        $query->whereIn('tagihan_tmp_id', function($q){
                                                    $q->select('tagihan_tmp_id')
                                                      ->from('bayar_manual');
                                                })
                                              ->orWhereIn('tagihan_tmp_id', function($q){
                                                    $q->select('tagihan_tmp_id')
                                                      ->from('bayar');
                                                });
                                  })->first();
                      if(!empty($bayar)){
                        $status = '<span class="label label-info">Telah terdaftar</span>';
                      } elseif(empty($bayar)){
                        $status = '<span class="label label-warning">Menunggu Pembayaran</span>';
                      }
                    } elseif($jadwal->is_bayar === FALSE){
                       $status = '<span class="label label-info">Telah terdaftar</span>';
                    } elseif($jadwal->is_bayar === NULL){
                       $status = '<span class="label label-warning">Belum dikonfirmasi</span>';
                    }
                  }

                    return $status;
                })
                ->addColumn('action', function ($jadwal) use($batch_id){
                   $btn_action = '<a data-jadwal="'.$jadwal->_id.'" title="Detail pendaftaran" class="fa fa-search-plus detail_pendaftaran"></a>&nbsp;';

                    return $btn_action;
                })
                ->rawColumns(['pembayaran','status','action']) // to html
                ->make(true);
        }
    }

    public function ajax_get_cabang(Request $request){
        if($request->ajax()){
            $lokasi_id = $request->lokasi;

            $cabang = DB::table('cabang')->select('cabang.cabang_id AS _id', 'cabang.nama')
                                    ->where('cabang.lokasi_id', '=', $lokasi_id)
                                    ->where('cabang.hapus', '=', false)
                                    ->where('cabang.aktif', '=', true)
                                    ->get()->toArray();
            if(!empty($cabang)){
                echo json_encode($cabang);
            }else{
                echo json_encode(array());
            }
        }
    }

    public function ajax_get_kp(Request $request){
        if($request->ajax()){
            $cabang_id = $request->cabang;

            $kp = DB::table('lembg_pdkn')->select('lembg_pdkn.lembg_pdkn_id AS _id', 'lembg_pdkn.nama')
                                    ->where('lembg_pdkn.cabang_id', '=', $cabang_id)
                                    ->where('lembg_pdkn.hapus', '=', false)
                                    ->where('lembg_pdkn.aktif', '=', true)
                                    ->get()->toArray();
            if(!empty($kp)){
                echo json_encode($kp);
            }else{
                echo json_encode(array());
            }
        }
    }

    public function ajax_get_detail_pendaftaran(Request $request){
        if($request->ajax()){
            $ujian_pendaftaran_id = $request->ujian_pendaftaran_id;

            $ujian_pendaftaran = DB::table('ujian_pendaftaran')
                                   ->select(['ujian_pendaftaran.ujian_pendaftaran_id AS _id','ujian_pendaftaran.nama_batch AS nama_batch',
                                    'ujian_pendaftaran.peserta_id AS peserta_id', 'ujian_pendaftaran.batch_id AS batch_id',
                                    'ujian_pendaftaran.nama_peserta AS nama_peserta','ujian_pendaftaran.email AS email_peserta',
                                    'lokasi.nama AS nama_lokasi','cabang.nama AS nama_cabang', 'lembg_pdkn.nama AS nama_kp',
                                    'ujian_pendaftaran.ulang_ke AS ulang_ke', 'ujian_pendaftaran.ulang_ke AS ulang_ke',
                                    'ujian_pendaftaran.no_invoice AS no_invoice', 'ujian_pendaftaran.no_va AS no_va',
                                    'ujian_pendaftaran.is_bayar AS is_bayar', 'ujian_pendaftaran.harga_pendaftaran AS harga_pendaftaran'
                                    ])
                                   ->join('ulang_jadwal', 'ulang_jadwal.ulang_jadwal_id', '=', 'ujian_pendaftaran.ulang_jadwal_id')
                                   ->join('ulang', 'ulang.ulang_id', '=', 'ulang_jadwal.ulang_id')
                                   ->join('lembg_pdkn', 'lembg_pdkn.lembg_pdkn_id', '=', 'ulang.lembg_pdkn_id')
                                   ->join('cabang', 'cabang.cabang_id', '=', 'lembg_pdkn.cabang_id')
                                   ->join('lokasi', 'lokasi.lokasi_id', '=', 'cabang.lokasi_id')
                                   ->where('ujian_pendaftaran.ujian_pendaftaran_id', $ujian_pendaftaran_id)
                                   ->first();
            if(!empty($ujian_pendaftaran)){
                echo json_encode($ujian_pendaftaran);
            }else{
                echo json_encode(array());
            }
        }
    }

    public function start_ujian(Request $request)
    {
      $batch_id             = $request->batch_id;
      $ujian_pendaftaran_id = $request->ujian_pendaftaran_id;
      $peserta_id           = Auth::user()->peserta_id;
      $ujian_peserta_id     = DB::table('ujian_peserta')
                                ->where('ujian_pendaftaran_id', $ujian_pendaftaran_id)
                                ->first()->ujian_peserta_id;
      $check = DB::table('ujian_pendaftaran')
                 ->where('peserta_id', $peserta_id)
                 ->where('ujian_pendaftaran_id', $ujian_pendaftaran_id)
                 ->where('batch_id', $batch_id)
                 ->whereIn('ujian_pendaftaran_id', function($query){
                                $query->select('ujian_pendaftaran_id')
                                      ->from('ujian_peserta')
                                      ->where('is_aktivasi', true)
                                      ->whereIn('ujian_peserta_id', function($q){
                                                $q->select('ujian_peserta_id')
                                                  ->from('soal_ujian_ulang');
                                      });
                       })
                 ->get();

      if($check->count() > 0) {
        // lanjut proses disini
        $ulang_jadwal        = DB::table('ulang_jadwal')
                                 ->whereIn('ulang_jadwal_id', function($query) use($ujian_pendaftaran_id){
                                      $query->select('ulang_jadwal_id')
                                            ->from('ujian_pendaftaran')
                                            ->where('ujian_pendaftaran_id', $ujian_pendaftaran_id);
                                  })
                                 ->first();
        $ujian_pendaftaran   = DB::table('ujian_pendaftaran')->where('ujian_pendaftaran_id', $ujian_pendaftaran_id)->first();
        $tanggal             = date("d-m-Y");
        $emp_id              = $ulang_jadwal->emp_id;
        $keyboard_lock       = $ulang_jadwal->keyboard_lock;
        $nama_batch          = $ujian_pendaftaran->nama_batch;
        $peserta             = $ujian_pendaftaran->nama_peserta;
        $soal_ujian_ulang_id = DB::table('soal_ujian_ulang')
                                  ->whereIn('ujian_peserta_id', function($query) use($peserta_id, $ujian_pendaftaran_id, $batch_id){
                                      $query->select('ujian_peserta_id')
                                            ->from('ujian_peserta')
                                            ->whereIn('ujian_pendaftaran_id',
                                              function($q) use($peserta_id, $ujian_pendaftaran_id, $batch_id){
                                                $q->select('ujian_pendaftaran_id')
                                                  ->from('ujian_pendaftaran')
                                                  ->where('peserta_id', $peserta_id)
                                                  ->where('ujian_pendaftaran_id', $ujian_pendaftaran_id)
                                                  ->where('batch_id', $batch_id);
                                            });

                              })
                              ->pluck('soal_ujian_ulang_id');
        $soal_peserta_id    = DB::table('soal_ujian_ulang')
                                ->whereIn('soal_ujian_ulang_id', $soal_ujian_ulang_id)
                                ->pluck('soal_peserta_id');

        /* cek waktu yang digunakan untuk uian ulang */
        // cek apakah sedang melakukan ujian ulang (disconnect ketika ujian ulang)
        $waktu_sisa         = Waktu_ujian_ulang::where('ujian_peserta_id', $ujian_peserta_id)->first();
        if($waktu_sisa){
          $waktu = $waktu_sisa->sisa_waktu;
        } else {
          // cek apakah pernah melakukan ujian ulang (waktu dari sisa soal terakhir ujian ulang)
          $waktu_ulang      = Waktu_sisa_ulang::where('batch_id', $batch_id)
                                              ->where('peserta_id', $peserta_id)
                                              ->latest('date_created')
                                              ->first();
          if($waktu_ulang){
            $waktu = $waktu_ulang->sisa_waktu;
          } else {
            // cek apakah pernah melakukan ujian perdana (waktu dari sisa soal terakhir ujian perdana)
            $waktu_sisa_perdana = Waktu_sisa_perdana::where('batch_id', $batch_id)->where('peserta_id', $peserta_id)->first();
            if($waktu_sisa_perdana){
              $waktu = $waktu_sisa_perdana->sisa_waktu;
            } else {
              $waktu = Waktu_persoal::where('batch_id', $batch_id)->value('durasi_ujian');
            }           
          }
        }
        /* end get waktu */
        $soal_peserta_ulang_id = DB::table('soal_peserta_ulang')
                                  ->whereIn('soal_ujian_ulang_id', $soal_ujian_ulang_id)
                                  ->pluck('soal_peserta_ulang_id');
        $peserta_jawab      = Peserta_jawab_ulang::select(['peserta_jawab_ulang.soal_id', 'peserta_jawab_ulang.kunci_id', 'modul.nama', 'modul.modul_id'])
                                                 ->whereIn('soal_peserta_ulang_id', function($query) use($soal_ujian_ulang_id){
                                                    $query->select('soal_peserta_ulang_id')
                                                          ->from('soal_peserta_ulang')
                                                          ->whereIn('soal_ujian_ulang_id', $soal_ujian_ulang_id);
                                                 })
                                                 ->join('modul_soal', 'modul_soal.soal_id', 'peserta_jawab_ulang.soal_id')
                                                 ->join('modul', 'modul.modul_id', 'modul_soal.modul_id')
                                                 ->inRandomOrder()
                                                 ->get()
                                                 ->groupBy('modul_id');

        $total_soal         = Peserta_jawab_ulang::whereIn('soal_peserta_ulang_id', function($query) use($soal_ujian_ulang_id){
                                    $query->select('soal_peserta_ulang_id')
                                          ->from('soal_peserta_ulang')
                                          ->whereIn('soal_ujian_ulang_id', $soal_ujian_ulang_id);
                                 })
                                 ->count();

        // array soal yang sudah dijawab
        $ans = [];
        foreach ($peserta_jawab as $value1) {
            foreach ($value1 as $value2) {
                if($value2->kunci_id != null){ // cek database
                    array_push($ans, $value2->soal_id);
                }
            }   
        }

        // update aktivasi -> false
        Ujian_peserta::where('ujian_pendaftaran_id', $ujian_pendaftaran_id)->update(['is_aktivasi'=>false]);
        // delete cookies hasil ujian
        // setcookie("hasil_ujian", "", TIME()- 3600,"/",\Config::get('APP_URL'));

        return view('sertifikasi.ujian_ulang.start_ujian', compact('nama_batch', 'peserta', 'tanggal', 'peserta_jawab', 'emp_id', 'keyboard_lock','soal_peserta_id','soal_peserta_ulang_id', 'total_soal', 'ans', 'waktu', 'ujian_peserta_id'));
      } else {
        return abort(404);
      }

    }

    public function ajax_save_peserta_jawab(Request $request)
    {
        $soal_id               = $request->soal_id;
        $kunci_id              = $request->kunci_id;
        $soal_peserta_ulang_id = $request->soal_peserta_ulang_id;
        $ujian_peserta_id      = $request->ujian_peserta_id;
        $time                  = $request->time;
        $kunci_id_master       = Soal::where('soal_id', $soal_id)->value('kunci_id');
        $check_peserta_jawab   = Peserta_jawab_ulang::whereIn('soal_peserta_ulang_id', $soal_peserta_ulang_id)->where('soal_id', $soal_id)->get();
        $jumlah_peserta_jawab  = count($check_peserta_jawab);

        DB::beginTransaction();
            $success_trans = false;
            try {
              $check_waktu = Waktu_ujian_ulang::where('ujian_peserta_id', $ujian_peserta_id)->first();
              if($check_waktu){
                  Waktu_ujian_ulang::where('ujian_peserta_id', $ujian_peserta_id)->update(['sisa_waktu'=> $time]);
              } else {
                  $waktu_ujian                     = new Waktu_ujian_ulang;
                  $waktu_ujian->ujian_peserta_id   = $ujian_peserta_id;
                  $waktu_ujian->sisa_waktu         = $time;
                  $waktu_ujian->save();
              }

              if($jumlah_peserta_jawab > 0){
                  foreach ($check_peserta_jawab as $cpj) {
                    // update tbl:peserta_jawab_ulang
                      $new_cpj = Peserta_jawab_ulang::where('soal_peserta_ulang_id', $cpj->soal_peserta_ulang_id)->where('soal_id', $cpj->soal_id)->first();
                      $new_cpj->kunci_id = $kunci_id;

                      if($kunci_id == $kunci_id_master){
                          $new_cpj->is_bener = true;
                      }else{
                          $new_cpj->is_bener = false;
                      }

                      $new_cpj->save();
                  }
              }else{
                  foreach ($soal_peserta_id as $spi) {
                      // insert tbl:peserta_jawab_ulang if no matching record
                      $peserta_jawab                        = new Peserta_jawab_ulang;
                      $peserta_jawab->soal_peserta_ulang_id = $spi;
                      $peserta_jawab->soal_id               = $soal_id;
                      $peserta_jawab->kunci_id              = $kunci_id;

                      if($kunci_id == $kunci_id_master){
                          $peserta_jawab->is_bener = true;
                      }else{
                          $peserta_jawab->is_bener = false;
                      }

                      $peserta_jawab->save();
                  }
              }

              DB::commit();
              $success_trans = true;
            } catch (\Exception $e) {
              DB::rollback();

              // error page
              abort(403, $e->getMessage());
            }

        if($success_trans == true){
            return response()->json([
                'check_peserta_jawab'=>$check_peserta_jawab,
                'soal_peserta_ulang_id'=>$soal_peserta_ulang_id,
                'soal_id'=>$soal_id
            ]);
        } else if($success_trans == false){
            return response()->json([
                'check_peserta_jawab'=>'',
                'soal_peserta_ulang_id'=>'',
                'soal_id'=>''
            ]);
        }

    }

    public function ajax_persentase_kelulusan(Request $request)
    {
        $soal_peserta_ulang_id    = array_unique($request->soal_peserta_ulang_id);
        $data = [];
        $temp = 0;
         DB::beginTransaction();
            $success_trans = false;
            try {
                $all_spi = collect();
                // set aktivasi false ujian peserta & update status ujian peserta menjadi selesai 
                $ujian_peserta = Ujian_peserta::whereIn('ujian_peserta_id', function($q)
                                    use($soal_peserta_ulang_id){
                                      $q->select('ujian_peserta_id')
                                        ->from('soal_ujian_ulang')
                                        ->whereIn('soal_ujian_ulang_id', function($y) use($soal_peserta_ulang_id){
                                            $y->select('soal_ujian_ulang_id')
                                              ->from('soal_peserta_ulang')
                                              ->whereIn('soal_peserta_ulang_id', $soal_peserta_ulang_id);
                                        });
                                    })
                                    ->first();
                $ujian_peserta->is_aktivasi = FALSE;
                $ujian_peserta->selesai     = TRUE;
                $ujian_peserta->update();
                // memberikan persentase kelulusan pada table soal_peserta
                foreach ($soal_peserta_ulang_id as $key => $spi) {
                    
                    $all_spi->push($spi);
                    $soal_peserta = Soal_peserta_ulang::where('soal_peserta_ulang_id', $spi)->first();
                    $soal_peserta->persentase_kelulusan = $soal_peserta->ujian_modul->batch_modul->persentase_kelulusan_modul;
                    // get soal_peserta_id with same modul_id
                    $ujian_modul_id        = $soal_peserta->ujian_modul_id;
                    $modul_id              = Ujian_modul::find($ujian_modul_id)->modul_id;
                    $ujian_batch_id        = Ujian_modul::find($ujian_modul_id)->ujian_batch_id;
                    $per_modul             = Ujian_modul::where('ujian_batch_id', $ujian_batch_id)->where('modul_id', $modul_id)->pluck('ujian_modul_id');
                    $soal_pesertas         = Soal_peserta_ulang::whereIn('soal_peserta_ulang_id', $soal_peserta_ulang_id)
                                                         ->whereIn('ujian_modul_id', $per_modul)
                                                         ->pluck('soal_peserta_ulang_id');
                    $total_soal_modul      = Peserta_jawab_ulang::whereIn('soal_peserta_ulang_id', $soal_pesertas)->count();
                    $total_benar_modul     = Peserta_jawab_ulang::whereIn('soal_peserta_ulang_id', $soal_pesertas)->where('is_bener', true)->count();
                    $persentase_kelulusan  = $soal_peserta->ujian_modul->batch_modul->persentase_kelulusan_modul;
                    (float)$nilai_modul    = @((float)$total_benar_modul / (float)$total_soal_modul) * 100;

                    $fix_persentase         = floor($nilai_modul);
                    if($fix_persentase < $persentase_kelulusan)
                    {
                        $is_lulus = false;
                    }else{
                        $is_lulus = true;
                    }

                        // update tbl:soal_peserta_ulang
                        $soal_peserta->nilai_ujian          = $fix_persentase;
                        $soal_peserta->persentase_kelulusan = $persentase_kelulusan;
                        $soal_peserta->is_lulus             = $is_lulus;
                        $soal_peserta->nilai                = $total_benar_modul;
                        $soal_peserta->save();

                /*
                    data untuk print hasil ujian
                */
                $peserta_id   = Auth::user()->peserta_id;
                $nama_peserta = Auth::user()->nama; 
                $tgl_lahir    = \Helper::date_formats(Auth::user()->tanggal_lahir, 'view'); 

                $ujian_batch  = DB::table('ujian_batch')->where('ujian_batch_id', $ujian_batch_id)->first();
                $batch        = DB::table('batch')->where('batch_id', $ujian_batch->batch_id)->first();
                $program      = DB::table('program')->where('program_id', $batch->program_id)->first();
                
                $nama_batch = $batch->nama;
                $reg_wppe   = DB::table('pendaftaran')->where('peserta_id', $peserta_id)->where('batch_id', $batch->batch_id)->whereNotNull('no_reg_wppe')->value('no_reg_wppe');                                  
                /*
                    end data
                */

                /*
                    data untuk insert tbl:ulang_trx
                */
                $ujian_peserta_ids = DB::table('ujian_peserta')
                                       ->whereIn('ujian_peserta_id', function($q) use($soal_peserta_ulang_id){
                                          $q->select('ujian_peserta_id')
                                            ->from('soal_ujian_ulang')
                                            ->whereIn('soal_ujian_ulang_id', function($x)use($soal_peserta_ulang_id){
                                                $x->select('soal_ujian_ulang_id')
                                                  ->from('soal_peserta_ulang')
                                                  ->whereIn('soal_peserta_ulang_id', $soal_peserta_ulang_id);
                                            });
                                       })->pluck('ujian_peserta_id');

                $ulang_jadwal = DB::table('ulang_jadwal')
                                  ->whereIn('ulang_jadwal_id', function($q) use($ujian_peserta_ids){
                                      $q->select('ulang_jadwal_id')
                                        ->from('ujian_pendaftaran')
                                        ->whereIn('ujian_pendaftaran_id', function($x) use($ujian_peserta_ids){
                                            $x->select('ujian_pendaftaran_id')
                                              ->from('ujian_peserta')
                                              ->where('ujian_peserta_id', $ujian_peserta_ids);
                                        });
                                  })->first();


                $modul = Ujian_modul::select(['ujian_modul.modul_id'])->where('ujian_batch_id', $ujian_batch_id)->distinct()->get();

                if($key == 0){
                  /* Insert table ujian */
                  $ujian               = new Ujian;
                  $ujian->batch_id     = $batch->batch_id;
                  $ujian->nama_batch   = $batch->nama;
                  $ujian->peserta_id   = $peserta_id;
                  $ujian->nama_peserta = $nama_peserta;
                  $ujian->date_created = date('Y-m-d H:i:s');
                  $ujian->save();
                }

                foreach ($modul as $value) {
                  //data untuk insert tbl:ulang_trx
                  if($modul_id != $temp)
                  {
                      // get lokasi cabang kp yang telah di set pembuat ujian
                      $ulang  = DB::table('ulang')->where('ulang_id', $ulang_jadwal->ulang_id)->first();
                      $kp     = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $ulang->lembg_pdkn_id)->first();
                      $cabang = DB::table('cabang')->where('cabang_id', $kp->cabang_id)->first();
                      $lokasi = DB::table('lokasi')->where('lokasi_id', $cabang->lokasi_id)->first();

                      /* Insert table detail ujian */
                      $ujian_dtl                       = new Ujian_detail;
                      $ujian_dtl->ujian_id             = $ujian->ujian_id;
                      $ujian_dtl->perdana_jadwal_id    = null;
                      $ujian_dtl->ulang_jadwal_id      = $ulang_jadwal->ulang_jadwal_id;
                      $ujian_dtl->nama_ujian           = $ulang_jadwal->nama;
                      $ujian_dtl->tgl_ujian            = $ulang_jadwal->tgl_ulang;
                      $ujian_dtl->modul_id             = $modul_id;
                      $ujian_dtl->nama_modul           = DB::table('modul')->where('modul_id', $modul_id)->value('nama');
                      $ujian_dtl->nilai                = $fix_persentase;
                      $ujian_dtl->lokasi               = $lokasi->nama;
                      $ujian_dtl->cabang               = $cabang->nama;
                      $ujian_dtl->kp                   = $kp->nama;
                      $ujian_dtl->status_ujian         = $is_lulus == true ? 'Lulus' : 'Gagal';
                      $ujian_dtl->persentase_kelulusan = $persentase_kelulusan;
                      $ujian_dtl->is_pengawas          = $ulang_jadwal->is_pengawas;
                      if($ulang_jadwal->is_pengawas == true){
                          $pengawas = DB::table('employee')->where('emp_id', $ulang_jadwal->emp_id)->first();
                          $ujian_dtl->nama_pengawas = $pengawas->first_name.' '.$pengawas->middle_name.' '.$pengawas->last_name;
                      }
                      $ujian_dtl->save();

                      // $trx = new Ulang_trx;
                      // $trx->nama_ulang   = $ulang_jadwal->nama;
                      // $trx->tgl_ulang    = $ulang_jadwal->tgl_ulang;
                      // $trx->nama_peserta = $nama_peserta;
                      // $trx->nama_batch   = $batch->nama;
                      // $trx->nama_modul   = DB::table('modul')->where('modul_id', $modul_id)->value('nama');
                      // $trx->nilai        = $fix_persentase;
                      // $trx->is_pengawas  = $ulang_jadwal->is_pengawas;
                      // if($ulang_jadwal->is_pengawas == true){
                      //       $pengawas = DB::table('employee')->where('emp_id', $ulang_jadwal->emp_id)->first();
                      //       $trx->nama_pengawas = $pengawas->first_name.' '.$pengawas->middle_name.' '.$pengawas->last_name;
                      //   }
                      // $trx->persentase_kelulusan = $persentase_kelulusan;
                      // $trx->status_ujian         = $is_lulus == true ? 'Lulus' : 'Gagal';
                      // $trx->peserta_id   = Auth::user()->peserta_id;
                      // $trx->batch_id     = $batch->batch_id;
                      // $trx->modul_id     = $modul_id;
                      // $trx->save();
                  }
                    $temp = $modul_id;
                    // end insert

                    // push data hasil ujian
                   $arr['nama_modul']    = DB::table('modul')->where('modul_id', $modul_id)->value('nama');
                   $arr['nama_program']  = $program->nama;
                   $arr['singkatan_ind'] = $program->singkatan_ind;
                   $arr['singkatan_eng'] = $program->singkatan_eng;
                   $arr['nama_peserta']  = $nama_peserta;
                   $arr['tgl_lahir']     = $tgl_lahir;
                   $arr['reg_wppe']      = $reg_wppe;
                   $arr['total_soal']    = $total_soal_modul;
                   $arr['total_benar']   = $total_benar_modul;
                   $arr['nilai']         = $fix_persentase;
                   $arr['lulus']         = $is_lulus;
                   array_push($data, $arr);
                }

            } // end foreach soal_peserta_id

            /* Create data & insert waktu sisa ujian */
            $sisa_soal  = 0;
            $modul_soal = array_unique($data, SORT_REGULAR);
            foreach ($modul_soal as $value) {
                // jumlahkan sisa soal dari modul yang gagal
                if($value['lulus'] == false){
                    $sisa_soal += $value['total_soal'];
                }
            }
            $waktu_persoal                 = Waktu_persoal::where('batch_id', $batch->batch_id)->first()->waktu_persoal;
            $waktu_sisa                    = new Waktu_sisa_ulang;
            $waktu_sisa->batch_id          = $batch->batch_id;
            $waktu_sisa->peserta_id        = $peserta_id;
            $waktu_sisa->sisa_soal         = $sisa_soal;
            $waktu_sisa->waktu_persoal     = $waktu_persoal;
            $waktu_sisa->sisa_waktu        = round($sisa_soal*$waktu_persoal);
            $waktu_sisa->ulang_jadwal_id   = $ulang_jadwal->ulang_jadwal_id;
            $waktu_sisa->nama_ulang_jadwal = $ulang_jadwal->nama;
            $waktu_sisa->date_created      = date('Y-m-d H:i:s');
            $waktu_sisa->save();

            $gagal = Soal_peserta_ulang::whereIn('soal_peserta_ulang_id', $all_spi)->where('is_lulus', false)->count();
            // jika tidak ada modul yang gagal / semua modul lulus
            if($gagal < 1) {
                // lihat no sertifikat terbesar berdasarkan kode program di old siak
                $old_sertifikat = DB::connection('mysql')
                                ->table('t_admisi')
                                ->where('kd_program', $program->program_id)
                                ->where('nip', '!=', '0')
                                ->max('nip');
                $old_sertifikat = str_pad($old_sertifikat, 6, 0, STR_PAD_LEFT);
                // cari no sertifikat terbesar berdasarkan kode program
                $no            = Peserta_lulus::where('no_sertifikat', 'like', $program->kode.'%')->max('no_sertifikat');
                $char          =  '-';
                $strpos        = strpos($no, $char); 
                $highest       = substr($no, $strpos + strlen($char)); 
                if($old_sertifikat > $highest){
                    $highest = $old_sertifikat;
                }
                $no_sertifikat = str_pad($highest + 1, 6, 0, STR_PAD_LEFT);
                $profile       = DB::table('profile_peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
                // insert to tbl:peserta_lulus
                $pl                  = new Peserta_lulus;
                $pl->peserta_id      = Auth::user()->peserta_id;
                $pl->batch_id        = $batch->batch_id;
                $pl->nama_batch      = $batch->nama;
                $pl->no_sertifikat   = $program->kode.'-'.$no_sertifikat;
                $pl->nama_peserta    = Auth::user()->nama;
                $pl->nama_program    = $program->nama;
                $pl->singkatan_ind   = $program->singkatan_ind;
                $pl->singkatan_eng   = $program->singkatan_eng;
                $pl->email           = Auth::user()->email;
                $pl->tempat_lahir    = $profile->tempat_lahir;
                $pl->photo           = $profile->photo;
                $pl->tgl_lahir       = Auth::user()->tanggal_lahir;
                $pl->tgl_lulus       = date('Y-m-d');
                $pl->date_created    = date('Y-m-d H:i:s');
                $pl->nama_lokasi     = DB::table('lokasi')->where('lokasi_id', $profile->lokasi_id)->value('nama');
                $pl->nama_lembg_pdkn = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $profile->lembg_pdkn_id)->value('nama');
                $pl->lembg_pdkn_id   = DB::table('lembg_pdkn')->where('lembg_pdkn_id', $profile->lembg_pdkn_id)->value('lembg_pdkn_id');
                $pl->nama_cabang     = DB::table('cabang')->where('cabang_id', function($q) use($profile){
                                            $q->select('cabang_id')
                                              ->from('lembg_pdkn')
                                              ->where('lembg_pdkn_id', $profile->lembg_pdkn_id);
                                        })->value('nama');
                $pl->save();

                /** Send email verifikasi data sertifikat kelulusan **/
                $header          = 'email-template.core.header';
                $footer          = 'email-template.core.footer';
                $config          = Config_email::first();
                $konfirmasi      = Konfirmasi::find(14);
                $peserta         = DB::table('peserta')->where('peserta_id', Auth::user()->peserta_id)->first();
                 Mail::send('email-template.email-konfirmasi-sertifikat', 
                    [   'header'        => $header, 
                        'footer'        => $footer, 
                        'config'        => $config, 
                        'konfirmasi'    => $konfirmasi, 
                        'peserta'       => $peserta,
                        'link'          => url('profile/konfirmasi_sertifikat/' . Crypt::encrypt($peserta->peserta_id)). '/' . Crypt::encrypt($batch->batch_id)
                    ],
                    function ($message) use ($peserta){
                        $message->subject(' TICMI - Verifikasi Data Sertifikat');
                        $message->from('mg@ticmi.co.id', 'TICMI');
                        $message->to($peserta->email);
                    }
                );

            }

            DB::commit();
            $success_trans = true;
        } catch (\Exception $e) {
            DB::rollback();

            // error page
            abort(403, $e->getMessage());
        }

        if($success_trans == true){
            $json = array_unique($data, SORT_REGULAR);

            // set cookies untuk print hasil ujian
            setcookie("hasil_ujian", json_encode($json), 0, '/');
            
            echo json_encode($json);
        }else if($success_trans == false){
            $json = 'failed';
            echo json_encode($json);
        }

    }


}
