<?php

namespace App\Http\Controllers;

use Auth;
use App\Helpers\PaketDataHelper;
use App\Http\Requests\KonfirmasiRequest;
use App\Jobs\SendEmailInvoicePembelianPaketData;
use App\Jobs\SendEmailKonfirmasiPaketData;
use App\Models\ticmi\DataType;
use App\Models\ticmi\Konfirmasi;
use App\Models\ticmi\PaketData;
use App\Models\ticmi\PaketUser;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class PackageDataController
 * @package App\Http\Controllers
 */
class PackageDataController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		 $pageTitle    = 'Paket Data';
		 $paketDataAll = PaketData::where('is_active', 1)->get();
		 $dataTypes    = DataType::where('is_active', 1)->get();
		 return view('ticmi.paket-data.index-front', compact('pageTitle', 'paketDataAll', 'dataTypes'));
	}
	
	public function memberPaketData()
	{
		$dashboardTitle = 'Paket Data';
		$paketUser      = PaketUser::where('user_id', auth()->user()->peserta_id)->where('is_active', 1)->where('valid_until', '>', date('Y-m-d H:i:s'))->orderBy('valid_until','DESC')->first();
		if ($paketUser && $paketUser->count() > 0) {
			return view('ticmi.paket-data.paket_view', compact('dashboardTitle', 'paketUser'));
		} else {
			$paketUser  = null;
			$paketUsers = PaketUser::where('user_id', auth()->user()->peserta_id)->orderBy('id', 'DESC')->get();
			$paketUsers->load('paket_data');
			return view('ticmi.paket-data.paket_list', compact('dashboardTitle', 'paketUsers'));
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\PaketData $paketData
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function checkout(Request $request, $code)
	{
//		$request->session()->forget('cart');
		$paketData = PaketData::where('slug',$code)->first();
		// dd($paketData);
		$request->session()->put('cart', $paketData);
		$biaya_admin = 3000;
		return view('ticmi.paket-data.checkout', compact('paketData', 'biaya_admin'));
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\PaketData $paketData
	 * @return \Illuminate\Http\RedirectResponse
	 */
	// public function removecart(Request $request, PaketData $paketData)
	// {
	// 	$request->session()->forget('cart');
	// 	return redirect()->back();
	// }
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\ticmi\PaketData $paketData
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request, $paketDataName)
	{
//		$paketData = PaketData::whereslug($paketDataName)->first();
//		 dd($paketDataName);
		$paketUser = new PaketUser($request->all());
		
		$paketUser->user_id             = auth()->user()->peserta_id;
		$paketUser->cmeds_paket_data_id = $paketDataName->id;
		$paketUser->virtual_account     = PaketDataHelper::getVirtualAccount();
		$paketUser->invoice_no          = PaketDataHelper::getInvoiceNo();
		$paketUser->due_date            = date('Y-m-d H:i:s', strtotime('+2 day'));
		
		// dd($paketUser);
		if ($paketUser->save()) {
			flash()->success('Berhasil melakukan pembelian Paket Data');
			
			/**
			 * Kirim Email Invoice Pembelian Data
			 */
			$job = (new SendEmailInvoicePembelianPaketData($paketUser))->onQueue('ticmi')->delay(3);
			$this->dispatch($job);
			
			return redirect()->route('paket-data.view', compact('paketUser'));
		} else {
			flash()->error('Gagal melakukan pembelian Paket Data');
			return redirect()->route('paket-data.checkout',['paketdatacode'=>$paketDataName->slug]);
		}
	}

// 	public function paketData()
// 	{
// 		// dd(auth()->user()->peserta_id);
// 		$dashboardTitle = 'Paket Data';
// 		$paketUser      = PaketUser::where('user_id', auth()->user()->peserta_id)->where('is_active', 1)->where('valid_until', '>', date('Y-m-d H:i:s'))->first();
// 		if ($paketUser && $paketUser->count() > 0) {
// //			$paketUser->load('paket_data');
// 			return view('ticmi.paket-data.paket_view', compact('dashboardTitle', 'paketUser'));
// 		} else {
// 			$paketUser  = null;
// 			$paketUsers = PaketUser::where('user_id', auth()->user()->peserta_id)->orderBy('id', 'DESC')->get();
// 			$paketUsers->load('paket_data');
// 			// dd($paketUsers);
// 			return view('ticmi.paket-data.paket_list', compact('dashboardTitle', 'paketUsers'));
// 		}
// 	}

	public function paketDataView($paketUser)
	{
//		return $paketUser;
		$dashboardTitle = 'Paket Data';
		$paketUser->load('paket_data');
		// dd($paketUser);
		return view('ticmi.paket-data.paket_view', compact('paketUser', 'dashboardTitle'));
	}
	
	public function konfirmasi(PaketUser $paketUser)
	{
		if ($paketUser->user_id == auth()->user()->peserta_id) {
			return view('ticmi.paket-data.konfirmasi', compact('paketUser'));
		} else {
			abort(404);
		}
	}
	
	public function konfirmasiStore(KonfirmasiRequest $request, PaketUser $paketUser)
	{
		if ($paketUser->user_id == auth()->user()->peserta_id) {
			/**
			 * Cek jika ada upload bukti transfer
			 */
			if ($request->hasFile('bukti_transfer')) {
				if ($request->file('bukti_transfer')->isValid()) {
					$image    = $request->file('bukti_transfer');
					$filename = $paketUser->invoice_no . '_' . $request->get('virtual_account') . '.' . $image->getClientOriginalExtension();
					
					$uploadPath = public_path('upload/bukti_konfirmasi/');
					$request->file('bukti_transfer')->move($uploadPath, $filename);
					/** set bukti transfer */
					$paketUser->confirm_file = $filename;
				}
			}
			
			$paketUser->confirm_bank          = "";
			$paketUser->confirm_rekening      = $request->get('no_rekening');
			$paketUser->confirm_rekening_nama = $request->get('nama_rekening');
			$paketUser->confirm_date          = $request->get('tgl_transfer');
			$paketUser->confirm_time          = $request->get('jam_transfer');
			$paketUser->confirm_nominal       = $request->get('nominal');
			
			
			/** save konfirmasi */
			if ($paketUser->save()) {
				/** Jika saving sukses maka set is_confirm = 1 ditabel keranjang */
				$paketUser->update(['is_confirm' => 1, 'confirm_date' => date('Y-m-d H:i:s')]);
	
	            /** Kirim Email Notifikasi Konfirmasi sukses */
	
				$job = (new SendEmailKonfirmasiPaketData($paketUser))->onQueue('ticmi')->delay(3);
				$this->dispatch($job);
				
				flash()->success('Konfirmasi berhasil');
				return redirect()->route('paket-data.view', ['paketuser' => $paketUser]);
			} else {
				flash()->error('Konfirmasi gagal tersimpan, silahkan lakukan konfirmasi kembali');
				return redirect()->route('paket-data.konfirmasi', ['paketdatainvoice' => $paketUser->invoice_no]);
			}
		} else {
			abort(404);
		}
	}
}
