<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use URL;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Tagihan;
use App\Models\Tagihan_tmp;

class TagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url_ajax_datatable = route('tagihan.ajax_datatable');

        return view('tagihan.tagihan_index', compact('url_ajax_datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tagihan = Tagihan_tmp::find($id);
        $char    = substr($tagihan->no_invoice, 0,2);
        if($char == 'ST'){
            $jenis = 'Pendaftaran Sertifikasi';
        } elseif($char == 'UU'){
            $jenis = 'Pendaftaran Ujian Ulang';
        }
        return view('tagihan.tagihan_show', compact('tagihan','jenis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ajax_datatable(Request $request)
    {
        if ($request->ajax()) {
            $sql_no_urut     = \Yajra_datatable::get_no_urut('tagihan_tmp.tagihan_tmp_id'/*primary_key*/, $request);
            $tagihan         = DB::table('tagihan_tmp')
                                ->select([
                                        DB::raw($sql_no_urut), // nomor urut
                                        'tagihan_tmp.tagihan_tmp_id AS tagihan_tmp_id',
                                        'tagihan_tmp.nama_batch AS nama_batch',
                                        'tagihan_tmp.tgl_daftar AS tgl_daftar',
                                        'tagihan_tmp.no_invoice AS no_invoice',
                                        'tagihan_tmp.account_no AS account_no',
                                        ])
                                ->where('peserta_id', Auth::user()->peserta_id)
                                ->whereNotIn('tagihan_tmp_id', function($q){
                                    $q->select('tagihan_tmp_id')
                                      ->from('bayar');
                                })
                                ->whereNotIn('tagihan_tmp_id', function($q){
                                    $q->select('tagihan_tmp_id')
                                      ->from('bayar_manual');
                                });

            return Datatables::of($tagihan)
                    ->addIndexColumn()
                    ->addColumn('jenis', function ($tagihan) {
                        $char = substr($tagihan->no_invoice, 0,2);
                        if($char == 'ST'){
                            $jenis = 'Pendaftaran Sertifikasi';
                        } elseif($char == 'UU'){
                            $jenis = 'Pendaftaran Ujian Ulang';
                        }
                        return $jenis;
                    })
                    ->addColumn('tgl_daftar', function($tagihan){
                        return \Helper::date_formats($tagihan->tgl_daftar, 'view');
                    })
                    ->addColumn('status', function($tagihan){
                        $verified = DB::table('validasi')->where('tagihan_tmp_id', $tagihan->tagihan_tmp_id)->first();
                        $paid     = DB::table('konfirmasi')->where('tagihan_tmp_id', $tagihan->tagihan_tmp_id)->first();
                        if($verified){
                            $status = "<span class='label label-sm label-success'> Sudah di bayar </span>";
                        } elseif($paid) {
                            $status = "<span class='label label-sm label-warning'> Menunggu verifikasi pembayaran </span>";
                        } else {
                            $status = "<span class='label label-sm label-danger'> Belum di bayar </span>";
                        }
                        
                        return "<center>".$status."</center>";
                    })
                    ->addColumn('action', function ($tagihan){
                        $btn_action = '<a href="'. route('tagihan.show', $tagihan->tagihan_tmp_id) .'" class="glyphicon glyphicon-search"></a>&nbsp;';

                        return $btn_action;
                    })
                    ->rawColumns(['tgl_daftar','jenis', 'status', 'action']) // to html
                    ->make(true);
        }
    }
}
