<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Program;
use App\Models\Batch;
use App\Models\ticmi\Workshop;

class ProdukLayananController extends Controller
{
    //
    public function wppeProduct()
    {
        $zona_dt    = 'Asia/Jakarta';
        $pageTitle  = "Wakil Perantara Pedagang Efek";
        $program    = Program::where('program_id', 1)->first();
        $otherbatch = Batch::leftJoin('cabang', function($join) {
                                    $join->on('batch.cabang_id', '=', 'cabang.cabang_id');
                           })->where("batch.program_id", 1)
                             ->where("batch.aktif", "t")
                             ->where("batch.hapus", "f")
                             ->where("batch.is_publish", "t")
                             ->where('batch.aktif', TRUE)
                             ->where('batch.is_hidden', FALSE)
                             ->where('batch.is_pelatihan', FALSE)
                             ->get(['batch.*', 'cabang.nama AS nama_cabang']);

        return view('ticmi.produk-layanan.wppe', compact('program', 'pageTitle', 'otherbatch'));
    }

    public function wmiProduct()
    {
        $zona_dt    = 'Asia/Jakarta';
        $pageTitle  = "Wakil Manajer Investasi";
        $program    = Program::where('program_id', 2)->first();
        $otherbatch = Batch::leftJoin('cabang', function($join) {
                                    $join->on('batch.cabang_id', '=', 'cabang.cabang_id');
                           })->where("batch.program_id", 2)
                             ->where("batch.aktif", "t")
                             ->where("batch.hapus", "f")
                             ->where("batch.is_publish", "t")
                             ->where('batch.aktif', TRUE)
                             ->where('batch.is_hidden', FALSE)
                             ->where('batch.is_pelatihan', FALSE)
                             ->get(['batch.*', 'cabang.nama AS nama_cabang']);

        return view('ticmi.produk-layanan.wmi', compact('program', 'pageTitle', 'otherbatch'));
    }

    public function aspmProduct()
    {
        $zona_dt    = 'Asia/Jakarta';
        $pageTitle  = "Ahli Syariah Pasar Modal";
        $program    = Program::where('program_id', 22)->first();
        $otherbatch = Batch::leftJoin('cabang', function($join) {
                                    $join->on('batch.cabang_id', '=', 'cabang.cabang_id');
                           })->where("batch.program_id", 22)
                             ->where("batch.aktif", "t")
                             ->where("batch.hapus", "f")
                             ->where("batch.is_publish", "t")
                             ->where('batch.aktif', TRUE)
                             ->where('batch.is_hidden', FALSE)
                             ->where('batch.is_pelatihan', FALSE)
                             ->get(['batch.*', 'cabang.nama AS nama_cabang']);

        return view('ticmi.produk-layanan.aspm', compact('program', 'pageTitle', 'otherbatch'));
    }

    public function wpeeProduct()
    {
        $zona_dt    = 'Asia/Jakarta';
        $pageTitle  = "Wakil Penjamin Emisi Efek";
        $program    = Program::where('program_id', 24)->first();
        $otherbatch = Batch::leftJoin('cabang', function($join) {
                                    $join->on('batch.cabang_id', '=', 'cabang.cabang_id');
                           })->where("batch.program_id", 24)
                             ->where("batch.aktif", "t")
                             ->where("batch.hapus", "f")
                             ->where("batch.is_publish", "t")
                             ->where('batch.aktif', TRUE)
                             ->where('batch.is_hidden', FALSE)
                             ->where('batch.is_pelatihan', FALSE)
                             ->get(['batch.*', 'cabang.nama AS nama_cabang']);

        return view('ticmi.produk-layanan.wpee', compact('program', 'pageTitle', 'otherbatch'));
    }

    public function pdpms()
    {
        $pageTitle  = "Pendidikan Dasar Pasar Modal Syariah";
        $zona_dt    = 'Asia/Jakarta';
        $program    = Program::where('program_id', 40)->first();
        $ogimage    = asset('/assets/images/ticmi/pdpms/pdpms.png');
        $otherbatch = Batch::leftJoin('cabang', function($join) {
                                    $join->on('batch.cabang_id', '=', 'cabang.cabang_id');
                           })->where("batch.program_id", 40)
                             ->where("batch.aktif", "t")
                             ->where("batch.hapus", "f")
                             ->where("batch.is_publish", "t")
                             ->where('batch.aktif', TRUE)
                             ->where('batch.is_hidden', FALSE)
                             ->where('batch.is_pelatihan', FALSE)
                             ->get(['batch.*', 'cabang.nama AS nama_cabang']);
        // $workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "syariah%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
        return view('ticmi.produk-layanan.pdpms', compact('pageTitle', 'otherbatch', 'ogimage'));
    }

    public function dataHistoris(){
    	$pageTitle = "Data Historis";
		return view('ticmi.produk-layanan.data-historis', compact('pageTitle'));
    }

    public function perpustakaan()
	{
		$pageTitle = "Perpustakaan";
		return view('ticmi.produk-layanan.perpustakaan', compact('pageTitle'));
	}
	
	public function permintaanData()
	{
		$pageTitle = "Permintaan Data Khusus";
		return view('ticmi.produk-layanan.permintaan-data', compact('pageTitle'));
	}

	public function cmpdp()
	{
		$pageTitle = "CMPDP | Capital Market Professional Development Program";
		return view('ticmi.produk-layanan.cmpdp', compact('pageTitle'));
	}

	public function cmk()
	{
		$pageTitle = "Cerdas Mengelola Keuangan";
		$ogimage   = asset('/assets/images/ticmi/cmk/banner-cmk2.jpg');
		$workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "cmk%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
		return view('ticmi.produk-layanan.cmk', compact('pageTitle', 'workshops', 'ogimage'));
	}

	
	public function moduler()
	{
		$pageTitle = "Kelas Moduler Online"; 
		$ogimage   = asset('/assets/images/ticmi/mol.png');
		$workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "moduler%")->where('is_online',1)->where('is_hide', '0')->get();
		return view('ticmi.produk-layanan.moduler', compact('pageTitle', 'workshops', 'ogimage'));
	}

	
    public function rivan()
    {
        $pageTitle = "Seminar Bersama Rivan Kurniawan - Investment Grade Investing Strategy";
        $workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "rivan%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
        return view('ticmi.produk-layanan.seminar-rivan', compact('pageTitle', 'workshops'));
    }

    public function ppl()
    {
        $pageTitle = "Program Pendidikan Lanjutan (PPL)";
//      $workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "ppl%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
        $workshops   = Workshop::where('is_publish', 1)->where('is_hide', '0')->where('t_program_id', 34)->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
        $kelasOnline = Workshop::where('is_publish', 1)->where('is_hide', '0')->where('t_program_id', 34)->where('is_online',1)->orderBy('tgl_mulai')->get();
        // dd($kelasOnline);
        return view('ticmi.produk-layanan.seminar-ppl', compact('pageTitle', 'workshops','kelasOnline'));
    }

    public function reksadana()
    {
        $pageTitle = "Sekolah Reksadana";
        $ogimage   = asset('/assets/ticmi/images/ticmi/Sekolah-reksa-dana-ticmi.png');
        $workshops = Workshop::where('is_publish', 1)->where('t_program_id', 39)->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
        return view('ticmi.produk-layanan.reksadana', compact('pageTitle', 'workshops', 'ogimage'));
    }

    public function kerjasamaPerguruanTinggi()
    {
        $pageTitle = "Kerjasama Perguruan Tinggi";
        
        return view('ticmi.produk-layanan.kerjasama-perguruan-tinggi', compact('pageTitle'));
    }
}
