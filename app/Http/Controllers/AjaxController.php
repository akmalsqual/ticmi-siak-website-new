<?php

namespace App\Http\Controllers;

use App\Models\Perusahaan;
use App\Models\Peserta;
use App\Models\ticmi\Admisi;
use App\Models\ticmi\Kabupaten;
use App\Models\ticmi\Kecamatan;
use App\Models\ticmi\Workshop;
use Illuminate\Http\Request;

use App\Http\Requests;

class AjaxController extends Controller
{
	public function getKabupatenByProvinsi(Request $request, $pluck = false)
	{
		$id_provinsi = $request->get('id_provinsi');
		$kabupaten   = Kabupaten::where('province_id', $id_provinsi)->get();
		
		if ($kabupaten && $kabupaten->count() > 0) {
			if ($pluck) {
				$kabupaten = array_pluck($kabupaten, 'name', 'name');
				return $kabupaten;
			} else {
				$select = "<option value=''>Pilih Kabupaten/Kota</option>";
				foreach ($kabupaten as $item) {
					$select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
				}
				return $select;
			}
		} else {
			return false;
		}
	}
	
	public function getKecamatanByKabupaten(Request $request, $pluck = false)
	{
		$id_kabupaten = $request->get('id_kabupaten');
		$kecamatan    = Kecamatan::where('regency_id', $id_kabupaten)->get();
		if ($kecamatan && $kecamatan->count() > 0) {
			if ($pluck) {
				$kecamatan = array_pluck($kecamatan, 'name', 'name');
				return $kecamatan;
			} else {
				$select = "<option value=''>Pilih Kecamatan</option>";
				foreach ($kecamatan as $item) {
					$select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
				}
				return $select;
			}
		} else {
			return false;
		}
	}
	
	public function getPerusahaan(Request $request, $pluck = false)
	{
		
		$id     = $_POST['id'];
		$sro    = Perusahaan::where('kat_perusahaan_id', 1)->get();
		$nonsro = Perusahaan::where('kat_perusahaan_id', 2)->get();
		if ($id == '1') {
			$select = "<option value=''>Pilih Anggota Bursa</option>";
			foreach ($sro as $item) {
				$select .= "<option value='" . $item->perusahaan_id . "'>" . $item->nama . "</option>";
			}
			return $select;
		} elseif ($id == '2') {
			$select = "<option value=''>Pilih Non Bursa</option>";
			foreach ($nonsro as $item1) {
				$select .= "<option value='" . $item1->perusahaan_id . "'>" . $item1->nama . "</option>";
			}
			return $select;
		} else {
			// $select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
		}
		
		dd($nonsro);
		
		
		/* $id_kabupaten = $request->get('id_kabupaten');
		$kecamatan    = Kecamatan::where('regency_id', $id_kabupaten)->get();
		if ($kecamatan && $kecamatan->count() > 0) {
			if ($pluck) {
				$kecamatan = array_pluck($kecamatan, 'name', 'name');
				return $kecamatan;
			} else {
				$select = "<option value=''>Pilih Kecamatan</option>";
				foreach ($kecamatan as $item) {
					$select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
				}
				return $select;
			}
		} else {
			return false;
		} */
	}
	
	public function testing()
	{
		/**
		 * Update peserta workshop untuk sinkronisasi moodle_user_id
		 */
//		$user = \DB::connection('mysql_elearning')->table('mdl_user')->select(['id','username'])->where('deleted',0)->orderBy('id')->get();
//		foreach ($user as $item) {
//			$peserta = Peserta::where('email',$item->username)->whereNotNull('password')->first();
//			if ($peserta && $peserta->count() > 0) {
//				$peserta->update(['moodle_user_id'=>$item->id]);
//				echo $item->id." - ".$item->username." - Updated<br/>";
//			} else {
//				echo $item->id." - ".$item->username."<br/>";
//			}
//		}
		
		$admisi = Admisi::find(30022);
		$workshop = Workshop::find($admisi->workshop_id);
		return view('email-template/kelasonline/pendaftaran-sertifikasi-waiver', compact('admisi', 'workshop'));
	}
}
