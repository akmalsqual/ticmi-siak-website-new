<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use File;
use Illuminate\Support\Facades\Mail;
use App\Models\Lowongan;
use App\Models\Pelamar;
use App\Models\Perusahaan;
use App\Http\Requests\PendaftaranRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\email_template\Konfirmasi;
use App\Models\email_template\Config_email;

class KarirController extends Controller
{
    //menampilkan data lowongan
    public function index()
    {
        $user = Auth::User();
        $arr_apply          = Pelamar::where('peserta_id',Auth::User()->peserta_id)
                                    ->pluck('lowongan_id')->toArray();
                                    
        $arr_not_apply      = Lowongan::whereNotIn('lowongan_id',$arr_apply)
                                    ->pluck('lowongan_id')->toArray();

        $perusahaan_tampil  = Perusahaan::join('lowongan','lowongan.perusahaan_id','=','perusahaan.perusahaan_id')
                                        ->whereIn('lowongan.lowongan_id',$arr_not_apply)
                                        ->where('lowongan.is_publish',1)
                                        ->where('lowongan.is_approved',1)
                                        ->where('tgl_open', '<=', date('Y-m-d'))
                                        ->where('tgl_close', '>=', date('Y-m-d'))
                                        ->pluck('perusahaan.perusahaan_id');

        $list_perusahaan    = Perusahaan::select('perusahaan.perusahaan_id','perusahaan.nama')
                                        ->whereIn('perusahaan_id',$perusahaan_tampil)
                                        ->join('kat_perusahaan','kat_perusahaan.kat_perusahaan_id','=','perusahaan.kat_perusahaan_id')
                                        ->get();
                                        //dd($perusahaan_tampil);
        return view('ticmi.karir.karir_index',compact('list_perusahaan','arr_not_apply'));
    }

    public function create($id)
    {
        $lowongan_id    = $id;
        return view('ticmi.karir.karir_create',compact('lowongan_id'));
    }

    //menyimpan data pelamar
    public function store(PendaftaranRequest $request){
        $header          = 'email-template.core.header';
        $footer          = 'email-template.core.footer';
        $config          = Config_email::first();
        $konfirmasi      = Konfirmasi::find(13);

        $milliseconds   = round(microtime(true));
        $cv             = $request['cv'];
        $sertifikat     = $request['sertifikat'];
        $sertifikat_ori = $request['sertifikat']->getClientOriginalName();
        $cv_ori         = $request['cv']->getClientOriginalName();

        $arr_sertifikat     = explode('.', $sertifikat_ori);
        $sertifikat_extensi = end($arr_sertifikat);
        $destinationPath    = 'pelamar';
        //$sertifikat_path    = 'C:/xampp/htdocs/siak_dev/public/upload_files/images/pelamar/';
        
        $arr_cv             = explode('.', $cv_ori);
        $cv_extensi         = end($arr_cv);
        $cv_name            = 'cv_'.$milliseconds.'.'.$cv_extensi;
        $sertifikat_name    = 'sertifikat_'.$milliseconds.'.'.$sertifikat_extensi;
        //$cv_path            = 'C:/xampp/htdocs/siak_dev/public/upload_files/images/pelamar/';

        
        
        //$cv->move($cv_path, $cv_name);
        //$sertifikat->move($sertifikat_path, $sertifikat_name);
        Storage::disk('siak')->putFileAs($destinationPath,$cv, $cv_name);
        Storage::disk('siak')->putFileAs($destinationPath,$sertifikat, $sertifikat_name);
        

        $pelamar                        = new Pelamar;
        $pelamar->lowongan_id           = $request['lowongan_id'];
        $pelamar->peserta_id            = Auth::User()->peserta_id;
        $pelamar->nama_peserta          = Auth::User()->nama;
        $pelamar->file_sertifikat_ori   = $sertifikat_ori;
        $pelamar->file_sertifikat_name  = $sertifikat_name;
        $pelamar->file_cv_ori           = $cv_ori;
        $pelamar->file_cv_name          = $cv_name;
        $pelamar->keterangan            = $request['keterangan'];
        $success = $pelamar->save();

        if($success){
            $lowongan = Lowongan::where('lowongan_id',$request['lowongan_id'])->first();
            // Mail::send(['html' => 'email-template/email-pelamar-baru'], $lowongan, function($message) use ($lowongan){
            //     $message->to($lowongan['email_pic'], $lowongan['nama_pic'])->subject($lowongan['nama_pekerjaan']);
            //     $message->from('mg@ticmi.co.id','TICMI');
            // });

            Mail::send(['html' => 'email-template.email-pemberitahuan-pelamar-baru'], 
                [   'header'        => $header, 
                    'footer'        => $footer, 
                    'config'        => $config, 
                    'konfirmasi'    => $konfirmasi, 
                    'lowongan'      => $lowongan,
                    'link'          => \Config::get('app.url_siak').'/cdc/pelamar'
                ],
                function ($message) use ($lowongan){
                    $message->subject($lowongan->nama_pekerjaan);
                    $message->from('cdc@ticmi.co.id', 'TICMI');
                    $message->to($lowongan->email_pic);
                });
        }

        return redirect(url('karir'));
    }

    // public function insert(){
    //     $peserta_id     = Auth::User()->peserta_id;
    //     $lowongan_id    = $id;

    //     $data = array('lowongan_id' => $id , 'peserta_id' => $peserta_id);
    //     $result = json_encode($data);
    //     return $result;
    // }

    //Bagian karir halaman depan
    public function karirFront()
    {
//        $perusahaan_tampil  = Perusahaan::join('lowongan','lowongan.perusahaan_id','=','perusahaan.perusahaan_id')
//                                        ->where('lowongan.is_publish',1)
//                                        ->where('lowongan.hapus',0)
//                                        ->pluck('perusahaan.perusahaan_id');
	
	    $perusahaan_tampil  = Perusahaan::join('lowongan','lowongan.perusahaan_id','=','perusahaan.perusahaan_id')
									    ->where('lowongan.is_publish',1)
									    ->where('lowongan.is_approved',1)
									    ->where('tgl_open', '<=', date('Y-m-d'))
									    ->where('tgl_close', '>=', date('Y-m-d'))
									    ->whereNull('lowongan.date_deleted')
									    ->pluck('perusahaan.perusahaan_id');
        
        $list_perusahaan    = Perusahaan::select('perusahaan.perusahaan_id','perusahaan.nama')
//                                        ->where('kat_perusahaan.nama','Anggota Bursa')
                                        ->whereIn('perusahaan_id',$perusahaan_tampil)
                                        ->join('kat_perusahaan','kat_perusahaan.kat_perusahaan_id','=','perusahaan.kat_perusahaan_id')
                                        ->get();
                
        return view('ticmi.karir.karir_front',compact('list_perusahaan','arr_not_apply'));
    }
}
