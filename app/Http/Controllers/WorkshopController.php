<?php

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Http\Requests\WorkshopRequest;
use App\Http\Requests\ReksadanaRequest;
use App\Jobs\SendEmailKonfirmasiWorkshop;
use App\Jobs\SendEmailPendaftaranWorkshop;
use App\Models\Batch;
use App\Models\ticmi\FormReksadana;
use App\Models\ticmi\Kecamatan;
use App\Models\ticmi\Kabupaten;
use App\Models\ticmi\Konfirmasi;
use App\Models\ticmi\PesertaWorkshop;
use App\Models\ticmi\Workshop;
use App\Models\Program;
use App\Models\ticmi\Provinsi;
use Mail;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class WorkshopController extends Controller
{
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Models\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request, Workshop $workshop)
	{
		$batch_path = \Config::get('app.url_siak') . '/public/upload_files/images/logo_batch/';
		if ($workshop && $workshop->count() > 0) {
			$pageTitle  = $workshop->workshop_name;
			$pelatihan  = $workshop;
			$otherbatch = Batch::where('aktif', TRUE)
				->where('hapus', FALSE)
				->where('disetujui', TRUE)
				->where('is_publish', TRUE)
				->where('is_hidden', FALSE)
				->where('is_pelatihan', FALSE)
				->get();
			return view('workshop.index', compact('otherbatch', 'pelatihan', 'pageTitle', 'batch_path'));
		} else {
			abort(404);
		}
	}
	
	public function form(Request $request, Workshop $workshop)
	{
		$agama      = [
			'Islam'   => 'Islam',
			'Katolik' => 'Katolik',
			'Kristen' => 'Kristen',
			'Hindu'   => 'Hindu',
			'Budha'   => 'Budha',
			'Lainnya' => 'Lainnya'
		];
		$pendidikan = [
			'SD'  => 'SD',
			'SMP' => 'SMP',
			'SMA' => 'SMA',
			'D3'  => 'D3',
			'S1'  => 'S1',
			'S2'  => 'S2',
			'S3'  => 'S3'
		];
		
		$provinsi         = Provinsi::all();
		$provinsi         = array_pluck($provinsi, 'name', 'id');
		$kabupaten        = Kabupaten::all();
		$kabupaten        = array_pluck($kabupaten, 'name', 'id');
		$kecamatan        = Kecamatan::all();
		$kecamatan        = array_pluck($kecamatan, 'name', 'id');
		$status_rumah     = [
			'Pribadi'         => 'Pribadi',
			'Kontrak'         => 'Kontrak',
			'Sewa / Kost'     => 'Sewa / Kost',
			'Milik Orang Tua' => 'Milik Orang Tua'
		];
		$pekerjaan        = [
			'Pegawai Swasta'   => 'Pegawai Swasta',
			'Pegawai Negeri'   => 'Pegawai Negeri',
			'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
			'Wiraswasta'       => 'Wiraswasta',
			'Pelajar'          => 'Pelajar',
			'TNI /Polisi'      => 'TNI / Polisi',
			'Pensiunan'        => 'Pensiunan',
			'Guru'             => 'Guru',
			'Lainnya'          => 'Lainnya'
		];
		$tujuan_investasi = [
			'Kenaikan Harga' => 'Kenaikan Harga',
			'Jangka Panjang' => 'Jangka Panjang',
			'Spekulasi'      => 'Spekulasi',
			'Pendapatan'     => 'Pendapatan',
			'Lainnya'        => 'Lainnya'
		];
		$sumber_dana      = [
			'Gaji'                               => 'Gaji',
			'Keuntungan Bisnis'                  => 'Keuntungan Bisnis',
			'Bunga'                              => 'Bunga',
			'Warisan'                            => 'Warisan',
			'Dana dari Orang Tua / Anak'         => 'Dana dari Orang Tua / Anak',
			'Dana dari Pasangan (Suami / Istri)' => 'Dana dari Pasangan (Suami / Istri)',
			'Dana Pensiun'                       => 'Dana Pensiun',
			'Hadiah Lotere/Undian'               => 'Hadiah Lotere/Undian',
			'Hasil Investasi'                    => 'Hasil Investasi',
			'Deposito'                           => 'Deposito',
			'Modal Usaha'                        => 'Modal Usaha',
			'Pinjaman'                           => 'Pinjaman',
			'Lainnya'                            => 'Lainnya',
		];
		
		$pendapatan = [
			'< 15 Juta'           => '< 15 Juta',
			'15 Juta - 25 Juta'   => '15 Juta - 25 Juta',
			'25 Juta - 50 Juta'   => '25 Juta - 50 Juta',
			'50 Juta - 100 Juta'  => '50 Juta - 100 Juta',
			'100 Juta - 250 Juta' => '100 Juta - 250 Juta',
			'250 Juta - 400 Juta' => '250 Juta - 400 Juta',
			'400 Juta - 500 Juta' => '400 Juta - 500 Juta',
			'500 Juta - 1 Milyar' => '500 Juta - 1 Milyar',
			'> 1 Milyar'          => '> 1 Milyar',
		];
		$pageTitle  = 'Form Pendaftaran Sekolah Reksa Dana';
		
		$program    = Program::whereIn('nama', ['WPPE', 'WPPE Pemasaran', 'WPPE Pemasaran Terbatas', 'ASPM', 'WMI', 'WPEE'])->get();
		$program    = array_pluck($program, 'nm_program', 'nm_program');
		$provinces  = Provinsi::all();
		$provinces  = array_pluck($provinces, 'name', 'name');
		return view('reksadana.index', compact('program', 'pageTitle', 'provinces', 'workshop', 'agama', 'pendidikan', 'provinsi', 'kabupaten', 'kecamatan', 'status_rumah', 'pekerjaan', 'tujuan_investasi', 'sumber_dana', 'pendapatan'));
	}
	
	/**
	 * Halaman Informasi Pendaftaran
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function pendaftaran(Request $request, Workshop $workshop)
	{
		if ($workshop && $workshop->count() > 0) {
			$pageTitle = $workshop->workshop_name;
			$pelatihan = $workshop;
//			if ($workshop->is_publish == 1 && $workshop->peserta()->count() < $workshop->minimal_peserta)
			if ($workshop->peserta()->count() < $workshop->minimal_peserta)
				return view('workshop.daftar', compact('pelatihan', 'pageTitle'));
			else {
				return redirect('/');
			}
		} else {
			abort(404);
		}
	}
	
	/**
	 * Halaman Checkout untuk Pendaftaran Workshop
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function checkout(Request $request, Workshop $workshop)
	{
//		session()->forget('referral_promo');
		if ($workshop && $workshop->count() > 0) {
//			if ($workshop->is_publish == 1 && $workshop->peserta()->count() < $workshop->minimal_peserta)
			if ($workshop->peserta()->count() < $workshop->minimal_peserta) {
				$provinsi  = Provinsi::all();
				$provinsi  = array_pluck($provinsi, 'name', 'id');
				$kabupaten = Kabupaten::all();
				$kabupaten = array_pluck($kabupaten, 'name', 'id');
				return view('workshop.checkout', compact('workshop', 'provinsi', 'kabupaten'));
			} else {
				return redirect('/');
			}
		} else {
			abort(404);
		}
	}
	
	/**
	 * Storing data pendaftaran workshop
	 *
	 * @param \App\Http\Requests\WorkshopRequest $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function reksadanaStore(ReksadanaRequest $request, Workshop $workshop)
	{
		$FormReksadana = new FormReksadana($request->all());
		if ($FormReksadana->alamat_sesuai_ktp) {
			$FormReksadana->alamat_skrg    = $FormReksadana->alamat_ktp;
			$FormReksadana->provinsi_skrg  = $FormReksadana->provinsi_ktp;
			$FormReksadana->kabupaten_skrg = $FormReksadana->kabupaten_ktp;
			$FormReksadana->kecamatan_skrg = $FormReksadana->kecamatan_ktp;
		}
		if ($request->hasFile('file_ktp')) {
			
			if ($request->file('file_ktp')->isValid()) {
				
				$image    = $request->file('file_ktp');
				$filename = $FormReksadana->no_ktp . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/sekolahreksadana/');
				$request->file('file_ktp')->move($uploadPath, $filename);
				
				$FormReksadana->file_ktp = $filename;
			}
		}
		
		// dd($FormReksadana);
		$workshopslug = Workshop::where('id', '=', $FormReksadana->workshop_id)->first();
		// dd($FormReksadana);
		$FormReksadana->save();
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshopslug->slugs]);
		
		
	}
	
	public function storePendaftaran(WorkshopRequest $request, Workshop $workshop)
	{
		setlocale(LC_TIME, 'id_ID.UTF-8');
		$pesertaWorkshop                  = new PesertaWorkshop($request->all());
		$pesertaWorkshop->invoice_no      = CartHelper::getWorkshopInvoice();
		$pesertaWorkshop->virtual_account = CartHelper::getVirtualAccount('041');
		$pesertaWorkshop->urutan          = CartHelper::getUrutanPeserta($workshop);
		$pesertaWorkshop->workshop_id     = $workshop->id;
		$pesertaWorkshop->biaya           = $workshop->biaya;
		$pesertaWorkshop->am_id           = $workshop->am_id;
		$pesertaWorkshop->t_program_id    = $workshop->t_program_id;
		$pesertaWorkshop->nama              = ucwords(strtolower($request->get('nama')));
		$pesertaWorkshop->no_sk_ojk         = strtoupper($request->get('no_sk_ojk'));
		
		
		/* if ($pesertaWorkshop->save() && $PesertaReksadana->save()) {
		dd($pesertaWorkshop);
		} */
		
		
		/**
		 * Cek jika ada upload surat izin OJK
		 */
		if ($request->hasFile('upload_surat_izin')) {
			if ($request->file('upload_surat_izin')->isValid()) {
				$image    = $request->file('upload_surat_izin');
				$filename = $pesertaWorkshop->invoice_no . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/surat_izin_ojk/');
				$request->file('upload_surat_izin')->move($uploadPath, $filename);
				/** set bukti transfer */
				$pesertaWorkshop->surat_izin_ojk = $filename;
			}
		}
		
		if (!empty($workshop->jml_peserta_diskon) && empty($workshop->jml_group_referral)) {
			
			if ($pesertaWorkshop->urutan <= $workshop->jml_peserta_diskon) {
				$pesertaWorkshop->diskon      = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar = $workshop->biaya - $workshop->nominal_diskon;
			} else {
				$pesertaWorkshop->diskon      = 0;
				$pesertaWorkshop->total_bayar = $workshop->biaya;
			}
			
		} elseif (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
			if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
				
				$pesertaWorkshop->diskon       = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar  = $workshop->biaya - $workshop->nominal_diskon;
				$pesertaWorkshop->has_referral = 1;
				
			} else {
				
				$pesertaWorkshop->total_bayar = $workshop->biaya;
				$pesertaWorkshop->diskon      = 0;
				
			}
		} else {
			
			$pesertaWorkshop->diskon      = 0;
			$pesertaWorkshop->total_bayar = $workshop->biaya;
			
		}
		
//		return $pesertaWorkshop;
		
		if ($pesertaWorkshop->save()) {

			if (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
				if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
					foreach (session()->get('referral_promo') as $key => $value) {

						$pesertaWorkshopReferral                  = new PesertaWorkshop();
						$pesertaWorkshopReferral->nama            = $value['nama'];
						$pesertaWorkshopReferral->email           = $value['email'];
						$pesertaWorkshopReferral->no_hp           = $value['no_hp'];
						$pesertaWorkshopReferral->invoice_no      = $pesertaWorkshop->invoice_no;
						$pesertaWorkshopReferral->virtual_account = $pesertaWorkshop->virtual_account;
						$pesertaWorkshopReferral->urutan          = CartHelper::getUrutanPeserta($workshop);
						$pesertaWorkshopReferral->workshop_id     = $workshop->id;
						$pesertaWorkshopReferral->biaya           = $workshop->biaya;
						$pesertaWorkshopReferral->diskon          = $workshop->nominal_diskon;
						$pesertaWorkshopReferral->total_bayar     = $workshop->biaya - $workshop->nominal_diskon;
						$pesertaWorkshopReferral->referral_from   = $pesertaWorkshop->id;
						$pesertaWorkshopReferral->save();

					}
				}
			}

			// $job = (new SendEmailPendaftaranWorkshop($pesertaWorkshop))->delay(10)->onQueue('ticmi');
			// $this->dispatch($job);

			$referral = null;
			if ($pesertaWorkshop->has_referral) {
				$referral = PesertaWorkshop::where('referral_from', $pesertaWorkshop->id)->get();
			}

			Mail::send('email-template.email-workshop', ['pesertaWorkshop' => $pesertaWorkshop, 'referral' => $referral], function ($m) use ($pesertaWorkshop) {
				$m->from('noreply@ticmi.co.id', 'TICMI');
				$m->to($pesertaWorkshop->email, $pesertaWorkshop->nama)->subject('TICMI - Konfirmasi Pendaftaran ' . $pesertaWorkshop->workshop->workshop_name . ' | No Tagihan : #' . $pesertaWorkshop->invoice_no);
			});

			session()->forget('referral_promo');
			flash()->success('Pendaftaran berhasil');
			return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
		} else {
			flash()->error('Pendaftaran gagal');
			return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
		}
	}
	
	/**
	 * Message Users get after Pendaftaran
	 *
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function afterPendaftaran(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$referral = null;
		if ($pesertaWorkshop->has_referral) {
			$referral = PesertaWorkshop::where('referral_from', $pesertaWorkshop->id)->get();
		}
		return view('workshop.message', compact('pesertaWorkshop', 'workshop', 'referral'));
	}
	
	/**
	 * Showing Konfirmasi Page
	 *
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function konfirmasi(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$referral = null;
		if ($pesertaWorkshop->has_referral) {
			$referral = PesertaWorkshop::where('referral_from', $pesertaWorkshop->id)->get();
		}
		return view('workshop.konfirmasi', compact('pesertaWorkshop', 'workshop', 'referral'));
	}
	
	/**
	 * Storing Confirmation data
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function storeKonfirmasi(Request $request, Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$this->validate($request, [
			'invoice_no'     => 'required',
			'nominal'        => 'required',
			'no_rekening'    => 'required',
			'nama_rekening'  => 'required',
			'tgl_transfer'   => 'required|date_format:Y-m-d',
			'jam_transfer'   => 'required',
			'bukti_transfer' => 'mimes:jpeg,jpg,png'
		]);
		
		$invoiceNo = $request->get('invoice_no');
		$cek       = Konfirmasi::where('invoice_no', $invoiceNo)->first();
		
		
		if ($cek && $cek->count() > 0) {
			flash()->success('Konfirmasi pembayaran sudah diterima');
			return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
		} else {
			
			$konfirmasi                  = new Konfirmasi($request->all());
			$konfirmasi->user_id         = $pesertaWorkshop->id;
			$konfirmasi->jns_konfirmasi  = 3;
			$konfirmasi->keranjang_id    = $workshop->id;
			$konfirmasi->virtual_account = $pesertaWorkshop->virtual_account;
			
			
			if ($request->hasFile('bukti_transfer')) {
				
				if ($request->file('bukti_transfer')->isValid()) {
					
					$image    = $request->file('bukti_transfer');
					$filename = $pesertaWorkshop->invoice_no . '.' . $image->getClientOriginalExtension();
					
					$uploadPath = public_path('upload/bukti_konfirmasi/');
					$request->file('bukti_transfer')->move($uploadPath, $filename);
					
					$konfirmasi->bukti_transfer = $filename;
				}
			}
			
			
			if ($konfirmasi->save()) {
				flash()->success('Konfirmasi pembayaran sudah diterima');
				$pesertaWorkshop->update(['is_confirm' => 1, 'confirm_at' => date('Y-m-d H:i:s')]);
				
				
				PesertaWorkshop::where('referral_from', $pesertaWorkshop->id)->update(['is_confirm' => 1, 'confirm_at' => date('Y-m-d H:i:s')]);
				
				// $job = (new SendEmailKonfirmasiWorkshop($pesertaWorkshop))->onQueue('ticmi')->delay(10);
				// $this->dispatch($job);
				Mail::send('email-template.email-konfirmasi-workshop', ['pesertaWorkshop' => $pesertaWorkshop], function ($m) use ($pesertaWorkshop) {
					$m->from('noreply@ticmi.co.id', 'TICMI');
					$m->to($pesertaWorkshop->email, $pesertaWorkshop->nama)->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #' . $pesertaWorkshop->invoice_no);
					// $m->bcc('tirza.omar@ticmi.co.id', 'Tirza Omar Dana')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
					// $m->bcc('syaipul.amri@ticmi.co.id', 'Syaipul Amri')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
					// $m->bcc('ria.eka@ticmi.co.id', 'Ria Eka')->subject('TICMI - Konfirmasi Pembayaran diterima | No Tagihan : #'.$pesertaWorkshop->invoice_no);
				});
				
				return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
			} else {
				flash()->error('Konfirmasi gagal disimpan, silahkan isi form kembali');
				return redirect()->route('pelatihan.konfirmasi', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice])->withInput();
			}
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function addNewReferral(Request $request, Workshop $workshop)
	{
		/**
		 * Request Validation
		 */
		$this->validate($request, [
			'add_referral_name'  => 'required|max:255',
			'add_referral_email' => 'email|max:255',
			'add_referral_no_hp' => 'required|max:64'
		]);
		$add_referral_name  = $request->get('add_referral_name');
		$add_referral_email = $request->get('add_referral_email');
		$add_referral_no_hp = $request->get('add_referral_no_hp');
		
		$user_referral = collect(['email' => $add_referral_email, 'nama' => $add_referral_name, 'no_hp' => $add_referral_no_hp]);
		
		/**
		 * Cek jika user memasukkan email sendiri
		 */
		if ($add_referral_email == auth()->user()->email) {
			flash()->error('Anda tidak bisa memasukkan email yang sama dengan akun Anda');
			return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
		}
		
		/**
		 * Cek user apakah sudah terdaftar pada seminar/workshop yang akan diikuti
		 */
		$check = PesertaWorkshop::where('workshop_id', $workshop->id)->where('email', trim($add_referral_email))->first();
		if ($check && $check->count() > 0) {
			/** Send Notification that user already registered on the workshop */
			flash()->error('Maaf Anda tidak dapat mendaftar User dengan Email : ' . $add_referral_email . ' dikarenakan sudah terdaftar di seminar/workshop ' . $workshop->workshop_name . '.');
		} else {
			/**
			 * Checking Session is already exist or not
			 */
			if ($request->session()->has('referral_promo')) {
				$dataReferral = $request->session()->get('referral_promo');
				
				/**
				 * Check if user is already on session
				 */
				if (in_array($add_referral_email, $dataReferral)) {
					flash()->error('Peserta sudah pernah dimasukkan ke tambahan peserta');
				} else {
					$dataReferral[] = $user_referral;
					$request->session()->put('referral_promo', $dataReferral);
				}
			} else {
				$dataReferral[] = $user_referral;
				$request->session()->put('referral_promo', $dataReferral);
				flash()->success('Peserta berhasil ditambahkan');
			}
		}
		
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function addNewReferralOld(Request $request, Workshop $workshop)
	{
		/**
		 * Request Validation
		 */
		$this->validate($request, [
			'add_referral' => 'required|max:255',
		]);
		$add_referral = $request->get('add_referral');
		
		/**
		 * Check if user exist on Users database
		 */
		$user_referral = User::where('email', trim($add_referral))->where('is_verify', 1)->first();
		if ($user_referral && $user_referral->count() > 0) {
			/**
			 * Check if user already registered on the workshop
			 */
			$check = PesertaWorkshop::where('workshop_id', $workshop->id)->where('email', trim($add_referral))->first();
			if ($check && $check->count() > 0) {
				/** Send Notification that user already registered on the workshop */
				flash()->error('Maaf Anda tidak dapat mendaftar User dengan Email : ' . $add_referral . ' dikarenakan sudah terdaftar di workshop ' . $workshop->workshop_name . '.');
			} else {
				/**
				 * Checking Session is already exist or not
				 */
				if ($request->session()->has('referral_promo')) {
					$dataReferral = $request->session()->get('referral_promo');
					
					/**
					 * Check if user is already on session
					 */
					if (array_key_exists($user_referral->id, $dataReferral)) {
						flash()->error('Peserta sudah pernah dimasukkan ke tambahan peserta');
					} else {
						$dataReferral[$user_referral->id] = $user_referral;
						$request->session()->put('referral_promo', $dataReferral);
					}
				} else {
					$dataReferral[$user_referral->id] = $user_referral;
					$request->session()->put('referral_promo', $dataReferral);
					flash()->success('Peserta berhasil ditambahkan');
				}
			}
		} else {
			/** Send Error Message if Users dont exist or not verified yet */
			flash()->error('Maaf kami tidak menemukan User dengan Email <strong>' . $add_referral . '</strong> pada data Member Kami, pastikan Peserta yang akan Anda daftarkan sudah melakukan <strong>Registrasi</strong> dan melakukan <strong>Verifikasi Email</strong>. ');
		}
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	public function removeReferral(Request $request, Workshop $workshop, $idreferral)
	{
		if (session()->has('referral_promo')) {
			$request->session()->forget('referral_promo.' . $idreferral);
			flash()->success('Data peserta berhasil dihapus');
		} else {
			flash()->error('Anda belum mempunyai data peserta tambahan');
		}
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	public function resendEmailRegister(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		// $job = (new SendEmailPendaftaranWorkshop($pesertaWorkshop))->onQueue('ticmi')->delay(10);
		// $this->dispatch($job);
		Mail::send('email-template.email-workshop', ['pesertaWorkshop' => $pesertaWorkshop, 'referral' => $referral], function ($m) use ($pesertaWorkshop) {
			$m->from('noreply@ticmi.co.id', 'TICMI');
			$m->to($pesertaWorkshop->email, $pesertaWorkshop->nama)->subject('TICMI - Konfirmasi Pendaftaran ' . $pesertaWorkshop->workshop->workshop_name . ' | No Tagihan : #' . $pesertaWorkshop->invoice_no);
		});
	}
	
	public function workshopReport(Request $request, Workshop $workshop)
	{
		$workshop->load('peserta');
//		return $workshop;
		return view('workshop.laporan', compact('workshop'));
	}
}