<?php
/**
 * Created by PhpStorm.
 * User: akmalbashan
 * Date: 12-Jul-16
 * Time: 1:26 PM
 */

namespace App\Http\ViewComposers;


use App\Models\ticmi\DownloadLog;
use App\Models\ticmi\PaketUser;
use Illuminate\View\View;
use Route;

class FrontendComposer
{
	public function compose($view)
	{
		$batchname = '';
//		if (is_object(Route::current()))
//			$batchname = Route::current()->parameter('batchname');
		
		$routename = Route::currentRouteName();
		
		$waktu      = strtotime("-7 days");
//		$batch      = Batch::where("mulai", '!=', "y")
//						->where("approve", "y")
//						->where("pasang", "y");
//		$batch->where("tanggal", ">", strtotime("-7 days"))->get();

		if (auth()->check()) {
			$paketUserActive = PaketUser::where('user_id',auth()->user()->peserta_id)->where('is_active',1)->where('valid_until','>',date('Y-m-d H:i:s'))->orderBy('valid_until','DESC')->first();
			
			$downloadCounter = 0;
			if ($paketUserActive && $paketUserActive->count() > 0) {
				$downloadCounter = DownloadLog::where('paket_user_id', $paketUserActive->id)->get()->count();
			}
			
		    $view->with('roles',auth()->user()->roles()->first())
		        ->with('paketUserActive', $paketUserActive)
		        ->with('downloadCounter', $downloadCounter);
        }

		$view->with('routename',$routename);
	}
}