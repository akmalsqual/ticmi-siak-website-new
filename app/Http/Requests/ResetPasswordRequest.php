<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $returns   = array();
        if ($this->isMethod('post')) {
            $returns = [
                'token' => 'required',
                'password' => 'required|min:6|confirmed'
  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'token' => 'required',
                'password' => 'required|min:6|confirmed'
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'token' => 'required',
                'password' => 'required|min:6|confirmed'
            ];
        }

        return $returns;
    }
}
