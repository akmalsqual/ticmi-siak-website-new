<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DB;
class Daftar_batchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns       = array();

        if ($this->isMethod('post')) {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_hp'              => ['required','numeric'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'tgl_lahir'          => ['required', 'date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],

  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_hp'              => ['required','numeric'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'tgl_lahir'          => ['required', 'date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_hp'              => ['required','numeric'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'tgl_lahir'          => ['required', 'date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],
            ];
        }

        return $returns;
    }
}
