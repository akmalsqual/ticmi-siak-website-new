<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Konfirmasi_pembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $returns   = array();
        if ($this->isMethod('post')) {
            $returns = [
                'nominal_transfer' => 'required|regex:'.\Helper::number_formats('', 'validation', 0).'|max:20',
                'transfer_dari'    => 'required|numeric',
                'tanggal_transfer' => 'required|date_format:"'.\Helper::date_formats('', 'date_php').'"',
                'bukti_transfer'   => 'required|max:2000|mimes:jpg,jpeg,png',
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'nominal_transfer' => 'required|regex:'.\Helper::number_formats('', 'validation', 0).'|max:20',
                'transfer_dari'    => 'required|numeric',
                'tanggal_transfer' => 'required|date_format:"'.\Helper::date_formats('', 'date_php').'"',
                'bukti_transfer'   => 'required|max:2000|mimes:jpg,jpeg,png',
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'nominal_transfer' => 'required|regex:'.\Helper::number_formats('', 'validation', 0).'|max:20',
                'transfer_dari'    => 'required|numeric',
                'tanggal_transfer' => 'required|date_format:"'.\Helper::date_formats('', 'date_php').'"',
                'bukti_transfer'   => 'required|max:2000|mimes:jpg,jpeg,png',
            ];
        }

        return $returns;
    }
}
