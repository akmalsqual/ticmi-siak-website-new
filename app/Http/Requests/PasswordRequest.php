<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns   = array();
        if ($this->isMethod('post')) {
            $returns = [
                'password' => 'required|confirmed|min:6',
  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'password' => 'required|confirmed|min:6',
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'password' => 'required|confirmed|min:6',
            ];
        }

        return $returns;
    }
}
