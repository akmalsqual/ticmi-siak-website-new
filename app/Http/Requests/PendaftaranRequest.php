<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PendaftaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $returns = array();
        if($this->isMethod('post')){
            $returns = [
                'keterangan'    => 'required',
                'cv'            => 'required|max:2000|mimes:jpg,jpeg,png,pdf',
                'sertifikat'    => 'required|max:2000|mimes:jpg,jpeg,png,pdf',
            ];
        }else{
            $returns = [
                'keterangan'    => 'required',
                'cv'            => 'required|max:2000|mimes:jpg,jpeg,png,pdf',
                'sertifikat'    => 'required|max:2000|mimes:jpg,jpeg,png,pdf',
            ];
        }
        return $returns;
    }
}
