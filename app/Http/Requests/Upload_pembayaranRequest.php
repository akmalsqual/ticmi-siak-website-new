<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Upload_pembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns   = array();
        if ($this->isMethod('post')) {
            $returns = [
                'foto'         => ['required','max:2000','mimes:jpg,jpeg,png'],
  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'foto'         => ['required','max:2000','mimes:jpg,jpeg,png'],
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'foto'         => ['required','max:2000','mimes:jpg,jpeg,png'],
            ];
        }

        return $returns;
    }
}
