<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReksadanaRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'kelamin'       => 'required|max:255',
			/* 'workshop_id' => 'required|max:255',
			't_program_id' => 'required|max:255', */
			'nama'          => 'required|max:255',
			'nama_ibu'      => 'required|max:255',
			'tempat_lahir'  => 'required|max:255',
			'no_ktp'        => 'required|max:255',
			'nasional'      => 'required',
			'tipe_id'       => 'required',
			'marital'       => 'required',
			'pendidikan'    => 'required',
			'agama'         => 'required',
			'alamat_ktp'    => 'required',
			'provinsi_ktp'  => 'required',
			'kabupaten_ktp' => 'required',
			'kecamatan_ktp' => 'required',
			/* 'alamat_skrg' => 'required',
			'provinsi_skrg' => 'required',
			'kabupaten_skrg' => 'required',
			'kecamatan_skrg' => 'required', */
			'file_ktp'      => 'required:jpeg,jpg,png',
			'bank'          => 'required',
			'cbg_bank'      => 'required',
			'rekening'      => 'required',
			'nama_rekening' => 'required',
			'no_hp'         => 'required',
			'pekerjaan'     => 'required',
			'sumber_dana'   => 'required',
			'goal'          => 'required',
			'pendapatan'    => 'required',
			'nilai'         => 'required',
			'tgl_lahir'     => 'required|date_format:Y-m-d',
			'email'         => 'required|email'
		];
	}
}
