<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DB;

class VerifySertifikatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns   = array();
        $profile       = DB::table('profile_peserta')->where('peserta_id', \Auth::user()->peserta_id)->first();
        $required_foto = !empty($profile->photo) ? '' : 'required';

        if ($this->isMethod('post')) {
            $returns = [
                'nama'      => ['required','string','max:255'],
                'tgl_lahir' => ['date'],
                'foto'      => [$required_foto,'max:2000','mimes:jpg,jpeg,png'],
                'cabang'    => ['required','integer','min:1'],
                'lokasi'    => ['required','integer','min:1'],
                'kp'        => ['required','integer','min:1'],
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'nama'      => ['required','string','max:255'],
                'tgl_lahir' => ['date'],
                'foto'      => [$required_foto,'max:2000','mimes:jpg,jpeg,png'],
                'cabang'    => ['required','integer','min:1'],
                'lokasi'    => ['required','integer','min:1'],
                'kp'        => ['required','integer','min:1'],
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'nama'      => ['required','string','max:255'],
                'tgl_lahir' => ['date'],
                'foto'      => [$required_foto,'max:2000','mimes:jpg,jpeg,png'],
                'cabang'    => ['required','integer','min:1'],
                'lokasi'    => ['required','integer','min:1'],
                'kp'        => ['required','integer','min:1'],
            ];
        }

        return $returns;
    }
}
