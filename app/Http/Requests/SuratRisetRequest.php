<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratRisetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//	    $suratRiset = $this->route()->getParameter('suratriset');
	    
	    switch ($this->method())
	    {
		    case 'GET':
		    case 'POST':
		    {
			    return [
				    'noSuratPengantar' => 'required|max:50',
				    'tglSuratPengantar' => 'required|date_format:Y-m-d',
				    'universitas' => 'required',
				    'noIndukMhs' => 'required',
				    'jenisTugasAkhir' => 'required',
				    'programStudi' => 'required',
				    'judulTugasAkhir' => 'required',
				    'surat_pengantar' => 'required|max:1000|image|mimes:jpeg,jpg,png',
				    'dokumenskripsi' => 'mimes:pdf'
			    ];
		    }
		    case 'PUT':
		    {
			    return [
				    'noSuratPengantar' => 'required|max:50',
				    'tglSuratPengantar' => 'required|date_format:Y-m-d',
				    'universitas' => 'required',
				    'noIndukMhs' => 'required',
				    'jenisTugasAkhir' => 'required',
				    'programStudi' => 'required',
				    'judulTugasAkhir' => 'required',
				    'surat_pengantar' => 'max:1000|image|mimes:jpeg,jpg,png',
				    'dokumenskripsi' => 'mimes:pdf'
			    ];
		    }
	    }
    }
}
