<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompleteProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns   = array();
        if ($this->isMethod('post')) {
            $returns = [
                'perusahaan_id'      => ['required'],
                'lembaga_id'         => ['required'],
                'lokasi_id'          => ['required'],
                'sex_id'             => ['required'],
                'negara_id'          => ['required'],
                'alamat_ktp'         => ['required'],
                'pekerjaan'          => ['required'],
                'jurusan'            => ['required'],
                'universitas'        => ['required'],
                'tempat_lahir'       => ['required'],
                'tlp_rumah'          => ['required'],
                'pendidikan'         => ['required'],
                'alamat_tinggal_skr' => ['required'],
                'photo'              => ['required','image'],
  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'perusahaan_id'    => ['required'],
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'perusahaan_id'      => ['required'],
                'lembaga_id'         => ['required'],
                'lokasi_id'          => ['required'],
                'sex_id'             => ['required'],
                'negara_id'          => ['required'],
                'alamat_ktp'         => ['required'],
                'pekerjaan'          => ['required'],
                'jurusan'            => ['required'],
                'universitas'        => ['required'],
                'tempat_lahir'       => ['required'],
                'tlp_rumah'          => ['required'],
                'pendidikan'         => ['required'],
                'alamat_tinggal_skr' => ['required'],
                'photo'              => ['required|image']
            ];
        }

        return $returns;
    }
}
