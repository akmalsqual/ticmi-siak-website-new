<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DB;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $returns   = array();

        if ($this->isMethod('post')) {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_telp'            => ['required','numeric'],
                'tgl_lahir'          => ['required','date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'alamat_tinggal_skr' => ['string','nullable','max:255'],
                'pendidikan'         => ['string','nullable','max:150'],
                'jurusan'            => ['string','nullable','max:100'],
                'universitas'        => ['string','nullable','max:255'],
                'profesi'            => ['required','integer'],
                'pekerjaan'          => ['string','nullable','max:100'],
                'perusahaan'         => ['required','integer'],
                // 'lembaga'            => ['required','integer'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'negara'             => ['nullable','integer'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],
                'no_sid'             => ['string','nullable','max:255'],
                'sid_card'           => ['max:2000','mimes:jpg,jpeg,png'],
  
            ];
        } // update data (backend validation)
        elseif ($this->isMethod('patch')) {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_telp'            => ['required','numeric'],
                'tgl_lahir'          => ['required','date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'alamat_tinggal_skr' => ['string','nullable','max:255'],
                'pendidikan'         => ['string','nullable','max:150'],
                'jurusan'            => ['string','nullable','max:100'],
                'universitas'        => ['string','nullable','max:255'],
                'profesi'            => ['required','integer'],
                'pekerjaan'          => ['string','nullable','max:100'],
                'perusahaan'         => ['required','integer'],
                // 'lembaga'            => ['required','integer'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'negara'             => ['nullable','integer'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],
                'no_sid'             => ['string','nullable','max:255'],
                'sid_card'           => ['max:2000','mimes:jpg,jpeg,png'],
            ];

            // if not empty password, then set validation password (if user input new password)
        } else {
            $returns = [
                'nama'               => ['required','string','max:255'],
                'sex'                => ['required','integer'],
                'no_telp'            => ['required','numeric'],
                'tgl_lahir'          => ['required','date'],
                'tempat_lahir'       => ['required','string','max:255'],
                'alamat_ktp'         => ['required','string','max:255'],
                'alamat_tinggal_skr' => ['string','nullable','max:255'],
                'pendidikan'         => ['string','nullable','max:150'],
                'jurusan'            => ['string','nullable','max:100'],
                'universitas'        => ['string','nullable','max:255'],
                'profesi'            => ['required','integer'],
                'profesi_lainnya'    => ['required_if:profesi,1','string','max:255'],
                'pekerjaan'          => ['string','nullable','max:100'],
                'perusahaan'         => ['required','integer'],
                'perusahaan_lainnya' => ['required_if:perusahaan,1','string','max:255'],
                // 'lembaga'            => ['required','integer'],
                'lokasi'             => ['required','integer'],
                'kp'                 => ['required','integer'],
                'negara'             => ['nullable','integer'],
                'foto'               => ['max:2000','mimes:jpg,jpeg,png'],
                'ktp'                => ['max:2000','mimes:jpg,jpeg,png'],
                'no_sid'             => ['string','nullable','max:255'],
                'sid_card'           => ['max:2000','mimes:jpg,jpeg,png'],
            ];
        }

        return $returns;
    }
}
