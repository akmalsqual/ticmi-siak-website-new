<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\ticmi\Workshop;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class WorkshopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//    	$workshop = $this->route()->getParameter('workshopslug');
//	    $workshop = $this->route()->parameter('workshopslug');
	    $workshop = $this->route('workshopslug');
	    
    	
    	if ($workshop->program->program_id == 34) {
		    return [
			    'nama' => 'required|max:255',
			    'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
			    'company' => 'required|max:255',
			    'jabatan' => 'required|max:255',
			    'provinsi_id' => 'required',
			    'kabupaten_id' => 'required',
			    'no_sk_ojk' => 'required|max:64',
			    'tgl_sk_ojk' => 'required|max:64',
			    'upload_surat_izin' => 'required|mimes:jpeg,jpg,png,pdf',
			    'agree_ol' => 'required'
		    ];
	    } elseif ($workshop->program->program_id == 39) {
		    return [
			    'nama' => 'required|max:255',
			    'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
			    //            'email' => 'required|email',
			    /* 'company' => 'required|max:255',
			    'jabatan' => 'required|max:255', */
			    'no_hp' => 'required|max:64',
			    //			    'no_sk_ojk' => 'required|max:64',
			    //			    'tgl_sk_ojk' => 'required|max:64',
		    ];
	    } else {
		    return [
			    'nama' => 'required|max:255',
			    'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
			    //            'email' => 'required|email',
			    'company' => 'required|max:255',
			    'jabatan' => 'required|max:255',
			    'no_hp' => 'required|max:64'
		    ];
	    }
    }
}
