<?php

Route::get('kelas-online/showcase','KelasOnlineController@Showcase')->name('kelasonline.showcase');

Route::get('wisuda/2018','WisudaController@index')->name('wisuda');
	Route::post('wisuda/2018/store','WisudaController@store')->name('wisuda.store');
	Route::get('wisuda/2018/message/{status?}','WisudaController@message')->name('wisuda.message');
	Route::get('wisuda/absensi/{emailwisuda}','WisudaController@absensi')->name('wisuda.absensi');
	Route::get('wisuda/absensi/{emailwisuda}/store/{from?}','WisudaController@absensiStore')->name('wisuda.absensi.store');
	Route::get('wisuda/report','WisudaController@report')->name('wisuda.report');
	Route::get('wisuda/hadir','WisudaController@hadir')->name('wisuda.hadir');
	Route::get('wisuda/blast-undangan','WisudaController@blastUndanganWisuda');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// akses ke image sid card peserta
Route::get('/public/assets/upload_files/peserta/sid_card/{sid}', function($path) {
    $path = public_path('assets/upload_files/peserta/sid_card/').$path;

    $response = Response::make(
     File::get($path), 
     200
    );
    $response->header(
    'Content-type',
    'image/png'
    );
    return $response;
});
// akses ke image photo peserta
Route::get('/public/assets/upload_files/peserta/photos/{photos}', function($path) {
    $path = public_path('assets/upload_files/peserta/photos/').$path;

    $response = Response::make(
     File::get($path), 
     200
    );
    $response->header(
    'Content-type',
    'image/png'
    );
    return $response;
});
// akses ke ktp peserta
Route::get('/public/assets/upload_files/peserta/ktp/{ktp}', function($path) {
    $path = public_path('assets/upload_files/peserta/ktp/').$path;

    $response = Response::make(
     File::get($path), 
     200
    );
    $response->header(
    'Content-type',
    'image/png'
    );
    return $response;
});
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::get('/','HomeController@index')->name('home');



/*  if(date("Y-m-d H:i")<"2018-10-26 21:00" && date("Y-m-d H:i")>"2018-10-26 20:00") { 
	 Route::get('/','HomeController@maintenance')->name('home');
 }else{
	
	 Route::get('/','HomeController@index')->name('home');
 } */

Route::get('/','HomeController@index')->name('home');
Route::get('/home','HomeController@index')->name('home');
Route::get('/daftar-batch/{batchid}/{batchname}',"HomeController@pendaftaranDetail")->name('daftar.detail');
Route::post('batch/find','HomeController@search')->name('search');

/*
|--------------------------------------------------------------------------
| Forgot Password
|--------------------------------------------------------------------------
*/
Route::get('/forgot-password','HomeController@forgot_pasword')->name('forgot_password');
Route::get('/forgot-password-admisi','HomeController@forgot_pasword_admisi')->name('forgot_password_admisi');
Route::post('/forgot-password/proses', 'HomeController@forgot_pasword_proses')->name('forgotpassword.proses');
Route::get('reset-password/{useremail}/{userid}','HomeController@reset_password')->name('reset_password');
Route::post('reset-password/proses/{useremail}/{userid}','HomeController@reset_password_proses')->name('resetpassword.proses');

/*
|--------------------------------------------------------------------------
| Tentang Kami
|--------------------------------------------------------------------------
*/
Route::get('/profil-perusahaan','AboutUsController@profilPerusahaan')->name('profil-perusahaan');
Route::get('/visi-misi','AboutUsController@visiMisi')->name('visi-misi');
Route::get('/sejarah','AboutUsController@sejarah')->name('sejarah');
Route::get('/struktur','AboutUsController@struktur')->name('struktur');

/*
|--------------------------------------------------------------------------
| Produk dan Layanan ------------------------------------------------------
|--------------------------------------------------------------------------
| Sertifikasi Profesi
|--------------------------------------------------------------------------
*/
Route::get('/wppe','ProdukLayananController@wppeProduct')->name('wppe-product');
Route::get('/wmi','ProdukLayananController@wmiProduct')->name('wmi-product');
Route::get('/aspm','ProdukLayananController@aspmProduct')->name('aspm-product');
Route::get('/wpee','ProdukLayananController@wpeeProduct')->name('wpee-product');
Route::get('/syariah','ProdukLayananController@pdpms')->name('pdpms');
Route::get('/kerjasama-perguruan-tinggi','ProdukLayananController@kerjasamaPerguruanTinggi')->name('kerjasama-perguruan-tinggi');
/*
|--------------------------------------------------------------------------
| Data Pasar Modal
|--------------------------------------------------------------------------
*/
Route::get('/data-historis','ProdukLayananController@dataHistoris')->name('data-historis');
Route::get('/perpustakaan','ProdukLayananController@perpustakaan')->name('perpustakaan');
Route::get('/permintaan-data','ProdukLayananController@permintaanData')->name('permintaan-data');
Route::get('paket-data','PackageDataController@index')->name('paket-data.index');
/*
|--------------------------------------------------------------------------
| Pengembangan Profesi
|--------------------------------------------------------------------------
*/
Route::get('cmpdp/2017',function () {
	return redirect()->route('cmpdp');
});
Route::get('cmpdp','ProdukLayananController@cmpdp')->name('cmpdp');

Route::get('moduler','ProdukLayananController@moduler')->name('moduler');
Route::get('cmk','ProdukLayananController@cmk')->name('cmk');
Route::get('rivan','ProdukLayananController@rivan')->name('rivan');
Route::get('ppl','ProdukLayananController@ppl')->name('ppl');
Route::get('sekolahreksadana','ProdukLayananController@reksadana')->name('reksadana');

/*
|--------------------------------------------------------------------------
| Akademik, Hubungi Kami & FAQ
|--------------------------------------------------------------------------
*/
// Route::get('/gratis','AkademikController@gratis')->name('gratis');
Route::get('/jadwal-kegiatan','AkademikController@jadwalKegiatan')->name('jadwal-kegiatan');
Route::get('jadwal-kegiatan/{jadwalkegiatan}/view','AkademikController@jadwalKegiatanView')->name('jadwal-kegiatan-view');
Route::get('/standar-kelulusan','AkademikController@standarKelulusan')->name('standar-kelulusan');
Route::get('/hubungi-kami','AkademikController@hubungiKami')->name('hubungi-kami');
Route::get('/faq','AkademikController@faq')->name('faq');
/*
|--------------------------------------------------------------------------
| Data Pasar Modal
|--------------------------------------------------------------------------
*/
Route::get('datapasarmodal','DataEmitenController@overview')->name('dataemiten.overview');
Route::get('dataemiten/{doctype}/{type}','DataEmitenController@index')->name('dataemiten');
Route::get('dataemiten/{doctype}/{type}/viewpdf/{emitendata}','DataEmitenController@viewpdf')->name('dataemiten.viewpdf');
Route::get('dataemiten/{doctype}/{type}/getpdf/{emitendata}','DataEmitenController@getpdf')->name('dataemiten.getpdf');
Route::get('dataemiten/{doctype}/{type}/getthumbnail/{emitendata}','DataEmitenController@getthumbnail')->name('dataemiten.gethumbnail');
Route::get('dataemiten/{doctype}/{type}/pengumuman','DataEmitenController@pengumuman')->name('dataemiten.pengumuman');
Route::get('dataemiten/{doctype}/{type}/viewpdfpengumuman/{pengumuman}','DataEmitenController@viewPengumumanPdf')->name('dataemiten.pengumuman.viewpdf');
Route::get('dataemiten/{doctype}/{type}/peraturan','DataEmitenController@peraturan')->name('dataemiten.peraturan');
Route::get('dataemiten/{doctype}/{type}/viewpdfperaturan/{peraturan}','DataEmitenController@viewPeraturanPdf')->name('dataemiten.peraturan.viewpdf');
Route::get('dataemiten/{doctype}/{type}/viewsingle/{emitendata}/{title?}','DataEmitenController@viewSingle')->name('dataemiten.viewsingle');
Route::get('datapasarmodal-lainnya','DataEmitenController@dataLainnya')->name('data-lainnya');

/**
 * Keranjang Belanja
 */

Route::get('cart/view','CartController@viewcart')->name('cart.view');
Route::post('cart/add_item','CartController@addItem')->name('cart.add');
Route::get('cart/remove_item/{id}','CartController@removeItem')->name('cart.remove');
Route::get('cart/remove_all','CartController@removeAllItem')->name('cart.removeall');
Route::post('cart/store', 'CartController@store')->name('cart.store');

/*
|--------------------------------------------------------------------------
| Pelatihan
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'seminar'], function (){
	Route::get('{workshopslug}','WorkshopController@index')->name('pelatihan');
	Route::get('{workshopslug}/laporan','WorkshopController@workshopReport')->name('pelatihan.laporan');
//	Route::get('{workshopslug}/daftar','WorkshopController@pendaftaran')->name('pelatihan.daftar');
});


//Karir Umum
Route::get('karir_front','KarirController@karirFront')->name('karir_front');

Route::group(['middleware' => 'guest'], function (){
	// Register Peserta
	Route::post('/register/storeNonSID', 'RegisterController@storeNonSID')->name('register.storeNonSID');
	Route::get('register/success', 'RegisterController@registrationSuccess')->name('register-success');
	Route::get('/verify/{useremail}/{userid}','RegisterController@verifyRegistration')->name('register.verify');
	Route::get('register/ajax_get_kabupaten','RegisterController@ajax_get_kabupaten')->name('register.ajax_get_kabupaten');
	Route::resource('/register', 'RegisterController');
	// Register Peserta - End
	
	Route::get('resend-email-verification/{useremail}', 'RegisterController@resendEmailVerification')->name('register.resend-email-verification');
	Route::post('resend-email-verification', 'RegisterController@resendEmailVerificationProses')->name('register.resend-email-verification.proses');
});

Route::group(['middleware' => 'auth'], function (){
	/* DASHBOARD */
	Route::get('dashboard/ajax_datatable','DashboardController@ajax_datatable')->name('dashboard.ajax_datatable');
	Route::resource('dashboard', 'DashboardController', ['names' => ['index' => 'dashboard',]]);	
	/* PROFILE */
	Route::get('profile/ajax_get_kp','ProfileController@ajax_get_kp')->name('profile.ajax_get_kp');
	// verifikasi sertifikat
	Route::get('profile/konfirmasi_sertifikat/{peserta_id}/{batch_id}','ProfileController@verifySertifikat')->name('profile.verify_sertifikat');
	Route::post('profile/insert_data_sertifikat','ProfileController@insertDataSertifikat')->name('profile.insert_sertifikat');
	Route::resource('profile', 'ProfileController');
	/* UBAH PASSWORD */
	Route::resource('password', 'PasswordController');
	// Schedule
	Route::resource('schedule', 'ScheduleController');
	Route::get('schedule/Calendarview/{event}/{id}','ScheduleController@Calendarview');
	/* PENDAFTARAN BATCH */
	//Route::get('register-batch/{id}', 'RegisterBatchController@index');
	Route::get('register-batch/success', 'RegisterBatchController@registrationSuccess')->name('registration-success');
	Route::get('register-batch/{id}/daftar', 'RegisterBatchController@daftar');
	Route::get('register-batch/cek_voucher', 'RegisterBatchController@cek_voucher')->name('register-batch.cek_voucher');
	Route::get('register-batch/reset_voucher', 'RegisterBatchController@reset_voucher')->name('register-batch.reset_voucher');
	Route::patch('register-batch/save_pendaftaran_batch/{batch_id}', 'RegisterBatchController@save_pendaftaran_batch')->name('register-batch.save');	
	/* SEMINAR & WORKHOP */
	Route::resource('seminar', 'SeminarController');
	/* Riwayat Pembelian Data */
	Route::resource('riwayat', 'RiwayatController');
	/* TAGIHAN */
	Route::get('tagihan/ajax_datatable','TagihanController@ajax_datatable')->name('tagihan.ajax_datatable');
	Route::resource('tagihan', 'TagihanController');
	/* Konfirmasi Pembayaran */
	Route::get('konfirmasi/ajax_datatable','KonfirmasiController@ajax_datatable')->name('konfirmasi.ajax_datatable');
	Route::resource('konfirmasi', 'KonfirmasiController');
	
	// Karir
	Route::get('karir/create/{id}', 'KarirController@create');
	Route::resource('karir', 'KarirController');
	
	// Calender Schedule
	// Route::post('schedule/perdana/index/{id}', 'ujian_perdana\Ujian_perdanaController@index');
	// Ujian Perdana Controller
	Route::get('start/ujian/{id}', 'ujian_perdana\Ujian_perdanaController@start_ujian');
	Route::post('sertifikasi/perdana/index/{id}', 'ujian_perdana\Ujian_perdanaController@index');
	Route::post('sertifikasi/perdana/password_ulang/{emp_id}', 'ujian_perdana\Ujian_perdanaController@ajax_password_ulang');
	Route::post('sertifikasi/perdana/save_peserta_jawab', 'ujian_perdana\Ujian_perdanaController@ajax_save_peserta_jawab');
	Route::post('sertifikasi/perdana/save_persentase_kelulusan', 'ujian_perdana\Ujian_perdanaController@ajax_persentase_kelulusan');
     // Jadwal Ujian
     Route::get('/sertifikasi/perdana/batch/{id}', 'SertifikasiController@ajax_perdana');
     Route::get('/sertifikasi/perdana/berakhir/{nilai}', 'ujian_perdana\Ujian_perdanaController@akhiri_ujian');
     Route::get('/sertifikasi/daftar/pelatihan_batch', 'SertifikasiController@ajax_daftar_pelatihan_batch');
     Route::get('/sertifikasi/perdana/ajax_batch_sesi/{batch}', 'SertifikasiController@ajax_batch_sesi');
     Route::get('/sertifikasi/nilai_perdana/{batch_id}', 'SertifikasiController@nilai_perdana');
     Route::get('/sertifikasi/nilai_ulang/{batch_id}', 'SertifikasiController@nilai_ulang');
     Route::get('/sertifikasi/jadwal_pelatihan/{batch_id}', 'SertifikasiController@jadwal_pelatihan');
     Route::get('/sertifikasi/materi_pelatihan/{batch_id}', 'SertifikasiController@materi_pelatihan');
     Route::get('/sertifikasi/ajax_get_jadwal','SertifikasiController@ajax_get_jadwal')->name('sertifikasi.ajax_get_jadwal');
     Route::get('/sertifikasi/ajax_get_materi','SertifikasiController@ajax_get_materi')->name('sertifikasi.ajax_get_materi');
     // Ujian Ulang
     Route::get('/sertifikasi/ulang/ajax_get_cabang','ujian_ulang\Ujian_ulangController@ajax_get_cabang')->name('ujian_ulang.ajax_get_cabang');
     Route::get('/sertifikasi/ulang/ajax_get_kp','ujian_ulang\Ujian_ulangController@ajax_get_kp')->name('ujian_ulang.ajax_get_kp');
     Route::get('/sertifikasi/ulang/ajax_datatable','ujian_ulang\Ujian_ulangController@ajax_datatable')->name('ujian_ulang.ajax_datatable');
     Route::get('/sertifikasi/ulang/{id}', 'ujian_ulang\Ujian_ulangController@index');
     Route::get('/sertifikasi/info_ulang/{batch_id}', 'ujian_ulang\Ujian_ulangController@info_ulang');
     Route::get('/sertifikasi/ajax_info_ulang', 'ujian_ulang\Ujian_ulangController@ajax_info_ulang')->name('sertifikasi.ajax_info_ulang');
     Route::get('/sertifikasi/ajax_get_detail_pendaftaran', 'ujian_ulang\Ujian_ulangController@ajax_get_detail_pendaftaran')->name('sertifikasi.ajax_get_detail_pendaftaran');
     Route::post('/sertifikasi/upload_pembayaran', 'ujian_ulang\Ujian_ulangController@upload_pembayaran')->name('sertifikasi.upload_pembayaran');
     Route::post('/sertifikasi/ulang/save_pendaftaran_ulang', 'ujian_ulang\Ujian_ulangController@save_pendaftaran_ulang');
	 Route::resource('sertifikasi', 'SertifikasiController');
	 // print ujian
	 Route::get('print_ujian', 'SertifikasiController@print_ujian')->name('print_ujian');
	 // Start Ujian Ulang
	 Route::post('ulang/start_ujian', 'ujian_ulang\Ujian_ulangController@start_ujian')->name('ulang.start_ujian');
	 Route::post('sertifikasi/ulang/save_peserta_jawab', 'ujian_ulang\Ujian_ulangController@ajax_save_peserta_jawab');
	 Route::post('sertifikasi/ulang/save_persentase_kelulusan', 'ujian_ulang\Ujian_ulangController@ajax_persentase_kelulusan');

	 // Survey 
	 Route::get('survey/ajax_survey/{id}/{pelatihan_aktual_dtl_id}','SurveyController@ajax_survey')->name('survey.ajax_survey');
	 Route::resource('survey','SurveyController');
	 
	 /**
	 * Pembelian Paket Data
	 */
	
	Route::get('paket-data/{code}/checkout','PackageDataController@checkout')->name('paket-data.checkout');
	Route::get('paket-data/{paketdatacode}/remove','PackageDataController@removecart')->name('paket-data.remove-cart');
	Route::post('paket-data/{paketdatacode}/store','PackageDataController@store')->name('paket-data.store');
	Route::get('paket-data/{paketdatainvoice}/konfirmasi','PackageDataController@konfirmasi')->name('paket-data.konfirmasi');
	Route::post('paket-data/{paketuser}/konfirmasi','PackageDataController@konfirmasiStore')->name('paket-data.konfirmasi.store');

	//Lihat Paket data
	// Route::get('paket-data','PackageDataController@paketData')->name('paket-data');
	Route::get('member/paket-data','PackageDataController@memberPaketData')->name('member.paketdata');
	Route::get('paket-data/{paketuser}/view','PackageDataController@paketDataView')->name('paket-data.view');
	
	/** Pembelian data Retail */
	Route::get('cart/checkout','CartController@checkout')->name('cart.checkout');
	Route::post('cart/store','CartController@store')->name('cart.store');
	Route::get('transaksi/pembelian/{invoice}/confirmation','CartController@viewKeranjang')->name('transaksi.pembelian');
	Route::get('transaksi/konfirmasi/{keranjang}','CartController@konfirmasi')->name('transaksi.konfirmasi');
	Route::post('transaksi/konfirmasi/{keranjang}/store','CartController@storeKonfirmasi')->name('transaksi.konfirmasi.store');
	Route::get('transaksi/tagihan/{keranjang}/view','CartController@viewTagihanMember')->name('transaksi.tagihan.view');
	Route::get('member/riwayat-pembelian-data','CartController@riwataPembelianData')->name('member.pembeliandata');
	
	/* Next DataEmiten */
	Route::get('dataemiten/{doctype}/{type}/getfile/{emitendataencrypt}','DataEmitenController@getFile')->name('dataemiten.getfile');
	Route::get('dataemiten/{doctype}/{type}/getfiledownload/{emitendataencrypt}','DataEmitenController@getFileDownload')->name('dataemiten.downloadfile');
	Route::get('dataemiten/{doctype}/{type}/corporate-action','DataEmitenController@corporateAction')->name('dataemiten.corporateaction');
	Route::get('dataemiten/{doctype}/{type}/getpengumumanfile/{pengumumanencrypt}','DataEmitenController@getPengumumanFile')->name('dataemiten.pengumuman.getfile');
	Route::get('dataemiten/{doctype}/{type}/getpengumumandownload/{pengumumanencrypt}','DataEmitenController@getPengumumanDownload')->name('dataemiten.pengumuman.download');
	
	/**
	 * Surat Riset (Member)
	 */
	Route::get('member/surat-riset','SuratRisetController@suratRiset')->name('member.suratriset');
	Route::get('member/surat-riset/create','SuratRisetController@createSuratRiset')->name('member.suratriset.create');
	Route::post('member/surat-riset/store','SuratRisetController@storeSuratRiset')->name('member.suratriset.store');
	Route::get('member/surat-riset/{suratriset}/edit','SuratRisetController@editSuratRiset')->name('member.suratriset.edit');
	Route::put('member/surat-riset/{suratriset}/update','SuratRisetController@updateSuratRiset')->name('member.suratriset.update');
	Route::get('member/surat-riset/{suratriset}/destroy','SuratRisetController@destroySuratRiset')->name('member.suratriset.destroy');
	Route::get('member/surat-riset/{invoicesuratriset}/info','SuratRisetController@infoSuratRiset')->name('member.suratriset.info');
	Route::get('member/surat-riset/{invoicesuratriset}/konfirmasi','SuratRisetController@konfirmasiSuratRiset')->name('member.suratriset.konfirmasi');
	Route::post('member/surat-riset/{suratriset}/konfirmasi','SuratRisetController@storeKonfirmasiSuratRiset')->name('member.suratriset.konfirmasi.store');
	
	/**
	 * Routing untuk Seminar / Workshop (Member)
	 */
	Route::get('member/seminar','DashboardController@seminarWorkshop')->name('member.seminar');
	Route::get('member/seminar/{workshopslug}/view','DashboardController@seminarWorkshopView')->name('member.seminar.view');
	
	/**
	 * Routing untuk Seminar / Workshop (middleware auth)
	 
	 
	 */
	
	 
	 
	 // Route::post('wisuda/2018/store','WisudaController@store')->name('wisuda.store');
	Route::group(['prefix'=>'seminar'], function (){
		Route::get('{workshopslug}/checkout','WorkshopController@checkout')->name('pelatihan.checkout');
		Route::get('{workshopslug}/form','WorkshopController@form')->name('pelatihan.form');
		Route::post('sekolahreksadana/store','WorkshopController@reksadanaStore')->name('reksadana.store');
		// Route::post('{workshopslug}/reksadanaStore','WorkshopController@reksadanaStore')->name('reksadana.store');
		Route::post('{workshopslug}/store','WorkshopController@storePendaftaran')->name('pelatihan.store');
		Route::get('{workshopslug}/info/{pesertaworkshopinvoice}','WorkshopController@afterPendaftaran')->name('pelatihan.info');
		Route::get('{workshopslug}/konfirmasi/{pesertaworkshopinvoice}','WorkshopController@konfirmasi')->name('pelatihan.konfirmasi');
		Route::post('{workshopslug}/konfirmasi/{pesertaworkshop}/store','WorkshopController@storeKonfirmasi')->name('pelatihan.konfirmasi.store');
		Route::get('{workshopslug}/resendemailregister/{pesertaworkshop}','WorkshopController@resendEmailRegister')->name('pelatihan.resendemailregister');
		Route::post('{workshopslug}/absensi/{pesertaworkshop}','WorkshopController@absensi')->name('pelatihan.konfirmasi.absensi');
		
		Route::post('{workshopslug}/checkreferral','WorkshopController@addNewReferral')->name('pelatihan.checkreferral');
		Route::get('{workshopslug}/removereferral/{idreferral}','WorkshopController@removeReferral')->name('pelatihan.removereferral');
	});
	
	/**
	 * Routing untuk Kelas Online (middleware auth)
	 */
	Route::group(['prefix'=>'kelas-online'], function (){
		Route::get('{workshopslug}/checkout','KelasOnlineController@checkout')->name('kelasonline.checkout');
		Route::post('{workshopslug}/store','KelasOnlineController@store')->name('kelasonline.store');
		Route::get('{workshopslug}/informasi','KelasOnlineController@informasi')->name('kelasonline.info');
	
		Route::get('{workshopslug}/form','KelasOnlineController@form1')->name('kelasonline.form');
		// Route::get('{workshopslug}/form1','KelasOnlineController@form1')->name('kelasonline.form1');
		// Route::get('{workshopslug}/validasi','KelasOnlineController@validasi')->name('kelasonline.form.validasi');
		Route::get('{workshopslug}/{pesertaWorkshop}/validasi','KelasOnlineController@validasi')->name('kelasonline.form.validasi');
	});
	
	Route::post('form-ujian/store','KelasOnlineController@FormUjianstore1')->name('ujian.store');
	Route::post('getperusahaan','AjaxController@getPerusahaan')->name('ajax.getPerusahaan');
	
	/**
	 * Testing
	 */
	Route::get('testingapp', function () {
//		$user = \DB::connection('mysql_elearning')->table('mdl_user')->where('id', auth()->user()->moodle_user_id)->get();
//		return  auth()->user();
		
		$user = \DB::connection('mysql_elearning')->table('mdl_user')->get();
	});
	
	Route::get('testingapp2', 'AjaxController@testing');
	
}); // end middleware auth

/**
 * Routing untuk Pelatihan
 */

Route::group(['prefix'=>'seminar'], function (){
	Route::get('{workshopslug}','WorkshopController@index')->name('pelatihan');
	Route::get('{workshopslug}/laporan','WorkshopController@workshopReport')->name('pelatihan.laporan');
});

//Route::group(['prefix'=>'ppl'], function (){
//	Route::get('{workshopslug}','WorkshopController@index')->name('ppl.index');
//});

Route::group(['prefix'=>'kelas-online'], function (){
	Route::get('{workshopslug}','KelasOnlineController@index')->name('kelasonline');
//	Route::get('{workshopslug}', function () {
//		return view('errors.perbaikan-kelas');
//	})->name('kelasonline');
});

/**
 * Ajax Route
 */
Route::post('getperusahaan','AjaxController@getPerusahaan')->name('ajax.getPerusahaan');
Route::group(['prefix'=>'ajax'], function (){
	Route::post('getkabupatenbyprovinsi','AjaxController@getKabupatenByProvinsi')->name('ajax.getkabupatenbyprovinsi');
	Route::post('getkecamatanbykabupaten','AjaxController@getKecamatanByKabupaten')->name('ajax.getkecamatanbykabupaten');
});

Route::get('ngetes', 'AjaxController@testing');